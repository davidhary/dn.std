# Cloning

* [Source Code](#Source-Code)
* [Repositories](#Repositories)
* [Packages](#Packages)
* [Facilitated By](#Facilitated-By)

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [std] - ISR framework libraries for .NET Standard
* [json] - ISR framework json libraries
* [traceing] - ISR tracing libraries
```
git clone git@bitbucket.org:davidhary/dn.std.git
git clone git@bitbucket.org:davidhary/dn.std.json
git clone git@bitbucket.org:davidhary/dn.std.tracing
```

Clone the repositories into the following folders (parents of the .git folder):
```
%vslib%\core\std
%vslib%\core\json
%vslib%\core\tracing
```
where %vslib% is the root folder of the .NET libraries, e.g., %my%\lib\vs 
and %my% is the root folder of the .NET solutions

### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

### Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[std]: https://www.bitbucket.org/davidhary/dn.std
[json]: https://www.bitbucket.org/davidhary/dn.json
[tracing]: https://www.bitbucket.org/davidhary/dn.tracing
[Engineering Format]: http://WallaceKelly.BlogSpot.com/
[Exception Extension]: https://www.CodeProject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
[High Resolution Clock]: https://www.CodeProject.com/articles/792410/high-resolution-clock-in-csharp  
[Linq Statistics]: http://www.CodeProject.com/Articles/42492/Using-LINQ-to-Calculate-Basic-Statistics
[NameValueCollection]: https://www.CodeProject.com/Articles/5323395/A-Generic-Form-of-the-NameValueCollection.
[Read Write Lock Simple]: https://www.CodeProject.com/Tips/5323262/The-Simplest-Implementation-of-a-Reader-Writer-Loc
[Safe Events1]: http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx
[Safe Events2]: http://www.DreamInCode.net/forums/user/334492-aeonhack
[Safe Events3]: http://www.DreamInCode.net/code/snippet5016.htm
[Sorted Bucket Collection]: https://www.CodeProject.com/Articles/5317083/SortedBucketCollection-A-memory-efficient-SortedLi

[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[external repositories]: ExternalReposCommits.csv

