# Open Source

* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Open-Source"></a>
## Open Source
Open source used by this software is described and licensed at the
following sites:  
[std]  
[json]  
[tracing]  
[Engineering Format]  
[Exception Extension]  
[High Resolution Clock]  
[Linq Statistics]  
[NameValueCollection] 
[Read Write Lock Simple]
[Safe Events1]  
[Safe Events2]  
[Safe Events3]  
[Sorted Bucket Collection] 

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
 
[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[std]: https://www.bitbucket.org/davidhary/dn.std
[json]: https://www.bitbucket.org/davidhary/dn.json
[tracing]: https://www.bitbucket.org/davidhary/dn.tracing
[Engineering Format]: http://WallaceKelly.BlogSpot.com/
[Exception Extension]: https://www.CodeProject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
[High Resolution Clock]: https://www.CodeProject.com/articles/792410/high-resolution-clock-in-csharp  
[Linq Statistics]: http://www.CodeProject.com/Articles/42492/Using-LINQ-to-Calculate-Basic-Statistics
[NameValueCollection]: https://www.CodeProject.com/Articles/5323395/A-Generic-Form-of-the-NameValueCollection.
[Read Write Lock Simple]: https://www.CodeProject.com/Tips/5323262/The-Simplest-Implementation-of-a-Reader-Writer-Loc
[Safe Events1]: http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx
[Safe Events2]: http://www.DreamInCode.net/forums/user/334492-aeonhack
[Safe Events3]: http://www.DreamInCode.net/code/snippet5016.htm
[Sorted Bucket Collection]: https://www.CodeProject.com/Articles/5317083/SortedBucketCollection-A-memory-efficient-SortedLi

[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[external repositories]: ExternalReposCommits.csv

