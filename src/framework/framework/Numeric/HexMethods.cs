namespace cc.isr.Std.NumericExtensions;

/// <summary> Hexadecimal extension methods. </summary>
/// <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para> </remarks>
public static partial class NumericExtensionMethods
{
    #region " byte "

    /// <summary> Returns an HEX string. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">       Extended value. </param>
    /// <param name="nibbleCount"> Number of nibbles. </param>
    /// <returns> An HEX string. </returns>
    public static string ToHex( this byte value, byte nibbleCount )
    {
        return string.Format( string.Format( System.Globalization.CultureInfo.CurrentCulture, "0x{{0:X{0}}}", nibbleCount ), value );
    }

    /// <summary> Returns an HEX string caption with preceding "0x". </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">       Extended value. </param>
    /// <param name="nibbleCount"> Number of nibbles. </param>
    /// <returns> An HEX string caption with preceding "0x". </returns>
    public static string ToHexCaption( this byte value, byte nibbleCount )
    {
        return string.Format( string.Format( System.Globalization.CultureInfo.CurrentCulture, "0x{{0:X{0}}}", nibbleCount ), value );
    }

    #endregion

    #region " integer "

    /// <summary> Returns an HEX string. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">       Extended value. </param>
    /// <param name="nibbleCount"> Number of nibbles. </param>
    /// <returns> An HEX string. </returns>
    public static string ToHex( this int value, int nibbleCount )
    {
        return string.Format( string.Format( System.Globalization.CultureInfo.CurrentCulture, "0x{{0:X{0}}}", nibbleCount ), value );
    }

    /// <summary> Returns an HEX string caption with preceding "0x". </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">       Extended value. </param>
    /// <param name="nibbleCount"> Number of nibbles. </param>
    /// <returns> An HEX string caption with preceding "0x". </returns>
    public static string ToHexCaption( this int value, int nibbleCount )
    {
        return string.Format( string.Format( System.Globalization.CultureInfo.CurrentCulture, "0x{{0:X{0}}}", nibbleCount ), value );
    }

    #endregion

    #region " long "

    /// <summary> Returns an HEX string. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">       Extended value. </param>
    /// <param name="nibbleCount"> Number of nibbles. </param>
    /// <returns> An HEX string. </returns>
    public static string ToHex( this long value, long nibbleCount )
    {
        return string.Format( string.Format( System.Globalization.CultureInfo.CurrentCulture, "0x{{0:X{0}}}", nibbleCount ), value );
    }

    /// <summary> Returns an HEX string caption with preceding "0x". </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">       Extended value. </param>
    /// <param name="nibbleCount"> Number of nibbles. </param>
    /// <returns> An HEX string caption with preceding "0x". </returns>
    public static string ToHexCaption( this long value, long nibbleCount )
    {
        return string.Format( string.Format( System.Globalization.CultureInfo.CurrentCulture, "0x{{0:X{0}}}", nibbleCount ), value );
    }

    #endregion

    #region " short "

    /// <summary> Returns an HEX string. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">       Extended value. </param>
    /// <param name="nibbleCount"> Number of nibbles. </param>
    /// <returns> An HEX string. </returns>
    public static string ToHex( this short value, short nibbleCount )
    {
        return string.Format( string.Format( System.Globalization.CultureInfo.CurrentCulture, "0x{{0:X{0}}}", nibbleCount ), value );
    }

    /// <summary> Returns an HEX string caption with preceding "0x". </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">       Extended value. </param>
    /// <param name="nibbleCount"> Number of nibbles. </param>
    /// <returns> The caption. </returns>
    public static string ToHexCaption( this short value, short nibbleCount )
    {
        return string.Format( string.Format( System.Globalization.CultureInfo.CurrentCulture, "0x{{0:X{0}}}", nibbleCount ), value );
    }

    #endregion
}
