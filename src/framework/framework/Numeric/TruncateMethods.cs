namespace cc.isr.Std.NumericExtensions;

/// <summary> A truncate extensions. </summary>
/// <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para> </remarks>
public static partial class NumericExtensionMethods
{
    #region " integer "

    /// <summary> Truncates a value to the desired precision. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Extended value. </param>
    /// <returns> An Int32. </returns>
    public static short Truncate( this int value )
    {
        value &= ushort.MaxValue;
        if ( value > short.MaxValue )
        {
            value -= ushort.MaxValue + 1; // -65536
        }

        return Convert.ToInt16( value );
    }

    #endregion

    #region " long "

    /// <summary> Truncates a value to the desired precision. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Extended value. </param>
    /// <returns> An Int32. </returns>
    public static int Truncate( this long value )
    {
        value &= uint.MaxValue;
        if ( value > int.MaxValue )
        {
            value -= uint.MaxValue + 1L;
        }

        return Convert.ToInt32( value );
    }

    #endregion

    #region " short "

    /// <summary> Truncates a value to the desired precision. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Extended value. </param>
    /// <returns> A Byte. </returns>
    public static byte Truncate( this short value )
    {
        short maxValue = (2 * byte.MaxValue) - 1;
        value = ( short ) (value & maxValue);
        if ( value > short.MaxValue )
        {
            value = ( short ) (value - ( short ) (maxValue + 1)); // -65536
        }

        return Convert.ToByte( value );
    }

    #endregion
}
