namespace cc.isr.Std.NumericExtensions;

/// <summary> Bitmask extension methods. </summary>
/// <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para> </remarks>
public static partial class NumericExtensionMethods
{
    /// <summary> Is any bit on. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="status">  The status. </param>
    /// <param name="bitmask"> The bit mask. </param>
    /// <returns> A Boolean. </returns>
    public static bool IsAnyBitOn( this int status, int bitmask )
    {
        return bitmask <= 0 ? throw new ArgumentException( "Bitmask must be greater than 0", nameof( bitmask ) ) : 0 != (status & bitmask);
    }

    /// <summary> Query if all bitmask bits in 'status' are on. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="status">  The status. </param>
    /// <param name="bitmask"> The bit mask. </param>
    /// <returns> A Boolean. </returns>
    public static bool AreAllBitsOn( this int status, int bitmask )
    {
        return bitmask <= 0 ? throw new ArgumentException( "Bitmask must be greater than 0", nameof( bitmask ) ) : bitmask == (status & bitmask);
    }

    /// <summary> Return the masked status value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="status">  The status. </param>
    /// <param name="bitmask"> The bit mask. </param>
    /// <returns> An Integer? </returns>
    public static int MaskedValue( this int status, int bitmask )
    {
        return bitmask <= 0 ? throw new ArgumentException( "Bitmask must be greater than 0", nameof( bitmask ) ) : status & bitmask;
    }

    /// <summary> Is any bit on. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="status">  The status. </param>
    /// <param name="bitmask"> The bit mask. </param>
    /// <returns> A Boolean. </returns>
    public static bool IsAnyBitOn( this long status, long bitmask )
    {
        return bitmask <= 0L ? throw new ArgumentException( "Bitmask must be greater than 0", nameof( bitmask ) ) : 0L != (status & bitmask);
    }

    /// <summary> Query if all bitmask bits in 'status' are on. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="status">  The status. </param>
    /// <param name="bitmask"> The bit mask. </param>
    /// <returns> A Boolean. </returns>
    public static bool AreAllBitsOn( this long status, long bitmask )
    {
        return bitmask <= 0L
            ? throw new ArgumentException( "Bitmask must be greater than 0", nameof( bitmask ) )
            : bitmask == (status & bitmask);
    }

    /// <summary> Return the masked status value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="status">  The status. </param>
    /// <param name="bitmask"> The bit mask. </param>
    /// <returns> An Long? </returns>
    public static long MaskedValue( this long status, long bitmask )
    {
        return bitmask <= 0L ? throw new ArgumentException( "Bitmask must be greater than 0", nameof( bitmask ) ) : status & bitmask;
    }
}
