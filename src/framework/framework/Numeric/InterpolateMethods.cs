
namespace cc.isr.Std.NumericExtensions;

/// <summary>   Interpolate methods. </summary>
/// <remarks>   David, 2020-09-15. </remarks>
public static partial class NumericExtensionMethods
{
    #region " double "

    /// <summary> Interpolate over the unity range. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">    The abscissa value to interpolate to an ordinate value. </param>
    /// <param name="minValue"> The minimum range point. </param>
    /// <param name="maxValue"> The maximum range point. </param>
    /// <returns>
    /// Returns the interpolated output over the specified range relative to the value over the unity
    /// [0,1) range.
    /// </returns>
    public static double Interpolate( this double value, double minValue, double maxValue )
    {
        return (value * (maxValue - minValue)) + minValue;
    }

    /// <summary> Linearly interpolated value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     The abscissa value to interpolate to an ordinate value. </param>
    /// <param name="minValue">  The first abscissa value. </param>
    /// <param name="maxValue">  The second abscissa value. </param>
    /// <param name="minOutput"> The first ordinate value. </param>
    /// <param name="maxOutput"> The second ordinate value. </param>
    /// <returns>
    /// The abscissa value derived using linear interpolation based on a pair of coordinates.
    /// </returns>
    public static double Interpolate( this double value, double minValue, double maxValue, double minOutput, double maxOutput )
    {
        // check if we have two distinct pairs
        if ( maxValue == minValue )
        {
            // if we have identical value values, return the
            // average of the ordinate value
            return 0.5d * (minOutput + maxOutput);
        }
        else
        {
            return minOutput + ((value - minValue) * (maxOutput - minOutput) / (maxValue - minValue));
        }
    }

    /// <summary> Linearly interpolated value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">  The abscissa value to interpolate to an ordinate value. </param>
    /// <param name="point1"> The first point. </param>
    /// <param name="point2"> The second point. </param>
    /// <returns>
    /// The abscissa value derived using linear interpolation based on a pair of coordinates.
    /// </returns>
    public static double Interpolate( this double value, System.Drawing.PointF point1, System.Drawing.PointF point2 )
    {
        // check if we have two distinct pairs
        if ( point2.X == point2.X )
        {
            // if we have identical X values, return the
            // average of the ordinate value
            return 0.5d * (point1.Y + point2.Y);
        }
        else
        {
            return point1.Y + ((value - point1.X) * (point2.Y - point1.Y) / (point2.X - point1.X));
        }
    }

    /// <summary> Linearly interpolated value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     The abscissa value to interpolate to an ordinate value. </param>
    /// <param name="rectangle"> The rectangle of coordinates. </param>
    /// <returns>
    /// The abscissa value derived using linear interpolation based on a pair of coordinates.
    /// </returns>
    public static double Interpolate( this double value, System.Drawing.RectangleF rectangle )
    {
        // check if we have two distinct pairs
        if ( rectangle.Width == 0f )
        {
            // if we have identical X values, return the
            // average of the ordinate value
            return rectangle.Y + (0.5d * rectangle.Height);
        }
        else
        {
            return rectangle.Y + ((value - rectangle.X) * rectangle.Height / rectangle.Width);
        }
    }

    #endregion

    #region " single "

    /// <summary> Interpolate over the unity range. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">    The abscissa value to interpolate to an ordinate value. </param>
    /// <param name="minValue"> The minimum range point. </param>
    /// <param name="maxValue"> The maximum range point. </param>
    /// <returns>
    /// Returns the interpolated output over the specified range relative to the value over the unity
    /// [0,1) range.
    /// </returns>
    public static double Interpolate( this float value, float minValue, float maxValue )
    {
        return (value * (maxValue - minValue)) + minValue;
    }

    /// <summary> Linearly interpolated value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     The abscissa value to interpolate to an ordinate value. </param>
    /// <param name="minValue">  The first abscissa value. </param>
    /// <param name="maxValue">  The second abscissa value. </param>
    /// <param name="minOutput"> The first ordinate value. </param>
    /// <param name="maxOutput"> The second ordinate value. </param>
    /// <returns>
    /// The abscissa value derived using linear interpolation based on a pair of coordinates.
    /// </returns>
    public static double Interpolate( this float value, float minValue, float maxValue, float minOutput, float maxOutput )
    {
        return System.Convert.ToDouble( value ).Interpolate( minValue, maxValue, minOutput, maxOutput );
    }

    /// <summary> Linearly interpolated value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">  The abscissa value to interpolate to an ordinate value. </param>
    /// <param name="point1"> The first point. </param>
    /// <param name="point2"> The second point. </param>
    /// <returns>
    /// The abscissa value derived using linear interpolation based on a pair of coordinates.
    /// </returns>
    public static double Interpolate( this float value, System.Drawing.PointF point1, System.Drawing.PointF point2 )
    {
        return System.Convert.ToDouble( value ).Interpolate( point1, point2 );
    }

    /// <summary> Linearly interpolated value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     The abscissa value to interpolate to an ordinate value. </param>
    /// <param name="rectangle"> The rectangle of coordinates. </param>
    /// <returns>
    /// The abscissa value derived using linear interpolation based on a pair of coordinates.
    /// </returns>
    public static double Interpolate( this float value, System.Drawing.RectangleF rectangle )
    {
        return System.Convert.ToDouble( value ).Interpolate( rectangle );
    }

    #endregion
}
