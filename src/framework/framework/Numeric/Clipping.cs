namespace cc.isr.Std.NumericExtensions;

/// <summary> Numeric clipping methods. </summary>
/// <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para> </remarks>
public static partial class NumericExtensionMethods
{
    #region " generic "

    /// <summary>   Returns the maximum of the two values. </summary>
    /// <remarks>   David, 2021-12-06. </remarks>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="v1">   The first value. </param>
    /// <param name="v2">   The second value. </param>
    /// <returns>   The maximum value. </returns>
    public static T Max<T>( this T v1, T v2 ) where T : struct, IComparable<T>
    {
        return v1.CompareTo( v2 ) > 0 ? v1 : v2;
    }

    /// <summary>   Returns the minimum of the two values. </summary>
    /// <remarks>   David, 2021-12-06. </remarks>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="v1">   The first value. </param>
    /// <param name="v2">   The second value. </param>
    /// <returns>   The minimum value. </returns>
    public static T Min<T>( this T v1, T v2 ) where T : struct, IComparable<T>
    {
        return v1.CompareTo( v2 ) > 0 ? v2 : v1;
    }

    /// <summary>   Clips the value between the minimum and maximum. </summary>
    /// <remarks>   David, 2021-12-06. </remarks>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="value">    The value. </param>
    /// <param name="min">      The minimum. </param>
    /// <param name="max">      The maximum. </param>
    /// <returns>   A Byte. </returns>
    public static T Clip<T>( this T value, T min, T max ) where T : struct, IComparable<T>
    {
        return value.Max( min ).Min( max );
    }

    #endregion
}
