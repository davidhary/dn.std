namespace cc.isr.Std.NumericExtensions;

/// <summary> Includes extensions for numeric objects. </summary>
/// <remarks> 
/// Requires: .\Numeric\GeometricMethods. <para>
/// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2009-04-09, 1.1.3386 </para></remarks>
public static partial class NumericExtensionMethods
{
    #region " decimal "

    /// <summary>
    /// Returns true if the two values are within the specified absolute precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns> <c>true</c> if the two values are within the specified absolute precision. </returns>
    public static bool Approximates( this decimal value, decimal other, decimal precision )
    {
        return other.Equals( value ) || Math.Abs( value - other ) <= precision;
    }

    /// <summary>
    /// Returns true if the two values are within the specified relative precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="tolerance"> relative difference using precision based on the tolerance. </param>
    /// <returns> <c>true</c> if the two values are within the specified absolute precision. </returns>
    public static bool Tolerates( this decimal value, decimal other, decimal tolerance )
    {
        return value.Approximates( other, value.Epsilon( other, tolerance ) );
    }

    /// <summary>
    /// Returns true if the two values are within the specified relative precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">             Value. </param>
    /// <param name="other">             The other value. </param>
    /// <param name="significantDigits"> Defines relative precision based on the significant digits of
    /// the <see cref="Hypotenuse(double, double)"></see> </param>
    /// <returns> <c>true</c> if the two values are within the specified relative precision. </returns>
    public static bool Approximates( this decimal value, decimal other, int significantDigits )
    {
        return other.Equals( value ) || Math.Abs( value - other ) < value.Epsilon( other, significantDigits );
    }

    /// <summary>
    /// Returns the Hypotenuse of the values scaled by the specified significant digits.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">             Value. </param>
    /// <param name="other">             The other value. </param>
    /// <param name="significantDigits"> Defines relative precision based on the significant digits of
    /// the <see cref="Hypotenuse(double, double)"></see> </param>
    /// <returns> The Hypotenuse of the values scaled by the specified significant digits. </returns>
    public static decimal Epsilon( this decimal value, decimal other, int significantDigits )
    {
        return ( decimal ) ((( float ) value).Hypotenuse( ( float ) other ) / Math.Pow( 10d, significantDigits - 1 ));
    }

    /// <summary> Returns the Hypotenuse of the values scaled by the tolerance. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="tolerance"> The tolerance. </param>
    /// <returns> The Hypotenuse of the values scaled by the specified significant digits. </returns>
    public static decimal Epsilon( this decimal value, decimal other, decimal tolerance )
    {
        return ( decimal ) (( double ) tolerance * (( float ) value).Hypotenuse( ( float ) other ));
    }

    /// <summary>
    /// Returns true if the first value is less than or equal the other within the specified absolute
    /// precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the first value is less than or equal the other within the specified absolute
    /// precision.
    /// </returns>
    public static bool SmallerEquals( this decimal value, decimal other, decimal precision )
    {
        return value.Approximates( other, precision ) || value < other;
    }

    /// <summary>
    /// Returns True if the first value is greater than or equal the other within the specified
    /// absolute precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the first value is greater than or equal the other within the specified
    /// absolute precision.
    /// </returns>
    public static bool GreaterEquals( this decimal value, decimal other, decimal precision )
    {
        return value.Approximates( other, precision ) || value > other;
    }

    /// <summary>
    /// Returns True if the values are unequal within the specified absolute precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the values are unequal within the specified absolute precision.
    /// </returns>
    public static bool Differs( this decimal value, decimal other, decimal precision )
    {
        return !value.Approximates( other, precision );
    }

    /// <summary>
    /// Returns True if the first value is less than the other within the specified absolute
    /// precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the first value is less than the other within the specified absolute precision.
    /// </returns>
    public static bool SmallerDiffers( this decimal value, decimal other, decimal precision )
    {
        return !value.GreaterEquals( other, precision );
    }

    /// <summary>
    /// Returns True if the first value is greater than the other within the specified absolute
    /// precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the first value is greater than the other within the specified absolute
    /// precision.
    /// </returns>
    public static bool GreaterDiffers( this decimal value, decimal other, decimal precision )
    {
        return !value.SmallerEquals( other, precision );
    }

    #endregion

    #region " decimal? "

    /// <summary>
    /// Returns true if the absolute difference is less than or equal than
    /// <paramref name="delta">delta</paramref>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <param name="other"> The other value. </param>
    /// <param name="delta"> The delta. </param>
    /// <returns>
    /// <c>true</c> if the absolute difference is greater than delta, <c>false</c> otherwise.
    /// </returns>
    public static bool Approximates( this decimal? value, decimal? other, decimal delta )
    {
        return value is null ? other is null : other is not null && value.Value.Approximates( other.Value, delta );
    }

    /// <summary>
    /// Returns true if the two values are within the specified relative precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="tolerance"> relative difference using precision based on the tolerance. </param>
    /// <returns> <c>true</c> if the two values are within the specified absolute precision. </returns>
    public static bool Tolerates( this decimal? value, decimal? other, decimal tolerance )
    {
        return value is null
            ? other is null
            : other is not null && value.Approximates( other, value.Value.Epsilon( other.Value, tolerance ) );
    }

    /// <summary>
    /// Returns true if the absolute difference is greater than
    /// <paramref name="delta">delta</paramref>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <param name="other"> The other value. </param>
    /// <param name="delta"> The delta. </param>
    /// <returns>
    /// <c>true</c> if the absolute difference is greater than delta, <c>false</c> otherwise.
    /// </returns>
    public static bool Differs( this decimal? value, decimal? other, decimal delta )
    {
        return value is null ? other is not null : other is null || value.Value.Differs( other.Value, delta );
    }

    #endregion

    #region " double "

    /// <summary>
    /// Returns True if the two values are within the specified absolute precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns> <c>true</c> if the two values are within the specified absolute precision. </returns>
    public static bool Approximates( this double value, double other, double precision )
    {
        return other.Equals( value ) || Math.Abs( value - other ) <= precision;
    }

    /// <summary>
    /// Returns true if the two values are within the specified relative precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="tolerance"> relative difference using precision based on the tolerance. </param>
    /// <returns> <c>true</c> if the two values are within the specified absolute precision. </returns>
    public static bool Tolerates( this double value, double other, double tolerance )
    {
        return value.Approximates( other, value.Epsilon( other, tolerance ) );
    }

    /// <summary>
    /// Returns true if the two values are within the specified relative precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">             Value. </param>
    /// <param name="other">             The other value. </param>
    /// <param name="significantDigits"> Defines relative precision based on the significant digits of
    /// the <see cref="Hypotenuse(double, double)"></see> </param>
    /// <returns>
    /// Returns true if the two values are within the specified relative precision.
    /// </returns>
    public static bool Approximates( this double value, double other, int significantDigits )
    {
        return other.Equals( value ) || Math.Abs( value - other ) < value.Epsilon( other, significantDigits );
    }

    /// <summary>
    /// Returns the Hypotenuse of the values scaled by the specified significant digits.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">             Value. </param>
    /// <param name="other">             The other value. </param>
    /// <param name="significantDigits"> Defines relative precision based on the significant digits of
    /// the <see cref="Hypotenuse(double, double)"></see> </param>
    /// <returns> the Hypotenuse of the values scaled by the specified significant digits. </returns>
    public static double Epsilon( this double value, double other, int significantDigits )
    {
        // Return Hypotenuse(value, other) / If(significantDigits >= 15, 1.0E+16, Math.Pow( 10 , (significantDigits - 1)))
        return value.Hypotenuse( other ) / Math.Pow( 10d, significantDigits - 1 );
    }

    /// <summary> Returns the Hypotenuse of the values scaled by the tolerance. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="tolerance"> The tolerance. </param>
    /// <returns> The Hypotenuse of the values scaled by the specified significant digits. </returns>
    public static double Epsilon( this double value, double other, double tolerance )
    {
        return tolerance * value.Hypotenuse( other );
    }

    /// <summary>
    /// Returns true if the first value is less than or equal the other within the specified absolute
    /// precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the first value is less than or equal the other within the specified absolute
    /// precision.
    /// </returns>
    public static bool SmallerEquals( this double value, double other, double precision )
    {
        return value.Approximates( other, precision ) || value < other;
    }

    /// <summary>
    /// Returns true if the first value is greater than or equal the other within the specified
    /// absolute precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the first value is greater than or equal the other within the specified
    /// absolute precision.
    /// </returns>
    public static bool GreaterEquals( this double value, double other, double precision )
    {
        return value.Approximates( other, precision ) || value > other;
    }

    /// <summary>
    /// Returns true if the values are unequal within the specified absolute precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the values are unequal within the specified absolute precision.
    /// </returns>
    public static bool Differs( this double value, double other, double precision )
    {
        return !value.Approximates( other, precision );
    }

    /// <summary>
    /// Returns true if the first value is less than the other within the specified absolute
    /// precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the first value is less than the other within the specified absolute precision.
    /// </returns>
    public static bool SmallerDiffers( this double value, double other, double precision )
    {
        return !value.GreaterEquals( other, precision );
    }

    /// <summary>
    /// Returns true if the first value is greater than the other within the specified absolute
    /// precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the first value is greater than the other within the specified absolute
    /// precision.
    /// </returns>
    public static bool GreaterDiffers( this double value, double other, double precision )
    {
        return !value.SmallerEquals( other, precision );
    }

    #endregion

    #region " double? "

    /// <summary>
    /// Returns true if the absolute difference is less than or equal than
    /// <paramref name="delta">delta</paramref>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <param name="other"> The other value. </param>
    /// <param name="delta"> The delta. </param>
    /// <returns>
    /// <c>true</c> if the absolute difference is greater than delta, <c>false</c> otherwise.
    /// </returns>
    public static bool Approximates( this double? value, double? other, double delta )
    {
        return value is null ? other is null : other is not null && value.Value.Approximates( other.Value, delta );
    }

    /// <summary>
    /// Returns true if the two values are within the specified relative precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="tolerance"> relative difference using precision based on the tolerance. </param>
    /// <returns> <c>true</c> if the two values are within the specified absolute precision. </returns>
    public static bool Approximates( this double? value, double? other, float tolerance )
    {
        return value is null ? other is null : other is not null && value.Value.Tolerates( other.Value, tolerance );
    }

    /// <summary>
    /// Returns true if the absolute difference is greater than
    /// <paramref name="delta">delta</paramref>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <param name="other"> The other value. </param>
    /// <param name="delta"> The delta. </param>
    /// <returns>
    /// <c>true</c> if the absolute difference is greater than delta, <c>false</c> otherwise.
    /// </returns>
    public static bool Differs( this double? value, double? other, double delta )
    {
        return value is null ? other is not null : other is null || value.Value.Differs( other.Value, delta );
    }

    #endregion

    #region " single "

    /// <summary>
    /// Returns true if the values are unequal within the specified absolute precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns> <c>true</c> if the values are different; <c>false</c> otherwise. </returns>
    public static bool Differs( this float value, float other, float precision )
    {
        return !value.Approximates( other, precision );
    }

    /// <summary>
    /// Returns true if the absolute difference is greater than
    /// <paramref name="delta">delta</paramref>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <param name="other"> The other value. </param>
    /// <param name="delta"> The delta. </param>
    /// <returns>
    /// <c>true</c> if the absolute difference is greater than delta, <c>false</c> otherwise.
    /// </returns>
    public static bool Differs( this float? value, float? other, float delta )
    {
        return value is null ? other is not null : other is null || value.Value.Differs( other.Value, delta );
    }

    /// <summary>
    /// Returns true if the two values are within the specified absolute precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns> <c>true</c> if the values are close; <c>false</c> otherwise. </returns>
    public static bool Approximates( this float value, float other, float precision )
    {
        return other.Equals( value ) || Math.Abs( value - other ) <= precision;
    }

    /// <summary>
    /// Returns true if the two values are within the specified relative precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="tolerance"> relative difference using precision based on the tolerance. </param>
    /// <returns> <c>true</c> if the two values are within the specified absolute precision. </returns>
    public static bool Tolerates( this float value, float other, float tolerance )
    {
        return value.Approximates( other, value.Epsilon( other, tolerance ) );
    }

    /// <summary>
    /// Returns true if the two values are within the specified relative precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">             Value. </param>
    /// <param name="other">             The other value. </param>
    /// <param name="significantDigits"> Defines relative precision based on the significant digits of
    /// the <see cref="Hypotenuse(double, double)"></see> </param>
    /// <returns>
    /// <c>true</c> if the two values are within the specified relative precision;
    /// <c>false</c> otherwise.
    /// </returns>
    public static bool Approximates( this float value, float other, int significantDigits )
    {
        return other.Equals( value ) || Math.Abs( value - other ) < value.Epsilon( other, significantDigits );
    }

    /// <summary>
    /// Returns the Hypotenuse of the values scaled by the specified significant digits.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">             Value. </param>
    /// <param name="other">             The other value. </param>
    /// <param name="significantDigits"> Defines relative precision based on the significant digits of
    /// the <see cref="Hypotenuse(double, double)"></see> </param>
    /// <returns> the Hypotenuse of the values scaled by the specified significant digits. </returns>
    public static float Epsilon( this float value, float other, int significantDigits )
    {
        return ( float ) (value.Hypotenuse( other ) / Math.Pow( 10d, significantDigits - 1 ));
    }

    /// <summary> Returns the Hypotenuse of the values scaled by the tolerance. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="tolerance"> The tolerance. </param>
    /// <returns> The Hypotenuse of the values scaled by the specified significant digits. </returns>
    public static float Epsilon( this float value, float other, float tolerance )
    {
        return ( float ) (tolerance * value.Hypotenuse( other ));
    }

    /// <summary>
    /// Returns true if the first value is smaller or equal the other within the specified absolute
    /// precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the first value is smaller or equal the other within the specified absolute
    /// precision; <c>false</c> otherwise.
    /// </returns>
    public static bool SmallerEquals( this float value, float other, float precision )
    {
        return value.Approximates( other, precision ) || value < other;
    }

    /// <summary>
    /// Returns true if the first value is greater than or equal the other within the specified
    /// absolute precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the first value is greater than or equal the other within the specified
    /// absolute precision; <c>false</c> otherwise.
    /// </returns>
    public static bool GreaterEquals( this float value, float other, float precision )
    {
        return value.Approximates( other, precision ) || value > other;
    }

    /// <summary>
    /// Returns true if the first value is less than the other within the specified absolute
    /// precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the first value is less than the other within the specified absolute precision.
    /// </returns>
    public static bool SmallerDiffers( this float value, float other, float precision )
    {
        return !value.GreaterEquals( other, precision );
    }

    /// <summary>
    /// Returns true if the first value is greater than the other within the specified absolute
    /// precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns>
    /// <c>true</c> if the first value is greater than the other within the specified absolute
    /// precision.
    /// </returns>
    public static bool GreaterDiffers( this float value, float other, float precision )
    {
        return !value.SmallerEquals( other, precision );
    }

    #endregion

    #region " point "

    /// <summary>
    /// Returns True if the two points are within the specified absolute precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="precision"> Absolute minimal difference for relative equality. </param>
    /// <returns> <c>true</c> if the two values are within the specified absolute precision. </returns>
    public static bool Approximates( this System.Drawing.PointF value, System.Drawing.PointF other, double precision )
    {
        return value.Equals( other ) || value.Hypotenuse( other ) <= precision;
    }

    /// <summary>
    /// Returns true if the two points are within the specified relative precision.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     Value. </param>
    /// <param name="other">     The other value. </param>
    /// <param name="tolerance"> relative difference using precision based on the tolerance. </param>
    /// <returns> <c>true</c> if the two values are within the specified relative precision. </returns>
    public static bool Tolerates( this System.Drawing.PointF value, System.Drawing.PointF other, double tolerance )
    {
        return value.Equals( other ) || value.Hypotenuse( other ) <= tolerance * (value.Hypotenuse() + other.Hypotenuse());
    }

    #endregion
}
