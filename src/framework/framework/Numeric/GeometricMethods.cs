namespace cc.isr.Std.NumericExtensions;

/// <summary> Includes extensions for <see cref="Random">random number generation</see>. </summary>
/// <remarks> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2013-11-14, 2.0.5066 </para></remarks>
public static partial class NumericExtensionMethods
{
    #region " hypoteneuse "

    /// <summary> Computes the length of a right triangle's hypotenuse. </summary>
    /// <remarks>
    /// <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
    /// would overflow.</para>
    /// </remarks>
    /// <param name="value">      The length of one side. </param>
    /// <param name="orthogonal"> The length of orthogonal side. </param>
    /// <returns>
    /// The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>).
    /// </returns>
    public static double Hypotenuse( this float value, float orthogonal )
    {
        return Hypotenuse( value, ( double ) orthogonal );
    }

    /// <summary> Computes the length of a right triangle's hypotenuse. </summary>
    /// <remarks>
    /// <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
    /// would overflow.</para>
    /// </remarks>
    /// <param name="value">      The length of one side. </param>
    /// <param name="orthogonal"> The length of orthogonal side. </param>
    /// <returns>
    /// The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>).
    /// </returns>
    public static double Hypotenuse( this double value, double orthogonal )
    {
        if ( value == 0.0d && orthogonal == 0.0d )
        {
            return 0.0d;
        }
        else
        {
            double ax = Math.Abs( value );
            double ay = Math.Abs( orthogonal );
            if ( ax > ay )
            {
                double r = orthogonal / value;
                return ax * Math.Sqrt( 1.0d + (r * r) );
            }
            else
            {
                double r = value / orthogonal;
                return ay * Math.Sqrt( 1.0d + (r * r) );
            }
        }
    }

    /// <summary> Compute the distance between the array and the zero array. </summary>
    /// <remarks> David, 2020-09-05. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="polygon"> The sides of the polygon. </param>
    /// <returns> The distance between the two arrays. </returns>
    public static double Hypotenuse( this double[] polygon )
    {
        if ( polygon is null )
        {
            throw new ArgumentNullException( nameof( polygon ) );
        }

        double d = 0d;
        foreach ( double p in polygon )
        {
            d += Math.Pow( p, 2d );
        }

        d = Math.Sqrt( d );
        return d;
    }

    /// <summary> Computes the length of a right triangle's hypotenuse. </summary>
    /// <remarks>
    /// <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
    /// would overflow.</para>
    /// </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="polygon"> The sides of the polygon. </param>
    /// <param name="other">   The other polygon. </param>
    /// <returns>
    /// The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>).
    /// </returns>
    public static double Hypotenuse( this double[] polygon, double[] other )
    {
        if ( polygon is null )
        {
            throw new ArgumentNullException( nameof( polygon ) );
        }

        if ( other is null )
        {
            throw new ArgumentNullException( nameof( other ) );
        }

        double d = 0d;
        for ( int j = 0, loopTo = polygon.Length - 1; j <= loopTo; j++ )
        {
            d += Math.Pow( polygon[j] - other[j], 2d );
        }

        d = Math.Sqrt( d );
        return d;
    }

    /// <summary> Compute the distance between the array and the zero array. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="polygon"> The sides of the polygon. </param>
    /// <returns> The distance between the two arrays. </returns>
    public static double Hypotenuse( this IEnumerable<double> polygon )
    {
        if ( polygon is null )
        {
            throw new ArgumentNullException( nameof( polygon ) );
        }

        double d = 0d;
        foreach ( double p in polygon )
        {
            d += Math.Pow( p, 2d );
        }

        d = Math.Sqrt( d );
        return d;
    }

    /// <summary> Compute the distance between two polygons. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="polygon"> The polygon. </param>
    /// <param name="other">   The other polygon. </param>
    /// <returns> The distance between the two arrays. </returns>
    public static double Hypotenuse( this IEnumerable<double> polygon, IEnumerable<double> other )
    {
        if ( polygon is null )
        {
            throw new ArgumentNullException( nameof( polygon ) );
        }

        if ( other is null )
        {
            throw new ArgumentNullException( nameof( other ) );
        }

        double d = 0d;
        for ( int j = 0, loopTo = polygon.Count() - 1; j <= loopTo; j++ )
        {
            d += Math.Pow( polygon.ElementAtOrDefault( j ) - other.ElementAtOrDefault( j ), 2d );
        }

        d = Math.Sqrt( d );
        return d;
    }

    /// <summary> Compute the distance between the point and the origin. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="point"> The point. </param>
    /// <returns> The distance between a point and the origin. </returns>
    public static double Hypotenuse( this System.Drawing.PointF point )
    {
        return point.X.Hypotenuse( point.Y );
    }

    /// <summary> Computes the distance between two points. </summary>
    /// <remarks>
    /// <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
    /// would overflow.</para>
    /// </remarks>
    /// <param name="left">  The left. </param>
    /// <param name="right"> The right. </param>
    /// <returns>
    /// The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>).
    /// </returns>
    public static double Hypotenuse( this System.Drawing.PointF left, System.Drawing.PointF right )
    {
        return (right.X - left.X).Hypotenuse( right.Y - left.Y );
    }

    #endregion
}
