using System;

namespace cc.isr.Std.NumericExtensions
{
    /// <summary> Numeric clipping methods. </summary>
    /// <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static partial class NumericExtensionMethods
    {
        #region " byte "

        /// <summary> Clips the value between the minimum and maximum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="min">   The minimum. </param>
        /// <param name="max">   The maximum. </param>
        /// <returns> A Byte. </returns>
        public static byte Clip( this byte value, byte min, byte max )
        {
            return value.Max( min ).Min( max );
        }

        /// <summary> Returns the maximum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> the other value. </param>
        /// <returns> The maximum value. </returns>
        public static byte Max( this byte value, byte other )
        {
            return value >= other ? value : other;
        }

        /// <summary> Returns the minimum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> The other value. </param>
        /// <returns> The minimum value. </returns>
        public static byte Min( this byte value, byte other )
        {
            return value <= other ? value : other;
        }

        #endregion

        #region " decimal "

        /// <summary> Clips the value between the minimum and maximum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="min">   The minimum. </param>
        /// <param name="max">   The maximum. </param>
        /// <returns> A Decimal. </returns>
        public static decimal Clip( this decimal value, decimal min, decimal max )
        {
            return value.Max( min ).Min( max );
        }

        /// <summary> Returns the maximum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> the other value. </param>
        /// <returns> The maximum value. </returns>
        public static decimal Max( this decimal value, decimal other )
        {
            return value >= other ? value : other;
        }

        /// <summary> Returns the minimum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> The other value. </param>
        /// <returns> The minimum value. </returns>
        public static decimal Min( this decimal value, decimal other )
        {
            return value <= other ? value : other;
        }

        #endregion

        #region " double "

        /// <summary> Clips the value between the minimum and maximum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="min">   The minimum. </param>
        /// <param name="max">   The maximum. </param>
        /// <returns> A Double. </returns>
        public static double Clip( this double value, double min, double max )
        {
            return value.Max( min ).Min( max );
        }

        /// <summary> Returns the maximum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> The other value. </param>
        /// <returns> The maximum value. </returns>
        public static double Max( this double value, double other )
        {
            return value >= other ? value : other;
        }

        /// <summary> Returns the minimum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> The other value. </param>
        /// <returns> The minimum value. </returns>
        public static double Min( this double value, double other )
        {
            return value <= other ? value : other;
        }

        #endregion

        #region " integer "

        /// <summary> Clips the value between the minimum and maximum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="min">   The minimum. </param>
        /// <param name="max">   The maximum. </param>
        /// <returns> A Integer. </returns>
        public static int Clip( this int value, int min, int max )
        {
            return value.Max( min ).Min( max );
        }

        /// <summary> Returns the maximum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> The other value. </param>
        /// <returns> The maximum value. </returns>
        public static int Max( this int value, int other )
        {
            return value >= other ? value : other;
        }

        /// <summary> Returns the minimum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> The other value. </param>
        /// <returns> The minimum value. </returns>
        public static int Min( this int value, int other )
        {
            return value <= other ? value : other;
        }

        #endregion

        #region " long "

        /// <summary> Clips the value between the minimum and maximum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="min">   The minimum. </param>
        /// <param name="max">   The maximum. </param>
        /// <returns> A Long. </returns>
        public static long Clip( this long value, long min, long max )
        {
            return value.Max( min ).Min( max );
        }

        /// <summary> Returns the maximum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> The other value. </param>
        /// <returns> The maximum value. </returns>
        public static long Max( this long value, long other )
        {
            return value >= other ? value : other;
        }

        /// <summary> Returns the minimum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> The other value. </param>
        /// <returns> The minimum value. </returns>
        public static long Min( this long value, long other )
        {
            return value <= other ? value : other;
        }

        #endregion

        #region " short "

        /// <summary> Clips the value between the minimum and maximum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="min">   The minimum. </param>
        /// <param name="max">   The maximum. </param>
        /// <returns> A Single. </returns>
        public static short Clip( this short value, short min, short max )
        {
            return value.Max( min ).Min( max );
        }

        /// <summary> Returns the maximum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> The other value. </param>
        /// <returns> The maximum value. </returns>
        public static short Max( this short value, short other )
        {
            return value >= other ? value : other;
        }

        /// <summary> Returns the minimum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> The other value. </param>
        /// <returns> The minimum value. </returns>
        public static short Min( this short value, short other )
        {
            return value <= other ? value : other;
        }

        #endregion

        #region " single "

        /// <summary> Clips the value between the minimum and maximum. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="min">   The minimum. </param>
        /// <param name="max">   The maximum. </param>
        /// <returns> A Single. </returns>
        public static float Clip( this float value, float min, float max )
        {
            return value.Max( min ).Min( max );
        }

        /// <summary> Returns the maximum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> The other value. </param>
        /// <returns> The maximum value. </returns>
        public static float Max( this float value, float other )
        {
            return value >= other ? value : other;
        }

        /// <summary> Returns the minimum of the two values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="value"> Extended value. </param>
        /// <param name="other"> The other value. </param>
        /// <returns> The minimum value. </returns>
        public static float Min( this float value, float other )
        {
            return value <= other ? value : other;
        }

        #endregion

    }
}
