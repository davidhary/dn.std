using System.Text.RegularExpressions;

namespace cc.isr.Std;

/// <summary> Time span formatter. </summary>
/// <remarks>
/// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-07-15 </para><para>
/// http://StackOverflow.com/questions/3627922/format-time-span-in-DataGridView-column </para>
/// </remarks>
public class TimeSpanFormatter : IFormatProvider, ICustomFormatter
{
    /// <summary> The format parser. </summary>
    private readonly Regex _formatParser;

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public TimeSpanFormatter() : base() => this._formatParser = new Regex( "d{1,2}|h{1,2}|m{1,2}|s{1,2}|f{1,7}", RegexOptions.Compiled );

    #region "i format provider members"

    /// <summary>
    /// Returns an object that provides formatting services for the specified type.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="formatType"> An object that specifies the type of format object to return. </param>
    /// <returns>
    /// An instance of the object specified by <paramref name="formatType" />, if the
    /// <see cref="IFormatProvider" /> implementation can supply that type of object;
    /// otherwise, null.
    /// </returns>
    public object? GetFormat( Type formatType )
    {
        return typeof( ICustomFormatter ).Equals( formatType ) ? this : null;
    }

    #endregion

    #region "i custom formatter members"

    /// <summary>
    /// Converts the value of a specified object to an equivalent string representation using
    /// specified format and culture-specific formatting information.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="format1">        A format string containing formatting specifications. </param>
    /// <param name="arg">            An object to format. </param>
    /// <param name="formatProvider"> An object that supplies format information about the current
    /// instance. </param>
    /// <returns>
    /// The string representation of the value of <paramref name="arg" />, formatted as specified by
    /// <paramref name="format1" /> and <paramref name="formatProvider" />.
    /// </returns>
    public string Format( string format1, object arg, IFormatProvider formatProvider )
    {
        return arg is TimeSpan timeSpan
            ? this._formatParser.Replace( format1, GetMatchEvaluator( timeSpan ) )
            : arg is IFormattable formattable ? formattable.ToString( format1, formatProvider ) : arg is not null ? arg.ToString() : string.Empty;
    }

    #endregion

    /// <summary> Gets match evaluator. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="timeSpan"> The time span. </param>
    /// <returns> The match evaluator. </returns>
    private static MatchEvaluator GetMatchEvaluator( TimeSpan timeSpan )
    {
        return m => EvaluateMatch( m, timeSpan );
    }

    /// <summary> Evaluate match. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="match">    Specifies the match. </param>
    /// <param name="timeSpan"> The time span. </param>
    /// <returns> A <see cref="string" />. </returns>
    private static string EvaluateMatch( Match match, TimeSpan timeSpan )
    {
        switch ( match.Value ?? "" )
        {
            case "dd":
                {
                    return timeSpan.Days.ToString( "00" );
                }

            case "d":
                {
                    return timeSpan.Days.ToString( "0" );
                }

            case "hh":
                {
                    return timeSpan.Hours.ToString( "00" );
                }

            case "h":
                {
                    return timeSpan.Hours.ToString( "0" );
                }

            case "mm":
                {
                    return timeSpan.Minutes.ToString( "00" );
                }

            case "m":
                {
                    return timeSpan.Minutes.ToString( "0" );
                }

            case "ss":
                {
                    return timeSpan.Seconds.ToString( "00" );
                }

            case "s":
                {
                    return timeSpan.Seconds.ToString( "0" );
                }

            case "fffffff":
                {
                    return (timeSpan.Milliseconds * 10000).ToString( "0000000" );
                }

            case "ffffff":
                {
                    return (timeSpan.Milliseconds * 1000).ToString( "000000" );
                }

            case "fffff":
                {
                    return (timeSpan.Milliseconds * 100).ToString( "00000" );
                }

            case "ffff":
                {
                    return (timeSpan.Milliseconds * 10).ToString( "0000" );
                }

            case "fff":
                {
                    return timeSpan.Milliseconds.ToString( "000" );
                }

            case "ff":
                {
                    return (timeSpan.Milliseconds / 10).ToString( "00" );
                }

            case "f":
                {
                    return (timeSpan.Milliseconds / 100).ToString( "0" );
                }

            default:
                {
                    return match.Value!;
                }
        }
    }
}
