namespace cc.isr.Std;
/// <summary>
/// A format provider for a value representing a digital number consisting from a sequence of
/// bits or bytes.
/// </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-11-21 </para>
/// </remarks>
public class DigitalValueFormatProvider : IFormatProvider, ICustomFormatter
{
    /// <summary> The register value default hex prefix. </summary>
    /// <value> The default hexadecimal prefix. </value>
    public static string DefaultHexPrefix { get; set; } = "0x";

    /// <summary> Gets or sets the default byte count. </summary>
    /// <value> The default byte count. </value>
    public static int DefaultByteCount { get; set; } = 2;

    /// <summary> Gets or sets the default format string. </summary>
    /// <value> The default format string. </value>
    public static string DefaultFormatString { get; set; } = $"{DefaultHexPrefix}{{0:X{DefaultByteCount}}}";

    /// <summary> Gets or sets the default null value caption. </summary>
    /// <value> The default null value caption. </value>
    public static string DefaultNullValueCaption { get; set; } = $"{DefaultHexPrefix}{new string( '.', DefaultByteCount )}";

    /// <summary> The format string. </summary>
    /// <value> The format string. </value>
    public string FormatString { get; set; } = DefaultFormatString;

    /// <summary> The null value caption. </summary>
    /// <value> The null value caption. </value>
    public string NullValueCaption { get; set; } = DefaultNullValueCaption;

    /// <summary> The caption prefix. </summary>
    /// <value> The prefix. </value>
    public string Prefix { get; set; } = DefaultHexPrefix;

    /// <summary>
    /// Returns an object that provides formatting services for the specified type.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="formatType"> An object that specifies the type of format object to return. </param>
    /// <returns>
    /// An instance of the object specified by <paramref name="formatType" />, if the
    /// <see cref="IFormatProvider" /> implementation can supply that type of object;
    /// otherwise, <see langword="null" />.
    /// </returns>
    public object? GetFormat( Type formatType )
    {
        return formatType == typeof( ICustomFormatter ) ? this : null;
    }

    /// <summary>
    /// Converts the value of a specified object to an equivalent string representation using
    /// specified format and culture-specific formatting information.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="formatString">   A format string containing formatting specifications. </param>
    /// <param name="arg">            An object to format. </param>
    /// <param name="formatProvider"> An object that supplies format information about the current
    /// instance. </param>
    /// <returns>
    /// The string representation of the value of <paramref name="arg" />, formatted as specified by
    /// <paramref name="formatString" /> and <paramref name="formatProvider" />.
    /// </returns>
    public string Format( string formatString, object arg, IFormatProvider formatProvider )
    {
        return Format( this.FormatString, this.NullValueCaption, arg );
    }

    /// <summary>
    /// Converts the value of a specified object to an equivalent string representation using
    /// specified format and culture-specific formatting information.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="arg"> An object to format. </param>
    /// <returns>
    /// The string representation of the value of <paramref name="arg" />, formatted as specified by.
    /// </returns>
    public string Format( object arg )
    {
        return Format( this.FormatString, this.NullValueCaption, arg );
    }

    /// <summary>
    /// Converts the value of a specified object to an equivalent string representation using
    /// specified format and culture-specific formatting information.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="formatString">     A format string containing formatting specifications. </param>
    /// <param name="nullValueCaption"> The null value caption. </param>
    /// <param name="arg">              An object to format. </param>
    /// <returns>
    /// The string representation of the value of <paramref name="arg" />, formatted as specified by.
    /// </returns>
    public static string Format( string formatString, string nullValueCaption, object arg )
    {
        return arg is null ? nullValueCaption : string.Format( System.Globalization.CultureInfo.CurrentCulture, formatString, ( int ) arg );
    }

    /// <summary> Parses the provided string to An enum value. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> A Nullable{T} </returns>
    public T? ParseEnumHexValue<T>( string value ) where T : struct
    {
        return ParseEnumValue<T>( this.Prefix, value, System.Globalization.NumberStyles.HexNumber );
    }

    /// <summary> Parses the provided string to An enum value. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
    /// type. </exception>
    /// <param name="prefix">      The caption prefix. </param>
    /// <param name="value">       The value. </param>
    /// <param name="numberStyle"> Number of styles. </param>
    /// <returns> A Nullable{T} </returns>
    public static T? ParseEnumValue<T>( string prefix, string value, System.Globalization.NumberStyles numberStyle ) where T : struct
    {
        T? result = new T?();
        if ( !string.IsNullOrWhiteSpace( value ) )
        {
            if ( prefix is not null )
            {
                value = value.TrimStart( prefix.ToCharArray() );
            }

            if ( value.Length > 0 )
            {
                result = int.TryParse( value, numberStyle, System.Globalization.CultureInfo.CurrentCulture, out int enumValue )
                    ? Enum.TryParse( enumValue.ToString(), out T val )
                        ? ( T? ) val
                        : throw new InvalidCastException( $"Can't convert {value}=({enumValue}) to {typeof( T )}" )
                    : throw new InvalidCastException( $"Can't convert {value} to {typeof( int )}" );
            }
        }

        return result;
    }

    /// <summary> Hexadecimal value format provider. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="byteCount"> Number of bytes. </param>
    /// <returns> A RegisterValueFormatProvider. </returns>
    public static DigitalValueFormatProvider HexValueFormatProvider( int byteCount )
    {
        return HexValueFormatProvider( DefaultHexPrefix, byteCount );
    }

    /// <summary> Hexadecimal value format provider. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="prefix">    The caption prefix. </param>
    /// <param name="byteCount"> Number of bytes. </param>
    /// <returns> A RegisterValueFormatProvider. </returns>
    public static DigitalValueFormatProvider HexValueFormatProvider( string prefix, int byteCount )
    {
        DigitalValueFormatProvider result = new()
        {
            Prefix = prefix,
            FormatString = $"{prefix}{{0:X{byteCount}}}",
            NullValueCaption = $"{prefix}{new string( '.', byteCount )}"
        };
        return result;
    }
}
