
// [assembly: System.Reflection.AssemblyDescription( "Core Framework Library" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "cc.isr.Std.Framework.MSTest,PublicKey=" + cc.isr.Std.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "cc.isr.Std.Framework.Trials,PublicKey=" + cc.isr.Std.SolutionInfo.PublicKey )]
