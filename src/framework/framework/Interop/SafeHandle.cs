namespace cc.isr.Std.Framework.Interop;

/// <summary>   Safe handle. </summary>
/// <remarks>   David, 2020-05-01. <para>
/// https://docs.Microsoft.com/en-us/dotnet/api/system.runtime.interopservices.safehandle?view=netframework-4.8
/// when to use a safe handle: </para><para>
/// https://StackOverflow.com/questions/155780/safehandle-in-c-sharp
/// Source code: </para><para>
/// https://GitHub.com/dotnet/coreclr/blob/4e2d07b5f592627530ee5645fd94325f17ee9487/src/System.Private.CoreLib/shared/System/Runtime/InteropServices/SafeHandle.cs
/// </para>
/// </remarks>
public abstract class SafeHandle : System.Runtime.InteropServices.SafeHandle
{
    /// <summary>   Default constructor. </summary>
    /// <remarks>
    /// David, 2020-12-05. <para>
    /// Creates a Safe Handle, informing the base class that this Safe Handle instance "owns" the
    /// handle, and therefore Safe Handle should call our ReleaseHandle method when the Safe Handle is
    /// no longer in use. </para>
    /// </remarks>
    public SafeHandle() : base( IntPtr.Zero, true )
    { }

    /// <summary>   Constructor. </summary>
    /// <remarks>
    /// David, 2020-12-05. <para>
    /// Creates a Safe Handle, informing the base class that this Safe Handle instance "owns" the
    /// handle, and therefore Safe Handle should call our ReleaseHandle method when the Safe Handle is
    /// no longer in use. </para>
    /// </remarks>
    /// <param name="invalidHandle">   The handle. </param>
    public SafeHandle( IntPtr invalidHandle ) : base( invalidHandle, true ) => this.SetHandle( invalidHandle );

    /// <summary>
    /// When overridden in a derived class, gets a value indicating whether the handle value is
    /// invalid.
    /// </summary>
    /// <value>
    /// <see langword="true" /> if the handle value is invalid; otherwise, <see langword="false" />.
    /// </value>
    public override bool IsInvalid => this.handle == IntPtr.Zero;

    /// <summary>   Sets valid handle. </summary>
    /// <remarks>   David, 2020-12-05. </remarks>
    /// <param name="handle">   The handle. </param>
    public void SetValidHandle( IntPtr handle )
    {
        this.SetHandle( handle );
    }
}
