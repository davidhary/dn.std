namespace cc.isr.Std.Threading;

/// <summary>   A wait for. </summary>
/// <remarks>   Patrick SMACHIA https://blog.ndepend.com/on-replacing-thread-abort-in-net-6-net-5-and-net-core/. </remarks>
public static class WaitFor
{
    /// <summary>   Attempts to call with timeout. </summary>
    /// <remarks>
    /// David, 2021-10-27. See the Replacing Thread Abort Demo program for a use case. Using the
    /// <see cref="CancellationTokenSource"/> for the timeout entails a timing resolution between
    /// zero and the system clock period, which is at ~15.4ms.
    /// </remarks>
    /// <typeparam name="TResult">  Type of the result. </typeparam>
    /// <param name="proc">     A procedure accepting a <see cref="CancellationToken"/> and returning
    ///                         a TResult. </param>
    /// <param name="timeout">  The timeout. </param>
    /// <param name="result">   [out] The result of type TResult. </param>
    /// <returns>   True if it executed, false if it timed out (canceled). </returns>
    public static bool TryCallWithTimeout<TResult>(
          Func<CancellationToken, TResult> proc,
          TimeSpan timeout,
          out TResult result )
    {
        // Request cancellation after a duration of 'timeout'
        CancellationTokenSource cts = new( timeout );
        try
        {
            result = proc( cts.Token );
            // executed
            return true;
        }
        catch ( OperationCanceledException ) { }
        finally { cts.Dispose(); }
        result = default!;
        return false;
    }

    /// <summary>   Wait for one timeout plus 0 to 15.4ms. </summary>
    /// <remarks>
    /// David, 2021-10-29. Resolution is between 0 and the system close interval of ~15.4 ms.
    /// </remarks>
    /// <param name="duration"> The duration. </param>
    /// <returns>   The actual delay time. </returns>
    public static TimeSpan One( TimeSpan duration )
    {
        System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
        EventWaitHandle waitHandle = new( false, EventResetMode.AutoReset );
        _ = waitHandle.WaitOne( duration );
        return sw.Elapsed;
    }

}
