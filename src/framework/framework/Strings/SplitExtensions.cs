namespace cc.isr.Std.SplitExtensions;

/// <summary> Includes Split extensions for <see cref="string">String</see>. </summary>
/// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2009-04-09, 1.1.3386. </para></remarks>
public static class SplitExtensionMethods
{
    /// <summary> The space. </summary>
    private const string SPACE = " ";

    /// <summary> The space character. </summary>
    private const char SPACE_CHAR = ' ';

    /// <summary>
    /// Splits the string to words by adding spaces between lower and upper case characters.
    /// </summary>
    /// <remarks>
    /// Timing on I7-2600K 3.4GHz <para>
    /// CamelCase123 - 0.33ns</para><para>
    /// </para>
    /// </remarks>
    /// <param name="value"> The <c>String</c> value to split. </param>
    /// <returns> A <c>String</c> of words separated by spaces. </returns>
    public static string SplitWords( this string value )
    {
        if ( string.IsNullOrWhiteSpace( value ) )
        {
            return string.Empty;
        }
        else
        {
            bool isSpace = false;
            bool isLowerCase = false;
            System.Text.StringBuilder newValue = new();
            foreach ( char c in value )
            {
                if ( !isSpace && isLowerCase && char.IsUpper( c ) )
                {
                    _ = newValue.Append( SPACE_CHAR );
                }

                isSpace = c.Equals( SPACE_CHAR );
                isLowerCase = !char.IsUpper( c );
                _ = newValue.Append( c );
            }

            return newValue.ToString();
        }
    }

    /// <summary> A code standing for a regular expression element. </summary>
    private const string ELEMENT = "$1";

    /// <summary> The complete split command finding case as well as numeric and string of upper case characters. </summary>
    /// <remarks>This will match against each word in Camel and Pascal case strings, while properly handling acronyms and including numbers. <para>
    /// (^[a-z]+)                               Match against any lower-case letters at the start of the command. </para><para>
    /// ([0-9]+)                                Match against one Or more consecutive numbers (anywhere in the string, including at the start). </para><para>
    /// ([A-Z]{1}[a-z]+)                        Match against Title case words (one upper case followed by lower case letters). </para><para>
    /// ([A-Z]+(?=([A-Z][a-z])|($)|([0-9])))    Match against multiple consecutive upper-case letters, leaving the last upper case letter
    /// out the match if it Is followed by lower case letters, and including it if it's followed
    /// by the end of the string or a number. </para><para>
    /// Timing on I7-2600K 3.4GHz </para><para>
    /// CamelCase123 - 2.8ns</para><para>
    /// </para></remarks>
    private const string SPLIT_CASE_REGX = "((^[a-z]+)|([0-9_]+)|([A-Z]{1}[a-z]+)|([A-Z]+(?=([A-Z][a-z])|($)|([0-9_]))))";

    /// <summary>
    /// Splits camel or pascal case match against each word in Camel and Pascal case strings, while
    /// properly handling acronyms and including numbers.
    /// </summary>
    /// <remarks>
    /// Timing on I7-2600K 3.4GHz <para>
    /// CamelCase123 - 2.6ns</para><para>
    /// </para>
    /// </remarks>
    /// <param name="value"> The <c>String</c> value to split. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string SplitCase( this string value )
    {
        return value.SplitCase( SPACE );
    }

    /// <summary>
    /// Splits camel case match against each word in Camel and Pascal case strings, while properly
    /// handling acronyms and including numbers.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     The <c>String</c> value to split. </param>
    /// <param name="delimiter"> The delimited. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string SplitCase( this string value, string delimiter )
    {
        return System.Text.RegularExpressions.Regex.Replace( value, SPLIT_CASE_REGX, $"{ELEMENT}{delimiter}", System.Text.RegularExpressions.RegexOptions.Compiled ).Trim();
    }

    /// <summary> The complete split command finding case as well as numeric and string of upper case characters needing no trim action. </summary>
    /// <remarks> See https://www.CodeProject.com/articles/108996/splitting-pascal-camel-case-with-regex-enhancement
    /// With a “Match if prefix is absent” group
    /// (https://docs.Microsoft.com/en-us/dotnet/standard/base-types/regular-expression-language-quick-reference?redirectedfrom=MSDN#grouping_constructs) <parap>
    /// containing the “beginning of line” character ^ prevents any matches from occurring on the first character,
    /// which eliminates the need for the String.Trim() call. </parap><parap>
    /// The formal name for this grouping construct is “Zero-width negative look-behind assertion”, but just think
    /// of it as “if you see what’s in here, don’t match the next thing”. </parap><parap>
    /// This was enhanced by adding split by numbers </parap><parap>
    /// Timing on I7-2600K 3.4GHz </parap><parap>
    /// CamelCase123 - 4.8ns</parap><parap>
    /// </parap> </remarks>
    private const string SPLIT_CASE_SLOWER_REGX = "(?<!^)([A-Z][a-z]|(?<=[a-z])[^a-z]|(?<=[A-Z])[0-9_])";

    /// <summary> Splits a Pascal/Camel case string into words. </summary>
    /// <remarks>
    /// Timing on I7-2600K 3.4GHz <para>
    /// CamelCase123 - 4.8ns</para><para>
    /// </para>
    /// </remarks>
    /// <param name="value">     The <c>String</c> value to split. </param>
    /// <param name="delimiter"> The delimited. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string SplitCaseSlower( this string value, string delimiter )
    {
        return System.Text.RegularExpressions.Regex.Replace( value, SPLIT_CASE_SLOWER_REGX, $"{delimiter}{ELEMENT}", System.Text.RegularExpressions.RegexOptions.Compiled );
    }

    /// <summary> Splits a Pascal/Camel case statement into words. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The <c>String</c> value to split. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string SplitCaseSlower( this string value )
    {
        return value.SplitCaseSlower( SPACE );
    }
}
