namespace cc.isr.Std.StringBuilderExtensions;

/// <summary> Includes extensions for <see cref="System.Text.StringBuilder">string builder</see>. </summary>
/// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2009-04-09, 1.1.3386 </para></remarks>
public static class StringBuilderExtensionMethods
{
    /// <summary> Converts a builder to a string and trim end of line new line characters. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="builder"> The builder. </param>
    /// <returns> Builder as a <see cref="string" />. </returns>
    public static string ToStringTrimEndNewLine( this System.Text.StringBuilder builder )
    {
        return builder is null ? string.Empty : builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
    }

    /// <summary> Appends a line if has value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="builder"> The builder. </param>
    /// <param name="value">   The value. </param>
    public static void AppendLineIf( this System.Text.StringBuilder builder, string value )
    {
        if ( !string.IsNullOrWhiteSpace( value ) )
        {
            _ = (builder?.AppendLine( value ));
        }
    }

    /// <summary> Appends a line if has value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="builder"> The builder. </param>
    /// <param name="format">  Describes the format to use. </param>
    /// <param name="args">    A variable-length parameters list containing arguments. </param>
    public static void AppendLineIf( this System.Text.StringBuilder builder, string format, params object[] args )
    {
        if ( !string.IsNullOrWhiteSpace( format ) )
        {
            _ = (builder?.AppendLine( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) ));
        }
    }

    /// <summary> Returns an array of strings split by the new line characters. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> An array that represents the data in this object. </returns>
    public static string[] ToArray( this System.Text.StringBuilder value )
    {
        return value is null
            ? []
            : value.ToString().Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
    }

    /// <summary> Adds text to string builder starting with a new line. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="builder"> The builder. </param>
    /// <param name="value">   The value. </param>
    public static void AddWord( this System.Text.StringBuilder builder, string value )
    {
        builder.AddWord( value, "," );
    }

    /// <summary>
    /// Adds text to string builder adding a delimiter if the string builder is not empty.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="builder">   The builder. </param>
    /// <param name="value">     The value. </param>
    /// <param name="delimiter"> The delimiter. </param>
    public static void AddWord( this System.Text.StringBuilder builder, string value, string delimiter )
    {
        if ( !string.IsNullOrWhiteSpace( value ) )
        {
            builder ??= new System.Text.StringBuilder();

            if ( builder.Length > 0 )
            {
                _ = builder.Append( delimiter );
            }

            _ = builder.Append( value );
        }
    }
}
