namespace cc.isr.Std.HashExtensions;

/// <summary> Includes hash extensions for <see cref="string">String</see>. </summary>
/// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2009-04-09, 1.1.3386 </para></remarks>
public static class HashExtensionMethods
{
    #region " hash "

    /// <summary> Truncates a value to the desired precision. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Extended value. </param>
    /// <returns> An Int32. </returns>
    public static int Truncate( this long value )
    {
        value &= uint.MaxValue;
        if ( value > int.MaxValue )
        {
            value -= uint.MaxValue + 1L;
        }

        return Convert.ToInt32( value );
    }

    /// <summary>
    /// Returns the hash code for <paramref name="value">this string</paramref>
    /// using the algorithm implements in .NET Framework 2.0.
    /// </summary>
    /// <remarks>
    /// From Visual Studio Common Language http://www.orthogonal.com.au/computers/StringHash.htm.
    /// </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> A 32-bit signed integer hash code. </returns>
    public static int GetHashCodeNet2( this string value )
    {
        int hash = 5381;
        if ( !string.IsNullOrWhiteSpace( value ) )
        {
            char[] characters = value.ToCharArray();
            int i = 0;
            long hash64;
            while ( i < value.Length )
            {
                // hash = ((hash << 5) + hash) XOR Convert.ToInt32(characters(i))
                hash64 = hash << 5;
                hash64 += hash;
                hash = HashExtensions.HashExtensionMethods.Truncate( hash64 ) ^ Convert.ToInt32( characters[i] );
                i += 1;
            }
        }

        return hash;
    }

    /// <summary> Converts the value to a Base 64 Hash. </summary>
    /// <remarks> Uses S.H.A Crypto service provider. </remarks>
    /// <param name="value"> Specifies the value to convert. </param>
    /// <returns> The Base 64 Hash. </returns>
    public static string ToBase64Hash( this string value )
    {
        if ( string.IsNullOrWhiteSpace( value ) )
        {
            return string.Empty;
        }

        using System.Security.Cryptography.SHA1CryptoServiceProvider algorithm = new();
        return value.ToBase64Hash( algorithm );
    }

    /// <summary> Converts the value to a Base 64 Hash. </summary>
    /// <remarks> Uses S.H.A Crypto service provider. </remarks>
    /// <param name="value">     Specifies the value to convert. </param>
    /// <param name="algorithm"> Specifies the algorithm for computing the hash. </param>
    /// <returns> The Base 64 Hash. </returns>
    public static string ToBase64Hash( this string value, System.Security.Cryptography.HashAlgorithm algorithm )
    {
        if ( string.IsNullOrWhiteSpace( value ) )
        {
            return string.Empty;
        }
        else if ( algorithm is null )
        {
            return value;
        }

        System.Text.UnicodeEncoding encoding = new();

        // Store the source string in a byte array         
        byte[] values = encoding.GetBytes( value );

        // get a S.H.A provider.
        // Dim provider As New System.Security.Cryptography.SHA1CryptoServiceProvider

        // Create the hash         
        values = algorithm.ComputeHash( values );

        // return as a base64 encoded string         
        return Convert.ToBase64String( values );
    }

    /// <summary> Converts a value to a base 64 string. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="values"> Specifies the value to convert. </param>
    /// <returns> Value as a <see cref="string" />. </returns>
    public static string ToBase64String( this byte[] values )
    {
        return Convert.ToBase64String( values );
    }

    #endregion
}
