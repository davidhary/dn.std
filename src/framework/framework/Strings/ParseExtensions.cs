namespace cc.isr.Std.ParseExtensions;

/// <summary> Parse extension methods. </summary>
/// <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para> </remarks>
public static class ParseExtensionMethods
{
    #region " numeric part "

    /// <summary> Returns true if the string represents any number. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Specifies the number candidate. </param>
    /// <returns> <c>true</c> if the string represents any number; otherwise, <c>false</c>. </returns>
    public static bool IsNumber( this string value )
    {
        return System.Text.RegularExpressions.Regex.IsMatch( value, "^[0-9 ]+$" );
    }

    /// <summary>
    /// Try parse number. An alternative to <see cref="IsNumber(string)"/> for determining is
    /// <paramref name="value"/> is a number.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Specifies the number candidate. </param>
    /// <returns> The (Success As Boolean, Value As Double) </returns>
    public static (bool Success, double Value) TryParseNumber( this string value )
    {
        return (double.TryParse( value, System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent, System.Globalization.CultureInfo.CurrentCulture, out double a ), a);
    }

    /// <summary>
    /// Try parse number. An alternative to <see cref="IsNumber(string)"/> for determining is.
    /// </summary>
    /// <remarks>   2025-01-23. </remarks>
    /// <param name="value">    . </param>
    /// <param name="result">   [out] The result. </param>
    /// <returns>   The (Success As Boolean, Value As Double) </returns>
    public static bool TryParseNumber( this string value, out double result )
    {
        return double.TryParse( value, System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent,
            System.Globalization.CultureInfo.CurrentCulture, out result );
    }

    /// <summary>   A string extension method that extracts the number described by value. </summary>
    /// <remarks>   2025-01-23. </remarks>
    /// <param name="value">    Specifies the number candidate. </param>
    /// <returns>   The extracted number. </returns>
    public static double? ExtractNumber( this string value )
    {
        if ( double.TryParse( value, System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent,
            System.Globalization.CultureInfo.CurrentCulture, out double result ) )
            return result;
        return null;
    }

    /// <summary> Extracts the exponential number described by value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Specifies the number candidate. </param>
    /// <returns> The extracted exponential number. </returns>
    public static string ExtractExponentialNumber( this string value )
    {
        return System.Text.RegularExpressions.Regex.Match( value, @"[-+]?[\d]+[.]?[\d]*([eE][-+]?[\d]+)?" ).Value;// Return System.Text.RegularExpressions.Regex.Match(value, "[-+]?[0-9]+[.]?[0-9]*([eE][-+]?[0-9]+)?").Value
    }

    /// <summary> Return the number part of the ;'value' stripping all non-numeric contents. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Specifies the number candidate. </param>
    /// <returns> A <see cref="string" /> containing the numeric part of the value. </returns>
    public static string ExtractNumberPart( this string value )
    {
        return System.Text.RegularExpressions.Regex.Match( value, @"([+-]?\d+(\.\d+)?)|([+-]?\.\d+)" ).Value;
    }

    /// <summary> Extracts the numeric characters described by value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Specifies the number candidate. </param>
    /// <returns> The extracted numeric characters. </returns>
    public static string ExtractNumericCharacters( this string value )
    {
        return System.Text.RegularExpressions.Regex.Replace( value, @"[^\d]+", string.Empty );
    }

    #endregion

    #region " parse safe "

    /// <summary>
    /// Returns the numeric value represented by the given String value. If value contains an invalid
    /// number then a <see cref="double.NaN"/> is returned.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> String containing the double value to convert. </param>
    /// <returns> A double value. </returns>
    public static double ToDoubleSafe( this string value )
    {
        return double.TryParse( value, System.Globalization.NumberStyles.Any, null, out double result ) ? result : double.NaN;
    }

    #endregion

    #region " parse enum "

    /// <summary> Parse enum value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
    /// type. </exception>
    /// <param name="value"> The value. </param>
    /// <returns> A Nullable(Of. </returns>
    public static T? ParseEnumValue<T>( this string value ) where T : struct
    {
        return string.IsNullOrWhiteSpace( value )
            ? new T?()
            : Enum.TryParse( value, out T result ) ? ( T? ) result : throw new InvalidCastException( $"Can't convert {value} to {typeof( T )}" );
    }

    #endregion
}
