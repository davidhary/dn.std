namespace cc.isr.Std.Framework.Strings;

/// <summary>   A control characters. </summary>
/// <remarks>   David, 2021-12-08. </remarks>
public static class ControlChars
{
    /// <summary>   (Immutable) the new line. </summary>
    public static readonly string NewLine = Environment.NewLine;

    /// <summary>   Gets the carriage return line feed. </summary>
    /// <value> The carriage return line feed. </value>
    public static string CrLf => $"{CR}{LF}";

    /// <summary>   (Immutable) the carriage return. </summary>
    public const char CR = '\r';

    /// <summary>   (Immutable) the line feed. </summary>
    public const char LF = '\n';

    /// <summary>   (Immutable) the form feed. </summary>
    public const char FORM_FEED = '\f';

    /// <summary>   (Immutable) the null. </summary>
    public const char NULL = '\0';

    /// <summary>   (Immutable) the back space. </summary>
    public const char BACKSPACE = '\b';

    /// <summary>   (Immutable) the tab. </summary>
    public const char TAB = '\t';

    /// <summary>   (Immutable) the vertical tab. </summary>
    public const char VERTICAL_TAB = '\v';

    /// <summary>   (Immutable) the single quote. </summary>
    public const char SINGLE_QUOTE = '\'';

    /// <summary>   (Immutable) the double quote. </summary>
    public const char DOUBLE_QUOTE = '\"';
}
