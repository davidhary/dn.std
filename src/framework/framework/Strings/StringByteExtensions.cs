namespace cc.isr.Std.StringByteExtensions;

/// <summary>   The extension methods. </summary>
/// <remarks>   David, 2020-11-16. </remarks>
public static class StringByteExtensionMethods
{
    #region " alpha "

    /// <summary> Query if 'value' is alpha numeric. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value"> The value. </param>
    /// <returns> <c>true</c> if alpha numeric; otherwise <c>false</c> </returns>
    public static bool IsAlphanumeric( this string value )
    {
        if ( value is null ) throw new ArgumentNullException( nameof( value ) ); bool result = true;
        foreach ( char c in value.ToCharArray() )
        {
            if ( !char.IsLetterOrDigit( c ) )
            {
                result = false;
                break;
            }
        }

        return result;
    }

    #endregion

    #region " delimiter "

    /// <summary> Builds the hex string by adding delimiter between each pair of nibbles. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <param name="value">     The value. </param>
    /// <param name="delimiter"> The delimiter. </param>
    /// <returns> The given data converted to a <see cref="string" />. </returns>
    public static string ToByteFormatted( this string value, string delimiter )
    {
        return value.RemoveDelimiter( delimiter ).InsertDelimiter( 2, delimiter );
    }

    /// <summary> Inserts the delimiter. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <param name="value">       The value. </param>
    /// <param name="everyLength"> Length of the every. </param>
    /// <param name="delimiter">   The delimiter. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string InsertDelimiter( this string value, int everyLength, string delimiter )
    {
        if ( string.IsNullOrWhiteSpace( value ) )
            return value;
        if ( string.IsNullOrEmpty( delimiter ) )
            return value;
        System.Text.StringBuilder sb = new();
        for ( int i = 0, loopTo = value.Length - 1; everyLength >= 0 ? i <= loopTo : i >= loopTo; i += everyLength )
        {
            if ( sb.Length > 0 )
                _ = sb.Append( delimiter );
            int maxLength = Math.Min( value.Length - i, everyLength );
            _ = sb.Append( value.Substring( i, maxLength ) );
        }

        return sb.ToString();
    }

    /// <summary> Removes the delimiter. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <param name="value">     The value. </param>
    /// <param name="delimiter"> The delimiter. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string RemoveDelimiter( this string value, string delimiter )
    {
        if ( string.IsNullOrWhiteSpace( value ) )
        {
            return value;
        }
        else if ( string.IsNullOrEmpty( delimiter ) )
        {
            return value;
        }
        else
        {
            string[] values = value.Split( [delimiter], StringSplitOptions.RemoveEmptyEntries );
            System.Text.StringBuilder sb = new();
            foreach ( string v in values )
                _ = sb.Append( v.Trim() );
            return sb.ToString();
        }
    }

    /// <summary> Convert a HEX string to its representing binary values. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <param name="hexText"> The hex values. </param>
    /// <returns> The converted bytes. </returns>
    public static IEnumerable<byte> ToHexBytes( this string hexText )
    {
        if ( string.IsNullOrWhiteSpace( hexText ) )
        {
            return [];
        }
        else
        {
            string d = " ";
            return hexText.InsertDelimiter( 2, d ).ToHexBytes( d );
        }
    }

    /// <summary> The parse error value. </summary>
    public const byte PARSE_ERROR_VALUE = 255;

    /// <summary> Convert a delimited HEX string to its representing binary values. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <param name="hexText">   The hex values. </param>
    /// <param name="delimiter"> The delimiter. </param>
    /// <returns>
    /// The converted bytes. If error, the first byte is set to <see cref="PARSE_ERROR_VALUE"/> and the
    /// second byte is set to the ASCII value of the offending character.
    /// </returns>
    public static IEnumerable<byte> ToHexBytes( this string hexText, string delimiter )
    {
        List<byte> data = [];
        if ( !(string.IsNullOrWhiteSpace( hexText ) || string.IsNullOrEmpty( delimiter )) )
        {
            string[] s = hexText.Split( [delimiter], StringSplitOptions.RemoveEmptyEntries );
            foreach ( string value in s )
            {
                if ( byte.TryParse( value, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.CurrentCulture, out byte result ) )
                    data.Add( result );
                else
                {
                    // if error, tag the first byte at the Parse Error Values and point to the location of the error.
                    data = new List<byte>( [PARSE_ERROR_VALUE, ( byte ) System.Convert.ToChar( value )] );
                    break;
                }
            }
        }

        return data;
    }

    #endregion

    #region " hex "

    /// <summary> Determines whether the specified value is hex type. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> <c>true</c> if the specified value is allowed; otherwise, <c>false</c>. </returns>
    public static bool IsHexadecimal( this char value )
    {
        const string ALLOWED_CHARS = "0123456789ABCDEFabcdef";
        return !string.IsNullOrWhiteSpace( value.ToString() ) && ALLOWED_CHARS.Contains( value.ToString() );
    }

    /// <summary> Returns an HEX string using 2 nibble presentation. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> Value as a <see cref="string" />. </returns>
    public static string ToHex( this byte value )
    {
        return $"{value:X2}";
    }

    /// <summary> Returns an HEX string using 2 nibble presentation. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <param name="values"> The values. </param>
    /// <returns> Values as a <see cref="string" />. </returns>
    public static string ToHex( this IEnumerable<byte> values )
    {
        if ( values is null )
        {
            return string.Empty;
        }
        else
        {
            System.Text.StringBuilder sb = new();
            if ( values is object )
            {
                foreach ( byte b in values )
                    _ = sb.Append( b.ToHex() );
            }

            return sb.ToString();
        }
    }

    /// <summary> Returns an HEX string using 2 nibble presentation. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <param name="values">     The values. </param>
    /// <param name="startIndex"> The start index. </param>
    /// <param name="length">     The length. </param>
    /// <returns> The given data converted to a <see cref="string" />. </returns>
    public static string ToHex( this IEnumerable<byte> values, int startIndex, int length )
    {
        if ( values is null )
        {
            return string.Empty;
        }
        else
        {
            System.Text.StringBuilder sb = new();
            if ( values is object )
            {
                if ( values.Count() > startIndex )
                {
                    length = Math.Min( length, values.Count() - startIndex );
                    for ( int i = startIndex, loopTo = startIndex + length - 1; i <= loopTo; i++ )
                        _ = sb.Append( values.ElementAtOrDefault( i ).ToHex() );
                }
            }

            return sb.ToString();
        }
    }

    /// <summary> Returns an HEX string. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <param name="value">       The value. </param>
    /// <param name="nibbleCount"> Number of nibbles. </param>
    /// <returns> The given data converted to a <see cref="string" />. </returns>
    public static string ToHex( this byte value, byte nibbleCount )
    {
        return string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0:X" + nibbleCount.ToString() + "}", value );
    }

    /// <summary> Returns a byte. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value"> The value. </param>
    /// <returns> Value as a Byte. </returns>
    public static byte ToByte( this string value )
    {
        return string.IsNullOrWhiteSpace( value )
            ? throw new ArgumentNullException( nameof( value ) )
            : byte.Parse( value, System.Globalization.NumberStyles.HexNumber );
    }

    /// <summary> Gets a string. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="encoding"> The encoding. </param>
    /// <param name="value">    The value. </param>
    /// <returns> The string. </returns>
    public static string GetString( this System.Text.Encoding encoding, byte value )
    {
        return encoding is null ? throw new ArgumentNullException( nameof( encoding ) ) : encoding.GetString( [value] );
    }

    #endregion

    #region " bytes "

    /// <summary> Nullable sequence equals. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <param name="left">  The left. </param>
    /// <param name="right"> The right. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    public static bool NullableSequenceEquals( IEnumerable<byte> left, IEnumerable<byte> right )
    {
        return left is object && right is object && left.SequenceEqual( right );
    }

    /// <summary> Converts a value to the bytes. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value"> The value. </param>
    /// <returns> Value as a Byte() </returns>
    public static byte[] ToBytes( this string value )
    {
        if ( string.IsNullOrWhiteSpace( value ) ) throw new ArgumentNullException( nameof( value ) );
        System.Text.ASCIIEncoding encoding = new();
        // Store the source string in a byte array         
        return encoding.GetBytes( value );
    }

    /// <summary> Adds values. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="values"> The values to add. </param>
    /// <returns> An Integer. </returns>
    public static int Sum( this IEnumerable<byte> values )
    {
        if ( values is null ) throw new ArgumentNullException( nameof( values ) );
        int result = 0;
        foreach ( byte value in values )
            result += value;
        return result;
    }

    #endregion

    #region " checksum "

    /// <summary> Get the check sum. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value"> The value. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string Checksum( this string value )
    {
        if ( string.IsNullOrWhiteSpace( value ) ) throw new ArgumentNullException( nameof( value ) );
        long sum = value.ToBytes().Sum();
        return (( byte ) (sum & 0xFFL)).ToHex();
    }

    /// <summary> Query if 'value' has a valid checksum. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value"> The value. </param>
    /// <returns> <c>true</c> if valid checksum; otherwise <c>false</c> </returns>
    public static bool IsValidChecksum( this string value )
    {
        if ( string.IsNullOrWhiteSpace( value ) ) throw new ArgumentNullException( nameof( value ) );
        string expected = value.Substring( value.Length - 3, 2 );
        string actual = value.Remove( value.Length - 3, 2 ).Checksum();
        return string.Equals( expected, actual, StringComparison.Ordinal );
    }

    #endregion

    #region " integer "

    /// <summary> Returns an integer. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value"> The value. </param>
    /// <returns> Value as an Integer. </returns>
    public static int ToInteger( this string value )
    {
        return string.IsNullOrWhiteSpace( value )
            ? throw new ArgumentNullException( nameof( value ) )
            : int.Parse( value, System.Globalization.NumberStyles.HexNumber );
    }

    #endregion
}
