namespace cc.isr.Std.ArrayExtensions;

/// <summary> Includes extensions for string arrays. </summary>
/// <remarks> (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2011-08-27, 1.2.4256 </para></remarks>
public static partial class ArrayExtensionMethods
{
    /// <summary>
    /// Combines the specified string array using the
    /// <paramref name="terminator">terminator</paramref>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">      The string array. </param>
    /// <param name="terminator"> The terminator. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string Combine( this IEnumerable<string> value, string terminator )
    {
        if ( value is null )
        {
            return string.Empty;
        }

        System.Text.StringBuilder builder = new();
        foreach ( string s in value )
        {
            if ( builder.Length > 0 )
            {
                _ = builder.Append( terminator );
            }

            _ = builder.Append( s );
        }

        return builder.ToString();
    }
}
