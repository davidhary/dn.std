
namespace cc.isr.Std.Framework.Strings;

/// <summary>   A serial constants. </summary>
/// <remarks>
/// David, 2021-12-08. https://StackOverflow.com/questions/49830303/controlchars-class-in-c-sharp.
/// </remarks>
public static class SerialConstants
{
    /// <summary>   (Immutable) the null 00. </summary>
    public const char NULL_00 = '\x00';

    /// <summary>   (Immutable) the soh 01. </summary>
    public const char SOH_01 = '\x01';

    /// <summary>   (Immutable) the stx 02. </summary>
    public const char STX_02 = '\x02';

    /// <summary>   (Immutable) the etx 03. </summary>
    public const char ETX_03 = '\x03';

    /// <summary>   (Immutable) the eot 04. </summary>
    public const char EOT_04 = '\x04';

    /// <summary>   (Immutable) the enq 05. </summary>
    public const char ENQ_05 = '\x05';

    /// <summary>   (Immutable) the acknowledge 06. </summary>
    public const char ACK_06 = '\x06';

    /// <summary>   (Immutable) the line feed 0 a. </summary>
    public const char LF_0A = '\x0A';

    /// <summary>   (Immutable) the horizontal tab 09. </summary>
    public const char HT_09 = '\x09';

    /// <summary>   (Immutable) the carriage return 0D. </summary>
    public const char CR_0D = '\x0D';

    /// <summary>   (Immutable) the vertical tab #1 0B. </summary>
    public const char VT_0B = '\x0B';

    /// <summary>   (Immutable) the device control #1 Hex 17. </summary>
    public const char DC1_11 = '\x17';

    /// <summary>   (Immutable) the cancel Hex 18. </summary>
    public const char CAN_18 = '\x18';

    /// <summary>   (Immutable) the synchronous idle Hex 22. </summary>
    public const char SYN_22 = '\x22';

    /// <summary>   (Immutable) the delete 7f. </summary>
    public const char DEL_7F = '\x7F';

    /// <summary>   (Immutable) the space Hex 20. </summary>
    public const char SPACE_20 = '\x20';

    /// <summary>   (Immutable) the escape 1b. </summary>
    public const char ESC_1B = '\x1B';
}
