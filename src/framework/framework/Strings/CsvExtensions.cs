namespace cc.isr.Std.CommaSeparatedValuesExtensions;

/// <summary> Extension methods for building dictionaries from comma-separated-values. </summary>
/// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para> </remarks>
public static class CommaSeparatedValuesExtensionMethods
{
    /// <summary> Splits the values into a queue of strings. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="commaSeparatedValues"> The comma separated values. </param>
    /// <returns> A Queue(Of String) </returns>
    public static Queue<string> SplitQueue( this string commaSeparatedValues )
    {
        return string.IsNullOrWhiteSpace( commaSeparatedValues ) ? new Queue<string>() : new Queue<string>( commaSeparatedValues.Split( ',' ) );
    }

    /// <summary> Builds a Dictionary(Of Integer, String). </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="keyValuePairs"> The key value pairs comma-separated text. </param>
    /// <param name="key">           A key argument for permitting overrides. </param>
    /// <param name="value">         A value argument for permitting overrides. </param>
    /// <returns> A Dictionary(Of Integer, String) </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
    public static Dictionary<int, string> BuildDictionary( this string keyValuePairs, int key, string value )
    {
        Dictionary<int, string> dix = [];
        Queue<string> values = keyValuePairs.SplitQueue();
        while ( values.Any() )
        {
            if ( int.TryParse( values.Dequeue(), out key ) )
            {
                if ( values.Any() )
                {
                    value = values.Dequeue();
                    dix.Add( key, value );
                }
            }
        }

        return dix;
    }

    /// <summary> Builds a Dictionary(Of string, String). </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="keyValuePairs"> The key value pairs comma-separated text. </param>
    /// <param name="key">           A key argument for permitting overrides. </param>
    /// <param name="value">         A value argument for permitting overrides. </param>
    /// <returns> A Dictionary(Of Integer, String) </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
    public static Dictionary<string, string> BuildDictionary( this string keyValuePairs, string key, string value )
    {
        Dictionary<string, string> dix = [];
        Queue<string> values = keyValuePairs.SplitQueue();
        while ( values.Any() )
        {
            key = values.Dequeue();
            if ( values.Any() )
            {
                value = values.Dequeue();
                dix.Add( key, value );
            }
        }

        return dix;
    }
}
