namespace cc.isr.Std;

/// <summary> Handles a coding exceptions. </summary>
/// <remarks>
/// Use this class to handle exceptions that were caused due to a coding oversight, such as
/// creating a dictionary and failing to update is as new items were added. <para>
/// requires: <see cref="ExceptionBase"/> </para><para>
/// (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-09-02, 3.0.6089 </para></remarks>
public class CodingException : ExceptionBase
{
    #region " construction and cleanup "

    /// <summary> The default message. </summary>
    private const string DEFAULT_MESSAGE = "Coding bug.";

    /// <summary>
    /// Initializes a new instance of the <see cref="CodingException" /> class. Uses the internal
    /// default message.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    public CodingException() : base( DEFAULT_MESSAGE )
    { }

    /// <summary> Initializes a new instance of the <see cref="CodingException" /> class. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="message"> The message. </param>
    public CodingException( string message ) : base( message )
    { }

    /// <summary> Initializes a new instance of the <see cref="CodingException" /> class. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="message">        The message. </param>
    /// <param name="innerException"> Specifies the exception that was trapped for throwing this
    /// exception. </param>
    public CodingException( string message, Exception innerException ) : base( message, innerException )
    { }

    /// <summary> Initializes a new instance of the <see cref="CodingException" /> class. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="format"> The format. </param>
    /// <param name="args">   The arguments. </param>
    public CodingException( string format, params object[] args ) : base( format, args )
    { }

    /// <summary> Initializes a new instance of the <see cref="CodingException" /> class. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="innerException"> Specifies the InnerException. </param>
    /// <param name="format">         Specifies the exception formatting. </param>
    /// <param name="args">           Specifies the message arguments. </param>
    public CodingException( Exception innerException, string format, params object[] args ) : base( innerException, format, args )
    { }

    /// <summary> Initializes a new instance of the class with serialized data. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="info">    The <see cref="System.Runtime.Serialization.SerializationInfo" />
    /// that holds the serialized object data about the exception being
    /// thrown. </param>
    /// <param name="context"> The <see cref="System.Runtime.Serialization.StreamingContext" />
    /// that contains contextual information about the source or destination.
    /// </param>
#if NET8_0_OR_GREATER
#pragma warning disable CA1041 // Provide ObsoleteAttribute message
    [Obsolete( DiagnosticId = "SYSLIB0051" )] // add this attribute to the serialization ctor
#pragma warning restore CA1041 // Provide ObsoleteAttribute message
#endif
    protected CodingException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
    { }

    #endregion

    #region " get object data "

    /// <summary> Overrides the GetObjectData method to serialize custom values. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="info">    Represents the SerializationInfo of the exception. </param>
    /// <param name="context"> Represents the context information of the exception. </param>
#if NET8_0_OR_GREATER
#pragma warning disable CA1041 // Provide ObsoleteAttribute message
    [Obsolete( DiagnosticId = "SYSLIB0051" )] // add this attribute to the serialization ctor
#pragma warning restore CA1041 // Provide ObsoleteAttribute message
#endif
    public override void GetObjectData( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context )
    {
        base.GetObjectData( info, context );
    }

    #endregion
}
