using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

namespace cc.isr.Std;

/// <summary> Event arguments returning action level and details. </summary>
/// <remarks>
/// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2018-04-08 </para>
/// </remarks>
public class ActionEventArgs : EventArgs
{
    #region " construction and cleanup "

    /// <summary>
    /// Gets the default action failure limen is set to <see cref="TraceEventType.Error"/>.
    /// </summary>
    /// <value> The default action failure limen. </value>
    public static TraceEventType DefaultActionFailureLimen { get; private set; } = TraceEventType.Error;

    /// <summary>
    /// Default constructor. Set the <see cref="FailureLimen"/> to
    /// <see cref="DefaultActionFailureLimen"/>.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public ActionEventArgs() : this( TraceEventType.Error )
    { }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="failureLimen"> The failure threshold level. </param>
    public ActionEventArgs( TraceEventType failureLimen ) : base()
    {
        this.FailureLimen = failureLimen;
        this.ClearThis();
    }

    /// <summary> Clears this object to its blank/initial state. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    [MemberNotNull( nameof( Details ) )]
    private void ClearThis()
    {
        this.HasOutcomeEvent = false;
        this.Details = string.Empty; ;
        this.OutcomeEvent = TraceEventType.Information;
    }

    /// <summary> Clears this object to its blank/initial state. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public void Clear()
    {
        this.ClearThis();
    }

    #endregion

    #region " properties "

    /// <summary> Gets an empty <see cref="ActionEventArgs">event arguments</see>. </summary>
    /// <value> The empty. </value>
    public static new ActionEventArgs Empty => new();

    /// <summary>
    /// The action is tagged as failed if the <see cref="OutcomeEvent"/> is lower or equals the
    /// <see cref="FailureLimen"/>.
    /// </summary>
    /// <value> The failure limen. </value>
    public TraceEventType FailureLimen { get; private set; }

    /// <summary> Gets the has outcome event. </summary>
    /// <value> The has outcome event. </value>
    public bool HasOutcomeEvent { get; private set; }

    /// <summary> Gets the action outcome level. </summary>
    /// <value> The action outcome level. </value>
    public TraceEventType OutcomeEvent { get; private set; }

    /// <summary>
    /// Affirmative if the action failed; that is, the <see cref="OutcomeEvent"/> was at or lower
    /// than the <see cref="FailureLimen"/>.
    /// </summary>
    /// <value> <c>true</c> if action failed; <c>false</c> otherwise. </value>
    public bool Failed => this.OutcomeEvent <= this.FailureLimen;

    /// <summary>
    /// Affirmative if the <see cref="OutcomeEvent"/> was at or lower than the
    /// <see cref="TraceEventType.Error"/> value.
    /// </summary>
    /// <value> <c>true</c> if action failed; <c>false</c> otherwise. </value>
    public bool Erred => this.OutcomeEvent <= TraceEventType.Error;

    /// <summary> Gets the details. </summary>
    /// <value> The details. </value>
    public string Details { get; private set; }

    /// <summary> Gets the has details. </summary>
    /// <value> The has details. </value>
    public bool HasDetails => !string.IsNullOrWhiteSpace( this.Details );

    /// <summary> Registers an outcome event and record the event details. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="outcomeEvent"> The outcome. </param>
    /// <param name="details">      The details. </param>
    public void RegisterOutcomeEvent( TraceEventType outcomeEvent, string details )
    {
        this.HasOutcomeEvent = true;
        this.OutcomeEvent = outcomeEvent;
        this.Details = details;
    }

    /// <summary> Registers an outcome event and record the event details. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="outcome"> The outcome. </param>
    /// <param name="format">  Specifies the message formatting string. </param>
    /// <param name="args">    Specifies the arguments. </param>
    public void RegisterOutcomeEvent( TraceEventType outcome, string format, params object[] args )
    {
        this.RegisterOutcomeEvent( outcome, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    /// <summary> Registers an outcome event as information and record the event details. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="details"> The details. </param>
    public void RegisterOutcomeEvent( string details )
    {
        this.RegisterOutcomeEvent( TraceEventType.Information, details );
    }

    /// <summary> Registers an outcome event as information and record the event details. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="format"> Specifies the message formatting string. </param>
    /// <param name="args">   Specifies the arguments. </param>
    public void RegisterOutcomeEvent( string format, params object[] args )
    {
        this.RegisterOutcomeEvent( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    /// <summary> Registers a Failure. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="details"> The details. </param>
    public void RegisterFailure( string details )
    {
        this.RegisterOutcomeEvent( this.FailureLimen, details );
    }

    /// <summary> Registers a Failure. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="format"> Specifies the message formatting string. </param>
    /// <param name="args">   Specifies the arguments. </param>
    public void RegisterFailure( string format, params object[] args )
    {
        this.RegisterFailure( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    /// <summary> Registers an Error. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="details"> The details. </param>
    public void RegisterError( string details )
    {
        this.RegisterOutcomeEvent( TraceEventType.Error, details );
    }

    /// <summary> Registers an Error. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="format"> Specifies the message formatting string. </param>
    /// <param name="args">   Specifies the arguments. </param>
    public void RegisterError( string format, params object[] args )
    {
        this.RegisterError( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    #endregion

    #region " obsoleted "

    /// <summary> Query if this object is action failed. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <returns> <c>true</c> if action failed; otherwise <c>false</c> </returns>
    [Obsolete( "replaced with Failed and Erred" )]
    public bool IsActionFailed()
    {
        return this.OutcomeEvent <= this.FailureLimen;
    }

    /// <summary> Gets or sets the canceling sentinel. </summary>
    /// <value> The cancel. </value>
    [Obsolete( "replaced with Failed and Erred" )]
    public bool Cancel => this.Failed;

    /// <summary> Registers the cancellation. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="details"> The details. </param>
    [Obsolete( "replaced with register failed or register error" )]
    public void RegisterCancellation( string details )
    {
        this.RegisterFailure( details );
    }

    /// <summary> Registers the cancellation. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="format"> Specifies the message formatting string. </param>
    /// <param name="args">   Specifies the arguments. </param>
    [Obsolete( "replaced with register failed or register error" )]
    public void RegisterCancellation( string format, params object[] args )
    {
        this.RegisterFailure( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    #endregion
}
