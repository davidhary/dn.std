namespace cc.isr.Std.EscapeSequencesExtensions;

/// <summary> Includes Escape Sequences extensions for <see cref="string">String</see>. </summary>
/// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2009-04-09, 1.1.3386 </para><para>
/// David, 2015-08-28, 2.1.5718.x"> Requires VS 2015. </para></remarks>
public static class EscapeSequencesExtensionMethods
{
    /// <summary> The new line escape. </summary>
    public const string NEW_LINE_ESCAPE = @"\n";

    /// <summary> The new line value. </summary>
    public const byte NEW_LINE_VALUE = 10;

    /// <summary> The new line character. </summary>
    public const char NEW_LINE_CHAR = ( char ) NEW_LINE_VALUE;

    /// <summary> The return escape. </summary>
    public const string RETURN_ESCAPE = @"\r";

    /// <summary> The return value. </summary>
    public const byte RETURN_VALUE = 13;

    /// <summary> The return character. </summary>
    public const char RETURN_CHAR = ( char ) RETURN_VALUE;

    /// <summary> Gets or sets the escapes. </summary>
    /// <value> The escapes. </value>
    private static string[] Escapes { get; set; } = [NEW_LINE_ESCAPE, RETURN_ESCAPE];

    /// <summary> Gets or sets the characters. </summary>
    /// <value> The characters. </value>
    private static char[] Characters { get; set; } = [EscapeSequencesExtensionMethods.NEW_LINE_CHAR, EscapeSequencesExtensionMethods.RETURN_CHAR];

    /// <summary> Gets or sets the values. </summary>
    /// <value> The values. </value>
    private static byte[] Values { get; set; } = [NEW_LINE_VALUE, RETURN_VALUE];

    /// <summary> Query if 'value' is escape value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Text including escape sequences. </param>
    /// <returns> <c>true</c> if escape value; otherwise <c>false</c> </returns>
    public static bool IsEscapeValue( this byte value )
    {
        return Values.Contains( value );
    }

    /// <summary> Converts a value to an escape sequence. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Text including escape sequences. </param>
    /// <returns> Value as a <see cref="string" />. </returns>
    public static string ToEscapeSequence( this byte value )
    {
        return Escapes[Array.IndexOf( Values, value )];
    }

    /// <summary> Query if 'value' is escape value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Text including escape sequences. </param>
    /// <returns> <c>true</c> if escape value; otherwise <c>false</c> </returns>
    public static bool IsEscapeValue( this string value )
    {
        return Escapes.Contains( value );
    }

    /// <summary> Query if 'value' is escape value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Text including escape sequences. </param>
    /// <returns> <c>true</c> if escape value; otherwise <c>false</c> </returns>
    public static bool IsEscapeValue( this char value )
    {
        return Characters.Contains( value );
    }

    /// <summary> Enumerates common escape values in the escape sequence. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="escapeSequence"> Text including escape sequence elements. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process common escape values in this
    /// collection.
    /// </returns>
    public static IEnumerable<byte> CommonEscapeValues( this string escapeSequence )
    {
        List<byte> l = [];
        if ( !string.IsNullOrEmpty( escapeSequence ) )
        {
            for ( int i = 0, loopTo = escapeSequence.Length - 1; i <= loopTo; i += 2 )
            {
                string s = escapeSequence.Substring( i, 2 );
                if ( Escapes.Contains( s, StringComparer.OrdinalIgnoreCase ) )
                {
                    l.Add( Values[Array.IndexOf( Escapes, s )] );
                }
            }
        }

        return l;
    }

    /// <summary>
    /// Replaces common escape strings such as <code>'\n'</code> or <code>'\r'</code>with control
    /// characters such as <code>10</code> and <code>13</code>, respectively.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Text including escape sequences. </param>
    /// <returns> String with escape string replaces with control characters. </returns>
    public static string ReplaceCommonEscapeSequences( this string value )
    {
        if ( !string.IsNullOrEmpty( value ) )
        {
            for ( int escapeIndex = 0, loopTo = Escapes.Length - 1; escapeIndex <= loopTo; escapeIndex++ )
            {
                // must assign the escaped values.
                value = value.Replace( Escapes[escapeIndex], Characters[escapeIndex].ToString() );
            }
        }

        return value;
    }

    /// <summary>
    /// Replaces control characters such as <code>10</code> and <code>13</code> with common escape
    /// strings such as <code>'\n'</code> or <code>'\r'</code>, respectively.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Text including control characters. </param>
    /// <returns> String with control characters replaces with escape codes. </returns>
    public static string InsertCommonEscapeSequences( this string value )
    {
        if ( !string.IsNullOrEmpty( value ) )
        {
            for ( int escapeIndex = 0, loopTo = Escapes.Length - 1; escapeIndex <= loopTo; escapeIndex++ )
            {
                // must assign the replacement value.
                value = value.Replace( Characters[escapeIndex].ToString(), Escapes[escapeIndex] );
            }
        }

        return value;
    }

    /// <summary> Remove common escape sequences. </summary>
    /// <remarks> David, 2020-07-29. </remarks>
    /// <param name="value"> Text including escape sequences. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string RemoveCommonEscapeSequences( this string value )
    {
        if ( !string.IsNullOrEmpty( value ) )
        {
            for ( int escapeIndex = 0, loopTo = Escapes.Length - 1; escapeIndex <= loopTo; escapeIndex++ )
            {
                value = value.Replace( Escapes[escapeIndex], string.Empty );
            }
        }

        return value;
    }
}
