namespace cc.isr.Std.ArrayExtensions;

/// <summary>   Array extension methods. </summary>
/// <remarks>   David, 2020-09-15. </remarks>
public static partial class ArrayExtensionMethods
{
    /// <summary> Converts the jagged array to a single prevision type. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="values"> The values. </param>
    /// <returns> The given data converted to a single. </returns>
    public static float[][] CastSingle( this double[][] values )
    {
        if ( values is null ) throw new ArgumentNullException( nameof( values ) );

        int len = values.Length;
        float[][] result = new float[len][];
        for ( int i = 0, loopTo = len - 1; i <= loopTo; i++ )
        {
            result[i] = values[i].CastArray<float[]>();
        }

        return result;
    }

    /// <summary> Determines if the two specified arrays have the same values. </summary>
    /// <remarks>
    /// <see langword="Array"/> or <see langword="ArrayList"/> equals methods cannot be used because it
    /// expects the two entities to be the same for equality.
    /// </remarks>
    /// <param name="left">  The left value. </param>
    /// <param name="right"> The right value. </param>
    /// <returns> True if it succeeds; otherwise, false. </returns>
    public static bool ValueEquals<T>( this T[] left, T[] right )
    {
        bool result = true;
        if ( left is null )
            result = right is null;
        else if ( right is null )
            result = false;
        else
        {
            for ( int i = 0, loopTo = left.Length - 1; i <= loopTo; i++ )
            {
                if ( left[i] is null || !left[i]!.Equals( right[i] ) )
                {
                    result = false;
                    break;
                }
            }
        }

        return result;
    }

    /// <summary> Casts an array from one type to another. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown when the specified type is not an array. </exception>
    /// <exception cref="ArgumentException">         Thrown when one or more arguments have
    /// unsupported or illegal values. </exception>
    /// <exception cref="InvalidCastException">      Thrown when an object cannot be cast to a
    /// required type. </exception>
    /// <param name="source"> The source array to cast. </param>
    /// <returns> A new array with the correct new target type. </returns>
    public static TArray CastArray<TArray>( this Array source ) where TArray : class
    {
        Array tempResult;
        if ( source is null )
        {
            return default!;
        }

        Type arrayType = typeof( TArray );
        if ( !arrayType.IsArray )
        {
            throw new InvalidOperationException( $"Type {arrayType} is not a  {typeof( TArray )} array" );
        }

        Type? itemType = arrayType.GetElementType();
        if ( itemType is null )
        {
            throw new ArgumentException( $"{nameof( Type.GetElementType )} {itemType} element type for the array couldn't be resolved" );
        }

        int[] lengths = new int[source.Rank];
        int[] lowerBounds = new int[source.Rank];
        int[] upperBounds = new int[source.Rank];
        for ( int i = 0, loopTo = source.Rank - 1; i <= loopTo; i++ )
        {
            lengths[i] = source.GetLength( i );
            lowerBounds[i] = source.GetLowerBound( i );
            upperBounds[i] = source.GetUpperBound( i );
        }

        // Creates the array
        tempResult = Array.CreateInstance( itemType, lengths, lowerBounds );
        int[] indexes = new int[lengths.Length];
        Array.Copy( lowerBounds, indexes, lowerBounds.Length );
        int pos = tempResult.Rank - 1;
        indexes[pos] -= 1;
        pos = CalculateIndexes( ref indexes, pos, lowerBounds, upperBounds );
        while ( pos >= 0 )
        {
            tempResult.SetValue( Convert.ChangeType( source.GetValue( indexes ), itemType ), indexes );
            pos = CalculateIndexes( ref indexes, pos, lowerBounds, upperBounds );
        }

        return tempResult is not TArray finalResult
            ? throw new InvalidCastException( $"Unable to cast array type {source.GetType()} to an {typeof( TArray )} array" )
            : finalResult;
    }

    /// <summary> Calculate the indexes of the next element to copy. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="indexes">     [in,out] The list of the indexes in the different dimensions for
    /// the element to copy. </param>
    /// <param name="pos">         The current position within the list of indexes. </param>
    /// <param name="lowerBounds"> The list of lower bounds to use as limit. </param>
    /// <param name="upperBounds"> The list of upper bounds to use as limit. </param>
    /// <returns>
    /// The current position or -1 if the operation failed which means there is no next element to
    /// copy.
    /// </returns>
    internal static int CalculateIndexes( ref int[] indexes, int pos, int[] lowerBounds, int[] upperBounds )
    {
        indexes[pos] += 1;
        if ( indexes[pos] > upperBounds[pos] )
        {
            indexes[pos] = lowerBounds[pos];
            pos -= 1;
            if ( pos >= 0 )
            {
                pos = CalculateIndexes( ref indexes, pos, lowerBounds, upperBounds );
                if ( pos >= 0 )
                {
                    pos += 1;
                }
            }
        }

        return pos;
    }
}
