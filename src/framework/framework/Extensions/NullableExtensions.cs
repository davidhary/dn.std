namespace cc.isr.Std.NullableExtensions;

/// <summary> Includes extensions for <see cref="Nullable{T}">nullable</see>. </summary>
/// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para> </remarks>
public static class NullableExtensionMethods
{
    /// <summary> The null value. </summary>
    public const string NULL_VALUE = "NULL";

    /// <summary>
    /// Convert this object into a string representation that can be saved in the database.
    /// </summary>
    /// <remarks>   2024-09-05. </remarks>
    /// <param name="value">        The value. </param>
    /// <param name="trueValue">    The true value. </param>
    /// <param name="falseValue">   The false value. </param>
    /// <param name="nullValue">    The null value. </param>
    /// <returns>   A <see cref="string" /> that represents this object. </returns>
    public static string ToDataString( this bool? value, string trueValue, string falseValue, string nullValue )
    {
        return $"{(value.HasValue ? value.Value ? trueValue : falseValue : nullValue)}";
    }

    /// <summary>
    /// Convert this object into a string representation that can be saved in the database.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> A <see cref="string" /> that represents this object. </returns>
    public static string ToDataString( this bool? value )
    {
        return $"{(value.HasValue ? value.Value ? "1" : "0" : NULL_VALUE)}";
    }

    /// <summary>
    /// Convert this object into a string representation that can be saved in the database.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> A <see cref="string" /> that represents this object. </returns>
    public static string ToDataString( this byte? value )
    {
        return $"{(value.HasValue ? value.Value.ToString() : NULL_VALUE)}";
    }

    /// <summary>
    /// Convert this object into a string representation that can be saved in the database.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> A <see cref="string" /> that represents this object. </returns>
    public static string ToDataString( this int? value )
    {
        return $"{(value.HasValue ? value.Value.ToString() : NULL_VALUE)}";
    }

    /// <summary>
    /// Convert this object into a string representation that can be saved in the database.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> A <see cref="string" /> that represents this object. </returns>
    public static string ToDataString( this long? value )
    {
        return $"{(value.HasValue ? value.Value.ToString() : NULL_VALUE)}";
    }

    /// <summary>
    /// Convert this object into a string representation that can be saved in the database.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> A <see cref="string" /> that represents this object. </returns>
    public static string ToDataString( this double? value )
    {
        return $"{(value.HasValue ? value.Value.ToString() : NULL_VALUE)}";
    }

    /// <summary>
    /// Convert this object into a string representation that can be saved in the database.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">  The value. </param>
    /// <param name="format"> Describes the format to use. </param>
    /// <returns> A <see cref="string" /> that represents this object. </returns>
    public static string ToDataString( this double? value, string format )
    {
        return value.ToDataString( format, NULL_VALUE );
    }

    /// <summary>
    /// Convert this object into a string representation that can be saved in the database.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">     The value. </param>
    /// <param name="format">    Describes the format to use. </param>
    /// <param name="nullValue"> The null value. </param>
    /// <returns> A <see cref="string" /> that represents this object. </returns>
    public static string ToDataString( this double? value, string format, string nullValue )
    {
        return $"{(value.HasValue ? $"{value.Value.ToString( format )}" : nullValue)}";
    }
}
