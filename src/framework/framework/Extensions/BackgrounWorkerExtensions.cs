namespace cc.isr.Std.BackgroundWorkerExtensions;

/// <summary> Includes extensions for <see cref="System.ComponentModel.BackgroundWorker">background worker</see>. </summary>
/// <remarks> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-11-07, 2.1.5425 </para></remarks>
public static class BackgroundWorkerExtensionMethods
{
    /// <summary> Reports the progress. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="worker">       The worker. </param>
    /// <param name="userState">    State of the user. </param>
    /// <param name="currentCount"> Number of currents. </param>
    /// <param name="totalCount">   Number of totals. </param>
    public static void ReportProgress( this System.ComponentModel.BackgroundWorker worker, object userState, int currentCount, int totalCount )
    {
        worker.ReportProgress( userState, currentCount, totalCount, TimeSpan.FromSeconds( 1d ) );
    }

    private static double _progressLevel;
    private static DateTimeOffset _nextTime = System.DateTimeOffset.Now;

    /// <summary> Reports the progress. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="worker">       The worker. </param>
    /// <param name="userState">    State of the user. </param>
    /// <param name="currentCount"> Number of currents. </param>
    /// <param name="totalCount">   Number of totals. </param>
    /// <param name="period">       The refractory period - progress is reported every so often. </param>
    public static void ReportProgress( this System.ComponentModel.BackgroundWorker worker, object userState, int currentCount, int totalCount, TimeSpan period )
    {
        if ( worker is null ) return;
        // update every second
        if ( currentCount == 0 )
        {
            _progressLevel = 0d;
            worker.ReportProgress( 0, userState );
        }
        else if ( currentCount >= totalCount )
        {
            worker.ReportProgress( 100, userState );
        }
        else
        {
            double progress = 100 * currentCount / ( double ) totalCount;
            if ( progress > _progressLevel && DateTimeOffset.Now > _nextTime )
            {
                worker.ReportProgress( ( int ) progress, userState );
                _progressLevel += 1d;
                _nextTime = DateTimeOffset.Now + period;
            }
        }
    }
}
