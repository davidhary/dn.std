using System.Collections.Specialized;

namespace cc.isr.Std.CollectionExtensions;
/// <summary>
/// Contains extension methods for <see cref="NameValueCollection"/>.
/// </summary>
/// <remarks> (c) 2016 Cory Charlton.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-12-12, 3.1.6192.x"> https://GitHub.com/CoryCharlton/CCSWE.Core. </para></remarks>
public static class CollectionExtensionMethods
{
    #region " add or replace "

    /// <summary> Adds or modifies. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="collection"> The <see cref="NameValueCollection"/> to retrieve the value from. </param>
    /// <param name="key">        The key associated with the value being retrieved. </param>
    /// <param name="value">      The value being retrieved. </param>
    public static void AddOrModify<TKey, TValue>( this IDictionary<TKey, TValue> collection, TKey key, TValue value )
    {
        if ( collection is null )
        {
            throw new ArgumentNullException( nameof( collection ) );
        }

        if ( collection.ContainsKey( key ) )
        {
            collection[key] = value;
        }
        else
        {
            collection.Add( key, value );
        }
    }

    /// <summary> Adds or replaces. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="collection"> The <see cref="NameValueCollection"/> to retrieve the value from. </param>
    /// <param name="key">        The key associated with the value being retrieved. </param>
    /// <param name="value">      The value being retrieved. </param>
    public static void AddOrReplace<TKey, TValue>( this IDictionary<TKey, TValue> collection, TKey key, TValue value )
    {
        if ( collection is null )
        {
            throw new ArgumentNullException( nameof( collection ) );
        }

        if ( collection.ContainsKey( key ) )
        {
            _ = collection.Remove( key );
        }

        collection.Add( key, value );
    }

    #endregion

    #region " generic name value selectors "

    /// <summary>
    /// Gets the value associated with the specified key from the <see cref="NameValueCollection"/>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="collection"> The <see cref="NameValueCollection"/> to retrieve the value from. </param>
    /// <param name="key">        The key associated with the value being retrieved. </param>
    /// <returns> The value associated with the specified key. </returns>
    public static T GetValueAs<T>( this NameValueCollection collection, string key )
    {
        if ( collection is null )
        {
            throw new ArgumentNullException( nameof( collection ) );
        }

        if ( string.IsNullOrWhiteSpace( key ) )
        {
            throw new ArgumentNullException( nameof( key ) );
        }

        string stringValue = collection[key];
        return Converter.ConvertValue<T>( stringValue );
    }

    /// <summary>
    /// Gets the value associated with the specified key from the <see cref="NameValueCollection"/>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="collection">   The <see cref="NameValueCollection"/> to retrieve the value from. </param>
    /// <param name="key">          The key associated with the value being retrieved. </param>
    /// <param name="defaultValue"> The default value to be returned if the specified key does not
    /// exist or the cast fails. </param>
    /// <returns> The value associated with the specified key. </returns>
    public static T GetValueAs<T>( this NameValueCollection collection, string key, T defaultValue )
    {
        if ( collection is null )
        {
            throw new ArgumentNullException( nameof( collection ) );
        }

        if ( string.IsNullOrWhiteSpace( key ) )
        {
            throw new ArgumentNullException( nameof( key ) );
        }

        string stringValue = collection[key];
        return string.IsNullOrWhiteSpace( stringValue )
            ? defaultValue
            : Converter.SafeConvert( stringValue, defaultValue );
    }

    /// <summary>
    /// Tries to get the value associated with the specified key from the
    /// <see cref="NameValueCollection"/>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="collection"> The <see cref="NameValueCollection"/> to
    /// retrieve the value from. </param>
    /// <param name="key">        The key associated with the value being
    /// retrieved. </param>
    /// <param name="value">      The value being retrieved. </param>
    /// <returns> True if the key exists. False is not. </returns>
    public static bool TryGetValueAs<T>( this NameValueCollection collection, string key, out T value )
    {
        if ( collection is null ) throw new ArgumentNullException( nameof( collection ) );

        if ( string.IsNullOrWhiteSpace( key ) ) throw new ArgumentNullException( nameof( key ) );

        string stringValue = collection[key];
        T? v = default;
        try
        {
            v = Converter.Convert<T>( stringValue );
        }
        catch
        {
        }
        value = v ?? default!;
        return v is not null;
    }

    #endregion

    #region " binary lookup "

    /// <summary> Uses a binary search to find the index of the maximum value from. </summary>
    /// <remarks> Assumes that the value is within the lower and higher bounds of the array. </remarks>
    /// <param name="orderedValues"> The ordered values. </param>
    /// <param name="value">         The lookup value. </param>
    /// <returns>
    /// An Integer pointing to the highest value lower or equal to the given value.
    /// </returns>
    private static int LookupBoundingIndexThis<T>( IEnumerable<T> orderedValues, T value ) where T : IComparable<T>
    {
        int first = 0;
        int last = orderedValues.Count() - 1;
        int middle;
        do
        {
            middle = (first + last) / 2;
            int currentCompare = orderedValues.ElementAtOrDefault( middle ).CompareTo( value );
            int nextCompare = orderedValues.ElementAtOrDefault( middle + 1 ).CompareTo( value );
            if ( currentCompare <= 0 && nextCompare >= 0 )
            {
                // the search is done when the two adjacent indexes bind the search value
                break;
            }
            else if ( currentCompare < 0 )
            {
                // if the middle is lower than the value, search the high (middle to last) part of the list
                first = middle;
            }
            else
            {
                // if the middle is higher than the value, search the low (first to middle) part of the list
                last = middle;
            }
        }
        while ( first < last );
        return middle;
    }

    /// <summary> Uses a binary search to find the index of the maximum value from. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="orderedValues"> The ordered values. </param>
    /// <param name="value">         The lookup value. </param>
    /// <returns>
    /// The first index if the ratio is equal or lower than the first value; or <para>
    /// The last index if the ratio is equal or above the last value.</para>
    /// </returns>
    public static int LookupBoundingIndex<T>( this IEnumerable<T> orderedValues, T value ) where T : IComparable<T>
    {
        return orderedValues is null
            ? throw new ArgumentNullException( nameof( orderedValues ) )
            : !orderedValues.Any()
            ? throw new ArgumentException( "Lookup collection is empty" )
            : value.CompareTo( orderedValues.ElementAtOrDefault( 0 ) ) <= 0
            ? 0
            : value.CompareTo( orderedValues.ElementAtOrDefault( orderedValues.Count() - 1 ) ) >= 0
                ? orderedValues.Count() - 1
                : LookupBoundingIndexThis( orderedValues, value );
    }

    /// <summary>
    /// Uses a binary search to find the index of the maximum value from
    /// <paramref name="orderedValues"/> lower than or equal the value.
    /// </summary>
    /// <remarks> Assumes that the value is within the lower and higher bounds of the array. </remarks>
    /// <param name="orderedValues"> The ordered values. </param>
    /// <param name="value">         The ratio. </param>
    /// <returns>
    /// An Integer pointing to the highest value lower or equal to the given value.
    /// </returns>
    private static int LookupBoundingIndexThis( IEnumerable<double> orderedValues, double value )
    {
        int first = 0;
        int last = orderedValues.Count() - 1;
        int middle;
        do
        {
            middle = (first + last) / 2;
            if ( orderedValues.ElementAtOrDefault( middle ) <= value && orderedValues.ElementAtOrDefault( middle + 1 ) >= value )
            {
                // the search is done when the two adjacent indexes bind the search value
                break;
            }
            else if ( orderedValues.ElementAtOrDefault( middle ) < value )
            {
                // if the middle is lower than the value, search the high (middle to last) part of the list
                first = middle;
            }
            else
            {
                // if the middle is higher than the value, search the low (first to middle) part of the list
                last = middle;
            }
        }
        while ( first < last );
        return middle;
    }

    /// <summary>
    /// Uses a binary search to find the index of the maximum value from
    /// <paramref name="orderedValues"/> lower than or equal the value.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="orderedValues"> The ordered values. </param>
    /// <param name="value">         The ratio. </param>
    /// <returns>
    /// The first index if the ratio is equal or lower than the first value; or <para>
    /// The last index if the ratio is equal or above the last value.</para>
    /// </returns>
    public static int LookupBoundingIndex( this IEnumerable<double> orderedValues, double value )
    {
        return orderedValues is null
            ? throw new ArgumentNullException( nameof( orderedValues ) )
            : !orderedValues.Any()
            ? throw new ArgumentException( "Lookup collection is empty" )
            : value <= orderedValues.ElementAtOrDefault( 0 )
            ? 0
            : value >= orderedValues.ElementAtOrDefault( orderedValues.Count() - 1 )
                ? orderedValues.Count() - 1
                : LookupBoundingIndexThis( orderedValues, value );
    }

    #endregion
}
