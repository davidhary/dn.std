namespace cc.isr.Std.TaskExtensions;

/// <summary> Task extension methods. </summary>
/// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para> </remarks>
public static class TaskExtensionMethods
{
    /// <summary> Queries if a task is active. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="task"> The task. </param>
    /// <returns> <c>true</c> if the action task is active; otherwise <c>false</c> </returns>
    public static bool IsTaskActive( this Task task )
    {
        return task is not null && !(task.IsCanceled || task.IsCompleted || task.IsFaulted);
    }

    /// <summary> Query if task ended. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="task"> The task. </param>
    /// <returns> <c>true</c> if task ended; otherwise <c>false</c> </returns>
    public static bool IsTaskEnded( this Task task )
    {
        return task is null || task.IsCanceled || task.IsCompleted || task.IsFaulted;
    }

    /// <summary> Queries if a task is active. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="status"> The status. </param>
    /// <returns> <c>true</c> if a task is active; otherwise <c>false</c> </returns>
    public static bool IsTaskActive( this TaskStatus status )
    {
        return status is TaskStatus.Created or TaskStatus.Running;
    }

    /// <summary> Query if task ended. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="status"> The status. </param>
    /// <returns> <c>true</c> if task ended; otherwise <c>false</c> </returns>
    public static bool IsTaskEnded( this TaskStatus status )
    {
        return status is TaskStatus.RanToCompletion or TaskStatus.Canceled or TaskStatus.Faulted;
    }

    /// <summary> Wait task active. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="task">    The task. </param>
    /// <param name="timeout"> The timeout. </param>
    /// <returns>   A (bool Success,TimeSpan Elapsed) </returns>
    public static (bool Success, TimeSpan Elapsed) WaitTaskActive( this Task task, TimeSpan timeout )
    {
        return TimeSpanExtensions.TimeSpanExtensionMethods.AsyncWaitUntil( timeout, TimeSpan.Zero, task.IsTaskActive );
    }

    /// <summary> Wait idle. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="task">    The task. </param>
    /// <param name="timeout"> The timeout. </param>
    public static (bool Success, TimeSpan Elapsed) WaitTaskIdle( Task task, TimeSpan timeout )
    {
        return TimeSpanExtensions.TimeSpanExtensionMethods.AsyncWaitUntil( timeout, TimeSpan.Zero, task.IsTaskEnded );
    }
}
