namespace cc.isr.Std.BinaryReaderExtensions;

/// <summary> Includes extensions for <see cref="BinaryReader">binary reader</see>. </summary>
/// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-01-23, 2.0.5136.x. based on I/O Library binary reader. </para></remarks>
public static class BinaryReaderExtensionMethods
{
    /// <summary> Closes the binary reader and base stream. </summary>
    /// <remarks> Use this method to close the instance. </remarks>
    /// <param name="reader"> The reader. </param>
    public static void CloseReader( this BinaryReader reader )
    {
        if ( reader is not null )
        {
            reader.Close();
            if ( reader is not null && reader.BaseStream is not null )
            {
                reader.BaseStream.Close();
            }
        }
    }

    /// <summary> Opens a stream. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="fullFileName"> Specifies the name of the binary file which to read. </param>
    /// <returns> A System.IO.FileStream. </returns>
    public static FileStream? OpenStream( string fullFileName )
    {
        FileStream? fileStream = null;
        FileStream? tempFileStream = null;
        if ( !string.IsNullOrWhiteSpace( fullFileName ) )
        {
            try
            {
                tempFileStream = new FileStream( fullFileName, FileMode.Open, FileAccess.Read );
                fileStream = tempFileStream;
            }
            catch
            {
                tempFileStream?.Dispose();
                throw;
            }
        }

        return fileStream;
    }

    /// <summary>
    /// Opens a binary file for reading and returns a reference to the reader. The file is
    /// <see cref="FileMode.Open">opened</see> in
    /// <see cref="FileAccess.Read">read access</see>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <exception cref="FileNotFoundException"> Thrown when the requested file is not present. </exception>
    /// <param name="fullFileName"> Specifies the file name. </param>
    /// <returns> A reference to an open <see cref="BinaryReader">binary reader</see>. </returns>
    public static BinaryReader? OpenReader( string fullFileName )
    {
        if ( string.IsNullOrWhiteSpace( fullFileName ) )
        {
            throw new ArgumentNullException( nameof( fullFileName ) );
        }

        if ( !File.Exists( fullFileName ) )
        {
            throw new FileNotFoundException( "Failed opening a binary reader -- file not found.", fullFileName );
        }

        BinaryReader? tempReader = null;
        FileStream? stream = null;
        BinaryReader? reader;
        try
        {
            stream = OpenStream( fullFileName );
            tempReader = new BinaryReader( stream );
            reader = tempReader;
        }
        catch
        {
            stream?.Dispose();
            tempReader?.Dispose();
            throw;
        }

        return reader;
    }

    #region " double "

    /// <summary>
    /// Reads a single-dimension <see cref="double">double-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <exception cref="IOException">           Thrown when an IO failure occurred. </exception>
    /// <param name="reader"> The reader. </param>
    /// <returns> A <see cref="double">double-precision</see> array. </returns>
    public static double[] ReadDoubleArray( this BinaryReader reader )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        long startingPosition = reader.BaseStream.Position;

        // read the stored length
        int storedLength = reader.ReadInt32();
        return storedLength < 0
            ? throw new System.IO.IOException( $"Program encountered a negative array length of {storedLength}. Possibly the file is corrupt the reader position at {startingPosition} is incorrect." )
            : reader.ReadDoubleArray( storedLength );
    }

    /// <summary>
    /// Reads a single-dimension <see cref="double">double-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="reader">       The reader. </param>
    /// <param name="elementCount"> Specifies the number of data points. </param>
    /// <returns> A <see cref="double">double-precision</see> array. </returns>
    public static double[] ReadDoubleArray( this BinaryReader reader, int elementCount )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        if ( elementCount < 0 )
        {
            throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount,
                                                   $"Array size specified as {elementCount} must be non-negative." );
        }

        if ( elementCount == 0 )
        {
            // return the empty array 
            double[] data = [];
            return data;
        }
        else
        {
            // allocate data array
            double[] data = new double[elementCount];

            // Read the file
            for ( int sampleNumber = 0, loopTo = elementCount - 1; sampleNumber <= loopTo; sampleNumber++ )
            {
                data[sampleNumber] = reader.ReadDouble();
            }

            return data;
        }
    }

    /// <summary>
    /// Reads a single-dimension <see cref="double">double-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="reader">       The reader. </param>
    /// <param name="elementCount"> Specifies the number of data points. </param>
    /// <param name="verifyLength"> If true, verifies the length against the given length. </param>
    /// <returns> A single-dimension <see cref="double">double-precision</see> array. </returns>
    public static double[] ReadDoubleArray( this BinaryReader reader, int elementCount, bool verifyLength )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        if ( verifyLength )
        {
            long startingPosition = reader.BaseStream.Position;
            double[] data = reader.ReadDoubleArray();
            return data.Length != elementCount
                ? throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount,
                    $"Data length stored in file of {data.Length} elements does not match the expected data length of {elementCount} elements at {startingPosition}." )
                : data;
        }
        else
        {
            return reader.ReadDoubleArray();
        }
    }

    /// <summary>
    /// Reads a single-dimension <see cref="double">double-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="reader">     The reader. </param>
    /// <param name="count">      Specifies the number of data points. </param>
    /// <param name="startIndex"> Specifies the index of the first data point. </param>
    /// <param name="stepSize">   Specifies the step size between adjacent data points. </param>
    /// <returns> A single-dimension <see cref="double">double-precision</see> array. </returns>
    public static double[] ReadDoubleArray( this BinaryReader reader, int count, int startIndex, int stepSize )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        long startingPosition = reader.BaseStream.Position;

        // read the stored length
        int storedLength = reader.ReadInt32();
        if ( storedLength != count )
        {
            throw new ArgumentOutOfRangeException( nameof( count ), count,
                    $"Data length stored in file of {storedLength} elements does not match the expected data length of {count} elements at {startingPosition}." );
        }

        // allocate data array
        double[] data = new double[count];

        // skip samples to get to the first channel of this sample set.
        if ( startIndex > 0 )
        {
            for ( int i = 1, loopTo = startIndex; i <= loopTo; i++ )
            {
                _ = reader.ReadDouble();
            }
        }
        // Read the file
        for ( int sampleNumber = 0, loopTo1 = count - 1; sampleNumber <= loopTo1; sampleNumber++ )
        {
            data[sampleNumber] = reader.ReadDouble();
            if ( stepSize > 1 )
            {
                for ( int i = 2, loopTo2 = stepSize; i <= loopTo2; i++ )
                {
                    _ = reader.ReadDouble();
                }
            }
        }

        return data;
    }

    /// <summary> Reads a double precision value from the data file. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="reader">   The reader. </param>
    /// <param name="location"> Specifies the file location. </param>
    /// <returns> A <see cref="double">value</see>. </returns>
    public static double ReadDoubleValue( this BinaryReader reader, long location )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        _ = reader.BaseStream.Seek( location, SeekOrigin.Begin );
        return reader.ReadDouble();
    }

    #endregion

    #region " int32 "

    /// <summary>
    /// Reads a single-dimension <see cref="int">Int32-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <exception cref="IOException">           Thrown when an IO failure occurred. </exception>
    /// <param name="reader"> The reader. </param>
    /// <returns> A <see cref="int">Int32-precision</see> array. </returns>
    public static int[] ReadInt32Array( this BinaryReader reader )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        long startingPosition = reader.BaseStream.Position;

        // read the stored length
        int storedLength = reader.ReadInt32();
        return storedLength < 0
            ? throw new System.IO.IOException( $"Program encountered a negative array length of {storedLength}. Possibly the file is corrupt the reader position at {startingPosition} is incorrect." )
            : reader.ReadInt32Array( storedLength );
    }

    /// <summary>
    /// Reads a single-dimension <see cref="int">Int32-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="reader">       The reader. </param>
    /// <param name="elementCount"> Specifies the number of data points. </param>
    /// <returns> A <see cref="int">Int32-precision</see> array. </returns>
    public static int[] ReadInt32Array( this BinaryReader reader, int elementCount )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        if ( elementCount < 0 )
        {
            throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount,
                                                   $"Array size specified as {elementCount} must be non-negative." );
        }

        if ( elementCount == 0 )
        {
            // return the empty array 
            int[] data = [];
            return data;
        }
        else
        {
            // allocate data array
            int[] data = new int[elementCount];

            // Read from the file
            for ( int sampleNumber = 0, loopTo = elementCount - 1; sampleNumber <= loopTo; sampleNumber++ )
            {
                data[sampleNumber] = reader.ReadInt32();
            }

            return data;
        }
    }

    /// <summary>
    /// Reads a single-dimension <see cref="int">Int32-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="reader">       The reader. </param>
    /// <param name="elementCount"> Specifies the number of data points. </param>
    /// <param name="verifyLength"> If true, verifies the length against the given length. </param>
    /// <returns> A single-dimension <see cref="int">Int32-precision</see> array. </returns>
    public static int[] ReadInt32Array( this BinaryReader reader, int elementCount, bool verifyLength )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        if ( verifyLength )
        {
            long startingPosition = reader.BaseStream.Position;
            int[] data = reader.ReadInt32Array();
            return data.Length != elementCount
                ? throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount,
                    $"Data length stored in file of {data.Length} elements does not match the expected data length of {elementCount} elements at {startingPosition}." )
                : data;
        }
        else
        {
            return reader.ReadInt32Array();
        }
    }

    /// <summary>
    /// Reads a single-dimension <see cref="int">Int32-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="reader">     The reader. </param>
    /// <param name="count">      Specifies the number of data points. </param>
    /// <param name="startIndex"> Specifies the index of the first data point. </param>
    /// <param name="stepSize">   Specifies the step size between adjacent data points. </param>
    /// <returns> A single-dimension <see cref="int">Int32-precision</see> array. </returns>
    public static int[] ReadInt32Array( this BinaryReader reader, int count, int startIndex, int stepSize )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        long startingPosition = reader.BaseStream.Position;

        // read the stored length
        int storedLength = reader.ReadInt32();
        if ( storedLength != count )
        {
            throw new ArgumentOutOfRangeException( nameof( count ), count,
                    $"Data length stored in file of {storedLength} elements does not match the expected data length of {count} elements at {startingPosition}." );
        }

        // allocate data array
        int[] data = new int[count];

        // skip samples to get to the first channel of this sample set.
        if ( startIndex > 0 )
        {
            for ( int i = 1, loopTo = startIndex; i <= loopTo; i++ )
            {
                _ = reader.ReadInt32();
            }
        }
        // Read from the file
        for ( int sampleNumber = 0, loopTo1 = count - 1; sampleNumber <= loopTo1; sampleNumber++ )
        {
            data[sampleNumber] = reader.ReadInt32();
            if ( stepSize > 1 )
            {
                for ( int i = 2, loopTo2 = stepSize; i <= loopTo2; i++ )
                {
                    _ = reader.ReadInt32();
                }
            }
        }

        return data;
    }

    /// <summary> Reads a Int32 precision value from the data file. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="reader">   The reader. </param>
    /// <param name="location"> Specifies the file location. </param>
    /// <returns> A <see cref="int">value</see>. </returns>
    public static int ReadInt32Value( this BinaryReader reader, long location )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        _ = reader.BaseStream.Seek( location, SeekOrigin.Begin );
        return reader.ReadInt32();
    }

    #endregion

    #region " int64 "

    /// <summary>
    /// Reads a single-dimension <see cref="long">Int64-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <exception cref="IOException">           Thrown when an IO failure occurred. </exception>
    /// <param name="reader"> The reader. </param>
    /// <returns> A <see cref="long">Int64-precision</see> array. </returns>
    public static long[] ReadInt64Array( this BinaryReader reader )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        long startingPosition = reader.BaseStream.Position;

        // read the stored length
        int storedLength = reader.ReadInt32();
        return storedLength < 0
            ? throw new System.IO.IOException( $"Program encountered a negative array length of {storedLength}. Possibly the file is corrupt the reader position at {startingPosition} is incorrect." )
            : reader.ReadInt64Array( storedLength );
    }

    /// <summary>
    /// Reads a single-dimension <see cref="long">Int64-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="reader">       The reader. </param>
    /// <param name="elementCount"> Specifies the number of data points. </param>
    /// <returns> A <see cref="long">Int64-precision</see> array. </returns>
    public static long[] ReadInt64Array( this BinaryReader reader, int elementCount )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        if ( elementCount < 0 )
        {
            throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount, "Array size specified as {elementCount} must be non-negative." );
        }

        if ( elementCount == 0 )
        {
            // return the empty array 
            long[] data = [];
            return data;
        }
        else
        {
            // allocate data array
            long[] data = new long[elementCount];

            // Read the file
            for ( int sampleNumber = 0, loopTo = elementCount - 1; sampleNumber <= loopTo; sampleNumber++ )
            {
                data[sampleNumber] = reader.ReadInt64();
            }

            return data;
        }
    }

    /// <summary>
    /// Reads a single-dimension <see cref="long">Int64-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="reader">       The reader. </param>
    /// <param name="elementCount"> Specifies the number of data points. </param>
    /// <param name="verifyLength"> If true, verifies the length against the given length. </param>
    /// <returns> A single-dimension <see cref="long">Int64-precision</see> array. </returns>
    public static long[] ReadInt64Array( this BinaryReader reader, int elementCount, bool verifyLength )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        if ( verifyLength )
        {
            long startingPosition = reader.BaseStream.Position;
            long[] data = reader.ReadInt64Array();
            return data.Length != elementCount
                ? throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount,
                     $"Data length stored in file of {data.Length} elements does not match the expected data length of {elementCount} elements at {startingPosition}." )
                : data;
        }
        else
        {
            return reader.ReadInt64Array();
        }
    }

    /// <summary>
    /// Reads a single-dimension <see cref="long">Int64-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="reader">     The reader. </param>
    /// <param name="count">      Specifies the number of data points. </param>
    /// <param name="startIndex"> Specifies the index of the first data point. </param>
    /// <param name="stepSize">   Specifies the step size between adjacent data points. </param>
    /// <returns> A single-dimension <see cref="long">Int64-precision</see> array. </returns>
    public static long[] ReadInt64Array( this BinaryReader reader, int count, int startIndex, int stepSize )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        long startingPosition = reader.BaseStream.Position;

        // read the stored length
        int storedLength = reader.ReadInt32();
        if ( storedLength != count )
        {
            throw new ArgumentOutOfRangeException( nameof( count ), count,
                        $"Data length stored in file of {storedLength} elements does not match the expected data length of {count} elements at {startingPosition}." );
        }

        // allocate data array
        long[] data = new long[count];

        // skip samples to get to the first channel of this sample set.
        if ( startIndex > 0 )
        {
            for ( int i = 1, loopTo = startIndex; i <= loopTo; i++ )
            {
                _ = reader.ReadInt64();
            }
        }
        // Read the file
        for ( int sampleNumber = 0, loopTo1 = count - 1; sampleNumber <= loopTo1; sampleNumber++ )
        {
            data[sampleNumber] = reader.ReadInt64();
            if ( stepSize > 1 )
            {
                for ( int i = 2, loopTo2 = stepSize; i <= loopTo2; i++ )
                {
                    _ = reader.ReadInt64();
                }
            }
        }

        return data;
    }

    /// <summary> Reads a Int64 precision value from the data file. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="reader">   The reader. </param>
    /// <param name="location"> Specifies the file location. </param>
    /// <returns> A <see cref="long">value</see>. </returns>
    public static long ReadInt64Value( this BinaryReader reader, long location )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        _ = reader.BaseStream.Seek( location, SeekOrigin.Begin );
        return reader.ReadInt64();
    }

    #endregion

    #region " single "

    /// <summary>
    /// Reads a single-dimension <see cref="float">Single-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <exception cref="IOException">           Thrown when an IO failure occurred. </exception>
    /// <param name="reader"> The reader. </param>
    /// <returns> A <see cref="float">Single-precision</see> array. </returns>
    public static float[] ReadSingleArray( this BinaryReader reader )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        long startingPosition = reader.BaseStream.Position;

        // read the stored length
        int storedLength = reader.ReadInt32();
        return storedLength < 0
            ? throw new System.IO.IOException( $"Program encountered a negative array length of {storedLength}. Possibly the file is corrupt the reader position at {startingPosition} is incorrect." )
            : reader.ReadSingleArray( storedLength );
    }

    /// <summary>
    /// Reads a single-dimension <see cref="float">Single-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="reader">       The reader. </param>
    /// <param name="elementCount"> Specifies the number of data points. </param>
    /// <returns> A <see cref="float">Single-precision</see> array. </returns>
    public static float[] ReadSingleArray( this BinaryReader reader, int elementCount )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        if ( elementCount < 0 )
        {
            throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount, "Array size specified as {elementCount} must be non-negative." );
        }

        if ( elementCount == 0 )
        {
            // return the empty array 
            float[] data = [];
            return data;
        }
        else
        {
            // allocate data array
            float[] data = new float[elementCount];

            // Read from the file
            for ( int sampleNumber = 0, loopTo = elementCount - 1; sampleNumber <= loopTo; sampleNumber++ )
            {
                data[sampleNumber] = reader.ReadSingle();
            }

            return data;
        }
    }

    /// <summary>
    /// Reads a single-dimension <see cref="float">Single-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="reader">       The reader. </param>
    /// <param name="elementCount"> Specifies the number of data points. </param>
    /// <param name="verifyLength"> If true, verifies the length against the given length. </param>
    /// <returns> A single-dimension <see cref="float">Single-precision</see> array. </returns>
    public static float[] ReadSingleArray( this BinaryReader reader, int elementCount, bool verifyLength )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        if ( verifyLength )
        {
            _ = reader.BaseStream.Position;
            float[] data = reader.ReadSingleArray();
            return data.Length != elementCount
                ? throw new ArgumentOutOfRangeException( nameof( elementCount ), elementCount,
                        "Data length stored in file of {data.Length} elements does not match the expected data length of {elementCount} elements at {startingPosition}." )
                : data;
        }
        else
        {
            return reader.ReadSingleArray();
        }
    }

    /// <summary>
    /// Reads a single-dimension <see cref="float">Single-precision</see>
    /// array from the data file.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="reader">     The reader. </param>
    /// <param name="count">      Specifies the number of data points. </param>
    /// <param name="startIndex"> Specifies the index of the first data point. </param>
    /// <param name="stepSize">   Specifies the step size between adjacent data points. </param>
    /// <returns> A single-dimension <see cref="float">Single-precision</see> array. </returns>
    public static float[] ReadSingleArray( this BinaryReader reader, int count, int startIndex, int stepSize )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        long startingPosition = reader.BaseStream.Position;

        // read the stored length
        int storedLength = reader.ReadInt32();
        if ( storedLength != count )
        {
            throw new ArgumentOutOfRangeException( nameof( count ), count,
                $"Data length stored in file of {storedLength} elements does not match the expected data length of {count} elements at {startingPosition}." );
        }

        // allocate data array
        float[] data = new float[count];

        // skip samples to get to the first channel of this sample set.
        if ( startIndex > 0 )
        {
            for ( int i = 1, loopTo = startIndex; i <= loopTo; i++ )
            {
                _ = reader.ReadSingle();
            }
        }
        // Read the file
        for ( int sampleNumber = 0, loopTo1 = count - 1; sampleNumber <= loopTo1; sampleNumber++ )
        {
            data[sampleNumber] = reader.ReadSingle();
            if ( stepSize > 1 )
            {
                for ( int i = 2, loopTo2 = stepSize; i <= loopTo2; i++ )
                {
                    _ = reader.ReadSingle();
                }
            }
        }

        return data;
    }

    /// <summary> Reads a Single precision value from the data file. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="reader">   The reader. </param>
    /// <param name="location"> Specifies the file location. </param>
    /// <returns> A <see cref="float">value</see>. </returns>
    public static float ReadSingleValue( this BinaryReader reader, long location )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        _ = reader.BaseStream.Seek( location, SeekOrigin.Begin );
        return reader.ReadSingle();
    }

    #endregion

    #region " string "

    /// <summary> Reads a string value from the data file. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="reader">   The reader. </param>
    /// <param name="location"> Specifies the file location. </param>
    /// <returns> A <see cref="string">String</see> value. </returns>
    public static string ReadStringValue( this BinaryReader reader, long location )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        _ = reader.BaseStream.Seek( location, SeekOrigin.Begin );
        return reader.ReadString();
    }

    #endregion

    #region " date time "

    /// <summary> Reads date time. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="reader"> The reader. </param>
    /// <returns> The date time. </returns>
    public static DateTime ReadDateTime( this BinaryReader reader )
    {
        return reader is null ? throw new ArgumentNullException( nameof( reader ) ) : DateTime.FromOADate( reader.ReadDouble() );
    }

    /// <summary> Reads date time. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="reader">   The reader. </param>
    /// <param name="location"> Specifies the file location. </param>
    /// <returns> The date time. </returns>
    public static DateTime ReadDateTime( this BinaryReader reader, long location )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        _ = reader.BaseStream.Seek( location, SeekOrigin.Begin );
        return reader.ReadDateTime();
    }

    #endregion

    #region " time span "

    /// <summary> Reads a timespan. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="reader"> The reader. </param>
    /// <returns> The timespan. </returns>
    public static TimeSpan ReadTimespan( this BinaryReader reader )
    {
        return reader is null ? throw new ArgumentNullException( nameof( reader ) ) : TimeSpan.FromMilliseconds( reader.ReadDouble() );
    }

    /// <summary> Reads a timespan. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="reader">   The reader. </param>
    /// <param name="location"> Specifies the file location. </param>
    /// <returns> The timespan. </returns>
    public static TimeSpan ReadTimespan( this BinaryReader reader, long location )
    {
        if ( reader is null )
        {
            throw new ArgumentNullException( nameof( reader ) );
        }

        _ = reader.BaseStream.Seek( location, SeekOrigin.Begin );
        return reader.ReadTimespan();
    }

    #endregion
}
