namespace cc.isr.Std.GuidExtensions;

/// <summary> GUID extension methods. </summary>
public static partial class GuidExtensionMethods
{
    /// <summary> Creates a new base 64 unique identifier. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Unique identifier. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string NewBase64Guid( this Guid value )
    {
        string result = Convert.ToBase64String( value.ToByteArray() );
        result = result[..^2];
        return result;
    }

    /// <summary> Appends the base 64 string suffix (==). </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value"> The value. </param>
    /// <returns> Value as a GUID. </returns>
    public static string ToBase64String( this string value )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( value, nameof( value ) );
#else
        if ( value is null ) throw new ArgumentNullException( nameof( value ) );
#endif

        string suffix = "==";
        if ( !value.EndsWith( suffix, StringComparison.Ordinal ) )
        {
            value = $"{value}{suffix}";
        }

        return value;
    }

    /// <summary> Initializes this object from the given from base 64 unique identifier. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value"> The value. </param>
    /// <returns> A GUID. </returns>
    public static Guid FromBase64Guid( this string value )
    {
        return value is null ? throw new ArgumentNullException( nameof( value ) ) : new Guid( Convert.FromBase64String( value.ToBase64String() ) );
    }
}
