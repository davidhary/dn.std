using System.Diagnostics;

namespace cc.isr.Std.StackTraceExtensions;

/// <summary> Includes extensions for <see cref="string">String</see> Compacting. </summary>
/// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-02-14, 2.0.5158"> Based on legacy Call Stack Parser. </para></remarks>
public static class StackTraceExtensionMethods
{
    #region " common "

    /// <summary> The framework locations. </summary>
    private static readonly string[] _frameworkLocations = ["at System", "at Microsoft"];

    /// <summary> Returns true if the line is a framework line. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value">        The value. </param>
    /// <param name="locationName"> [in,out] Name of the location. </param>
    /// <returns> <c>true</c> if the line is a framework line; otherwise, <c>false</c>. </returns>
    public static bool IsFrameworkLine( this string value, ref string locationName )
    {
        foreach ( string currentLocationName in _frameworkLocations )
        {
            locationName = currentLocationName;
            if ( !string.IsNullOrWhiteSpace( value ) && value.TrimStart().StartsWith( locationName, StringComparison.Ordinal ) )
            {
                return true;
            }
        }

        return false;
    }

    #endregion

    #region " use lines "

    /// <summary> Parses the stack trace and returns the user or full call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="trace">         The Stack Trace. </param>
    /// <param name="skipLines">     Specifies the number of lines to skip. If getting the stack trace
    /// from the <see cref="Environment.StackTrace">environment</see>,
    /// the first two lines include the environment methods. </param>
    /// <param name="totalLines">    Specifies the maximum number of lines to include in the trace.
    /// Use 0 to include all lines. </param>
    /// <param name="callStackType"> Type of the call stack to return. </param>
    /// <returns> The full or user call stacks. </returns>
    public static string ParseCallStack( this StackTrace trace, int skipLines, int totalLines, CallStackType callStackType )
    {
        return trace is null ? string.Empty : trace.ToString().ParseCallStack( skipLines, totalLines, callStackType );
    }

    /// <summary>   Parses the stack trace and returns the user or full call stack. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="stackTrace">       Normally formatted Stack Trace. </param>
    /// <param name="skipLines">        Specifies the number of lines to skip. If getting the stack
    ///                                 trace from the <see cref="Environment.StackTrace">environment</see>,
    ///                                 the first two lines include the environment methods. </param>
    /// <param name="totalLines">       Specifies the maximum number of lines to include in the
    ///                                 trace. Use 0 to include all lines. </param>
    /// <param name="callStackType">    Type of the call stack to return. </param>
    /// <param name="prefix">           The prefix. </param>
    /// <param name="indent">           (Optional) The indent. </param>
    /// <returns>   The full or user call stacks. </returns>
    public static string ParseCallStack( this string stackTrace, int skipLines, int totalLines, CallStackType callStackType, string prefix, string indent = "\t" )
    {
        System.Text.StringBuilder userCallStackBuilder = new( 0xFFFF );
        System.Text.StringBuilder fullCallStackBuilder = new( 0xFFFF );
        if ( string.IsNullOrWhiteSpace( stackTrace ) )
        {
            return string.Empty;
        }

        string[] callStack = stackTrace.Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
        if ( callStack.Length <= 0 )
        {
            return string.Empty;
        }

        bool isFrameworkBlock = false;
        int lineNumber = 0;
        int lineCount = 0;
        foreach ( string line in callStack )
        {
            if ( totalLines == 0 || lineCount <= totalLines )
            {
                lineNumber += 1;
                if ( lineNumber > skipLines )
                {
                    string locationName = string.Empty;
                    string searchFor = " in ";
                    string stackLine = $"{indent}{prefix}{line.Replace( searchFor, $"\n{indent}{prefix}  {searchFor}" )}";
                    if ( IsFrameworkLine( line, ref locationName ) )
                    {
                        isFrameworkBlock = true;
                        _ = fullCallStackBuilder.AppendLine( stackLine );
                    }
                    else
                    {
                        if ( isFrameworkBlock && !string.IsNullOrWhiteSpace( locationName ) )
                        {
                            // if previously had external code, append
                            _ = userCallStackBuilder.AppendLine( $"{indent}{prefix}{locationName}" );
                        }

                        isFrameworkBlock = false;
                        _ = fullCallStackBuilder.AppendLine( stackLine );
                        _ = userCallStackBuilder.AppendLine( stackLine );
                        lineCount += 1;
                    }
                }
            }
        }

        return CallStackType.UserCallStack == callStackType ? userCallStackBuilder.ToString().TrimEnd( Environment.NewLine.ToCharArray() ) : fullCallStackBuilder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
    }

    /// <summary> The default prefix. </summary>
    private const string DEFAULT_PREFIX = "    s->";

    /// <summary> Parses the stack trace and returns the user or full call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackTrace">    Normally formatted Stack Trace. </param>
    /// <param name="skipLines">     Specifies the number of lines to skip. If getting the stack trace
    /// from the <see cref="Environment.StackTrace">environment</see>,
    /// the first two lines include the environment methods. </param>
    /// <param name="totalLines">    Specifies the maximum number of lines to include in the trace.
    /// Use 0 to include all lines. </param>
    /// <param name="callStackType"> Type of the call stack to return. </param>
    /// <returns> The full or user call stacks. </returns>
    public static string ParseCallStack( this string stackTrace, int skipLines, int totalLines, CallStackType callStackType )
    {
        return stackTrace.ParseCallStack( skipLines, totalLines, callStackType, DEFAULT_PREFIX );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackTrace"> The Stack Trace. </param>
    /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    /// environment methods. </param>
    /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    /// to include all lines. </param>
    /// <param name="prefix">     The prefix. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    public static string UserCallStack( this string stackTrace, int skipLines, int totalLines, string prefix )
    {
        return string.IsNullOrWhiteSpace( stackTrace ) ? string.Empty : stackTrace.ParseCallStack( skipLines, totalLines, CallStackType.UserCallStack, prefix );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackTrace"> The Stack Trace. </param>
    /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    /// environment methods. </param>
    /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    /// to include all lines. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    public static string UserCallStack( this string stackTrace, int skipLines, int totalLines )
    {
        return string.IsNullOrWhiteSpace( stackTrace ) ? string.Empty : stackTrace.ParseCallStack( skipLines, totalLines, CallStackType.UserCallStack, DEFAULT_PREFIX );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackTrace"> The Stack Trace. </param>
    /// <param name="prefix">     The prefix. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    /// <example>
    /// This example returns the entire user call stack skipping no lines.
    /// <code>
    /// exception.StackTrace.UserCallStack("1-&gt; ")
    /// </code>
    /// </example>
    public static string UserCallStack( this string stackTrace, string prefix )
    {
        return string.IsNullOrWhiteSpace( stackTrace ) ? string.Empty : stackTrace.ParseCallStack( 0, 0, CallStackType.UserCallStack, prefix );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="trace">  The Stack Trace. </param>
    /// <param name="prefix"> The prefix. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    /// <example>
    /// This example returns the entire user call stack skipping no lines.
    /// <code>
    /// Environment.StackTrace.UserCallStack("1-&gt; ")
    /// </code>
    /// </example>
    public static string UserCallStack( this StackTrace trace, string prefix )
    {
        return trace is null ? string.Empty : trace.ToString().ParseCallStack( 0, 0, CallStackType.UserCallStack, prefix );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="trace">     The Stack Trace. </param>
    /// <param name="skipLines"> Specifies the number of lines to skip. Two lines include the
    /// environment methods. </param>
    /// <param name="prefix">    The prefix. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    /// <example> This example returns the entire user call stack skipping no lines.
    /// <code>
    /// Environment.StackTrace.UserCallStack(0,0)
    /// </code></example>
    /// <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
    /// from the stack frame including file information (file name, line number and
    /// column number):
    /// <code>
    /// New StackFrame(Debugger.IsAttached).UserCallStack(3,10)
    /// </code></example>
    public static string UserCallStack( this StackTrace trace, int skipLines, string prefix )
    {
        return trace is null
            ? throw new ArgumentNullException( nameof( trace ) )
            : trace.ToString().ParseCallStack( skipLines, 0, CallStackType.UserCallStack, prefix );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="trace">      The Stack Trace. </param>
    /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    /// environment methods. </param>
    /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    /// to include all lines. </param>
    /// <param name="prefix">     The prefix. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    /// <example> This example returns the entire user call stack skipping no lines.
    /// <code>
    /// Environment.StackTrace.UserCallStack(0,0)
    /// </code></example>
    /// <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
    /// from the stack frame including file information (file name, line number and
    /// column number):
    /// <code>
    /// New StackFrame(Debugger.IsAttached).UserCallStack(3,10)
    /// </code></example>
    public static string UserCallStack( this StackTrace trace, int skipLines, int totalLines, string prefix )
    {
        return trace is null
            ? throw new ArgumentNullException( nameof( trace ) )
            : trace.ToString().ParseCallStack( skipLines, totalLines, CallStackType.UserCallStack, prefix );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="trace">      The Stack Trace. </param>
    /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    /// environment methods. </param>
    /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    /// to include all lines. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    /// <example> This example returns the entire user call stack skipping no lines.
    /// <code>
    /// Environment.StackTrace.UserCallStack(0,0)
    /// </code></example>
    /// <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
    /// from the stack frame including file information (file name, line number and
    /// column number):
    /// <code>
    /// New StackFrame(Debugger.IsAttached).UserCallStack(3,10)
    /// </code></example>
    public static string UserCallStack( this StackTrace trace, int skipLines, int totalLines )
    {
        return trace.ParseCallStack( skipLines, totalLines, CallStackType.UserCallStack );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackFrame"> The stack frame. </param>
    /// <param name="skipLines">  The number of lines to  skip. </param>
    /// <param name="prefix">     The prefix. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    /// <example> This example returns the entire user call stack skipping no lines.
    /// <code>
    /// New StackFrame().UserCallStack(0,0)
    /// </code></example>
    /// <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
    /// from the stack frame including file information (file name, line number and
    /// column number):
    /// <code>
    /// New StackFrame(Debugger.IsAttached).UserCallStack(3,10)
    /// </code></example>
    public static string UserCallStack( this StackFrame stackFrame, int skipLines, string prefix )
    {
        return stackFrame is null ? string.Empty : new StackTrace( true ).UserCallStack( skipLines, 0, prefix );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackFrame"> The stack frame. </param>
    /// <param name="skipLines">  The number of lines to  skip. </param>
    /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    /// to include all lines. </param>
    /// <param name="prefix">     The prefix. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    /// <example> This example returns the entire user call stack skipping no lines.
    /// <code>
    /// New StackFrame().UserCallStack(0,0)
    /// </code></example>
    /// <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
    /// from the stack frame including file information (file name, line number and
    /// column number):
    /// <code>
    /// New StackFrame(Debugger.IsAttached).UserCallStack(3,10)
    /// </code></example>
    public static string UserCallStack( this StackFrame stackFrame, int skipLines, int totalLines, string prefix )
    {
        return stackFrame is null ? string.Empty : new StackTrace( true ).UserCallStack( skipLines, totalLines, prefix );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackFrame"> The stack frame. </param>
    /// <param name="skipLines">  The number of lines to  skip. </param>
    /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    /// to include all lines. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    /// <example> This example returns the entire user call stack skipping no lines.
    /// <code>
    /// New StackFrame().UserCallStack(0,0)
    /// </code></example>
    /// <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
    /// from the stack frame including file information (file name, line number and
    /// column number):
    /// <code>
    /// New StackFrame(Debugger.IsAttached).UserCallStack(3,10)
    /// </code></example>
    public static string UserCallStack( this StackFrame stackFrame, int skipLines, int totalLines )
    {
        return stackFrame is null ? string.Empty : new StackTrace( true ).UserCallStack( skipLines, totalLines );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="exception">  A system exception. </param>
    /// <param name="skipLines">  The number of lines to  skip. </param>
    /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    /// to include all lines. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    /// <example> This example returns the entire user call stack skipping no lines.
    /// <code>
    /// ex.UserCallStack(0,0)
    /// </code></example>
    /// <example> This example returns the user call stack of up to 10 lines skipping the first 3 lines
    /// from the exception stack trace:
    /// <code>
    /// ex.UserCallStack(3,10)
    /// </code></example>
    public static string UserCallStack( this Exception exception, int skipLines, int totalLines )
    {
        return exception is null ? string.Empty : exception.StackTrace.ParseCallStack( skipLines, totalLines, CallStackType.UserCallStack );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="exception">  A system exception. </param>
    /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    /// environment methods. </param>
    /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    /// to include all lines. </param>
    /// <param name="prefix">     The prefix. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    public static string UserCallStack( this Exception exception, int skipLines, int totalLines, string prefix )
    {
        return exception is null ? string.Empty : exception.StackTrace.ParseCallStack( skipLines, totalLines, CallStackType.UserCallStack, prefix );
    }

    /// <summary> User call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="exception"> A system exception. </param>
    /// <returns> The user call stacks. </returns>
    public static string UserCallStack( this Exception exception )
    {
        return exception.UserCallStack( 0, 0 );
    }

    /// <summary> Parses and returns the user call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="exception"> A system exception. </param>
    /// <param name="prefix">    The prefix. </param>
    /// <returns> The user call stack, which includes no .NET Framework functions. </returns>
    public static string UserCallStack( this Exception exception, string prefix )
    {
        return exception.UserCallStack( 0, 0, prefix );
    }

    /// <summary> Parses and returns the full call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="trace">      The Stack Trace. </param>
    /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    /// environment methods. </param>
    /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    /// to include all lines. </param>
    /// <returns> The full call stack includes .NET Framework functions. </returns>
    public static string FullCallStack( this StackTrace trace, int skipLines, int totalLines )
    {
        return trace.ParseCallStack( skipLines, totalLines, CallStackType.FullCallStack );
    }

    /// <summary> Parses and returns the full call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackFrame"> The stack frame. </param>
    /// <param name="skipLines">  The number of lines to  skip. </param>
    /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    /// to include all lines. </param>
    /// <returns> The full call stack includes .NET Framework functions. </returns>
    public static string FullCallStack( this StackFrame stackFrame, int skipLines, int totalLines )
    {
        return stackFrame is null
            ? string.Empty
            : new StackTrace( true ).FullCallStack( skipLines, totalLines );
    }

    /// <summary> Parses and returns the full call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="exception">  A system exception. </param>
    /// <param name="skipLines">  The number of lines to  skip. </param>
    /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    /// to include all lines. </param>
    /// <returns> The full call stack includes .NET Framework functions. </returns>
    public static string FullCallStack( this Exception exception, int skipLines, int totalLines )
    {
        return exception is null
            ? string.Empty
            : exception.StackTrace.ParseCallStack( skipLines, totalLines, CallStackType.FullCallStack );
    }

    #endregion

    #region " use frame "

    /// <summary> Parses the stack trace into the user and full call stacks. </summary>
    /// <remarks> Starts with the first call as defined for the stack frame. </remarks>
    /// <param name="trace">         The Stack Trace. </param>
    /// <param name="frame">         The Stack Frame. </param>
    /// <param name="callStackType"> Type of the call stack to return. </param>
    /// <returns> The full or user call stacks. </returns>
    /// <example>
    /// This example returns the entire user call stack starting with the frame stack call and
    /// skipping all .NET Framework calls.
    /// <code>
    /// New StackTrace(True).ParseCallStack(New StackFrame(True), CallStackType.UserCallStack)
    /// </code>
    /// </example>
    public static string ParseCallStack( this StackTrace trace, StackFrame frame, CallStackType callStackType )
    {
        return trace is null
            ? string.Empty
            : trace.ToString().ParseCallStack( frame, callStackType );
    }

    /// <summary>   Parses the stack trace into the user and full call stacks. </summary>
    /// <remarks>   Starts with the first call as defined for the stack frame. </remarks>
    /// <param name="callStack">        The Stack Trace. </param>
    /// <param name="frame">            The Stack Frame. </param>
    /// <param name="callStackType">    Type of the call stack to return. </param>
    /// <param name="indent">           (Optional) The indent. </param>
    /// <returns>   The full or user call stacks. </returns>
    public static string ParseCallStack( this string callStack, StackFrame frame, CallStackType callStackType, string indent = "\t" )
    {
        if ( frame is null )
            return string.Empty;

        System.Text.StringBuilder userCallStackBuilder = new( 0xFFFF );
        System.Text.StringBuilder fullCallStackBuilder = new( 0xFFFF );
        string callFrame = new StackTrace( frame ).ToString();
        if ( string.IsNullOrWhiteSpace( callStack ) )
            return string.Empty;
        else if ( string.IsNullOrWhiteSpace( callFrame ) )
            return string.Empty;

        string[] callFrameLines = callFrame.Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
        if ( callFrameLines.Length <= 0 )
            return string.Empty;
        else
            callFrame = callFrameLines[0].Trim()[..callFrame.IndexOf( "in", StringComparison.Ordinal )];

        string[] callStackLines = callStack.Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
        if ( callStackLines.Length <= 0 )
            return string.Empty;

        bool isFrameworkBlock = false;
        bool isCallFrameBlock = false;
        foreach ( string line in callStackLines )
        {
            if ( !isCallFrameBlock )
                // looking for the call frame block
                isCallFrameBlock = line.TrimStart().StartsWith( callFrame, StringComparison.Ordinal );

            if ( isCallFrameBlock )
            {
                string locationName = string.Empty;
                if ( StackTraceExtensions.StackTraceExtensionMethods.IsFrameworkLine( line, ref locationName ) )
                {
                    // locationName = "   " & locationName
                    isFrameworkBlock = true;
                    _ = fullCallStackBuilder.AppendLine( $"{indent}{indent}{line}" );
                }
                else
                {
                    if ( isFrameworkBlock )
                    {
                        // if previously had external code, append
                        _ = userCallStackBuilder.AppendLine( $"{indent}{locationName}" );
                        isFrameworkBlock = false;
                    }

                    _ = fullCallStackBuilder.AppendLine( $"{indent}{line}" );
                    _ = userCallStackBuilder.AppendLine( $"{indent}{line}" );
                }
            }
        }

        return CallStackType.UserCallStack == callStackType
            ? userCallStackBuilder.ToString().TrimEnd( Environment.NewLine.ToCharArray() )
            : fullCallStackBuilder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
    }

    /// <summary> User call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackTrace"> Normally formatted Stack Trace. </param>
    /// <param name="stackFrame"> The stack frame. </param>
    /// <returns> The user call stacks. </returns>
    public static string UserCallStack( this StackTrace stackTrace, StackFrame stackFrame )
    {
        return stackTrace.ParseCallStack( stackFrame, CallStackType.UserCallStack );
    }

    /// <summary> User call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackFrame"> The stack frame. </param>
    /// <returns> The user call stacks. </returns>
    /// <example>
    /// This example returns the entire user call stack starting with the frame stack call and
    /// skipping all .NET Framework calls.
    /// <code>
    /// New StackFrame(True).UserCallStack()
    /// </code>
    /// </example>
    public static string UserCallStack( this StackFrame stackFrame )
    {
        return stackFrame is null
            ? string.Empty
            : new StackTrace( true ).UserCallStack( stackFrame );
    }

    /// <summary> User call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="exception">  A system exception. </param>
    /// <param name="stackFrame"> The stack frame. </param>
    /// <returns> The user call stacks. </returns>
    public static string UserCallStack( this Exception exception, StackFrame stackFrame )
    {
        return exception is null
            ? string.Empty
            : exception.StackTrace.ParseCallStack( stackFrame, CallStackType.UserCallStack );
    }

    /// <summary> Full call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackTrace"> Normally formatted Stack Trace. </param>
    /// <param name="stackFrame"> The stack frame. </param>
    /// <returns> The full call stacks. </returns>
    /// <example>
    /// This example returns the entire call stack starting with the frame stack call.
    /// <code>
    /// New StackFrame(True).FullCallStack()
    /// </code>
    /// </example>
    public static string FullCallStack( this StackTrace stackTrace, StackFrame stackFrame )
    {
        return stackTrace.ParseCallStack( stackFrame, CallStackType.FullCallStack );
    }

    /// <summary> Full call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackFrame"> The stack frame. </param>
    /// <returns> The full call stacks. </returns>
    public static string FullCallStack( this StackFrame stackFrame )
    {
        return stackFrame is null
            ? string.Empty
            : new StackTrace( true ).FullCallStack( stackFrame );
    }

    /// <summary> Full call stack. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="exception">  A system exception. </param>
    /// <param name="stackFrame"> The stack frame. </param>
    /// <returns> The full call stack. </returns>
    public static string FullCallStack( this Exception exception, StackFrame stackFrame )
    {
        return exception is null
            ? string.Empty
            : exception.StackTrace.ParseCallStack( stackFrame, CallStackType.FullCallStack );
    }

    /// <summary> Stack line. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="stackFrame"> The stack frame. </param>
    /// <returns> The call line for the stack frame. </returns>
    public static string StackLine( this StackFrame stackFrame )
    {
        return new StackTrace( stackFrame ).ToString().TrimEnd( Environment.NewLine.ToCharArray() );
    }

    #endregion
}
/// <summary> Values that represent CallStackType. </summary>
/// <remarks> David, 2020-09-15. </remarks>
public enum CallStackType
{
    /// <summary> No .NET Framework functions. </summary>
    UserCallStack,

    /// <summary> Includes .NET Framework functions. </summary>
    FullCallStack
}
