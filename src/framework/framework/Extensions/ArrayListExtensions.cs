using System.Collections;

namespace cc.isr.Std.ArrayListExtensions;

/// <summary> Includes extensions for <see cref="ArrayList">array lists</see>. </summary>
/// <remarks> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-11-07, 2.1.5425 </para></remarks>
public static class ArrayListExtensionMethods
{
    /// <summary> Concatenates. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="values">    The values. </param>
    /// <param name="delimiter"> The delimiter. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string Concatenate( this ArrayList values, string delimiter )
    {
        System.Text.StringBuilder builder = new();
        if ( values is not null )
        {
            foreach ( string v in values )
            {
                if ( builder.Length > 0 )
                {
                    _ = builder.Append( delimiter );
                }

                _ = builder.Append( v );
            }
        }

        return builder.ToString();
    }
}
