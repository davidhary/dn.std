namespace cc.isr.Std.SystemVersionExtensions;

/// <summary> Includes extensions for <see cref="Version">System Version</see> Information. </summary>
/// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2009-04-09, 1.1.3386 </para></remarks>
public static class SystemVersionExtensionMethods
{
    /// <summary> The reference date. </summary>
    private const string REFERENCE_DATE = "2000-01-01";

    /// <summary> Returns the reference date for the build number. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <returns> A Date. </returns>
    public static DateTime BuildNumberReferenceDate()
    {
        return DateTime.Parse( REFERENCE_DATE );
    }

    /// <summary> The build number of today. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <returns> The build number for today. </returns>
    public static int BuildNumber()
    {
        return ( int ) Math.Floor( DateTimeOffset.UtcNow.Subtract( BuildNumberReferenceDate() ).TotalDays );
    }

    /// <summary>
    /// Returns the date corresponding to the
    /// <see cref="Version.Build">build number</see>
    /// which is the day since the <see cref="REFERENCE_DATE">build number reference date</see> of
    /// January 1, 2000.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Specifies the version information. </param>
    /// <returns> A Date. </returns>
    public static DateTime BuildDate( this Version value )
    {
        return value is null ? BuildNumberReferenceDate() : BuildNumberReferenceDate().Add( TimeSpan.FromDays( value.Build ) );
    }

    /// <summary> Returns a standard version caption for forms. </summary>
    /// <remarks> Use this function to set the captions for forms. </remarks>
    /// <param name="value"> Specifies the version information. </param>
    /// <returns>
    /// A <see cref="string">String</see> data type in the form WW.XX.YYY.ZZZZß or
    /// WW.XX.YYY.ZZZZa.
    /// </returns>
    public static string Caption( this Version value )
    {
        if ( value is null )
        {
            return string.Empty;
        }
        // set the caption using the product name
        System.Text.StringBuilder builder = new();
        _ = builder.Append( value.ToString() );
        if ( value.Major < 1 )
        {
            _ = builder.Append( '.' );
            switch ( value.Minor )
            {
                case 0:
                    {
                        // builder.Append("Alpha") 
                        _ = builder.Append( Convert.ToChar( 0x3B1 ) );
                        break;
                    }

                case 1:
                    {
                        // builder.Append("Beta")  
                        _ = builder.Append( Convert.ToChar( 0x3B2 ) );
                        break;
                    }

                case var @case when @case is >= 2 and <= 8:
                    {
                        _ = builder.Append( $"RC{value.Minor - 1}" );
                        break;
                    }

                default:
                    {
                        _ = builder.Append( "Gold" );
                        break;
                    }
            }
        }

        return builder.ToString();
    }

    /// <summary>
    /// Compares the two versions returning 0 if they are equal, 1 if left version is greater (newer)
    /// than the right version or -1 if the left version is smaller (older) than the newer one.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="leftVersion">  Left hand side version. </param>
    /// <param name="rightVersion"> The right version. </param>
    /// <returns>
    /// Negative if 'leftVersion' is less than 'rightVersion', 0 if they are equal, or positive if it
    /// is greater.
    /// </returns>
    public static int Compare( this Version leftVersion, Version rightVersion )
    {
        return leftVersion.Compare( rightVersion, 4 );
    }

    /// <summary>
    /// Compares the two versions returning 0 if they are equal, 1 if left version is greater (newer)
    /// than the right version or -1 if the left version is smaller (older) than the newer one.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="leftVersion">          Left hand side version. </param>
    /// <param name="rightVersion">         The right version. </param>
    /// <param name="comparedElementCount"> The compared element count. Use 1 to compare major, 2 to
    /// include minor, 3 to include build and 4 to include
    /// revision. 1 is the minimum allowed. </param>
    /// <returns> System.Int32. </returns>
    public static int Compare( this Version leftVersion, Version rightVersion, int comparedElementCount )
    {
        if ( leftVersion is null )
        {
            throw new ArgumentNullException( nameof( leftVersion ) );
        }

        if ( rightVersion is null )
        {
            throw new ArgumentNullException( nameof( rightVersion ) );
        }

        int result = leftVersion.Major.CompareTo( rightVersion.Major );
        if ( result == 0 && comparedElementCount > 1 )
        {
            result = leftVersion.Minor.CompareTo( rightVersion.Minor );
            if ( result == 0 && comparedElementCount > 2 )
            {
                result = leftVersion.Build.CompareTo( rightVersion.Build );
                if ( result == 0 && comparedElementCount > 3 )
                {
                    result = leftVersion.Revision.CompareTo( rightVersion.Revision );
                }
            }
        }

        return result;
    }

    /// <summary>
    /// Compares the two versions returning 0 if they are equal, 1 if left version is greater (newer)
    /// than the right version or -1 if the left version is smaller (older) than the newer one.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="leftVersion">  Left hand side version. </param>
    /// <param name="rightVersion"> The right version. </param>
    /// <returns>
    /// Negative if 'leftVersion' is less than 'rightVersion', 0 if they are equal, or positive if it
    /// is greater.
    /// </returns>
    public static int Compare( this Version leftVersion, string rightVersion )
    {
        return leftVersion.Compare( rightVersion, 4 );
    }

    /// <summary>
    /// Compares the two versions returning 0 if they are equal, 1 if left version is greater (newer)
    /// than the right version or -1 if the left version is smaller (older) than the newer one.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="leftVersion">          Left hand side version. </param>
    /// <param name="rightVersion">         The right version. </param>
    /// <param name="comparedElementCount"> The compared element count. Use 1 to compare major, 2 to
    /// include minor, 3 to include build and 4 to include
    /// revision. 1 is the minimum allowed. </param>
    /// <returns>
    /// Negative if 'leftVersion' is less than 'rightVersion', 0 if they are equal, or positive if it
    /// is greater.
    /// </returns>
    public static int Compare( this Version leftVersion, string rightVersion, int comparedElementCount )
    {
        return leftVersion is null
            ? throw new ArgumentNullException( nameof( leftVersion ) )
            : string.IsNullOrWhiteSpace( rightVersion )
            ? throw new ArgumentNullException( nameof( rightVersion ) )
            : !rightVersion.HasValidVersionInfo()
            ? throw new ArgumentException( "Invalid version information", nameof( rightVersion ) )
            : leftVersion.Compare( Version.Parse( rightVersion ), comparedElementCount );
    }

    /// <summary> Determines if the given value has valid version info. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns>
    /// <c>true</c> if [has valid version info] [the specified value]; otherwise,
    /// <c>false</c>.
    /// </returns>
    public static bool HasValidVersionInfo( this string value )
    {
        _ = new Version();
        return Version.TryParse( value, out _ );
    }
}
