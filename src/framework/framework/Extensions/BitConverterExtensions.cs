using System.Text;

namespace cc.isr.Std.BitConverterExtensions;

/// <summary>   Bit converter and related methods. </summary>
/// <remarks>
/// 
/// In a big-endian system, the most significant value in the sequence is stored at the lowest
/// storage address (i.e., first). In a little-endian system, the least significant value in the
/// sequence is stored first. For example, consider the number 1025 (2 to the tenth power plus
/// one) stored in a 4-byte integer: <para>
/// 00000000 00000000 00000100 00000001. </para><para>
/// Address     Big-Endian  Little-Endian
/// 00          00000000    00000001     </para><para>
/// 01          00000000    00000100     </para><para>
/// 02          00000100    00000000     </para><para>
/// 03          00000001    00000000     </para>
/// </remarks>
public static class BitConverterExtensionMethods
{
    #region " bit converter convrsions: big endian "

    /// <summary> Converts the values to a unsigned short using Big Endian byte order. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    /// null. </exception>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <param name="values"> Source byte array. </param>
    /// <param name="offset"> Buffer offset. </param>
    /// <returns> The given data converted to an UInt16. </returns>
    [CLSCompliant( false )]
    public static ushort BigEndUnsignedShort( this byte[] values, int offset )
    {
        if ( values is null )
        {
            throw new ArgumentNullException( nameof( values ) );
        }

        int byteCount = 2;
        return values.Length < offset + byteCount
            ? throw new InvalidOperationException( $"{nameof( values )}({values.Length}) must have at list {byteCount} + {offset} elements" )
            : BitConverter.ToUInt16( values.Skip( offset ).Take( byteCount ).Reverse().ToArray(), 0 );
    }

    /// <summary> Converts the values to a single using Big Endian order. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    /// null. </exception>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <param name="values"> Source byte array. </param>
    /// <param name="offset"> Buffer offset. </param>
    /// <returns> The given data converted to a Single. </returns>
    public static float BigEndSingle( this byte[] values, int offset )
    {
        if ( values is null )
        {
            throw new ArgumentNullException( nameof( values ) );
        }

        int byteCount = 4;
        return values.Length < offset + byteCount
            ? throw new InvalidOperationException( $"{nameof( values )}({values.Length}) must have at list {byteCount} + {offset} elements" )
            : BitConverter.ToSingle( values.Skip( offset ).Take( byteCount ).Reverse().ToArray(), 0 );
        // Return ToSingle(values.ToUInt16(offset), values.ToUInt16(offset + 2))
    }

    /// <summary> Converts the values to a single using Big Endian order. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    /// null. </exception>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <param name="values"> Source byte array. </param>
    /// <param name="offset"> Buffer offset. </param>
    /// <returns> The given data converted to a Single. </returns>
    [CLSCompliant( false )]
    public static float BigEndSingle( this ushort[] values, int offset )
    {
        if ( values is null )
        {
            throw new ArgumentNullException( nameof( values ) );
        }

        int wordCount = 2;
        return values.Length < offset + wordCount
            ? throw new InvalidOperationException( $"{nameof( values )}({values.Length}) must have at list {wordCount} + {offset} elements" )
            : BitConverter.ToSingle( [.. BitConverter.GetBytes( values[0] ), .. BitConverter.GetBytes( values[1] )], 0 );
        // Return ToSingle(values.ToUInt16(offset), values.ToUInt16(offset + 2))
    }

    /// <summary> Converts the values to a single using Big Endian order. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="highOrderValue"> The high order value. </param>
    /// <param name="lowOrderValue">  The low order value. </param>
    /// <returns> The given data converted to a Single. </returns>
    [CLSCompliant( false )]
    public static float BigEndSingle( ushort highOrderValue, ushort lowOrderValue )
    {
        return BitConverter.ToSingle( [.. BitConverter.GetBytes( highOrderValue ), .. BitConverter.GetBytes( lowOrderValue )], 0 );// Return BitConverter.ToSingle(BitConverter.GetBytes(lowOrderValue).Concat(BitConverter.GetBytes(highOrderValue)).ToArray(), 0)
    }

    /// <summary> Converts a value to the bytes using Big Endian order. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Value to convert. </param>
    /// <returns> value as a Byte() </returns>
    [CLSCompliant( false )]
    public static byte[] BigEndBytes( this float value )
    {
        return BitConverter.GetBytes( value ).Reverse().ToArray();
    }

    /// <summary> Converts a value to the bytes using Big Endian order. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Value to convert. </param>
    /// <returns> value as a Byte() </returns>
    [CLSCompliant( false )]
    public static byte[] BigEndBytes( this ushort value )
    {
        return BitConverter.GetBytes( value ).Reverse().ToArray();
    }

    /// <summary> Little Endian bytes. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Value to convert. </param>
    /// <returns> A Byte() </returns>
    [CLSCompliant( false )]
    public static byte[] LittleEndBytes( this ushort value )
    {
        return [.. BitConverter.GetBytes( value )];
    }

    #endregion

    #region " direct convrsions: big endian "

    /// <summary>
    /// Return an array of bytes from an unsigned 16 bit integer using BIG ENDIAN codification.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> Value to convert. </param>
    /// <returns> Bytes array. </returns>
    [CLSCompliant( false )]
    public static byte[] BigEndInt8( this ushort value )
    {
        return [( byte ) (value >> 8), ( byte ) (value & 0xFF)];
    }

    /// <summary>
    /// Return a 16 bit unsigned integer from two bytes according to BIG ENDIAN codification.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="values"> Source byte array. </param>
    /// <param name="offset"> Buffer offset. </param>
    /// <returns> Integer returned. </returns>
    [CLSCompliant( false )]
    public static ushort BigEndUnsignedInt16( this byte[] values, int offset )
    {
        return values is null ? throw new ArgumentNullException( nameof( values ) ) : BigEndUnsignedInt16( values[offset], values[offset + 1] );
    }

    /// <summary>
    /// Return a 16 bit unsigned integer from two bytes according to BIG ENDIAN codification.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="high"> The high byte. </param>
    /// <param name="low">  The low byte. </param>
    /// <returns> Integer returned. </returns>
    [CLSCompliant( false )]
    public static ushort BigEndUnsignedInt16( byte high, byte low )
    {
        return ( ushort ) ((high << 8) | (low & 0xFF));
    }

    #endregion

    #region " bits convrsions "

    /// <summary> Return a byte from an 8-bit boolean array. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    /// null. </exception>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <param name="values"> Array of boolean 8 bit. </param>
    /// <param name="offset"> Array offset. </param>
    /// <returns> Byte returned. </returns>
    public static byte EightBitToByte( this bool[] values, int offset )
    {
        if ( values is null )
        {
            throw new ArgumentNullException( nameof( values ) );
        }

        if ( values.Length < 8 )
        {
            throw new InvalidOperationException( $"{nameof( values )}({values.Length}) must have at list 8 elements" );
        }

        if ( values.Length < offset + 7 )
        {
            throw new InvalidOperationException( $"{nameof( values )}({values.Length}) must have at list 7 + {offset} elements" );
        }

        byte ret = 0x0;
        for ( int ii = 0; ii <= 7; ii++ )
        {
            switch ( values[offset + ii] )
            {
                case true:
                    {
                        ret = ( byte ) (ret | (1 << ii));
                        break;
                    }

                case false:
                    {
                        ret = ( byte ) (ret & ~(1 << ii));
                        break;
                    }
            }
        }

        return ret;
    }

    #endregion

    #region " ascii convrsions "

    /// <summary> Return an array of bytes coded in ASCII according to Modbus specification. </summary>
    /// <remarks>
    /// codification : Byte = 0x5B Codified in two chars   : 0x35 = '5' and 0x42 = 'B' in ASCII The
    /// returned vector is exactly the double of the introduced one.
    /// </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="buffer"> Buffer to codify. </param>
    /// <returns> Buffer codified. </returns>
    public static byte[] ToAsciiBytes( this byte[] buffer )
    {
        if ( buffer is null )
        {
            throw new ArgumentNullException( nameof( buffer ) );
        }

        List<char> chars = [];
        int ii = 0;
        int jj = 0;
        while ( ii < buffer.Length * 2 )
        {
            char ch;
            byte val = ( byte ) (ii % 2 == 0 ? buffer[jj] >> 4 : buffer[jj] & 0xF);
            switch ( val )
            {
                case 0x1:
                    {
                        ch = '1';
                        break;
                    }

                case 0x2:
                    {
                        ch = '2';
                        break;
                    }

                case 0x3:
                    {
                        ch = '3';
                        break;
                    }

                case 0x4:
                    {
                        ch = '4';
                        break;
                    }

                case 0x5:
                    {
                        ch = '5';
                        break;
                    }

                case 0x6:
                    {
                        ch = '6';
                        break;
                    }

                case 0x7:
                    {
                        ch = '7';
                        break;
                    }

                case 0x8:
                    {
                        ch = '8';
                        break;
                    }

                case 0x9:
                    {
                        ch = '9';
                        break;
                    }

                case 0xA:
                    {
                        ch = 'A';
                        break;
                    }

                case 0xB:
                    {
                        ch = 'B';
                        break;
                    }

                case 0xC:
                    {
                        ch = 'C';
                        break;
                    }

                case 0xD:
                    {
                        ch = 'D';
                        break;
                    }

                case 0xE:
                    {
                        ch = 'E';
                        break;
                    }

                case 0xF:
                    {
                        ch = 'F';
                        break;
                    }

                default:
                    {
                        ch = '0';
                        break;
                    }
            }

            chars.Add( ch );
            if ( ii % 2 != 0 )
            {
                jj += 1;
            }

            ii += 1;
        }

        return Encoding.ASCII.GetBytes( [.. chars] );
    }

    /// <summary>
    /// Return a binary buffer from a byte array codified in ASCII according to Modbus specification.
    /// </summary>
    /// <remarks>
    /// codification : Char1 = 0x35 ('5') and Char2 = 0x42 ('B')
    /// Byte decoded : Byte = 0x5B The returned vector is exactly the half of the introduced one.
    /// </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="buffer"> ASCII codified buffer. </param>
    /// <returns> Binary buffer. </returns>
    public static byte[] FromAsciiBytes( this byte[] buffer )
    {
        if ( buffer is null )
        {
            throw new ArgumentNullException( nameof( buffer ) );
        }

        List<byte> ret = [];
        char[] chars = Encoding.ASCII.GetChars( buffer );
        byte bt = 0;
        for ( int ii = 0, loopTo = buffer.Length - 1; ii <= loopTo; ii++ )
        {
            byte tmp;
            switch ( chars[ii] )
            {
                case '1':
                    {
                        tmp = 0x1;
                        break;
                    }

                case '2':
                    {
                        tmp = 0x2;
                        break;
                    }

                case '3':
                    {
                        tmp = 0x3;
                        break;
                    }

                case '4':
                    {
                        tmp = 0x4;
                        break;
                    }

                case '5':
                    {
                        tmp = 0x5;
                        break;
                    }

                case '6':
                    {
                        tmp = 0x6;
                        break;
                    }

                case '7':
                    {
                        tmp = 0x7;
                        break;
                    }

                case '8':
                    {
                        tmp = 0x8;
                        break;
                    }

                case '9':
                    {
                        tmp = 0x9;
                        break;
                    }

                case 'A':
                    {
                        tmp = 0xA;
                        break;
                    }

                case 'B':
                    {
                        tmp = 0xB;
                        break;
                    }

                case 'C':
                    {
                        tmp = 0xC;
                        break;
                    }

                case 'D':
                    {
                        tmp = 0xD;
                        break;
                    }

                case 'E':
                    {
                        tmp = 0xE;
                        break;
                    }

                case 'F':
                    {
                        tmp = 0xF;
                        break;
                    }

                default:
                    {
                        tmp = 0x0;
                        break;
                    }
            }

            if ( ii % 2 != 0 )
            {
                bt = ( byte ) (bt | tmp);
                ret.Add( bt );
                bt = 0;
            }
            else
            {
                bt = ( byte ) (tmp << 4);
            }
        }

        return [.. ret];
    }

    #endregion
}
