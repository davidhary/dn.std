using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

namespace cc.isr.Std;

/// <summary>   Information about the assembly. </summary>
/// <remarks>   David, 2021-02-16. </remarks>
public class AssemblyInfo
{
    /// <summary>   Constructors. </summary>
    /// <remarks>   David, 2021-02-16. </remarks>
    public AssemblyInfo() : this( Assembly.GetExecutingAssembly() )
    { }

    ///<summary>   Constructor. </summary>
    /// <remarks>   David, 2021-02-16. </remarks>
    /// <param name="assembly"> The assembly. </param>
    public AssemblyInfo( Assembly assembly )
    {
        // Get values from the assembly.
        AssemblyTitleAttribute titleAttribute =
            GetAssemblyAttribute<AssemblyTitleAttribute>( assembly );
        if ( titleAttribute is not null ) this.Title = titleAttribute.Title;

        AssemblyDescriptionAttribute assemblyAttribute =
            GetAssemblyAttribute<AssemblyDescriptionAttribute>( assembly );
        if ( assemblyAttribute is not null ) this.Description = assemblyAttribute.Description;

        AssemblyCompanyAttribute companyAttribute =
            GetAssemblyAttribute<AssemblyCompanyAttribute>( assembly );
        if ( companyAttribute is not null ) this.Company = companyAttribute.Company;

        AssemblyProductAttribute productAttribute =
            GetAssemblyAttribute<AssemblyProductAttribute>( assembly );
        if ( productAttribute is not null ) this.Product = productAttribute.Product;

        AssemblyCopyrightAttribute copyrightAttribute =
            GetAssemblyAttribute<AssemblyCopyrightAttribute>( assembly );
        if ( copyrightAttribute is not null ) this.Copyright = copyrightAttribute.Copyright;

        AssemblyTrademarkAttribute trademarkAttribute =
            GetAssemblyAttribute<AssemblyTrademarkAttribute>( assembly );
        if ( trademarkAttribute is not null ) this.Trademark = trademarkAttribute.Trademark;

        this.AssemblyVersion = assembly.GetName().Version;

        AssemblyFileVersionAttribute fileVersionAttribute =
            GetAssemblyAttribute<AssemblyFileVersionAttribute>( assembly );
        if ( fileVersionAttribute is not null ) this.FileVersion = new Version( fileVersionAttribute.Version );

        GuidAttribute guidAttribute = GetAssemblyAttribute<GuidAttribute>( assembly );
        if ( guidAttribute is not null ) this.Guid = guidAttribute.Value;

        NeutralResourcesLanguageAttribute languageAttribute =
            GetAssemblyAttribute<NeutralResourcesLanguageAttribute>( assembly );
        if ( languageAttribute is not null ) this.NeutralLanguage = languageAttribute.CultureName;

        ComVisibleAttribute comAttribute =
            GetAssemblyAttribute<ComVisibleAttribute>( assembly );
        if ( comAttribute is not null ) this.IsComVisible = comAttribute.Value;

        this.ProcessName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;

        this.ProcessProductName = this.Product.StartsWith( this.ProcessName, StringComparison.Ordinal )
            ? this.Product
            : $"{this.ProcessName}.{this.Product}";

        this.FileVersionInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo( assembly.Location );

        this.Location = assembly.Location;

        this.DirectoryPath = new DirectoryInfo( this.Location ).FullName;
    }

    /// <summary>   Gets an assembly attribute. </summary>
    /// <remarks>   David, 2021-02-16. </remarks>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="assembly"> The assembly. </param>
    /// <returns>   The assembly attribute. </returns>
    public static T GetAssemblyAttribute<T>( Assembly assembly ) where T : Attribute
    {
        // Get attributes of this type.

        object[] attributes = assembly.GetCustomAttributes( typeof( T ), true );

        // If we didn't get anything, return default.

        if ( (attributes == null) || (attributes.Length == 0) ) return default!;

        // Convert the first attribute value into the desired type and return it.

        return ( T ) attributes[0];
    }

    /// <summary>   Gets the title. </summary>
    /// <value> The title. </value>
    public string Title { get; } = string.Empty;

    /// <summary>   Gets the description. </summary>
    /// <value> The description. </value>
    public string Description { get; } = string.Empty;

    /// <summary>   Gets the company. </summary>
    /// <value> The company. </value>
    public string Company { get; } = string.Empty;

    /// <summary>   Gets the product. </summary>
    /// <value> The product. </value>
    public string Product { get; } = string.Empty;

    /// <summary>   Gets the copyright. </summary>
    /// <value> The copyright. </value>
    public string Copyright { get; } = string.Empty;

    /// <summary>   Gets the trademark. </summary>
    /// <value> The trademark. </value>
    public string Trademark { get; } = string.Empty;

    /// <summary>   Gets the assembly version. </summary>
    /// <value> The assembly version. </value>
    public Version AssemblyVersion { get; } = new();

    /// <summary>   Gets the file version. </summary>
    /// <value> The file version. </value>
    public Version FileVersion { get; } = new();

    /// <summary>   Gets a unique identifier. </summary>
    /// <value> The identifier of the unique. </value>
    public string Guid { get; } = string.Empty;

    /// <summary>   Gets the neutral language. </summary>
    /// <value> The neutral language. </value>
    public string NeutralLanguage { get; } = string.Empty;

    /// <summary>   Gets a value indicating whether the com is visible. </summary>
    /// <value> True if the com is visible, false if not. </value>
    public bool IsComVisible { get; }

    /// <summary>   Gets the name of the process. </summary>
    /// <value> The name of the process. </value>
    public string ProcessName { get; } = string.Empty;

    /// <summary>   Gets the name of the process product. </summary>
    /// <value> The name of the process product. </value>
    public string ProcessProductName { get; } = string.Empty;

    /// <summary>   Gets information describing the file version. </summary>
    /// <value> Information describing the file version. </value>
    public System.Diagnostics.FileVersionInfo FileVersionInfo { get; }

    /// <summary>   Gets the location. </summary>
    /// <value> The location. </value>
    public string Location { get; } = string.Empty;

    /// <summary>   Gets the full pathname of the directory file. </summary>
    /// <value> The full pathname of the directory file. </value>
    public string DirectoryPath { get; } = string.Empty;

    /// <summary> Gets the application information caption with ISO 8601 UTC time, e.g., '2019-09-10 19:27:04Z'. </summary>
    /// <value> The UTC caption. </value>
    public string IsoUniversalTimeCaption => $"{this.ProcessProductName}.r.{this.AssemblyVersion.ToString( 4 )} {DateTimeOffset.Now:u)}";

    /// <summary> Gets the GMT caption, e.g., 'Tue, 10 May 2019 19:26:42 GMT'. </summary>
    /// <value> The GMT caption. </value>
    public string GmtCaption => $"{this.ProcessProductName}.r.{this.AssemblyVersion.ToString( 4 )} {DateTimeOffset.Now:r)}";

    /// <summary> Gets the application information caption with ISO complete time, e.g., '2019-09-10T12:12:29.7552627-07:00'. </summary>
    /// <value> The UTC caption. </value>
    public string IsoCompleteCaption => $"{this.ProcessProductName}.r.{this.AssemblyVersion.ToString( 4 )} {DateTimeOffset.Now:o)}";

    /// <summary> Gets the application ISO 8691 Short date and time caption, e.g., 2019-09-10T12:24:47-07:00. </summary>
    /// <value> The time caption. </value>
    public string IsoShortCaption => $"{this.ProcessProductName}.r.{this.AssemblyVersion.ToString( 4 )} {DateTimeOffset.Now:s)}{DateTimeOffset.Now:zzz)}";


}
