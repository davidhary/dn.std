using System.Diagnostics;
using System.Reflection;

namespace cc.isr.Std.AssemblyExtensions;
/// <summary>
/// Includes extensions for <see cref="Assembly">Assembly</see>
/// and <see cref="Assembly">assembly info</see>.
/// </summary>
/// <remarks>
/// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para>
/// </remarks>
[CLSCompliant( false )]
public static class AssemblyExtensionMethods
{
    #region " product process caption "

    /// <summary>
    /// An Assembly extension method that gets calling assembly by stack trace.
    /// </summary>
    /// <remarks>   2024-07-29. <para>
    /// <see href="https://www.codeproject.com/tips/791878/get-calling-assembly-from-stacktrace"/>
    /// </para></remarks>
    /// <param name="assembly"> Represents the executing assembly, which is a reusable, versionable, and self-
    ///                         describing building block of a common language runtime application. </param>
    /// <returns>   The calling assembly by stack trace. </returns>
    public static Assembly GetCallingAssemblyByStackTrace( this Assembly assembly )
    {
        StackTrace stackTrace = new();
        StackFrame[] frames = stackTrace.GetFrames();

        foreach ( StackFrame stackFrame in frames )
        {
            Assembly ownerAssembly = stackFrame.GetMethod().DeclaringType.Assembly;
            if ( ownerAssembly != assembly )
                return ownerAssembly;
        }
        return assembly;
    }


    #endregion

    #region " assembly property "

    /// <summary>   Returns the assembly public key. </summary>
    /// <remarks>
    /// <examples>
    /// <code>
    /// Dim pk As String = System.Reflection.Assembly.GetExecutingAssembly.GetPublicKey()
    /// </code></examples>
    /// </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="assembly"> Represents an assembly, which is a reusable, version-able, and self-
    ///                         describing building block of a common language runtime application. </param>
    /// <returns>   The public key of the assembly. </returns>
    public static string GetPublicKey( this Assembly assembly )
    {
        if ( assembly is null )
        {
            throw new ArgumentNullException( nameof( assembly ) );
        }

        byte[] values = assembly.GetName().GetPublicKey();
        return values.Aggregate( string.Empty, ( current, value ) => current + value.ToString( "X2", System.Globalization.CultureInfo.CurrentCulture ) );
    }

    /// <summary>   Gets public key token. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="assembly"> Represents an assembly, which is a reusable, version-able, and self-
    ///                         describing building block of a common language runtime application. </param>
    /// <returns>   The public key token of the assembly. </returns>
    public static string GetPublicKeyToken( this Assembly assembly )
    {
        if ( assembly is null )
        {
            throw new ArgumentNullException( nameof( assembly ) );
        }

        byte[] values = assembly.GetName().GetPublicKeyToken();
        return values.Aggregate( string.Empty, ( current, value ) => current + value.ToString( "X2", System.Globalization.CultureInfo.CurrentCulture ) );
    }

    /// <summary>   Gets the application <see cref="FileVersionInfo">File Version Info.</see> </summary>
    /// <remarks>
    /// Use this version information source for the application so that it won't be affected by any
    /// changes that might occur in the <see cref="AssemblyInfo">application info</see> code.
    /// </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="assembly"> Represents an assembly, which is a reusable, version-able, and self-
    ///                         describing building block of a common language runtime application. </param>
    /// <returns>   The application <see cref="FileVersionInfo">File Version Info</see>. </returns>
    public static FileVersionInfo GetFileVersionInfo( this Assembly assembly )
    {
        return assembly is null ? throw new ArgumentNullException( nameof( assembly ) ) : FileVersionInfo.GetVersionInfo( assembly.Location );
    }

    /// <summary>   Gets a unique identifier. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="assembly"> Represents an assembly, which is a reusable, version-able, and self-
    ///                         describing building block of a common language runtime application. </param>
    /// <returns>   The unique identifier. </returns>
    public static Guid GetGuid( this Assembly assembly )
    {
        if ( assembly is null )
        {
            throw new ArgumentNullException( nameof( assembly ) );
        }

        System.Runtime.InteropServices.GuidAttribute attribute = ( System.Runtime.InteropServices.GuidAttribute ) assembly.GetCustomAttributes( typeof( System.Runtime.InteropServices.GuidAttribute ), true )[0];
        return new Guid( attribute.Value );
    }

    /// <summary>   Directory path. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="assembly"> Represents an assembly, which is a reusable, version-able, and self-
    ///                         describing building block of a common language runtime application. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string DirectoryPath( this Assembly assembly )
    {
        return assembly is null ? throw new ArgumentNullException( nameof( assembly ) ) : System.IO.Path.GetDirectoryName( assembly.Location );
    }

    /// <summary>   The build number of the assembly. </summary>
    /// <remarks>   David, 2020-11-19. </remarks>
    /// <param name="assembly"> Represents an assembly, which is a reusable, version-able, and self-
    ///                         describing building block of a common language runtime application. </param>
    /// <returns>   The build number for today. </returns>
    public static int CalculateBuildNumber( this Assembly assembly )
    {
        FileInfo fi = new( assembly.Location );
        return CalculateBuildNumber( fi.LastWriteTime.ToUniversalTime() );
    }

    /// <summary>   The build number of the assembly. </summary>
    /// <remarks>   David, 2021-02-16. </remarks>
    /// <param name="date"> The date. </param>
    /// <returns>   The build number for today. </returns>
    public static int CalculateBuildNumber( DateTime date )
    {
        return ( int ) Math.Floor( date.Subtract( DateTime.Parse( "2000-01-01" ) ).TotalDays );
    }

    /// <summary>   The default version format. </summary>
    public const string DEFAULT_VERSION_FORMAT = "{0}.{1:00}.{2:0000}";

    /// <summary>   Gets a formatted assembly version. </summary>
    /// <remarks>
    /// <examples>
    /// <code>
    /// Dim version As String = My.Application.Info.ProductVersion("")
    /// </code></examples>
    /// </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="assembly"> Represents an assembly, which is a reusable, version-able, and self-
    ///                         describing building block of a common language runtime application. </param>
    /// <param name="format">   The version format. Set to empty to use the
    ///                         <see cref="DEFAULT_VERSION_FORMAT">default version format</see>. </param>
    /// <returns>   The application version. </returns>
    public static string AssemblyVersion( this Assembly assembly, string format )
    {
        if ( assembly is null )
        {
            throw new ArgumentNullException( nameof( assembly ) );
        }

        if ( string.IsNullOrWhiteSpace( format ) )
        {
            format = DEFAULT_VERSION_FORMAT;
        }
        Version version = assembly.GetName().Version;

        return string.Format( System.Globalization.CultureInfo.CurrentCulture, format, version.Major, version.Minor, version.Build );
    }

    #endregion

    #region " product process caption "

    /// <summary>   Prefix process name to the product name. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="assembly"> Represents an assembly, which is a reusable, version-able, and self-
    ///                         describing building block of a common language runtime application. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string PrefixProcessName( this Assembly assembly )
    {
        if ( assembly is null )
        {
            throw new ArgumentNullException( nameof( assembly ) );
        }

        string productName = assembly
            .GetCustomAttributes( typeof( AssemblyProductAttribute ) )
            .OfType<AssemblyProductAttribute>()
            .FirstOrDefault().Product;
        string processName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
        if ( !productName.StartsWith( processName, StringComparison.Ordinal ) )
        {
            productName = $"{processName}.{productName}";
        }
        return productName;
    }

    /// <summary>
    /// Builds time caption using <see cref="DateTimeOffset"/> showing local time and offset.
    /// </summary>
    /// <remarks>
    /// <list type="bullet">Use the following format options: <item>
    /// u - UTC - 2019-09-10 19:27:04Z</item><item>
    /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
    /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
    /// s - ISO - 2019-09-10T12:24:47</item><item>
    /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
    /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
    /// </remarks>
    /// <param name="timeCaptionFormat">    The time caption format. </param>
    /// <param name="kindFormat">           The kind format. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildLocalTimeCaption( string timeCaptionFormat, string kindFormat )
    {
        string result = string.IsNullOrWhiteSpace( timeCaptionFormat )
            ? $"{DateTimeOffset.Now}"
            : string.IsNullOrWhiteSpace( kindFormat )
                ? $"{DateTimeOffset.Now.ToString( timeCaptionFormat, System.Globalization.CultureInfo.CurrentCulture )}"
                : $"{DateTimeOffset.Now.ToString( timeCaptionFormat )}{DateTimeOffset.Now.ToString( kindFormat, System.Globalization.CultureInfo.CurrentCulture )}";
        return result;
    }

    /// <summary>   Builds product time caption. </summary>
    /// <remarks>
    /// <list type="bullet">Use the following format options: <item>
    /// u - UTC - 2019-09-10 19:27:04Z</item><item>
    /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
    /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
    /// s - ISO - 2019-09-10T12:24:47</item><item>
    /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
    /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
    /// </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="assembly">             Represents an assembly, which is a reusable, version-able,
    ///                                     and self-describing building block of a common language
    ///                                     runtime application. </param>
    /// <param name="versionElements">      The version elements. </param>
    /// <param name="timeCaptionFormat">    The time caption format. </param>
    /// <param name="kindFormat">           The kind format. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildProductTimeCaption( this Assembly assembly, int versionElements, string timeCaptionFormat, string kindFormat )
    {
        return assembly is null
            ? throw new ArgumentNullException( nameof( assembly ) )
            : $"{PrefixProcessName( assembly )}.r.{assembly.GetName().Version.ToString( versionElements )} {BuildLocalTimeCaption( timeCaptionFormat, kindFormat )}";
    }

    /// <summary>
    /// Builds product time caption using full version and local time plus kind format.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="assembly"> Represents an assembly, which is a reusable, version-able, and self-
    ///                         describing building block of a common language runtime application. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildProductTimeCaption( this Assembly assembly )
    {
        return assembly.BuildProductTimeCaption( 4, string.Empty, string.Empty );
    }

    #endregion

    #region " embedded resurces "

    /// <summary>   Builds full resource scriptName. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="assembly">     The assembly. </param>
    /// <param name="resourceName"> Name of the resource. </param>
    /// <returns>   The full resource scriptName, which starts with the Assembly scriptName. </returns>
    public static string BuildFullResourceName( this System.Reflection.Assembly assembly, string resourceName )
    {
        if ( string.IsNullOrWhiteSpace( resourceName ) ) return string.Empty;

        assembly ??= System.Reflection.Assembly.GetExecutingAssembly();

        string assemblyName = assembly.GetName().Name;
        return resourceName.StartsWith( assemblyName, StringComparison.OrdinalIgnoreCase ) ? resourceName : $"{assembly.GetName().Name}.{resourceName}";
    }

    /// <summary>   Builds full resource scriptName. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="resourceName"> Name of the resource. </param>
    /// <returns>   The full resource scriptName, which starts with the Assembly scriptName. </returns>
    public static string BuildFullResourceName( string resourceName )
    {
        return BuildFullResourceName( System.Reflection.Assembly.GetExecutingAssembly(), resourceName );
    }

    /// <summary>
    /// Reads text from an embedded resource file. Returns empty if not found.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="assembly">     The assembly. </param>
    /// <param name="resourceName"> Name of the resource. </param>
    /// <returns>   The embedded text resource. </returns>
    public static string ReadEmbeddedTextResource( this System.Reflection.Assembly assembly, string resourceName )
    {
        if ( assembly is null ) throw new ArgumentNullException( nameof( assembly ) );

        string contents = string.Empty;
        using ( Stream resourceStream = assembly.GetManifestResourceStream( BuildFullResourceName( assembly, resourceName ) ) )
        {
            if ( resourceStream is not null )
            {
                using StreamReader sr = new( resourceStream );
                contents = sr.ReadToEnd();
            }
        }

        return contents;
    }

    #endregion
}
