using cc.isr.Std.AssemblyExtensions;

namespace cc.isr.Std.Framework.Assembly.Tests;
/// <summary>
/// This is a test class for <see cref="Framework.AssemblyInfo"/> and <see cref="AssemblyExtensions"/> and is intended
/// to contain all MyAssemblyInfoTest Unit Tests
/// </summary>
[TestClass]
public class AssemblyInfoTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " assembly info tests "

    /// <summary>   (Unit Test Method) public key token should match. </summary>
    /// <remarks>   David, 2021-02-16. </remarks>
    [TestMethod()]
    public void PublicKeyTokenShouldMatch()
    {
        System.Reflection.Assembly target = typeof( AssemblyInfoTests ).Assembly;
        string expected = target.GetName().ToString().Split( ',' )[3];
        expected = expected[(expected.IndexOf( "=", StringComparison.OrdinalIgnoreCase ) + 1)..];
        string actual = target.GetPublicKeyToken();
        Assert.AreEqual( expected, actual, true, System.Globalization.CultureInfo.CurrentCulture );
    }

    /// <summary>   The build number of the assembly. </summary>
    /// <remarks>   David, 2021-08-21. </remarks>
    /// <param name="fileName"> Filename of the file. </param>
    /// <returns>   The build number for today. </returns>
    public static int BuildNumber( string fileName )
    {
        FileInfo fi = new( fileName );
        return ( int ) Math.Floor( fi.LastWriteTime.ToUniversalTime().Subtract( DateTime.Parse( "2000-01-01", System.Globalization.CultureInfo.CurrentCulture ) ).TotalDays );
    }

    /// <summary>   (Unit Test Method) file version build number should match. </summary>
    /// <remarks>   David, 2021-02-16. </remarks>
    [TestMethod()]
    public void FileVersionBuildNumberShouldMatch()
    {
        string fileName = "version.build.props";
        int expected = BuildNumber( fileName );
        System.Reflection.Assembly assembly = typeof( AssemblyInfoTests ).Assembly;
        FileVersionInfo fvi = FileVersionInfo.GetVersionInfo( assembly.Location );
        int actual = fvi.FileBuildPart;

        // allow one day difference because some projects set all files
        // to the same version event if the date skips as the version is set.
        Assert.AreEqual( expected, actual, 1 );
    }

    /// <summary>   (Unit Test Method) assembly version should match. </summary>
    /// <remarks>   David, 2021-02-16. </remarks>
    [TestMethod()]
    public void AssemblyVersionShouldMatch()
    {
        string fileName = "version.build.props";
        int expected = BuildNumber( fileName );
        System.Reflection.Assembly assembly = typeof( AssemblyInfoTests ).Assembly;
        AssemblyInfo target = new( assembly );
        int actual = target.AssemblyVersion.Build;
        // allow one day difference because some projects set all files
        // to the same version event if the date skips as the version is set.
        Assert.AreEqual( expected, actual, 1 );
    }

    #endregion
}
