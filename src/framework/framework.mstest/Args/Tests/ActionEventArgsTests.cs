namespace cc.isr.Std.Framework.Args.Tests;

/// <summary> Action Event Arguments tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-04-08 </para>
/// </remarks>
[TestClass]
public class ActionEventArgsTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " action event argments tests "

    /// <summary> (Unit Test Method) tests action event. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void ActionEventTest()
    {
        ActionEventArgs args = new( TraceEventType.Error );
        string message = "Information";
        TraceEventType eventType = TraceEventType.Information;
        args.RegisterOutcomeEvent( eventType, message );
        Assert.IsFalse( args.Failed, $"Action {args.Details} at {eventType} failed" );
        Assert.AreEqual( eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected" );
        Assert.IsTrue( args.HasDetails, $"Action {args.Details} at {eventType} reported not having details" );
        eventType = TraceEventType.Error;
        message = "Error";
        args.RegisterOutcomeEvent( eventType, message );
        Assert.IsTrue( args.Failed, $"Action {args.Details} at {eventType} did not failed" );
        Assert.AreEqual( eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected" );
        eventType = TraceEventType.Critical;
        message = "Critical";
        args.RegisterOutcomeEvent( eventType, message );
        Assert.IsTrue( args.Failed, $"Action {args.Details} at {eventType} did not failed" );
        Assert.AreEqual( eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected" );
    }

    /// <summary> (Unit Test Method) tests action event cancellation. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void ActionEventCancellationTest()
    {
        ActionEventArgs args = new( TraceEventType.Error );
        string message = "Cancel";
        args.RegisterFailure( message );
        Assert.IsTrue( args.Failed, $"Action {args.Details} failed to cancel" );
    }

    #endregion
}
