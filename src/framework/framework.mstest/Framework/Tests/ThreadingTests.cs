using cc.isr.Std.Framework.Tests;
using cc.isr.Std.Threading;

namespace cc.isr.Std.Framework.Framework.Tests;

/// <summary>   tests of threading functions. </summary>
/// <remarks>   David, 2021-02-12. </remarks>
[TestClass]
public class ThreadingTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        // assert reading of test settings from the configuration file.
        double expectedUpperLimit = 12d;
        Assert.IsTrue( Math.Abs( TestSiteSettings.Instance.TimeZoneOffset() ) < expectedUpperLimit,
                       $"{nameof( TestSiteSettings.Instance.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " wait for tests "

    private static int _pollInterval = 100;
    private static int _processDuration = 200;

    private static int ProcessMethod( CancellationToken cancellationToken )
    {
        _pollInterval = _processDuration / 10;
        Stopwatch sw = Stopwatch.StartNew();
        while ( sw.ElapsedMilliseconds < _processDuration )
        {
            Thread.Sleep( _pollInterval );
            // co-operative cancellation implies periodically check IsCancellationRequested 
            cancellationToken.ThrowIfCancellationRequested();
        }
        return 123; // the result 
    }

    /// <summary>   (Unit Test Method) process should timeout. </summary>
    /// <remarks>   David, 2021-10-27. </remarks>
    [TestMethod()]
    public void ProcessShouldTimeout()
    {
        _processDuration = 100;
        int expectedResult = 0;
        bool expectedOutcome = false;
        int msTimeout = _processDuration / 2;
        Stopwatch sw = Stopwatch.StartNew();
        bool isExecuted = WaitFor.TryCallWithTimeout(
           ProcessMethod,
           TimeSpan.FromMilliseconds( msTimeout ), // Timeout < Process Duration => ProcessMethod() gets Canceled
                                                   // Timeout > Process Duration => ProcessMethod() gets Executed
           out int result );
        Assert.AreEqual( expectedOutcome, isExecuted, $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_pollInterval}ms should time out (be canceled)" );
        Assert.AreEqual( expectedResult, result, $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_pollInterval}ms should time out (be canceled) with result of {expectedResult}" );
    }

    /// <summary>   (Unit Test Method) process should execute. </summary>
    /// <remarks>   David, 2021-10-27. </remarks>
    [TestMethod()]
    public void ProcessShouldExecute()
    {
        _processDuration = 100;
        int expectedResult = 123;
        bool expectedOutcome = true;
        int msTimeout = _processDuration * 2;
        Stopwatch sw = Stopwatch.StartNew();
        bool isExecuted = WaitFor.TryCallWithTimeout(
           ProcessMethod,
           TimeSpan.FromMilliseconds( msTimeout ), // Timeout < Process Duration => ProcessMethod() gets Canceled
                                                   // Timeout > Process Duration => ProcessMethod() gets Executed
           out int result );
        Assert.AreEqual( expectedOutcome, isExecuted, $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_pollInterval}ms should execute (not time out)" );
        Assert.AreEqual( expectedResult, result, $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_pollInterval}ms should execute (not time out) with results of {expectedResult}" );
    }

    [TestMethod()]
    public void WaitOneTests()
    {
        TimeSpan delayTime = TimeSpan.FromMilliseconds( 1 );
        TimeSpan actualTime = WaitFor.One( delayTime );
        Console.Out.Write( $"{delayTime.TotalMilliseconds}ms delayed for {actualTime.TotalMilliseconds}ms" );
    }

    #endregion
}
