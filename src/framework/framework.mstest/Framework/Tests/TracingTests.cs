using cc.isr.Std.Framework.Tests;

namespace cc.isr.Std.Framework.Framework.Tests;

/// <summary>   tests using tracing to assert hidden warnings and errors. </summary>
/// <remarks>   David, 2021-02-12. </remarks>
[TestClass]
public class TracingTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        // assert reading of test settings from the configuration file.
        double expectedUpperLimit = 12d;
        Assert.IsTrue( Math.Abs( TestSiteSettings.Instance.TimeZoneOffset() ) < expectedUpperLimit,
                       $"{nameof( TestSiteSettings.Instance.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " unpublished tracing tests  "

    /// <summary>   (Unit Test Method) trace listener should be empty. </summary>
    /// <remarks>   David, 2021-02-12. </remarks>
    [TestMethod()]
    public void TraceListenerShouldBeEmpty()
    {
        Assert.IsNotNull( TraceListener );
        Assert.IsTrue( TraceListener.Queue.IsEmpty );
    }

    /// <summary>   (Unit Test Method) trace listener should add warnings. </summary>
    /// <remarks>   David, 2021-02-12. </remarks>
    [TestMethod()]
    public void TraceListenerShouldAddWarnings()
    {
        Assert.IsNotNull( TraceListener );
        Trace.TraceWarning( $"tracing a warning message from {System.Reflection.MethodBase.GetCurrentMethod()?.Name}" );
        Assert.IsFalse( TraceListener.Queue.IsEmpty );
        TraceListener?.ClearQueue();
    }

    /// <summary>   (Unit Test Method) trace listener should not add information. </summary>
    /// <remarks>   David, 2021-02-12. </remarks>
    [TestMethod()]
    public void TraceListenerShouldNotAddInformation()
    {
        Assert.IsNotNull( TraceListener );
        Trace.TraceInformation( $"tracing an information message from {System.Reflection.MethodBase.GetCurrentMethod()?.Name}" );
        Assert.IsTrue( TraceListener.Queue.IsEmpty );
        TraceListener?.ClearQueue();
    }

    /// <summary>   (Unit Test Method) trace listener should add a 'write line' trace message. </summary>
    /// <remarks>   David, 2021-02-12. </remarks>
    [TestMethod()]
    public void TraceListenerShouldAddWriteLine()
    {
        Assert.IsNotNull( TraceListener );
        Trace.WriteLine( $"tracing an information message from {System.Reflection.MethodBase.GetCurrentMethod()?.Name}" );
        Assert.IsFalse( TraceListener.Queue.IsEmpty );
        TraceListener?.ClearQueue();
    }

    #endregion
}
