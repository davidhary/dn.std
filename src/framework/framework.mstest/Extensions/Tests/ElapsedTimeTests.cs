using cc.isr.Std.TimeSpanExtensions;

namespace cc.isr.Std.Framework.Extensions.Tests;

/// <summary> Tests for time span calculations. </summary>
/// <remarks>
/// <para>
/// David, 2018-03-14 </para>
/// </remarks>
[TestClass]
public class ElapsedTimeTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " elapsed time tests "

    /// <summary> Measure sleep time. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="sleepTime">       The sleep time. </param>
    /// <param name="firstStopWatch">  The first stop watch. </param>
    /// <param name="secondStopwatch"> The second stopwatch. </param>
    /// <returns>
    /// A (FirstStartTime As DateTimeOffset, FirstEndTime As DateTimeOffset, SecondStartTime As DateTimeOffset,
    /// SecondEndTime As DateTimeOffset)
    /// </returns>
    private static (DateTimeOffset FirstStartTime, DateTimeOffset FirstEndTime, DateTimeOffset SecondStartTime, DateTimeOffset SecondEndTime)
        MeasureSleepTime( TimeSpan sleepTime, Stopwatch firstStopWatch, Stopwatch secondStopwatch )
    {
        DateTimeOffset firstStartTime = DateTimeOffset.Now;
        DateTimeOffset secondStartTime = DateTimeOffset.Now;
        firstStopWatch.Restart();
        secondStopwatch.Restart();
        Thread.Sleep( sleepTime );
        secondStopwatch.Stop();
        firstStopWatch.Stop();
        DateTimeOffset secondEndTime = DateTimeOffset.Now;
        DateTimeOffset firstEndTime = DateTimeOffset.Now;
        return (firstStartTime, firstEndTime, secondStartTime, secondEndTime);
    }

    /// <summary> Measure do events time. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="doEventsCount">   Number of do events. </param>
    /// <param name="firstStopWatch">  The first stop watch. </param>
    /// <param name="secondStopwatch"> The second stopwatch. </param>
    /// <returns>
    /// A (FirstStartTime As DateTimeOffset, FirstEndTime As DateTimeOffset, SecondStartTime As DateTimeOffset,
    /// SecondEndTime As DateTimeOffset)
    /// </returns>
    private static (DateTimeOffset FirstStartTime, DateTimeOffset FirstEndTime, DateTimeOffset SecondStartTime, DateTimeOffset SecondEndTime)
        MeasureDoEventsTime( int doEventsCount, Stopwatch firstStopWatch, Stopwatch secondStopwatch )
    {
        DateTimeOffset firstStartTime = DateTimeOffset.Now;
        DateTimeOffset secondStartTime = DateTimeOffset.Now;
        firstStopWatch.Restart();
        secondStopwatch.Restart();
        if ( doEventsCount == 1 )
            TimeSpan.Zero .AsyncWait( );
        else
            for ( int i = 1, loopTo = doEventsCount; i <= loopTo; i++ )
                TimeSpan.Zero .AsyncWait( );

        secondStopwatch.Stop();
        firstStopWatch.Stop();
        DateTimeOffset secondEndTime = DateTimeOffset.Now;
        DateTimeOffset firstEndTime = DateTimeOffset.Now;
        return (firstStartTime, firstEndTime, secondStartTime, secondEndTime);
    }

    /// <summary> Measure spin wait time. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="spinWaitCount">   Number of spin waits. </param>
    /// <param name="firstStopWatch">  The first stop watch. </param>
    /// <param name="secondStopwatch"> The second stopwatch. </param>
    /// <returns>
    /// A (FirstStartTime As DateTimeOffset, FirstEndTime As DateTimeOffset, SecondStartTime As DateTimeOffset,
    /// SecondEndTime As DateTimeOffset)
    /// </returns>
    private static (DateTimeOffset FirstStartTime, DateTimeOffset FirstEndTime, DateTimeOffset SecondStartTime, DateTimeOffset SecondEndTime)
        MeasureSpinWaitTime( int spinWaitCount, Stopwatch firstStopWatch, Stopwatch secondStopwatch )
    {
        DateTimeOffset firstStartTime = DateTimeOffset.Now;
        DateTimeOffset secondStartTime = DateTimeOffset.Now;
        firstStopWatch.Restart();
        secondStopwatch.Restart();
        Thread.SpinWait( spinWaitCount );
        secondStopwatch.Stop();
        firstStopWatch.Stop();
        DateTimeOffset secondEndTime = DateTimeOffset.Now;
        DateTimeOffset firstEndTime = DateTimeOffset.Now;
        return (firstStartTime, firstEndTime, secondStartTime, secondEndTime);
    }

    /// <summary> Measure thread yield wait time. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="firstStopWatch">  The first stop watch. </param>
    /// <param name="secondStopwatch"> The second stopwatch. </param>
    /// <returns>
    /// A (FirstStartTime As DateTimeOffset, FirstEndTime As DateTimeOffset, SecondStartTime As DateTimeOffset,
    /// SecondEndTime As DateTimeOffset)
    /// </returns>
    private static (DateTimeOffset FirstStartTime, DateTimeOffset FirstEndTime, DateTimeOffset SecondStartTime, DateTimeOffset SecondEndTime)
        MeasureThreadYieldWaitTime( Stopwatch firstStopWatch, Stopwatch secondStopwatch )
    {
        DateTimeOffset firstStartTime = DateTimeOffset.Now;
        DateTimeOffset secondStartTime = DateTimeOffset.Now;
        firstStopWatch.Restart();
        secondStopwatch.Restart();
        _ = Thread.Yield();
        secondStopwatch.Stop();
        firstStopWatch.Stop();
        DateTimeOffset secondEndTime = DateTimeOffset.Now;
        DateTimeOffset firstEndTime = DateTimeOffset.Now;
        return (firstStartTime, firstEndTime, secondStartTime, secondEndTime);
    }

    /// <summary> Assert stopwatch measurements. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="name">            The name. </param>
    /// <param name="firstStopwatch">  The first stopwatch. </param>
    /// <param name="secondStopwatch"> The second stopwatch. </param>
    private static void AssertStopwatchMeasurements( string name, Stopwatch firstStopwatch, Stopwatch secondStopwatch )
    {
        Assert.IsTrue( firstStopwatch.ElapsedTicks > 0L, $"{name}: Ticks of second stopwatch {secondStopwatch.ElapsedTicks} should be greater than zero" );
        Assert.IsTrue( secondStopwatch.ElapsedTicks > 0L, $"{name}: Ticks of second stopwatch {secondStopwatch.ElapsedTicks} should be greater than zero" );
        Assert.AreEqual( firstStopwatch.ElapsedTicks, TimeSpan.FromTicks( firstStopwatch.ElapsedTicks ).Ticks, $"{name}: First stopwatch time span from ticks to ticks should match" );
        Assert.AreEqual( secondStopwatch.ElapsedTicks, TimeSpan.FromTicks( secondStopwatch.ElapsedTicks ).Ticks, $"{name}: stopwatch time span from ticks to ticks should match" );
        Assert.IsTrue( secondStopwatch.ElapsedTicks <= firstStopwatch.ElapsedTicks, $"{name}: Ticks of second stopwatch {secondStopwatch.ElapsedTicks} should be <= first stop watch {firstStopwatch.ElapsedTicks}" );
        Assert.AreEqual( firstStopwatch.ElapsedTicks, firstStopwatch.Elapsed.Ticks, $"{name}: First stopwatch elapsed ticks should match elapsed time" );
        Assert.AreEqual( secondStopwatch.ElapsedTicks, secondStopwatch.Elapsed.Ticks, $"{name}: Second stopwatch elapsed ticks should match elapsed time" );
        Assert.AreEqual( firstStopwatch.ElapsedTicks - secondStopwatch.ElapsedTicks, firstStopwatch.Elapsed.Subtract( secondStopwatch.Elapsed ).Ticks, $"{name}: Tick difference must match timespan difference in ticks" );
    }

    /// <summary> Asserts date time measurements. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="name">    The name. </param>
    /// <param name="outcome"> The outcome. </param>
    private static void AssertDateTimeMeasurements( string name, (DateTimeOffset FirstStartTime, DateTimeOffset FirstEndTime,
                                                                  DateTimeOffset SecondStartTime, DateTimeOffset SecondEndTime) outcome )
    {
        if ( string.IsNullOrEmpty( name ) )
            _ = outcome.FirstEndTime.AddTicks( 0L );
    }

    /// <summary> (Unit Test Method) tests date time resolution. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void DateTimeResolutionTest()
    {
        DateTimeOffset startTime = DateTimeOffset.Now;
        DateTimeOffset endTime = startTime.Add( TimeSpan.FromTicks( 1L ) );
        Assert.AreEqual( 1L, endTime.Ticks - startTime.Ticks, $"Adding one tick timespan should add one tick to the date time" );
        Assert.AreEqual( TimeSpan.FromTicks( 1L ), (1.0d / Stopwatch.Frequency).FromSeconds(), $"Time span tick resolution should match the stopwatch frequency {1.0d / Stopwatch.Frequency}" );
    }

    /// <summary> (Unit Test Method) tests measure elapsed time. </summary>
    /// <remarks>
    /// This measurement asserts only the stop watch measurements because of the following:<para>
    /// https://StackOverflow.com/questions/307582/how-frequent-is-datetime-now-updated-or-is-there-a-more-precise-api-to-get-the
    /// </para><para>
    /// The problem with DateTime when dealing with milliseconds isn't due to the DateTime class at
    /// all, but rather, has to do with CPU ticks and thread slices. Essentially, when an operation
    /// is paused by the scheduler to allow other threads to execute, it must wait at a minimum of 1
    /// time slice before resuming which is around 15ms on modern Windows OSes. Therefore, any
    /// attempt to pause for less than this 15ms precision will lead to unexpected results.</para>
    /// </remarks>
    [TestMethod()]
    public void MeasureElapsedTimeTest()
    {
        Stopwatch firstStopwatch = new();
        Stopwatch secondStopwatch = new();
        (DateTimeOffset FirstStartTime, DateTimeOffset FirstEndTime, DateTimeOffset SecondStartTime, DateTimeOffset SecondEndTime) outcome;
        TimeSpan sleepTimespan = TimeSpan.FromMilliseconds( 1d );

        outcome = MeasureSleepTime( sleepTimespan, firstStopwatch, secondStopwatch );
        AssertStopwatchMeasurements( $"Sleep {sleepTimespan.ToMilliseconds()}", firstStopwatch, secondStopwatch );
        AssertDateTimeMeasurements( $"Sleep {sleepTimespan.ToMilliseconds()}", outcome );
        int doEventsCount = 1;
        _ = MeasureDoEventsTime( doEventsCount, firstStopwatch, secondStopwatch );
        // Do Events 1 first stopwatch elapsed ticks 723877
        // Do Events 1 second stopwatch elapsed ticks 723876
        Trace.TraceInformation( $"Do Events {doEventsCount} first stopwatch elapsed ticks {firstStopwatch.ElapsedTicks}" );
        Trace.TraceInformation( $"Do Events {doEventsCount} second stopwatch elapsed ticks {secondStopwatch.ElapsedTicks}" );

        outcome = MeasureDoEventsTime( doEventsCount, firstStopwatch, secondStopwatch );
        // Do Events 1 first stopwatch elapsed ticks 2621
        // Do Events 1 first stopwatch elapsed ticks 2621
        // Do Events 1 second stopwatch elapsed ticks 2619
        Trace.TraceInformation( $"Do Events {doEventsCount} first stopwatch elapsed ticks {firstStopwatch.ElapsedTicks}" );
        Trace.TraceInformation( $"Do Events {doEventsCount} second stopwatch elapsed ticks {secondStopwatch.ElapsedTicks}" );
        AssertStopwatchMeasurements( $"Do Events {doEventsCount}", firstStopwatch, secondStopwatch );
        AssertDateTimeMeasurements( $"Do Events {doEventsCount}", outcome );

        outcome = MeasureThreadYieldWaitTime( firstStopwatch, secondStopwatch );
        AssertStopwatchMeasurements( $"Thread Yield", firstStopwatch, secondStopwatch );
        // Thread Yield first stopwatch elapsed ticks 57, 32
        // Thread Yield second stopwatch elapsed ticks 55, 31
        Trace.TraceInformation( $"Thread Yield first stopwatch elapsed ticks {firstStopwatch.ElapsedTicks}" );
        Trace.TraceInformation( $"Thread Yield second stopwatch elapsed ticks {secondStopwatch.ElapsedTicks}" );
        AssertDateTimeMeasurements( $"Thread Yield", outcome );
        int spinWaitCount = 10;

        outcome = MeasureSpinWaitTime( spinWaitCount, firstStopwatch, secondStopwatch );
        AssertStopwatchMeasurements( $"Spin wait {spinWaitCount}", firstStopwatch, secondStopwatch );
        // Spin wait 10 first stopwatch elapsed ticks 7 = 0.7 micro seconds
        // Spin wait 10 second stopwatch elapsed ticks 6
        Trace.TraceInformation( $"Spin wait {spinWaitCount} first stopwatch elapsed ticks {firstStopwatch.ElapsedTicks}" );
        Trace.TraceInformation( $"Spin wait {spinWaitCount} second stopwatch elapsed ticks {secondStopwatch.ElapsedTicks}" );
        AssertDateTimeMeasurements( $"Spin wait {spinWaitCount}", outcome );
    }

    #endregion
}
