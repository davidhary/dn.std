namespace cc.isr.Std.Framework.Extensions.Tests;
/// <summary>
/// Exception methods for adding exception data and building a detailed exception message.
/// </summary>
public static class ExceptionExtensionMethods
{
    /// <summary> Adds an exception data. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="exception"> The exception. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    public static bool AddExceptionDataThis( Exception exception )
    {
        return ExceptionExtensions.ExceptionDataMethods.AddExceptionData( exception );
    }
}
