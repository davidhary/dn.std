namespace cc.isr.Std.Framework.Extensions.Tests;

/// <summary> Combinations tests. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-02-18. </para>
/// </remarks>
[TestClass]
public sealed class CombinationsTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " combination test "

    /// <summary> Builds the given values. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="values"> The values. </param>
    /// <returns> A <see cref="string" />. </returns>
    private static string Build<T>( List<T> values )
    {
        System.Text.StringBuilder builder = new();
        foreach ( T i in values )
            _ = builder.Append( System.Globalization.CultureInfo.CurrentCulture, $"{i,3}" );

        return builder.ToString();
    }

    /// <summary> (Unit Test Method) tests combination. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void CombinationTest()
    {
        List<List<int>> collectionOfSeries = [new( [1, 2, 3, 4, 5] ), new( [0, 1] ), new( [6, 3] ), new( [1, 3, 5] )];
        List<List<int>> combinations = CombinationsExtensions.CombinationsExtensionMethods.GenerateCombinations( collectionOfSeries );
        List<int> expectedList = new( [1, 0, 6, 1] );
        List<int> actualList = combinations.First();
        Assert.IsTrue( expectedList.SequenceEqual( actualList ) );
        expectedList = new List<int>( [1, 0, 6, 3] );
        actualList = combinations[1];
        Assert.IsTrue( expectedList.SequenceEqual( actualList ), $"actual: [{Build( actualList )}] <> expected: [{Build( actualList )}]" );
        expectedList = new List<int>( [5, 1, 3, 5] );
        actualList = combinations.Last();
        Assert.IsTrue( expectedList.SequenceEqual( actualList ), $"actual: [{Build( actualList )}] <> expected: [{Build( actualList )}]" );
    }

    #endregion
}
