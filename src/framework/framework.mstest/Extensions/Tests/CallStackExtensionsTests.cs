using cc.isr.Std.StackTraceExtensions;

namespace cc.isr.Std.Framework.Extensions.Tests;
/// <summary>
/// This is a test class for ExtensionsTest and is intended to contain all ExtensionsTest Unit
/// Tests.
/// </summary>
/// <remarks> David, 2020-09-18. </remarks>
[TestClass]
public class CallStackExtensionsTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " call stack extension tests "

    /// <summary> A test for ParseCallStack for user. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void ParseUserCallStackFrameTest()
    {
        StackTrace trace = new( true );
        StackFrame frame = new( true );
        CallStackType callStackType = CallStackType.UserCallStack;
        string expected = "Tests.ParseUserCallStackFrameTest() in ";
        string actual = trace.ParseCallStack( frame, callStackType ).Trim();
        Assert.IsTrue( actual.Contains( expected ), $@"Actual trace 
{actual}
contains 
{expected}" );
    }

    /// <summary> A test for ParseCallStack for user. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void ParseFullCallStackFrameTest()
    {
        StackTrace trace = new( true );
        StackFrame frame = new( true );
        CallStackType callStackType = CallStackType.FullCallStack;
        string expected = string.Empty;
        string actual;
        actual = trace.ParseCallStack( frame, callStackType );
        Assert.AreNotEqual( expected, actual );
    }

    /// <summary> A test for ParseCallStack. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void ParseUserCallStackTest()
    {
        StackTrace trace = new( true );
        int skipLines = 0;
        int totalLines = 0;
        CallStackType callStackType = CallStackType.UserCallStack;
        string expected = "s->   at cc.isr.";
        string actual = trace.ParseCallStack( skipLines, totalLines, callStackType ).Trim();
        Assert.IsTrue( actual.StartsWith( expected, StringComparison.OrdinalIgnoreCase ), $"Trace results Actual: {actual[..expected.Length]} Starts with {expected}" );
    }

    /// <summary> A test for ParseCallStack. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void ParseFullCallStackTest()
    {
        StackTrace trace = new( true );
        int skipLines = 0;
        int totalLines = 0;
        CallStackType callStackType = CallStackType.FullCallStack;
        string expected = string.Empty;
        string actual;
        actual = trace.ParseCallStack( skipLines, totalLines, callStackType );
        Assert.AreNotEqual( expected, actual );
    }
    #endregion
}
