using cc.isr.Std.TimeSpanExtensions;
using cc.isr.Std.DateTimeExtensions;
using cc.isr.Std.Framework.Tests;

namespace cc.isr.Std.Framework.Extensions.Tests;

/// <summary> Time Span extensions tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-03-14 </para>
/// </remarks>
[TestClass]
public class TimeSpanExtensionsTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " wait "

    /// <summary>   (Unit Test Method) time span wait should delay. </summary>
    /// <remarks>   David, 2021-01-30. <para> 
    /// Yield = False </para><para>
    /// StartWaitTask(1ms)   waits: 00:00:00.0010772ms </para><para>
    /// StartWaitTask(2ms)   waits: 00:00:00.0020738ms </para><para>
    /// StartWaitTask(5ms)   waits: 00:00:00.0050905ms </para><para>
    /// StartWaitTask(10ms)  waits: 00:00:00.0101497ms </para><para>
    /// StartWaitTask(20ms)  waits: 00:00:00.0201509ms </para><para>
    /// StartWaitTask(50ms)  waits: 00:00:00.0501130ms </para><para>
    /// StartWaitTask(100ms) waits: 00:00:00.1001314ms </para><para>
    /// Yield = True </para><para>
    /// StartWaitTask(1ms)   waits: 00:00:00.0010824ms </para><para>
    /// StartWaitTask(2ms)   waits: 00:00:00.0020599ms </para><para>
    /// StartWaitTask(5ms)   waits: 00:00:00.0050710ms </para><para>
    /// StartWaitTask(10ms)  waits: 00:00:00.0100598ms </para><para>
    /// StartWaitTask(20ms)  waits: 00:00:00.0200864ms </para><para>
    /// StartWaitTask(50ms)  waits: 00:00:00.0500930ms </para><para>
    /// StartWaitTask(100ms) waits: 00:00:00.1000608ms </para><para>
    /// </remarks>
    [TestMethod()]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>" )]
    public void TimeSpanWaitShouldDelay()
    {
        System.Text.StringBuilder builder = new();
        TimeSpan expectedTimespan;
        TimeSpan maximumError;
        TimeSpan waitTimespan;
        TimeSpan actualTimespan;
        double[] testIntervals = [1, 5, 10, 50];

        bool yield = true;
        // initialize
        TimeSpan.FromMilliseconds( 1 ).AsyncWait( yield );

        Stopwatch sw = Stopwatch.StartNew();
        foreach ( double testInterval in testIntervals )
        {
            waitTimespan = TimeSpan.FromMilliseconds( testInterval );
            maximumError = TimeSpan.FromMilliseconds( 0.0003 );
            expectedTimespan = waitTimespan;
            sw.Restart();
            waitTimespan.AsyncWait( yield );
            sw.Stop();
            actualTimespan = sw.Elapsed;
            _ = builder.AppendLine( $"StartWaitTask({testInterval}ms) waits: {actualTimespan}ms" );
            // Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, maximumError, $"Start Wait Task time interval should equal within error {maximumError}" );

        }
        Trace.TraceInformation( builder.ToString() );
    }

    /// <summary>   (Unit Test Method) time span time span wait should delay. </summary>
    /// <remarks>   David, 2021-01-30. <para> 
    /// Yield = False
    /// StartWaitTask(1ms)   waits: 00:00:00.0010939ms internal 00:00:00.0010000ms
    /// StartWaitTask(2ms)   waits: 00:00:00.0020608ms internal 00:00:00.0020000ms
    /// StartWaitTask(5ms)   waits: 00:00:00.0050608ms internal 00:00:00.0050000ms
    /// StartWaitTask(10ms)  waits: 00:00:00.0100781ms internal 00:00:00.0100000ms
    /// StartWaitTask(20ms)  waits: 00:00:00.0201004ms internal 00:00:00.0200000ms
    /// StartWaitTask(50ms)  waits: 00:00:00.0501085ms internal 00:00:00.0500000ms
    /// StartWaitTask(100ms) waits: 00:00:00.1002178ms internal 00:00:00.1000000ms
    /// Yield = True
    /// StartWaitTask(1ms)   waits: 00:00:00.0011354ms internal 00:00:00.0010000ms </para><para>
    /// StartWaitTask(2ms)   waits: 00:00:00.0020714ms internal 00:00:00.0020000ms </para><para>
    /// StartWaitTask(5ms)   waits: 00:00:00.0051486ms internal 00:00:00.0050000ms </para><para>
    /// StartWaitTask(10ms)  waits: 00:00:00.0100803ms internal 00:00:00.0100000ms </para><para>
    /// StartWaitTask(20ms)  waits: 00:00:00.0201377ms internal 00:00:00.0200000ms </para><para>
    /// StartWaitTask(50ms)  waits: 00:00:00.0501807ms internal 00:00:00.0500000ms </para><para>
    /// StartWaitTask(100ms) waits: 00:00:00.1000946ms internal 00:00:00.1000000ms </para><para>
    ///  </para></remarks>
    [TestMethod()]
    public void TimeSpanTimeSpanWaitShouldDelay()
    {
        System.Text.StringBuilder builder = new();
        TimeSpan expectedTimespan;
        TimeSpan maximumError;
        TimeSpan waitTimespan;
        TimeSpan actualTimespan;
        TimeSpan internalTimespan;
        double[] testIntervals = [1, 5, 10, 50];

        bool yield = true;

        // initialize
        _ = TimeSpan.FromMilliseconds( 1 ).SyncWait( yield );
        _ = TimeSpan.FromMilliseconds( 1 ).AsyncWaitElapsed( yield );

        Stopwatch sw = Stopwatch.StartNew();
        foreach ( double testInterval in testIntervals )
        {
            waitTimespan = TimeSpan.FromMilliseconds( testInterval );
            maximumError = TimeSpan.FromMilliseconds( 0.0003 );
            expectedTimespan = waitTimespan;
            sw.Restart();
            internalTimespan = waitTimespan.AsyncWaitElapsed( yield );
            sw.Stop();
            actualTimespan = sw.Elapsed;
            _ = builder.AppendLine( System.Globalization.CultureInfo.CurrentCulture,
                $"StartWaitTask({testInterval}ms) waits: {actualTimespan}ms internal {internalTimespan}ms" );
            // Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, maximumError, $"Start Wait Task time interval should equal within error {maximumError}" );

        }
        Trace.TraceInformation( builder.ToString() );
    }

    #endregion

    #region " precise date time "

    /// <summary>   (Unit Test Method) tests date time resolution. </summary>
    /// <remarks>   David, 2020-12-07. <para>
    /// .NET 5.0 elevated <see cref="DateTime.Now.Ticks"/> resolution to the <see cref="Stopwatch"/> resolution. </para> </remarks>
    [TestMethod()]
    public void DateTimeResolutionTest()
    {
        double millisecondsPerTick = 1000.0d / Stopwatch.Frequency;
        double expectedMillisecondResolution = millisecondsPerTick;
        double resolutionEpsilon = 2 * expectedMillisecondResolution;
        string output = "";
        double millisecondResolution;
        double minimumMillisecondResolution = double.MaxValue;
        long ticks;
        long newTicks;
        int count = 20;
        for ( int ctr = 0; ctr <= count; ctr++ )
        {
            output += string.Format( System.Globalization.CultureInfo.CurrentCulture, $"{DateTime.Now.Ticks}\n" );

            // Introduce a delay loop.
            ticks = DateTime.UtcNow.Ticks;
            do
newTicks = DateTime.UtcNow.Ticks;             while ( ticks == newTicks );
            millisecondResolution = millisecondsPerTick * (newTicks - ticks);
            if ( minimumMillisecondResolution > millisecondResolution )
                minimumMillisecondResolution = millisecondResolution;
            if ( minimumMillisecondResolution == expectedMillisecondResolution )
                break;
            else
                output += string.Format( System.Globalization.CultureInfo.CurrentCulture, $"Millisecond resolution = {millisecondResolution}\n" );

            if ( ctr == 10 )
            {
                output += "Thread.Sleep called...\n";
                Thread.Sleep( 5 );
            }

        }
        output += string.Format( System.Globalization.CultureInfo.CurrentCulture, $"Minimum resolution = {minimumMillisecondResolution}\n" );
        Trace.TraceInformation( output );
        Assert.AreEqual( expectedMillisecondResolution, minimumMillisecondResolution, resolutionEpsilon, $"Date time resolution {minimumMillisecondResolution} ms should match within {resolutionEpsilon} ms" );
    }

    /// <summary>   (Unit Test Method) tests date time offset file time resolution. </summary>
    /// <remarks>   David, 2020-12-07. <para>
    /// .NET 5.0 elevated <see cref="DateTimeOffset.UtcNow.ToFileTime()"/> to twice the <see cref="Stopwatch"/> resolution. </para> </remarks>
    [TestMethod()]
    public void DateTimeOffsetFileTimeResolutionTest()
    {
        double millisecondsPerTick = 1000.0d / Stopwatch.Frequency;
        double expectedMillisecondResolution = 2 * millisecondsPerTick;
        double resolutionEpsilon = 0.5 * expectedMillisecondResolution;
        string output = "";
        double actualMillisecondResolution;
        double minimumMillisecondResolution = double.MaxValue;
        long ticks;
        long newTicks;
        int count = 20;
        for ( int ctr = 0; ctr <= count; ctr++ )
        {
            output += string.Format( System.Globalization.CultureInfo.CurrentCulture, $"{DateTimeOffset.UtcNow.ToFileTime()}\n" );

            // Introduce a delay loop.
            ticks = DateTimeOffset.UtcNow.ToFileTime();
            do
newTicks = DateTimeOffset.UtcNow.ToFileTime();             while ( ticks == newTicks );
            actualMillisecondResolution = millisecondsPerTick * (newTicks - ticks);
            if ( minimumMillisecondResolution > actualMillisecondResolution )
                minimumMillisecondResolution = actualMillisecondResolution;
            if ( minimumMillisecondResolution == expectedMillisecondResolution )
                break;
            else
                output += string.Format( System.Globalization.CultureInfo.CurrentCulture, $"Millisecond resolution = {actualMillisecondResolution}\n" );

            if ( ctr == 10 )
            {
                output += "Thread.Sleep called...\n";
                Thread.Sleep( 5 );
            }

        }
        output += string.Format( System.Globalization.CultureInfo.CurrentCulture, $"Minimum resolution = {minimumMillisecondResolution}\n" );
        Trace.TraceInformation( output );
        Assert.AreEqual( expectedMillisecondResolution, minimumMillisecondResolution, resolutionEpsilon, $"Date time resolution {minimumMillisecondResolution} ms should match within {resolutionEpsilon} ms" );
    }

    /// <summary>   (Unit Test Method) time span from milliseconds is accurate. </summary>
    /// <remarks>   David, 2021-01-30. <para>
    /// .NET 5.0 fixed time span inaccuracies. </para></remarks>
    [TestMethod()]
    public void TimeSpanFromMillisecondsIsAccurate()
    {
        double milliseconds = 0.001;
        TimeSpan expectedTimespan = TimeSpan.FromMilliseconds( milliseconds );
        TimeSpan actualTimespan = TimeSpan.Zero.AddMicroseconds( 1000 * milliseconds );
        // the C# extensions do not add fractions of milliseconds
        Assert.AreEqual( expectedTimespan, actualTimespan, $"{actualTimespan:s\\.fffffff} should equal {expectedTimespan:s\\.fffffff}" );
    }

    [TestMethod()]
    public void DateTimeAddingMicroSecondsIsAccurate()
    {
        double milliseconds = 0.001;
        TimeSpan expectedTimespan = TimeSpan.FromMilliseconds( milliseconds );
        DateTime utcNow = DateTime.UtcNow;
        DateTime exactUtcNow = utcNow.Add( expectedTimespan );
        TimeSpan actualTimespan = exactUtcNow.Subtract( utcNow );
        Assert.AreEqual( expectedTimespan, actualTimespan, $"{actualTimespan:s\\.fffffff} should equal {expectedTimespan:s\\.fffffff} after adding {1000 * milliseconds} microseconds" );
    }

    [TestMethod()]
    public void DateTimeAddingSecondsIsAccurate()
    {
        double milliseconds = 0.001;
        TimeSpan expectedTimespan = TimeSpan.FromSeconds( 0.001 * milliseconds );
        DateTime utcNow = DateTime.UtcNow;
        DateTime exactUtcNow = utcNow.Add( expectedTimespan );
        TimeSpan actualTimespan = exactUtcNow.Subtract( utcNow );
        Assert.AreEqual( expectedTimespan, actualTimespan, $"{actualTimespan:s\\.fffffff} should equal {expectedTimespan:s\\.fffffff} after adding {1000 * milliseconds} microseconds" );
    }

    #endregion

    #region " timespan time "

    /// <summary> (Unit Test Method) tests exact time span. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void ExactTimeSpanTest()
    {
        TimeSpan expectedTimespan;
        TimeSpan actualTimespan;
        double expectedMilliseconds;
        double actualMilliseconds;
        double expectedMicroseconds;
        double actualMicroseconds;
        double expectedSeconds;
        double actualSeconds;
        long ticksEpsilon = 2L;
        long stepSize = 7L;
        long ticks = 1L;
        while ( ticks < 100000L )
        {
            expectedSeconds = ticks * TimeSpanExtensionMethods.SecondsPerTick;
            expectedTimespan = TimeSpan.FromTicks( ticks );
            actualTimespan = expectedSeconds.FromSeconds();
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 1L ), $"Expected timespan from ticks should equal precise time span from seconds within {TimeSpan.FromTicks( 1L )}" );
            Assert.AreEqual( ticks, actualTimespan.Ticks, ticksEpsilon, $"Ticks of timespan converted to Seconds should match withing {ticksEpsilon} tick(s)" );
            Assert.AreEqual( expectedSeconds, actualTimespan.ToSeconds(), 2d * TimeSpanExtensionMethods.SecondsPerTick, $"Timespan converted to seconds should match" );

            actualSeconds = actualTimespan.ToSeconds();
            Assert.AreEqual( expectedSeconds, actualSeconds, 2d * TimeSpanExtensionMethods.SecondsPerTick, $"Timespan seconds should match within {2d * TimeSpanExtensionMethods.SecondsPerTick}s" );

            expectedMilliseconds = ticks * TimeSpanExtensionMethods.MillisecondsPerTick;
            expectedTimespan = TimeSpan.FromTicks( ticks );
            actualTimespan = expectedMilliseconds.FromMilliseconds();
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 1L ), $"Expected timespan from ticks should equal precise time span from milliseconds within {TimeSpan.FromTicks( 1L )}" );
            Assert.AreEqual( ticks, actualTimespan.Ticks, ticksEpsilon, $"Ticks of timespan converted to milliseconds should match withing {ticksEpsilon} tick(s)" );
            Assert.AreEqual( 0.001d * expectedMilliseconds, actualTimespan.ToSeconds(), TimeSpanExtensionMethods.SecondsPerTick, $"Timespan converted to seconds should match" );

            actualMilliseconds = actualTimespan.ToMilliseconds();
            Assert.AreEqual( expectedMilliseconds, actualMilliseconds, TimeSpanExtensionMethods.MillisecondsPerTick, $"Timespan milliseconds should match within {TimeSpanExtensionMethods.MillisecondsPerTick}ms" );

            expectedMicroseconds = ticks * TimeSpanExtensionMethods.MicrosecondsPerTick;
            expectedTimespan = TimeSpan.FromTicks( ticks );
            actualTimespan = expectedMicroseconds.FromMicroseconds();
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 1L ), $"Expected timespan from ticks should equal precise time span from Microseconds within {TimeSpan.FromTicks( 1L )}" );
            Assert.AreEqual( ticks, actualTimespan.Ticks, ticksEpsilon, $"Ticks of timespan converted to microseconds should match withing {ticksEpsilon} tick(s)" );
            Assert.AreEqual( 0.000001d * expectedMicroseconds, actualTimespan.ToSeconds(), TimeSpanExtensionMethods.SecondsPerTick, $"Timespan converted to seconds should match" );

            actualMicroseconds = actualTimespan.ToMicroseconds();
            Assert.AreEqual( expectedMicroseconds, actualMicroseconds, TimeSpanExtensionMethods.MicrosecondsPerTick, $"Timespan Microseconds should match within {TimeSpanExtensionMethods.MicrosecondsPerTick}µs" );

            expectedTimespan = actualMilliseconds.FromMilliseconds();
            actualTimespan = (1000d * actualMilliseconds).FromMicroseconds();
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 2L ), $"Time spans converted from Milliseconds and Microseconds should match within {TimeSpan.FromTicks( 2L )}" );

            expectedTimespan = actualMilliseconds.FromMilliseconds();
            actualTimespan = (0.001d * actualMilliseconds).FromSeconds();
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 2L ), $"Time spans converted from Milliseconds and seconds should match within {TimeSpan.FromTicks( 2L )}" );

            // Test timespan subtraction and additions
            expectedTimespan = (2d * actualMilliseconds).FromMilliseconds();
            actualTimespan = actualTimespan.Add( actualTimespan );
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 2L ), $"Added time spans converted from Milliseconds should match within {TimeSpan.FromTicks( 2L )}" );

            expectedTimespan = actualMilliseconds.FromMilliseconds();
            actualTimespan = actualTimespan.Subtract( expectedTimespan );
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.FromTicks( 2L ), $"Subtracted time spans converted from Milliseconds should match within {TimeSpan.FromTicks( 2L )}" );

            expectedTimespan = TimeSpan.Zero;
            actualTimespan = actualTimespan.Subtract( actualTimespan );
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, TimeSpan.Zero, $"Self subtracted time spans should be {TimeSpan.Zero}" );
            switch ( ticks )
            {
                case var @case when @case < 100L:
                    {
                        stepSize = 7L;
                        break;
                    }

                case var case1 when case1 < 1000L:
                    {
                        stepSize = 71L;
                        break;
                    }

                case var case2 when case2 < 10000L:
                    {
                        stepSize = 711L;
                        break;
                    }

                case var case3 when case3 < 100000L:
                    {
                        stepSize = 7101L;
                        break;
                    }

                default:
                    break;
            }

            ticks += stepSize;
        }
    }

    #endregion
}
