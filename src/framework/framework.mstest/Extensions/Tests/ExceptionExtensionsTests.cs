namespace cc.isr.Std.Framework.Extensions.Tests;

/// <summary> An exception extensions tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-01-26 </para>
/// </remarks>
[TestClass]
public class ExceptionExtensionsTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " to full blown string "

    /// <summary> Throw argument exception. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="version"> The version. </param>
    private static void ThrowArgumentException( Version version )
    {
        throw new ArgumentException( "Forced argument exception", nameof( version ) );
    }

    /// <summary>   (Unit Test Method) exception data should be added. </summary>
    /// <remarks>   David, 2020-09-18. </remarks>
    [TestMethod()]
    public void ExceptionDataShouldBeAdded()
    {
        Version version = new( 0, 0, 0, 0 );
        string expectedKey = "0-Name";
        string expectedValue = nameof( version );
        try
        {
            ThrowArgumentException( version );
        }
        catch ( ArgumentException ex )
        {
            bool exceptionDataAdded = ExceptionExtensions.ExceptionDataMethods.AddExceptionData( ex );
            // this checks the exception data.
            Assert.IsTrue( exceptionDataAdded, "Exception data should be added" );
            Assert.IsTrue( ex.Data.Contains( expectedKey ), $"Data contains key {expectedKey}" );
            Assert.AreEqual( expectedValue, Convert.ToString( ex.Data[expectedKey], System.Globalization.CultureInfo.CurrentCulture ), $"Data value of {expectedKey}" );
        }
        catch ( Exception ex )
        {
            Assert.Fail( $"Expected exception not caught; actual exception: {ex}" );
        }
    }

    #endregion
}
