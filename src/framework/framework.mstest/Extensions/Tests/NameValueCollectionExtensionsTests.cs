using System.Collections.Specialized;
using cc.isr.Std.CollectionExtensions;
using cc.isr.Std.Framework.Tests;

namespace cc.isr.Std.Framework.Extensions.Tests;

/// <summary> A name value collection extensions tests. </summary>
/// <remarks> David, 2020-09-23. </remarks>
[TestClass]
public class NameValueCollectionExtensionsTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    /// <summary> The collection. </summary>
    private static readonly NameValueCollection _collection = new() { { "ValidEntry", "ValidEntry" } };

    /// <summary> (Unit Test Method) does not throw exception when key does not exist. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Usage", "MSTEST0017:Assertion arguments should be passed in the correct order", Justification = "<Pending>" )]
    public void DoesNotThrowExceptionWhenKeyDoesNotExist()
    {
        Assert.AreEqual( expected: _collection.GetValueAs( "InvalidEntry", "DefaultValue" ), actual: "DefaultValue" );
    }

    /// <summary>
    /// (Unit Test Method) queries if a given does not throw exception when key exists.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Usage", "MSTEST0017:Assertion arguments should be passed in the correct order", Justification = "<Pending>" )]
    public void DoesNotThrowExceptionWhenKeyExists()
    {
        Assert.AreEqual( _collection.GetValueAs<string>( "ValidEntry" ), "ValidEntry" );
    }

    /// <summary> (Unit Test Method) throws exception when key is null or white space. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ThrowsExceptionWhenKeyIsNullOrWhiteSpace()
    {
        try
        {
            _ = _collection.GetValueAs<string>( string.Empty );
        }
        catch ( ArgumentException )
        {
        }
        catch
        {
            Assert.Fail( "expected exception was not thrown" );
        }

        try
        {
            _ = _collection.GetValueAs<string>( string.Empty );
        }
        catch ( ArgumentException )
        {
        }
        catch
        {
            Assert.Fail( "expected exception was not thrown" );
        }

        _ = Asserts.Instance.Throws<ArgumentException>( () => _collection.GetValueAs<string>( " " ) );
        _ = Asserts.Instance.Throws<ArgumentException>( () => _collection.GetValueAs<string>( "\t" ) );
        _ = Asserts.Instance.Throws<ArgumentException>( () => _collection.GetValueAs( string.Empty, string.Empty ) );
        _ = Asserts.Instance.Throws<ArgumentException>( () => _collection.GetValueAs( " ", string.Empty ) );
        _ = Asserts.Instance.Throws<ArgumentException>( () => _collection.GetValueAs( "\t", string.Empty ) );
    }
}
