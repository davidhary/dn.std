using cc.isr.Std.FileInfoExtensions;

namespace cc.isr.Std.Framework.Extensions.Tests;

/// <summary> File information extensions tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-03-14 </para>
/// </remarks>
[TestClass]
public class FileInfoExtensionsTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " file info extensions tests "

    /// <summary>   (Unit Test Method) file should move to folder. </summary>
    /// <remarks>   David, 2020-09-18. </remarks>
    [TestMethod()]
    public void FileShouldMoveToFolder()
    {
        string applicationFolder = AppContext.BaseDirectory;
        string fromFolder = Path.Combine( applicationFolder, "Extensions" );
        fromFolder = Path.Combine( fromFolder, "Tests" );
        string toFolder = Path.Combine( applicationFolder, @"..\_log" );
        _ = Directory.CreateDirectory( toFolder );
        string fileName = "MoveToFolderTestFile.txt";
        FileInfo value = new( Path.Combine( fromFolder, fileName ) );
        Assert.IsTrue( value.Exists, $"{fileName} not found in {fromFolder}" );
        string filePath = Path.Combine( toFolder, fileName );
        _ = value.CopyTo( filePath, true );
        value = new FileInfo( filePath );
        Assert.IsTrue( value.Exists, $"{fileName} not found in {toFolder}" );
        value.MoveToFolder( fromFolder, true );
        value = new FileInfo( Path.Combine( fromFolder, fileName ) );
        Assert.IsTrue( value.Exists, $"{fileName} not found in {toFolder}" );
    }

    #endregion
}
