using cc.isr.Std.BitConverterExtensions;

namespace cc.isr.Std.Framework.Extensions.Tests;

/// <summary> A bit converter tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-03-08 </para>
/// </remarks>
[TestClass]
public class BitConverterTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " bit converter tests "

    /// <summary> (Unit Test Method) tests true if little endian. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void TrueIfLittleEndianTest()
    {
        bool isLittleEndian = true;
        bool expected = isLittleEndian;
        bool actual = BitConverter.IsLittleEndian;
        Assert.AreEqual( expected, actual );
    }

    /// <summary> (Unit Test Method) tests byte converter short. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void ByteConverterShortTest()
    {
        ushort value = 1234;
        byte[] values = value.BigEndInt8();
        ushort expected = values.BigEndUnsignedShort( 0 );
        ushort actual = values.BigEndUnsignedInt16( 0 );
        Assert.AreEqual( expected, value );
        Assert.AreEqual( expected, actual );
    }

    /// <summary> (Unit Test Method) tests bit converter short. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void BitConverterShortTest()
    {
        ushort value = 1234;
        byte[] values = value.BigEndBytes();
        ushort actual = values.BigEndUnsignedInt16( 0 );
        ushort expected = values.BigEndUnsignedShort( 0 );
        Assert.AreEqual( expected, value );
        Assert.AreEqual( expected, actual );
    }

    /// <summary> (Unit Test Method) tests bit converter single. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void BitConverterSingleTest()
    {
        float expected = 1234.567f;
        byte[] values = expected.BigEndBytes();
        float actual = values.BigEndSingle( 0 );
        Assert.AreEqual( expected, actual );
    }

    /// <summary> (Unit Test Method) tests bit converter endianness. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void BitConverterEndiannessTest()
    {
        ushort value = 1234;
        byte[] bigEndBytes = value.BigEndBytes();
        byte[] bigEndInt8 = value.BigEndInt8();
        byte actual = bigEndBytes[0];
        byte expected = bigEndInt8[0];
        Assert.AreEqual( expected, actual );
        actual = bigEndBytes[1];
        expected = bigEndInt8[1];
        Assert.AreEqual( expected, actual );
        byte[] littleBytes = BitConverter.GetBytes( value );
        actual = littleBytes[0];
        expected = bigEndBytes[1];
        Assert.AreEqual( expected, actual );
        actual = littleBytes[1];
        expected = bigEndBytes[0];
        Assert.AreEqual( expected, actual );
    }
    #endregion
}
