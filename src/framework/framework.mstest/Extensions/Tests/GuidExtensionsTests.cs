using cc.isr.Std.GuidExtensions;

namespace cc.isr.Std.Framework.Extensions.Tests;

/// <summary> GUID extensions tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-03-14 </para>
/// </remarks>
[TestClass]
public class GuidExtensionsTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " guid extension tests "

    /// <summary> (Unit Test Method) tests base 64 unique identifier. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void Base64GuidTest()
    {
        Guid newGuid = Guid.NewGuid();
        string base64Guid = Convert.ToBase64String( newGuid.ToByteArray() );
        Guid fromBase64 = new( Convert.FromBase64String( base64Guid ) );
        Assert.AreEqual( newGuid, fromBase64, "Converting from GUID to base 64 and back" );
        string extBase64Guid = newGuid.NewBase64Guid();
        Assert.AreEqual( base64Guid, extBase64Guid.ToBase64String(), "Comparing conversion and extension" );
        fromBase64 = base64Guid.FromBase64Guid();
        Assert.AreEqual( newGuid, fromBase64, "Extension conversion from GUID to base 64 and back" );
    }

    #endregion
}
