namespace cc.isr.Std.Framework.Trials;

/// <summary> Tests for using Nullable tests. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-10-11 </para>
/// </remarks>
[TestClass]
public class NullableTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " nullable tests "

    /// <summary> A test for Nullable equality. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void NullableBooleanTest()
    {
        bool? @bool = default;
        bool boolValue = true;
        Assert.AreEqual( true, @bool is null, "Initialized to nothing" );
        Assert.AreEqual( true, @bool.Equals( new bool?() ), "Nullable not set equals to a new Boolean" );
        Assert.AreEqual( false, @bool.Equals( boolValue ), "Nullable not set is not equal to {0}", boolValue );
        // VB: Assert.AreEqual( default(bool?), @bool == boolValue, "Nullable '=' operator yields Nothing");
        Assert.AreEqual( false, @bool == boolValue, "Nullable '=' operator yields Nothing" );

        boolValue = false;
        Assert.AreEqual( false, @bool.Equals( boolValue ), "Nullable not set is not equal to True" );

        @bool = boolValue;
        Assert.AreEqual( true, @bool.HasValue, "Has value -- set to {0}", boolValue );

        @bool = new bool?();
        Assert.AreEqual( true, @bool is null, "Nullable set to new Boolean is still nothing" );
        Assert.AreEqual( true, @bool.Equals( new bool?() ), "Nullable set to new Boolean equals to a new Boolean" );
        Assert.AreEqual( false, @bool.Equals( boolValue ), "Nullable set to new Boolean not equal to {0}", boolValue );

        boolValue = false;
        Assert.AreEqual( false, @bool.Equals( boolValue ), "Nullable set to new Boolean not equal to True" );

        @bool = boolValue;
        Assert.AreEqual( true, @bool.HasValue, "Has value -- set to {0}", boolValue );
    }

    /// <summary> A test for Nullable Integer equality. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void NullableIntegerTest()
    {
        int integerValue = 1;
        int? nullInt = default;
        Assert.AreEqual( true, nullInt is null, "Initialized to nothing" );
        nullInt = new int?();
        Assert.AreEqual( true, nullInt is null, "Nullable set to new Boolean is nothing" );
        Assert.AreEqual( true, nullInt.Equals( new int?() ), "Nullable set to new Integer equals to a new Integer" );
        Assert.AreEqual( false, nullInt.Equals( integerValue ), "Nullable set to new Integer not equal to {0}", integerValue );
        Assert.AreEqual( false, nullInt.Equals( integerValue ), "Nullable set to new Integer not equal to {0}", integerValue );
        nullInt = integerValue;
        Assert.AreEqual( true, nullInt.HasValue, "Set to {0}", integerValue );
        Assert.AreEqual( integerValue, nullInt.Value, "Set to  {0}", integerValue );
    }

    /// <summary>   (Unit Test Method) tests nullable. </summary>
    /// <remarks>   David, 2020-10-28. </remarks>
    [TestMethod()]
    public void NullableTest()
    {
        Assert.AreEqual( new bool?(), new bool?(), "? new bool?() = new bool?()" );
        Assert.AreNotEqual( new bool?( true ), new bool?(), "? new bool?()(true) = new bool?()" );
        Assert.IsNotNull( new bool?() == new bool?(), "? Null = new bool?() = new bool?()" );
    }

    #endregion
}
