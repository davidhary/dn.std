namespace cc.isr.Std.Framework.Trials;

/// <summary> Includes extensions for <see cref="TimeSpan"/> calculations. </summary>
/// <remarks> Requires: DispatcherExtensions; Reference to Windows Base DLL. <para>
/// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
/// Licensed under The MIT License.</para> </remarks>
public static class TimeSpanExtensionMethods
{
    /// <summary>   Static constructor. </summary>
    /// <remarks>   David, 2021-02-10. </remarks>
    static TimeSpanExtensionMethods() => SystemClockResolution = EstimateSystemClockResolution( SystemClockResolution );

    #region " equals "

    /// <summary>
    /// A TimeSpan extension method that checks if the two timespan values are equal within
    /// <paramref name="epsilon"/>.
    /// </summary>
    /// <remarks>   David, 2020-11-25. </remarks>
    /// <param name="leftHand">     The leftHand to act on. </param>
    /// <param name="rightHand">    The right hand. </param>
    /// <param name="epsilon">      The epsilon. </param>
    /// <returns>   True if it succeeds; otherwise, false. </returns>
    public static bool Approximates( this TimeSpan leftHand, TimeSpan rightHand, TimeSpan epsilon )
    {
        return Math.Abs( leftHand.Subtract( rightHand ).Ticks ) <= epsilon.Ticks;
    }

    #endregion

    #region " resolutions "

    /// <summary> The system clock resolution. </summary>
    /// <value> The system clock resolution. </value>
    public static TimeSpan SystemClockResolution { get; private set; } = (1000d / 64).FromMilliseconds();

    /// <summary>   The thread clock resolution. </summary>
    /// <remarks>
    /// This might have changed from previous test results with the upgrade to Windows 20H2. Test
    /// results consistently show the thread sleep resolution at 15.6 ms.
    /// https://StackOverflow.com/questions/7614936/can-i-improve-the-resolution-of-thread-sleep The
    /// Thread.Sleep cannot be expected to provide reliable timing. It is notorious for behaving
    /// differently on different hardware Thread.Sleep(1) could sleep for 15.6 ms.
    /// https://social.msdn.Microsoft.com/Forums/vstudio/en-US/facc2b57-9a27-4049-bb32-ef093fbf4c29/threadsleep1-sleeps-for-156-ms?forum=clr.
    /// </remarks>
    /// <value> The thread clock resolution. </value>
    public static TimeSpan ThreadClockResolution { get; private set; } = 15.6001d.FromMilliseconds();

    /// <summary> The high resolution clock resolution. </summary>
    /// <value> The high resolution clock resolution. </value>
    public static TimeSpan HighResolutionClockResolution { get; private set; } = (1d / Stopwatch.Frequency).FromSeconds();

    /// <summary>   Estimate system clock resolution. </summary>
    /// <remarks>   David, 2021-02-10. </remarks>
    /// <param name="guess">    The guess. </param>
    /// <returns>   A TimeSpan. </returns>
    private static TimeSpan EstimateSystemClockResolution( TimeSpan guess )
    {
        TimeSpan resolution = guess;
        for ( int i = 0; i < 3; i++ )
        {
            Stopwatch sw = Stopwatch.StartNew();
            Thread.Sleep( 1 );
            sw.Stop();
            if ( resolution < sw.Elapsed )
                resolution = sw.Elapsed;
        }
        return resolution;
    }
    #endregion

    #region " exact times "

    /// <summary> Gets or sets the microseconds per tick. </summary>
    /// <value> The microseconds per tick. </value>
    public static double MicrosecondsPerTick { get; private set; } = 1000000.0d / Stopwatch.Frequency;

    /// <summary> Gets or sets the millisecond per tick. </summary>
    /// <value> The millisecond per tick. </value>
    public static double MillisecondsPerTick { get; private set; } = 1000.0d / Stopwatch.Frequency;

    /// <summary> Gets or sets the seconds per tick. </summary>
    /// <value> The seconds per tick. </value>
    public static double SecondsPerTick { get; private set; } = 1.0d / TimeSpan.TicksPerSecond;

    /// <summary> Gets or sets the ticks per microseconds. </summary>
    /// <value> The ticks per microseconds. </value>
    public static double TicksPerMicroseconds { get; private set; } = 0.001d * TimeSpan.TicksPerMillisecond;

    /// <summary> Converts seconds to time span with tick timespan accuracy. </summary>
    /// <remarks>
    /// <code>
    /// Dim actualTimespan As TimeSpan = TimeSpan.Zero.FromSecondsPrecise(42.042)
    /// </code>
    /// </remarks>
    /// <param name="seconds"> The number of seconds. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan FromSeconds( this double seconds )
    {
        return TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerSecond * seconds) );
    }

    /// <summary> Converts a timespan to the seconds with tick timespan accuracy. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="timespan"> The timespan. </param>
    /// <returns> Timespan as a Double. </returns>
    public static double ToSeconds( this TimeSpan timespan )
    {
        return timespan.Ticks * SecondsPerTick;
    }

    /// <summary> Converts milliseconds to time span with tick timespan accuracy. </summary>
    /// <remarks>
    /// <code>
    /// Dim actualTimespan As TimeSpan = TimeSpan.Zero.FromMillisecondsPrecise(42.042)
    /// </code>
    /// </remarks>
    /// <param name="milliseconds"> The number of milliseconds. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan FromMilliseconds( this double milliseconds )
    {
        return TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerMillisecond * milliseconds) );
    }

    /// <summary> Converts a timespan to an exact milliseconds with tick timespan accuracy. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="timespan"> The timespan. </param>
    /// <returns> Timespan as a Double. </returns>
    public static double ToMilliseconds( this TimeSpan timespan )
    {
        return timespan.Ticks * MillisecondsPerTick;
    }

    /// <summary> Converts microseconds to time span with tick timespan accuracy. </summary>
    /// <remarks>
    /// <code>
    /// Dim actualTimespan As TimeSpan = TimeSpan.Zero.FromMicroseconds(42.2)
    /// </code>
    /// </remarks>
    /// <param name="microseconds"> The value. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan FromMicroseconds( this double microseconds )
    {
        return TimeSpan.FromTicks( ( long ) (TicksPerMicroseconds * microseconds) );
    }

    /// <summary> Converts a timespan to an exact microseconds with tick timespan accuracy. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="timespan"> The timespan. </param>
    /// <returns> Timespan as a Double. </returns>
    public static double ToMicroseconds( this TimeSpan timespan )
    {
        return timespan.Ticks * MicrosecondsPerTick;
    }

    /// <summary>
    /// A TimeSpan extension method that adds the microseconds to 'microseconds'.
    /// </summary>
    /// <remarks>   David, 2020-12-07. </remarks>
    /// <param name="self">         The self to act on. </param>
    /// <param name="microseconds"> The value. </param>
    /// <returns>   A TimeSpan. </returns>
    public static TimeSpan AddMicroseconds( this TimeSpan self, double microseconds )
    {
        return self.Add( TimeSpan.FromTicks( ( long ) (microseconds * TicksPerMicroseconds) ) );
    }

    #endregion

    #region " delays "

    /// <summary>
    /// Gets or sets the relative resolution for selecting the relevant clock. Defaults to one fifth
    /// of the desired timespan.
    /// </summary>
    /// <value> The relative resolution. </value>
    public static double RelativeResolution { get; private set; } = 0.2;

    /// <summary>
    /// A TimeSpan extension method that returns the expected resolution for the delay and wait
    /// functions.
    /// </summary>
    /// <remarks>   David, 2020-11-06. </remarks>
    /// <param name="timeInterval"> The timeInterval to act on. </param>
    /// <returns>   A TimeSpan. </returns>
    public static TimeSpan ExpectedWaitClockResolution( this TimeSpan timeInterval )
    {
        return timeInterval.ExpectedWaitClockResolution( RelativeResolution );
    }

    /// <summary>   A TimeSpan extension method that returns the expected resolution for the delay and wait functions. </summary>
    /// <remarks>   David, 2020-11-06. </remarks>
    /// <param name="timeInterval">         The timeInterval to act on. </param>
    /// <param name="relativeResolution">   The relative resolution for selecting the clock. </param>
    /// <returns>   A TimeSpan. </returns>
    public static TimeSpan ExpectedWaitClockResolution( this TimeSpan timeInterval, double relativeResolution )
    {
        return (relativeResolution * timeInterval.ToSeconds()).FromSeconds() > SystemClockResolution
                    ? SystemClockResolution : SystemClockResolution;
    }

    /// <summary>   A TimeSpan extension method that spin waits for the specified time. </summary>
    /// <remarks>   David, 2020-11-05. <para>
    /// SpinWait(1ms) waits:   00:00:00.0012747ms </para><para>
    /// SpinWait(2ms) waits:   00:00:00.0020004ms </para><para>
    /// SpinWait(5ms) waits:   00:00:00.0050003ms </para><para>
    /// SpinWait(10ms) waits:  00:00:00.0100003ms </para><para>
    /// SpinWait(20ms) waits:  00:00:00.0200011ms </para><para>
    /// SpinWait(50ms) waits:  00:00:00.0500028ms </para><para>
    /// SpinWait(100ms) waits: 00:00:00.1000114ms </para><para>
    /// SpinWait(1ms, yield)   waits: 00:00:00.0010010ms </para><para>
    /// SpinWait(2ms, yield)   waits: 00:00:00.0020003ms </para><para>
    /// SpinWait(5ms, yield)   waits: 00:00:00.0050003ms </para><para>
    /// SpinWait(10ms, yield)  waits: 00:00:00.0100003ms </para><para>
    /// SpinWait(20ms, yield)  waits: 00:00:00.0200013ms </para><para>
    /// SpinWait(50ms, yield)  waits: 00:00:00.0500047ms </para><para>
    /// SpinWait(100ms, yield) waits: 00:00:00.1000016ms </para><para>
    /// </para> </remarks>
    /// <param name="delayTime">    The delay time. </param>
    /// <param name="yield">        (Optional) True to yield between spin waits. </param>
    public static void SpinWait( this TimeSpan delayTime, bool yield = false )
    {
        // set expected resolution to one fifth or the requested delay time
        delayTime.SpinWait( RelativeResolution, yield );
    }

    /// <summary>   A TimeSpan extension method that spin waits for the specified time. </summary>
    /// <remarks>   David, 2020-11-05. </remarks>
    /// <param name="delayTime">            The delay time. </param>
    /// <param name="relativeResolution">   The relative resolution for selecting the clock. </param>
    /// <param name="yield">                (Optional) True to yield between spin waits. </param>
    public static void SpinWait( this TimeSpan delayTime, double relativeResolution, bool yield = false )
    {
        // set expected resolution time
        TimeSpan timeResolution = (relativeResolution * delayTime.ToSeconds()).FromSeconds();
        if ( delayTime <= HighResolutionClockResolution )
            // if timespan is zero, run one split wait
            Thread.SpinWait( 1 );
        else if ( timeResolution > SystemClockResolution )
        {
            // if resolution higher than the system clock, use the system clock
            DateTimeOffset endTime = DateTimeOffset.Now.Add( delayTime );
            while ( DateTimeOffset.Now < endTime )
            {
                Thread.SpinWait( 1 );
                if ( yield ) _ = Thread.Yield();             }
        }
        else
        {
            // otherwise, use the high resolution timer via the stopwatch.
            long ticks = delayTime.Ticks;
            Stopwatch sw = Stopwatch.StartNew();
            while ( sw.ElapsedTicks < ticks )
            {
                Thread.SpinWait( 1 );
                if ( yield ) _ = Thread.Yield();             }
        }
    }

    /// <summary>
    /// Starts a delay task for delaying operations by the given delay time selecting the delay clock
    /// which resolution exceeds 0.2 times the delay time.
    /// </summary>
    /// <remarks> David, 2020-09-15. <para>
    /// StartDelayTask(1ms) waits: 00:00:00.0018267ms <para></para>
    /// StartDelayTask(2ms) waits: 00:00:00.0020956ms <para></para>
    /// StartDelayTask(5ms) waits: 00:00:00.0050699ms <para></para>
    /// StartDelayTask(10ms) waits: 00:00:00.0101197ms <para></para>
    /// StartDelayTask(20ms) waits: 00:00:00.0200819ms <para></para>
    /// StartDelayTask(50ms) waits: 00:00:00.0502617ms <para></para>
    /// StartDelayTask(100ms) waits: 00:00:00.1155308ms </para>
    /// </remarks>
    /// <param name="delayTime"> The delay time. </param>
    /// <returns> A Task. </returns>
    public static Task StartDelayTask( this TimeSpan delayTime )
    {
        // set expected resolution to one fifth or the requested delay time
        return delayTime.StartDelayTask( RelativeResolution );
    }

    /// <summary>
    /// A TimeSpan extension method that synchronously waits for a specific time delay that is
    /// accurate up to the high resolution clock resolution.
    /// </summary>
    /// <remarks>   David, 2021-01-30. </remarks>
    /// <param name="delayTime">    The delay time. </param>
    /// <param name="yield">        (Optional) True to yield between <see cref="Thread.Sleep(int)"/>
    ///                             and <see cref="Thread.SpinWait(int)"/>. </param>
    /// <returns>   The actual wait time. </returns>
    public static TimeSpan SyncWait( this TimeSpan delayTime, bool yield = false )
    {
        Stopwatch sw = Stopwatch.StartNew();
        int yieldCount = 100;
        int counter = yieldCount;
        int systemClockCycles = ( int ) Math.Floor( delayTime / SystemClockResolution );
        if ( systemClockCycles > 0 )
            if ( yield )
                while ( delayTime.Subtract( sw.Elapsed ) < SystemClockResolution )
                {
                    int ms = ( int ) Math.Floor( SystemClockResolution.TotalMilliseconds );
                    Thread.Sleep( ms );
                    _ = Thread.Yield();
                }
            else
            {
                Thread.Sleep( delayTime );
                _ = Thread.Yield();
            }
        while ( sw.Elapsed < delayTime )
        {
            if ( counter >= yieldCount )
            {
                Thread.SpinWait( 1 );
                if ( yield )
                    _ = Thread.Yield();
            }
            counter = 0;
        }
        return sw.Elapsed;
    }

    /// <summary>   A TimeSpan extension method that starts a wait task. </summary>
    /// <remarks>   David, 2021-01-30. </remarks>
    /// <param name="delayTime">    The delay time. </param>
    /// <param name="yield">        (Optional) True to yield between spin waits. </param>
    /// <returns>   A Task. </returns>
    public static Task StartWaitTask( this TimeSpan delayTime, bool yield = false )
    {
        return Task.Factory.StartNew( () => delayTime.SyncWait( yield ) );
    }

    /// <summary>   A TimeSpan extension method that asynchronously waits for a specific time delay that is
    /// accurate up to the high resolution clock resolution. </summary>
    /// <remarks>   David, 2021-01-30. </remarks>
    /// <param name="delayTime">    The delay time. </param>
    /// <param name="yield">        (Optional) True to yield between spin waits. </param>
    public static void AsyncWait( this TimeSpan delayTime, bool yield = false )
    {
        delayTime.StartWaitTask( yield ).Wait();
    }

    /// <summary>   A TimeSpan extension method that starts wait task. </summary>
    /// <remarks>   David, 2021-01-30. </remarks>
    /// <param name="delayTime">    The delay time. </param>
    /// <param name="yield">        (Optional) True to yield between spin waits. </param>
    /// <returns>   A TimeSpan Task. </returns>
    public static Task<TimeSpan> StartWaitTimespanTask( this TimeSpan delayTime, bool yield = false )
    {
        return Task<TimeSpan>.Factory.StartNew( () => delayTime.SyncWait( yield ) );
    }

    /// <summary>
    /// A TimeSpan extension method that asynchronously waits for a specific time delay that is
    /// accurate up to the high resolution clock resolution.
    /// </summary>
    /// <remarks>   David, 2021-01-30. </remarks>
    /// <param name="delayTime">    The delay time. </param>
    /// <param name="yield">        (Optional) True to yield between spin waits. </param>
    /// <returns>   A TimeSpan. </returns>
    public static TimeSpan AsyncWaitTimespan( this TimeSpan delayTime, bool yield = false )
    {
        Task<TimeSpan> t = delayTime.StartWaitTimespanTask( yield );
        t.Wait();
        return t.Result;
    }

    /// <summary>
    /// Starts a delay task for delaying operations by the given delay time selecting the delay clock
    /// which resolution exceeds
    /// <paramref name="relativeResolution"/> times the delay time.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="delayTime">  The delay time. </param>
    /// <param name="relativeResolution"> The relative resolution for selecting the clock. </param>
    /// <returns> A Task. </returns>
    public static Task StartDelayTask( this TimeSpan delayTime, double relativeResolution )
    {
        // set expected resolution time
        TimeSpan timeResolution = (relativeResolution * delayTime.ToSeconds()).FromSeconds();
        if ( delayTime <= HighResolutionClockResolution )
            // if timespan is zero, run a NOP task
            return Task.Factory.StartNew( () => Thread.SpinWait( 1 ) );
        else if ( timeResolution > SystemClockResolution )
            // if resolution higher than the system clock, use the system clock
            return delayTime.StartSystemClockDelayTask();
        else
            // otherwise, use the high resolution timer via the stopwatch.
            return delayTime.StartHighResolutionClockDelayTask();
    }

    /// <summary>
    /// Starts a delay task using the system clock timing of about 15.4 ms resolution.
    /// </summary>
    /// <remarks>
    /// Summary: With zero delay the task does not delay. With less than the clock
    /// resolution, the delays seems random averaging at the clock resolution. With above
    /// resolution, we get the quantizes time at multiples of the clock resolution<para>
    /// Q: Removed outliers</para><para>
    /// TaskDelay(0.000)   0.002±0.013ms  100:[0.0,0.135]</para><para>
    /// TaskDelay(0.009)   15.238±4.895ms 100:[9.302,20.502]</para><para>
    /// TaskDelay(0.020)   30.942±1.986ms 100:[25.768,34.789]</para><para>
    /// TaskDelay(0.000).Q  0.0±0.0ms      73:[0.0,0.0]</para><para>
    /// TaskDelay(0.009).Q 15.065±4.965ms 100:[9.384,20.46]</para><para>
    /// TaskDelay(0.020).Q 31.715±1.11ms   87:[28.614,33.27]</para><para>
    /// 
    /// SystemClockDelayTask(1ms) waits: 00:00:00.0156340ms  </para><para>
    /// SystemClockDelayTask(2ms) waits: 00:00:00.0155193ms  </para><para>
    /// SystemClockDelayTask(5ms) waits: 00:00:00.0160676ms  </para><para>
    /// SystemClockDelayTask(10ms) waits: 00:00:00.0160262ms  </para><para>
    /// SystemClockDelayTask(20ms) waits: 00:00:00.0320374ms  </para><para>
    /// SystemClockDelayTask(50ms) waits: 00:00:00.0624929ms  </para><para>
    /// SystemClockDelayTask(100ms) waits: 00:00:00.1105533ms  </para><para>
    /// </para>
    /// </remarks>
    /// <param name="delay"> The delay. </param>
    /// <returns> A Task. </returns>
    public static Task StartSystemClockDelayTask( this TimeSpan delay )
    {
        return Task.Delay( delay );
    }

    /// <summary> Starts a delay task using the thread clock of 1 ms resolution. </summary>
    /// <remarks> David, 2020-09-15. <para></para>
    /// ThreadClockDelayTask(1ms) waits: 00:00:00.0129022ms  <para></para>
    /// ThreadClockDelayTask(2ms) waits: 00:00:00.0074258ms  <para></para>
    /// ThreadClockDelayTask(5ms) waits: 00:00:00.0110409ms <para></para>
    /// ThreadClockDelayTask(10ms) waits: 00:00:00.0214591ms <para></para>
    /// ThreadClockDelayTask(20ms) waits: 00:00:00.0311390ms <para></para>
    /// ThreadClockDelayTask(50ms) waits: 00:00:00.0527569ms <para></para>
    /// ThreadClockDelayTask(100ms) waits: 00:00:00.1100563ms <para></para>
    /// </remarks>
    /// <param name="delay"> The delay. </param>
    /// <returns> A Task. </returns>
    public static Task StartThreadClockDelayTask( this TimeSpan delay )
    {
        return Task.Factory.StartNew( () => Thread.Sleep( delay ) );
    }

    /// <summary> A delay task using the system clock timing of about 15.4 ms resolution. </summary>
    /// <remarks>
    /// The thread delay seems totally quantized at 1 ms with zero below 1ms.
    /// <para>Q: Removed outliers </para><para>
    /// ThreadTask(0.0000)     0.033±0.16ms  100:[0.005,1.607] </para><para>
    /// ThreadTask(0.0005)     0.027±0.021ms 100:[0.005,0.114] </para><para>
    /// ThreadTask(0.0015)    15.235±2.602ms  20:[4.277,16.196] </para><para>
    /// ThreadTask(0.0000).Q   0.018±0.007ms  91:[0.008,0.042] </para><para>
    /// ThreadTask(0.0005).Q   0.054±0.047ms  97:[0.009,0.165] </para><para>
    /// ThreadTask(0.0022).Q  15.623±0.502ms  20:[14.721,16.221] </para><para>
    /// 
    /// 2020-11-05: THESE ARE NO LONGER THE CASE:
    /// 
    /// ThreadTask(0.0000)   0.033±0.16ms  100:[0.005,1.607] </para><para>
    /// ThreadTask(0.0005)   0.027±0.021ms 100:[0.005,0.114] </para><para>
    /// ThreadTask(0.0015)   1.88±0.174ms  100:[1.334,2.582] </para><para>
    /// ThreadTask(0.0019)   1.851±0.184ms 100:[1.262,2.339] </para><para>
    /// ThreadTask(0.0022)   2.862±0.219ms 100:[2.197,3.441] </para><para>
    /// ThreadTask(0.0000).Q 0.018±0.007ms  91:[0.008,0.042] </para><para>
    /// ThreadTask(0.0005).Q 0.054±0.047ms  97:[0.009,0.165] </para><para>
    /// ThreadTask(0.0015).Q 1.877±0.079ms  78:[1.675,2.083] </para><para>
    /// ThreadTask(0.0019).Q 1.879±0.084ms  85:[1.627,2.078] </para><para>
    /// ThreadTask(0.0022).Q 2.891±0.082ms  85:[2.647,3.107] </para><para>
    /// 
    /// </para>
    /// </remarks>
    /// <param name="delay"> The delay. </param>
    /// <returns> A Task. </returns>
    public static Task ThreadClockTask( this TimeSpan delay )
    {
        return new Task( () => Thread.Sleep( delay ) );
    }

    /// <summary>
    /// Starts a delay task using the high resolution timer of about 0.1 microsecond.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="delay">    The delay. </param>
    /// <param name="yield">    (Optional) True to yield between spin waits. </param>
    /// <returns>   A Task. </returns>
    public static Task StartHighResolutionClockDelayTask( this TimeSpan delay, bool yield = false )
    {
        return Task.Factory.StartNew( () =>
                                                                                                            {
                                                                                                                long ticks = delay.Ticks;
                                                                                                                Stopwatch sw = Stopwatch.StartNew();
                                                                                                                while ( sw.ElapsedTicks < ticks )
                                                                                                                {
                                                                                                                    Thread.SpinWait( 1 );
                                                                                                                    if ( yield ) _ = Thread.Yield();                                                                                                                 }
                                                                                                            } );
    }

    /// <summary>   High resolution clock task. </summary>
    /// <remarks>
    /// Looks like minimum timespan would be 15 microseconds. at 100 microseconds delay we get actual
    /// 150 microseconds average. Q: Outliers removed.  <para>
    /// HighRes(0.00000)   0.031±0.137ms 100:[0.003,1.341] </para><para>
    /// HighRes(0.00001)   0.036±0.033ms 100[0.014,0.268] </para><para>
    /// HighRes(0.00010)   0.171±0.033ms 100:[0.126,0.351] </para><para>
    /// HighRes(0.00000).Q 0.015±0.003ms 84:[0.009,0.023] </para><para>
    /// HighRes(0.00001).Q 0.025±0.004ms 86:[0.019,0.037] </para><para>
    /// HighRes(0.00010).Q 0.159±0.008ms 84:[0.143,0.19] </para><para>
    /// </para>
    /// </remarks>
    /// <param name="delay">    The delay. </param>
    /// <param name="yield">    (Optional) True to yield between spin waits. </param>
    /// <returns>   A Task. </returns>
    public static Task HighResolutionClockTask( this TimeSpan delay, bool yield = false )
    {
        return new Task( () =>
                                                                                                  {
                                                                                                      long ticks = delay.Ticks;
                                                                                                      Stopwatch sw = Stopwatch.StartNew();
                                                                                                      while ( sw.ElapsedTicks < ticks )
                                                                                                      {
                                                                                                          Thread.SpinWait( 1 );
                                                                                                          if ( yield ) _ = Thread.Yield();                                                                                                       }
                                                                                                      // sw.Stop()
                                                                                                      // Dim x As Long = sw.ElapsedTicks
                                                                                                      // Dim elapsed As Boolean = x >= ticks
                                                                                                  } );
    }

    /// <summary> NoOp task. </summary>
    /// <remarks>
    /// <para>
    /// NoOp   0.029±0.121ms 100:[0.004,1.211]</para><para>
    /// NoOp.Q 0.021±0.017ms  88:[0.004,0.069]</para><para>
    /// </para>
    /// </remarks>
    /// <returns> A Task. </returns>
    public static Task NoOpTask()
    {
        return new Task( () => { } );
    }

    /// <summary> Spin wait task. </summary>
    /// <remarks>
    /// <para>
    /// q: Removed outliers.</para><para>
    /// SpinWait(1)   0.039±0.142ms 100:[0.004,1.319]</para><para>
    /// SpinWait(9)   0.027±0.026ms 100:[0.004,0.174]</para><para>
    /// SpinWait(1).Q 0.024±0.02ms   99:[0.004,0.093]</para><para>
    /// SpinWait(9).Q 0.025±0.018ms  98:[0.004,0.077]</para><para>
    /// </para>
    /// </remarks>
    /// <param name="count"> Number of. </param>
    /// <returns> A Task. </returns>
    public static Task SpinWaitTask( int count )
    {
        return new Task( () => Thread.SpinWait( count ) );
    }

    #endregion
}
