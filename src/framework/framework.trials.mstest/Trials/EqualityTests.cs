namespace cc.isr.Std.Framework.Trials;

/// <summary> tests of equalities. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-10-10 </para>
/// </remarks>
[TestClass]
public class EqualityTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " equality tests "

    /// <summary> Query if 'value' is empty. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> <c>true</c> if empty; otherwise <c>false</c> </returns>
    public static bool IsEmpty( string value )
    {
        return string.IsNullOrEmpty( value ) && value is not null;
    }

    /// <summary> Information getter. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string InfoGetter( string? value )
    {
        string result = value is null ? "null" : string.IsNullOrEmpty( value ) ? "empty" : string.IsNullOrWhiteSpace( value ) ? "white" : value;
        return result;
    }

    /// <summary> (Unit Test Method) tests string equality. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="left">  The left. </param>
    /// <param name="right"> The right. </param>
    public static void EqualityTest( string? left, string? right )
    {
        Assert.IsTrue( (left ?? "") == (right ?? ""), $"1. Left.{InfoGetter( left )} == Right.{InfoGetter( right )}" );
        Assert.IsTrue( (left ?? "") == (left ?? ""), $"2. Left.{InfoGetter( left )} == Left.{InfoGetter( left )}" );
        Assert.IsTrue( string.Equals( left, right, StringComparison.Ordinal ), $"3. string.Equals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsTrue( string.Equals( left, left, StringComparison.Ordinal ), $"4. string.Equals(Left.{InfoGetter( left )}, left.{InfoGetter( left )})" );
        Assert.IsTrue( ReferenceEquals( left, right ), $"5. Object.ReferenceEquals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsTrue( ReferenceEquals( left, left ), $"6. Object.ReferenceEquals(Left.{InfoGetter( left )}, left.{InfoGetter( left )})" );
        Assert.IsTrue( ReferenceEquals( left, right ), $"7. (Object)Left.{InfoGetter( left )} Is (Object)Right.{InfoGetter( right )}" );
        Assert.IsTrue( ReferenceEquals( left, left ), $"8. (Object)Left.{InfoGetter( left )} Is (Object)left.{InfoGetter( left )}" );
    }

    /// <summary> (Unit Test Method) tests string equality. </summary>
    /// <remarks>
    /// Results:
    /// <code>
    /// TRUE: string.Empty == string.Empty
    /// TRUE: string.Equals(string.Empty, string.Empty)
    /// TRUE: string.ReferenceEquals(string.Empty, string.Empty)
    /// TRUE: string.Empty Is string.Empty
    /// 
    /// TRUE: null == null
    /// TRUE: string.Equals(null, null)
    /// TRUE: string.ReferenceEquals(null, null)
    /// TRUE: null Is null
    /// 
    /// TRUE: "" == string.Empty
    /// TRUE: string.Equals("", "")
    /// TRUE: string.ReferenceEquals("", "")
    /// TRUE: "" Is ""
    /// 
    /// TRUE: string.Empty == string.Empty
    /// TRUE: string.Equals(string.Empty, "")
    /// TRUE: string.ReferenceEquals(string.Empty, "")
    /// TRUE: string.Empty Is ""
    /// 
    /// TRUE: "a" == "a"
    /// TRUE: string.Equals("a", "a")
    /// TRUE: string.ReferenceEquals("a", "a")
    /// TRUE: "a" Is "a"
    /// </code>
    /// Conclusions: (1) Nothing Equals Nothing and Empty equals "" (note below that Empty == Nothing
    /// but not equals Nothing )
    /// </remarks>
    [TestMethod()]
    public void StringEqualityTest()
    {
        string? left = string.Empty;
        string? right = string.Empty;
        EqualityTest( left, right );
        left = null;
        right = null;
        EqualityTest( left, right );
        left = string.Empty;
        right = string.Empty;
        EqualityTest( left, right );
        left = string.Empty;
        right = string.Empty;
        EqualityTest( left, right );
        left = "a";
        right = "a";
        EqualityTest( left, right );
    }

    /// <summary> (Unit Test Method) tests string inequality. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="left">  The left. </param>
    /// <param name="right"> The right. </param>
    public static void InequalityTest( string? left, string? right )
    {
        Assert.IsFalse( string.Equals( left, right, StringComparison.Ordinal ), $"2. !string.Equals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsFalse( ReferenceEquals( left, right ), $"3. !Object.ReferenceEquals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsFalse( ReferenceEquals( left, right ), $"4. !(Object)Left.{InfoGetter( left )} Is (Object)Right.{InfoGetter( right )}" );
    }

    /// <summary> (Unit Test Method) tests string equality. </summary>
    /// <remarks>
    /// Results:
    /// <code>
    /// TRUE: string.Empty = null
    /// FALSE: string.Equals(string.Empty, null)
    /// FALSE: string.ReferenceEquals(string.Empty, string.Empty)
    /// FALSE: string.Empty Is string.Empty
    /// 
    /// TRUE: null == string.Empty
    /// FALSE: string.Equals(null, "")
    /// FALSE: string.ReferenceEquals(null, "")
    /// FALSE: null Is ""
    /// 
    /// FALSE: "a" == string.Empty
    /// FALSE: string.Equals("a", "")
    /// FASLE: string.ReferenceEquals("a", "")
    /// FALSE: "a" Is ""
    /// 
    /// FALSE: "a" == "b"
    /// FALSE: string.Equals("a", "b")
    /// FALSE: string.ReferenceEquals("a", "b")
    /// FALSE: "a" Is "b"
    /// </code>
    /// Conclusions: (1) string.Equals() correct; except if (2) Nothing == Empty but Not("" =
    /// Nothing)  !!!!
    /// </remarks>
    [TestMethod()]
    public void StringInequalityTest()
    {
        string? left = string.Empty;
        string? right = null;
        Assert.IsTrue( (left ?? "") == (right ?? ""), $"1. Left.{InfoGetter( left )} == Right.{InfoGetter( right )}" );
        InequalityTest( left, right );
        left = null;
        right = string.Empty;
        Assert.IsTrue( (left ?? "") == (right ?? ""), $"1. Left.{InfoGetter( left )} != Right.{InfoGetter( right )}" );
        InequalityTest( left, right );
        left = "a";
        right = string.Empty;
        Assert.IsFalse( (left ?? "") == (right ?? ""), $"1. Left.{InfoGetter( left )} != Right.{InfoGetter( right )}" );
        InequalityTest( left, right );
        left = "a";
        right = "b";
        Assert.IsFalse( (left ?? "") == (right ?? ""), $"1. Left.{InfoGetter( left )} != Right.{InfoGetter( right )}" );
        InequalityTest( left, right );
    }

    /// <summary> Information getter. </summary>
    /// <remarks> David, 2020-03-07. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string? InfoGetter( IEquatable<TimeSpan>? value )
    {
        return value is null ? "null" : value.ToString();
    }

    /// <summary> (Unit Test Method) tests string equality. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="left">  The left. </param>
    /// <param name="right"> The right. </param>
    public static void EqualityTest( IEquatable<TimeSpan>? left, IEquatable<TimeSpan>? right )
    {
        Assert.IsTrue( Equals( left, right ), $"1. Object.Equals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsTrue( Equals( left, left ), $"2. Object.Equals(Left.{InfoGetter( left )}, left.{InfoGetter( left )})" );
        Assert.IsFalse( ReferenceEquals( left, right ), $"3. Object.ReferenceEquals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsTrue( ReferenceEquals( left, left ), $"4. Object.ReferenceEquals(Left.{InfoGetter( left )}, left.{InfoGetter( left )})" );
        Assert.IsFalse( ReferenceEquals( left, right ), $"5. (Object)Left.{InfoGetter( left )} Is (Object)Right.{InfoGetter( right )}" );
        Assert.IsTrue( ReferenceEquals( left, left ), $"6. (Object)Left.{InfoGetter( left )} Is (Object)left.{InfoGetter( left )}" );
    }

    /// <summary> (Unit Test Method) tests string inequality. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="left">  The left. </param>
    /// <param name="right"> The right. </param>
    public static void InequalityTest( IEquatable<TimeSpan>? left, IEquatable<TimeSpan>? right )
    {
        Assert.IsFalse( Equals( left, right ), $"1. !Object.Equals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsFalse( ReferenceEquals( left, right ), $"2. !Object.ReferenceEquals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsFalse( ReferenceEquals( left, right ), $"3. !(Object)Left.{InfoGetter( left )} Is (Object)Right.{InfoGetter( right )}" );
    }

    /// <summary> (Unit Test Method) tests Structure equality. </summary>
    /// <remarks>
    /// Results:
    /// <code>
    /// The following applies to timespan value, nothing or zero and from zero seconds
    /// TRUE: TimeSpan.Zero == TimeSpan.Zero
    /// TRUE: Object.Equals(TimeSpan.Zero, TimeSpan.Zero)
    /// FALSE: Object.ReferenceEquals(left, right)
    /// TRUE: Object.ReferenceEquals(left, left)
    /// FALSE: left Is right
    /// TRUE: left Is left
    /// TRUE: TimeSpan.Zero == TimeSpan = nothing
    /// </code>
    /// Conclusions: (1) Nothing (Null) objects are equal. (2) Time span = nothing is the same as
    /// Timespan.Zero.
    /// </remarks>
    [TestMethod()]
    public void StructureEqualityTest()
    {
        TimeSpan left = TimeSpan.Zero;
        TimeSpan right = TimeSpan.Zero;
        Assert.IsTrue( left == right, $"Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        EqualityTest( left, right );
        left = default;
        right = default;
        Assert.IsTrue( left == right, $"Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        EqualityTest( left, right );
        left = TimeSpan.Zero;
        right = TimeSpan.FromSeconds( 0d );
        Assert.IsTrue( left == right, $"Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        EqualityTest( left, right );
        left = TimeSpan.Zero;
        right = default;
        Assert.IsTrue( left == right, $"Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        EqualityTest( left, right );
    }

    /// <summary> (Unit Test Method) tests structure inequality. </summary>
    /// <remarks>
    /// Results:
    /// <code>
    /// The following applies to timespan value, nothing or zero and from zero seconds
    /// FALSE: TimeSpan.One == TimeSpan.Zero or nothing
    /// FALSE: Object.Equals(TimeSpan.One, TimeSpan.Zero or nothing)
    /// FALSE: Object.ReferenceEquals(left, right)
    /// FALSE: left Is right
    /// TRUE: TimeSpan.Zero == TimeSpan nothing
    /// </code>
    /// Conclusions: (1) Nothing (Null) objects are equal. (2) Time span = nothing is the same as
    /// Timespan.Zero.
    /// </remarks>
    [TestMethod()]
    public void StructureInequalityTest()
    {
        TimeSpan left = TimeSpan.FromSeconds( 1d );
        TimeSpan right = default;
        Assert.IsFalse( left == right, $"a. Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        InequalityTest( left, right );
        left = TimeSpan.FromSeconds( 1d );
        right = TimeSpan.Zero;
        Assert.IsFalse( left == right, $"b. Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        InequalityTest( left, right );
    }

    /// <summary> Information getter. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string? InfoGetter( object? value )
    {
        return value is null ? "null" : value.ToString();
    }

    /// <summary> (Unit Test Method) tests string equality. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="left">  The left. </param>
    /// <param name="right"> The right. </param>
    public static void EqualityTest( object? left, object? right )
    {
        Assert.IsTrue( Equals( left, right ), $"1. Object.Equals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsTrue( Equals( left, left ), $"2. Object.Equals(Left.{InfoGetter( left )}, left.{InfoGetter( left )})" );
        Assert.IsTrue( ReferenceEquals( left, left ), $"3. Object.ReferenceEquals(Left.{InfoGetter( left )}, left.{InfoGetter( left )})" );
        Assert.IsTrue( ReferenceEquals( left, left ), $"4. (Object)Left.{InfoGetter( left )} Is (Object)left.{InfoGetter( left )}" );
    }

    /// <summary> (Unit Test Method) tests string inequality. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="left">  The left. </param>
    /// <param name="right"> The right. </param>
    public static void InequalityTest( object? left, object? right )
    {
        Assert.IsFalse( Equals( left, right ), $"1. !Object.Equals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsFalse( ReferenceEquals( left, right ), $"2. !Object.ReferenceEquals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsFalse( ReferenceEquals( left, right ), $"3. !(Object)Left.{InfoGetter( left )} Is (Object)Right.{InfoGetter( right )}" );
    }

    /// <summary> (Unit Test Method) tests object equality. </summary>
    /// <remarks>
    /// Results:
    /// <code>
    /// The following applies to version nothing or zero
    /// TRUE: Version(0,0) == Version(0,0)
    /// TRUE: Object.Equals(Version(0,0), Version(0,0))
    /// FALSE: Object.ReferenceEquals(left, right)
    /// TRUE: Object.ReferenceEquals(left(null), right(null))
    /// TRUE: Object.ReferenceEquals(left, left)
    /// FALSE: left Is right
    /// TRUE: left(null) Is right(null)
    /// TRUE: left Is left
    /// TRUE: TimeSpan.Zero == TimeSpan = nothing
    /// </code>
    /// Conclusions: Nothing (Null) objects are equal.
    /// </remarks>
    [TestMethod()]
    public void VersionEqualityTest()
    {
        Version? left = new( 0, 0 );
        Version? right = new( 0, 0 );
        Assert.IsTrue( left == right, $"Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        Assert.IsFalse( ReferenceEquals( left, right ), $"a. Object.ReferenceEquals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsFalse( ReferenceEquals( left, right ), $"b. (Object)Left.{InfoGetter( left )} Is (Object)Right.{InfoGetter( right )}" );
        EqualityTest( left, right );
        left = null;
        right = null;
        Trace.TraceInformation( $"Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        Assert.IsTrue( left == right, $"Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        Assert.IsTrue( ReferenceEquals( left, right ), $"a. Object.ReferenceEquals(Left.{InfoGetter( left )}, Right.{InfoGetter( right )})" );
        Assert.IsTrue( ReferenceEquals( left, right ), $"b. (Object)Left.{InfoGetter( left )} Is (Object)Right.{InfoGetter( right )}" );
        EqualityTest( left, right );
    }

    /// <summary> (Unit Test Method) tests object inequality. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void VersionInequalityTest()
    {
        Version left = new( 0, 0 );
        Version? right = new( 0, 1 );
        Trace.TraceInformation( $"Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        Assert.IsFalse( left == right, $"Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        InequalityTest( left, right );
        left = new Version( 0, 0 );
        right = null;
        Trace.TraceInformation( $"Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        Assert.IsFalse( left == right, $"Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        InequalityTest( left, right );
    }

    /// <summary> (Unit Test Method) tests nullable equality. </summary>
    /// <remarks>
    /// Results:
    /// <code>
    /// VB: NULL: New Integer? == New Integer?
    /// C#: TRUE: New Integer? == New Integer?
    /// TRUE: Object.Equals(left(New Integer?) , right(New Integer?))
    /// TRUE: left(New Integer?) Is right(New Integer?)
    /// TRUE: left(New Integer?) Is left(New Integer?)
    /// 
    /// TRUE: New Integer?(1) == New Integer?(1)
    /// TRUE: Object.Equals(left(New Integer?(1)) , right(New Integer?(1)))
    /// FALSE: left(New Integer?(1)) Is right(New Integer?(1))
    /// TRUE: left(New Integer?(1)) Is left(New Integer?(1))
    /// 
    /// VB: NULL: Integer?(null) == Integer?(null)
    /// C#: TRUE: Integer?(null) == Integer?(null)
    /// TRUE: Object.Equals(left(Integer?(null)) , right(Integer?(null)))
    /// TRUE: left(Integer?(null)) Is right(Integer?(null))
    /// TRUE: left(Integer?(null)) Is left(Integer?(null))
    /// 
    /// VB: NULL: Integer?(null) == Integer?(null)
    /// C#: TRUE: Integer?(null) == Integer?(null)
    /// TRUE: Object.Equals(left(New Integer?) , right(Integer?(null)))
    /// TRUE: left(New Integer?) Is right(Integer?(null))
    /// TRUE: left(New Integer?) Is left(New Integer?)
    /// </code>
    /// Conclusions: Nothing (Null) objects are equal but equality operations yield nothing for VB and True for C#.
    /// David, 2020-09-22. </remarks>
    [TestMethod()]
    public void NullableEqualityTest()
    {
        int? left = new int?();
        int? right = new int?();
        bool? val = left == right;
        Assert.IsTrue( val.Value, $"C#: Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        EqualityTest( left, right );

        left = new int?( 1 );
        right = new int?( 1 );
        val = left == right;
        Assert.IsTrue( val.Value, $"Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        EqualityTest( left, right );

        left = default;
        right = default;
        val = left == right;
        Assert.IsTrue( val.Value, $"C#: Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        EqualityTest( left, right );

        left = new int?();
        right = default;
        val = left == right;
        Assert.IsTrue( val.Value, $"C#: Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        EqualityTest( left, right );

    }

    /// <summary> (Unit Test Method) tests nullable inequality. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void NullableInequalityTest()
    {
        int? left = new int?( 0 );
        int? right = new int?();
        bool? val = left == right;
        Assert.IsFalse( val.Value, $"C#: Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        InequalityTest( left, right );

        left = new int?( 0 );
        right = default;
        val = left == right;
        Assert.IsFalse( val.Value, $"C#: Left.{InfoGetter( left )} == right.{InfoGetter( right )}" );
        InequalityTest( left, right );

        left = new int?( 1 );
        right = default;
        val = left > right;
        Assert.IsFalse( val.Value, $"C#: Left.{InfoGetter( left )} > right.{InfoGetter( right )}" );
        InequalityTest( left, right );

    }

    #endregion
}
