using System.Collections.ObjectModel;
using System.Data;

namespace cc.isr.Std.Framework.Trials;

/// <summary> Tests for using Enumerable tests. </summary>
/// <remarks>
/// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 09/021/2020 </para>
/// </remarks>
[TestClass]
public class EnumerableTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " enumerable tests "

    /// <summary> Iterate array. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="objectName"> Name of the object. </param>
    /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan IterateArray( string objectName, double[] a )
    {
        Stopwatch sw = Stopwatch.StartNew();
        double v = default;
        for ( int i = 0, loopTo = a.Length - 1; i <= loopTo; i++ )
        {
            v = a[i];
        }

        TimeSpan elapsed = sw.Elapsed;
        Trace.TraceInformation( $"Iterate {objectName}(Of Double)[{a.Length}] cast as Array in {elapsed.TotalMilliseconds}ms" );
        a[0] = v;
        return elapsed;
    }

    /// <summary> For each array. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="objectName"> Name of the object. </param>
    /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan ForEachArray( string objectName, double[] a )
    {
        Stopwatch sw = Stopwatch.StartNew();
        double v = default;
        foreach ( double item in a )
        {
            v = item;
        }

        TimeSpan elapsed = sw.Elapsed;
        Trace.TraceInformation( $"For Each {objectName}(Of Double)[{a.Length}] cast as Array in {elapsed.TotalMilliseconds}ms" );
        a[0] = v;
        return elapsed;
    }

    /// <summary> Populates from array. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="objectName"> Name of the object. </param>
    /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan PopulateFromArray( string objectName, double[] a )
    {
        Collection<double> col = [];
        Stopwatch sw = Stopwatch.StartNew();
        foreach ( double item in a )
        {
            col.Add( item );
        }

        TimeSpan elapsed = sw.Elapsed;
        Trace.TraceInformation( $"Populate Collection from {objectName}(Of Double)[{a.Length}] cast as Array in {elapsed.TotalMilliseconds}ms" );
        return elapsed;
    }

    /// <summary> Iterate i list. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="objectName"> Name of the object. </param>
    /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan IterateIList( string objectName, IList<double> a )
    {
        Stopwatch sw = Stopwatch.StartNew();
        for ( int i = 0, loopTo = a.Count - 1; i <= loopTo; i++ )
        {
            _ = a[i];
        }

        TimeSpan elapsed = sw.Elapsed;
        Trace.TraceInformation( $"Iterate {objectName}(Of Double)[{a.Count}] cast as IList in {elapsed.TotalMilliseconds}ms" );
        a[0] = 0d;
        // collection is read only: a(0) = 0
        return elapsed;
    }

    /// <summary> For each i list. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="objectName"> Name of the object. </param>
    /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan ForEachIList( string objectName, IList<double> a )
    {
        Stopwatch sw = Stopwatch.StartNew();
        double v;
        foreach ( double item in a )
        {
            v = item;
        }

        TimeSpan elapsed = sw.Elapsed;
        Trace.TraceInformation( $"For Each {objectName}(Of Double)[{a.Count}] cast as IList in {elapsed.TotalMilliseconds}ms" );
        // collection is read only: a(0) = 0
        return elapsed;
    }

    /// <summary> Populates from i list. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="objectName"> Name of the object. </param>
    /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan PopulateFromIList( string objectName, IList<double> a )
    {
        Collection<double> col = [];
        Stopwatch sw = Stopwatch.StartNew();
        foreach ( double item in a )
        {
            col.Add( item );
        }

        TimeSpan elapsed = sw.Elapsed;
        Trace.TraceInformation( $"Populate Collection from {objectName}(Of Double)[{a.Count}] cast as IList in {elapsed.TotalMilliseconds}ms" );
        return elapsed;
    }

    /// <summary> Iterate i enumerable. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="objectName"> Name of the object. </param>
    /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan IterateIEnumerable( string objectName, IEnumerable<double> a )
    {
        Stopwatch sw = Stopwatch.StartNew();
        for ( int i = 0, loopTo = a.Count() - 1; i <= loopTo; i++ )
        {
            _ = a.ElementAtOrDefault( i );
        }

        TimeSpan elapsed = sw.Elapsed;
        Trace.TraceInformation( $"Iterate {objectName}(Of Double)[{a.Count()}] cast as IEnumerable in {elapsed.TotalMilliseconds}ms" );
        return elapsed;
    }

    /// <summary> For each i enumerable. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="objectName"> Name of the object. </param>
    /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan ForEachIEnumerable( string objectName, IEnumerable<double> a )
    {
        Stopwatch sw = Stopwatch.StartNew();
        double v;
        foreach ( double item in a )
        {
            v = item;
        }

        TimeSpan elapsed = sw.Elapsed;
        Trace.TraceInformation( $"For Each {objectName}(Of Double)[{a.Count()}] cast as IEnumerable in {elapsed.TotalMilliseconds}ms" );
        return elapsed;
    }

    /// <summary> Populates from i enumerable. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="objectName"> Name of the object. </param>
    /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan PopulateFromIEnumerable( string objectName, IEnumerable<double> a )
    {
        Collection<double> col = [];
        Stopwatch sw = Stopwatch.StartNew();
        foreach ( double item in a )
        {
            col.Add( item );
        }

        TimeSpan elapsed = sw.Elapsed;
        Trace.TraceInformation( $"Populate Collection from {objectName}(Of Double)[{a.Count()}] cast as IEnumerable in {elapsed.TotalMilliseconds}ms" );
        return elapsed;
    }

    /// <summary> Iterate i collection. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="objectName"> Name of the object. </param>
    /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan IterateICollection( string objectName, ICollection<double> a )
    {
        Stopwatch sw = Stopwatch.StartNew();
        for ( int i = 0, loopTo = a.Count - 1; i <= loopTo; i++ )
        {
            _ = a.ElementAtOrDefault( i );
        }

        TimeSpan elapsed = sw.Elapsed;
        Trace.TraceInformation( $"Iterate {objectName}(Of Double)[{a.Count}] cast as ICollection in {elapsed.TotalMilliseconds}ms" );
        return elapsed;
    }

    /// <summary> For each i collection. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="objectName"> Name of the object. </param>
    /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan ForEachICollection( string objectName, ICollection<double> a )
    {
        Stopwatch sw = Stopwatch.StartNew();
        double v;
        foreach ( double item in a )
        {
            v = item;
        }

        TimeSpan elapsed = sw.Elapsed;
        Trace.TraceInformation( $"ForEach {objectName}(Of Double)[{a.Count}] cast as ICollection in {elapsed.TotalMilliseconds}ms" );
        return elapsed;
    }

    /// <summary> Populates from i collection. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="objectName"> Name of the object. </param>
    /// <param name="a">          An IEnumerable(OfDouble) to process. </param>
    /// <returns> A TimeSpan. </returns>
    public static TimeSpan PopulateFromICollection( string objectName, ICollection<double> a )
    {
        Collection<double> col = [];
        Stopwatch sw = Stopwatch.StartNew();
        foreach ( double item in a )
        {
            col.Add( item );
        }

        TimeSpan elapsed = sw.Elapsed;
        // expression is a value and cannot be a target of assignment: a(0) = 0
        Trace.TraceInformation( $"Populate Collection from {objectName}(Of Double)[{a.Count}] cast as ICollection in {elapsed.TotalMilliseconds}ms" );
        return elapsed;
    }

    /// <summary> Enumerates array as i enumerable in this collection. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="a"> An IEnumerable(OfDouble) to process. </param>
    /// <returns>
    /// An enumerator that allows foreach to be used to process array as i enumerable in this
    /// collection.
    /// </returns>
    public static IEnumerable<double> ArrayAsIEnumerable( IEnumerable<double> a )
    {
        Collection<double> col = [];
        _ = Stopwatch.StartNew();
        foreach ( double item in a )
        {
            col.Add( item );
        }

        return [.. col];
    }

    /// <summary> Array as i list. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="a"> An IEnumerable(OfDouble) to process. </param>
    /// <returns> A list of. </returns>
    public static IList<double> ArrayAsIList( IEnumerable<double> a )
    {
        Collection<double> col = [];
        _ = Stopwatch.StartNew();
        foreach ( double item in a )
        {
            col.Add( item );
        }

        return [.. col];
    }

    /// <summary> List as i list. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="a"> An IEnumerable(OfDouble) to process. </param>
    /// <returns> A list of. </returns>
    public static IList<double> ListAsIList( IEnumerable<double> a )
    {
        List<double> l = [];
        _ = Stopwatch.StartNew();
        foreach ( double item in a )
        {
            l.Add( item );
        }

        return l;
    }

    /// <summary> Lists as <see cref="IEnumerable{T}"/> in this collection. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="a"> An IEnumerable(OfDouble) to process. </param>
    /// <returns>
    /// An enumerator that allows foreach to be used to process as <see cref="IEnumerable{T}"/>  in this collection.
    /// </returns>
    public static IEnumerable<double> ListAsIEnumerable( IEnumerable<double> a )
    {
        List<double> l = [];
        _ = Stopwatch.StartNew();
        foreach ( double item in a )
        {
            l.Add( item );
        }

        return l;
    }

    /// <summary> Executes the tests. </summary>
    /// <remarks> David, 2020-09-07. </remarks>
    /// <param name="testLevel"> The test level. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0305:Simplify collection initialization", Justification = "<Pending>" )]
    private static void RunTests( TestLevel testLevel )
    {
        List<double> l = [];
        Collection<double> x = [];
        TimeSpan elapsed;
        int count = 10000000;
        Stopwatch sw = Stopwatch.StartNew();
        for ( int i = 1, loopTo = count; i <= loopTo; i++ )
        {
            l.Add( i );
        }
        elapsed = sw.Elapsed;
        Trace.TraceInformation( $"Test {testLevel}: {nameof( List<double> )}.Add n={count} in {elapsed.TotalMilliseconds}ms" );

        sw.Restart();
        for ( int i = 1, loopTo = count; i <= loopTo; i++ )
        {
            x.Add( i );
        }
        elapsed = sw.Elapsed;
        Trace.TraceInformation( $"Test {testLevel}: {nameof( Collection<double> )}.Add n={count} in {elapsed.TotalMilliseconds}ms" );

        double[] a;
        sw.Restart();
        a = [.. x];
        elapsed = sw.Elapsed;
        Trace.TraceInformation( $"{nameof( Collection<double> )}.ToArray in {elapsed.TotalMilliseconds}ms" );
        a[0] = 0d;

        sw.Restart();
        a = [.. l];
        elapsed = sw.Elapsed;
        Trace.TraceInformation( $"{nameof( List<double> )}.ToArray in {elapsed.TotalMilliseconds}ms" );

        sw.Restart();
        IReadOnlyList<double> ro = new List<double>( a );
        elapsed = sw.Elapsed;
        Trace.TraceInformation( $"{nameof( IReadOnlyList<double> )} = New {nameof( List<double> )} in {elapsed.TotalMilliseconds}ms" );
        if ( testLevel == TestLevel.None ) return;

        if ( (testLevel & TestLevel.Iterate) != 0 )
        {
            _ = IterateArray( "Array", a );
            _ = IterateIList( "Array", a );
            _ = IterateIEnumerable( "Array", a );
            _ = IterateICollection( "Array", a );
            _ = IterateArray( "List", [.. l] );
            _ = IterateIList( "List", l );
            _ = IterateIEnumerable( "List", l );
            _ = IterateICollection( "List", l );
            _ = IterateArray( "ICollection{T}", [.. x] );
            _ = IterateIList( "ICollection{T}", x );
            _ = IterateIEnumerable( "ICollection{T}", x );
            _ = IterateICollection( "ICollection{T}", x );
            _ = IterateArray( "Read-Only List", [.. ro] );
            _ = IterateIList( "Read-Only List", ro.ToList() );
            _ = IterateIEnumerable( "Read-Only List", ro );
            _ = IterateICollection( "Read-Only List", ro.ToList() );
        }

        if ( (testLevel & TestLevel.ForEach) != 0 )
        {
            _ = ForEachArray( "Array", a );
            _ = ForEachIList( "Array", a );
            _ = ForEachIEnumerable( "Array", a );
            _ = ForEachICollection( "Array", a );
            _ = ForEachArray( "List", [.. l] );
            _ = ForEachIList( "List", l );
            _ = ForEachIEnumerable( "List", l );
            _ = ForEachICollection( "List", l );
            _ = ForEachArray( "ICollection{T}", [.. x] );
            _ = ForEachIList( "ICollection{T}", x );
            _ = ForEachIEnumerable( "ICollection{T}", x );
            _ = ForEachICollection( "ICollection{T}", x );
            _ = ForEachArray( "Read-Only List", [.. ro] );
            _ = ForEachIList( "Read-Only List", ro.ToList() );
            _ = ForEachIEnumerable( "Read-Only List", ro );
            _ = ForEachICollection( "Read-Only List", ro.ToList() );
        }

        if ( (testLevel & TestLevel.Populate) != 0 )
        {
            _ = PopulateFromArray( "Array", a );
            _ = PopulateFromIList( "Array", a );
            _ = PopulateFromIEnumerable( "Array", a );
            _ = PopulateFromICollection( "Array", a );
            _ = PopulateFromArray( "List", [.. l] );
            _ = PopulateFromIList( "List", l );
            _ = PopulateFromIEnumerable( "List", l );
            _ = PopulateFromICollection( "List", l );
            _ = PopulateFromArray( "ICollection{T}", [.. x] );
            _ = PopulateFromIList( "ICollection{T}", x );
            _ = PopulateFromIEnumerable( "ICollection{T}", x );
            _ = PopulateFromICollection( "ICollection{T}", x );
        }

        if ( (testLevel & TestLevel.ReturnValue) != 0 )
        {
            IList<double> lal = ListAsIList( a );
            IEnumerable<double> aae = ArrayAsIEnumerable( a );
            IList<double> aal = ArrayAsIList( a );
            IEnumerable<double> lae = ListAsIEnumerable( a );
            if ( (testLevel & TestLevel.Iterate) != 0 )
            {
                _ = IterateArray( "Array as IEnumerable", aae.ToArray() );
                _ = IterateIList( "Array as IEnumerable", aae.ToList() );
                _ = IterateIEnumerable( "Array as IEnumerable", aae );
                _ = IterateICollection( "Array as IEnumerable", aae.ToList() );
                _ = IterateArray( "Array as IList", [.. aal] );
                _ = IterateIList( "Array as IList", aal );
                _ = IterateIEnumerable( "Array as IList", aal );
                _ = IterateICollection( "Array as IList", aal );
                _ = IterateArray( "List as IEnumerable", lae.ToArray() );
                _ = IterateIList( "List as IEnumerable", aae.ToList() );
                _ = IterateIEnumerable( "List as IEnumerable", aae );
                _ = IterateICollection( "List as IEnumerable", aae.ToList() );
                _ = IterateArray( "List as IList", [.. lal] );
                _ = IterateIList( "List as IList", aal );
                _ = IterateIEnumerable( "List as IList", aal );
                _ = IterateICollection( "List as IList", aal );
            }

            if ( (testLevel & TestLevel.ForEach) != 0 )
            {
                _ = ForEachArray( "Array as IEnumerable", aae.ToArray() );
                _ = ForEachIList( "Array as IEnumerable", aae.ToList() );
                _ = ForEachIEnumerable( "Array as IEnumerable", aae );
                _ = ForEachICollection( "Array as IEnumerable", aae.ToList() );
                _ = ForEachArray( "Array as IList", [.. aal] );
                _ = ForEachIList( "Array as IList", aal );
                _ = ForEachIEnumerable( "Array as IList", aal );
                _ = ForEachICollection( "Array as IList", aal );
                _ = ForEachArray( "List as IEnumerable", lae.ToArray() );
                _ = ForEachIList( "List as IEnumerable", aae.ToList() );
                _ = ForEachIEnumerable( "List as IEnumerable", aae );
                _ = ForEachICollection( "List as IEnumerable", aae.ToList() );
                _ = ForEachArray( "List as IList", [.. lal] );
                _ = ForEachIList( "List as IList", aal );
                _ = ForEachIEnumerable( "List as IList", aal );
                _ = ForEachICollection( "List as IList", aal );
            }
        }
    }

    /// <summary> A bit-field of flags for specifying test levels. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [Flags]
    private enum TestLevel
    {
        /// <summary> An enum constant representing the none option. </summary>
        None = 1 << 0,
        /// <summary> An enum constant representing the iterate option. </summary>
        Iterate = 1 << 1,
        /// <summary> An enum constant representing for each option. </summary>
        ForEach = 1 << 2,
        /// <summary> An enum constant representing the populate option. </summary>
        Populate = 1 << 3,
        /// <summary> An enum constant representing the return value option. </summary>
        ReturnValue = 1 << 4
    }

    /// <summary> (Unit Test Method) iteration performance. </summary>
    /// <remarks>
    /// <list type="bullet"> Populate Double: Enumeration Tests N = 10000000 Time in ms<item>
    ///                 List           Collection       New Read </item><item>
    /// Test          .Add .To Array .Add  .To Array  Only List</item><item>
    /// None          126  28        190   34         23</item><item>
    /// Iterate       126  34        188   34         29</item><item>
    /// For Each      112  26        148   30         26 </item><item>
    /// Populate       77  19        124   26         20 </item><item>
    /// Return Value   92   9        150   14         34 </item><item>
    /// Return Value 105   26        158   26         25 </item><item>
    /// Return Value 105   26        158   26         25 </item><item>
    /// </item>
    /// </list>
    /// <list type="bullet"> Enumerate Double Tests N = 10000000 Time in ms<item>
    ///                                               For   Collection  Iterate Return Value As      For each Return As  </item><item>
    /// Item                                  Iterate Each  From        Iterate IList ..IEnumerable  IList  IEnumerable</item><item>
    /// Array[] cast as Array                 26      22     188         24     24        24          24    22 </item><item>
    /// Array[] cast as IList                 55      68     164         31     31        31          58    67 </item><item>
    /// Array[] cast as IEnumerable          249      68     170        173    173       173          53    53 </item><item>
    /// Array[] cast as ICollection          171      68     163        173    174       115          53    67 </item><item> 
    /// List() cast as Array                  26      21     133         24     24        24          22    22 </item><item>
    /// List() cast as IList                  52      90     196         31     31        31          67    52 </item><item>
    /// List() cast as IEnumerable           133      89     294        118    172       172          67    52 </item><item>
    /// List() cast as ICollection           139      78     201        118    173       115          72    53 </item><item>
    /// Collection() cast as Array            25      22     152         24                           22    22 </item><item>
    /// Collection() cast as IList            58      67     202         39                           72    67 </item><item>
    /// Collection() cast as IEnumerable     162      68     183        131                           72    53 </item><item>
    /// Collection() cast as ICollection     135      67     208        135                           70    67 </item><item>
    /// Read-Only List()[] as Array           24      22                 24                           22    22 </item><item>
    /// Read-Only List()[] as IList in        32      67                 32                           67    53 </item><item>
    /// Read-Only List()[] as IEnumerable    115      66                116                           67    53 </item><item>
    /// Read-Only List()[] as ICollection    114      67                115                           67    53 </item><item>
    /// </item><item>
    /// Summary: Iterations using indexes has a significant penalty. </item><item>
    /// .NET 5.0 does much better on IEnumerable and collection.  </item><item></item></list>
    /// </remarks>
    [TestMethod()]
    public void IterationPerformance()
    {
        foreach ( TestLevel testLevel in Enum.GetValues<TestLevel>() )
        {
            RunTests( testLevel );
            break;
        }
        // RunTests( TestLevel.ReturnValue | TestLevel.Iterate );
        // RunTests( TestLevel.ReturnValue | TestLevel.ForEach );

    }

    #endregion
}
