namespace cc.isr.Std.Framework.Trials;

/// <summary> Time Span extensions tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-03-14 </para>
/// </remarks>
[TestClass]
public class TimeSpanExtensionsTrials
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " wait trials "

    /// <summary>   (Unit Test Method) tests time span wait. </summary>
    /// <remarks>   David, 2021-01-30.
    /// SpinWait(1ms)               waits: 00:00:00.0012485ms
    /// SpinWait(1ms, yield)        waits: 00:00:00.0010010ms  </para><para>
    /// DoEventsWait(1ms)           waits: 00:00:00.0010090ms  </para><para>
    /// StartDelayTask(1ms)         waits: 00:00:00.0018318ms  </para><para>
    /// ThreadClockDelayTask(1ms)   waits: 00:00:00.0082343ms  </para><para>
    /// SystemClockDelayTask(1ms)   waits: 00:00:00.0156340ms  </para><para>
    /// SpinWait(2ms)               waits: 00:00:00.0020006ms  </para><para>
    /// SpinWait(2ms, yield)        waits: 00:00:00.0020004ms  </para><para>
    /// DoEventsWait(2ms)           waits: 00:00:00.0020060ms  </para><para>
    /// StartDelayTask(2ms)         waits: 00:00:00.0020804ms  </para><para>
    /// ThreadClockDelayTask(2ms)   waits: 00:00:00.0071415ms  </para><para>
    /// SystemClockDelayTask(2ms)   waits: 00:00:00.0155193ms  </para><para>
    /// SpinWait(5ms)               waits: 00:00:00.0050013ms  </para><para>
    /// SpinWait(5ms, yield)        waits: 00:00:00.0050016ms  </para><para>
    /// DoEventsWait(5ms)           waits: 00:00:00.0050045ms  </para><para>
    /// StartDelayTask(5ms)         waits: 00:00:00.0050993ms  </para><para>
    /// ThreadClockDelayTask(5ms)   waits: 00:00:00.0104145ms  </para><para>
    /// SystemClockDelayTask(5ms)   waits: 00:00:00.0160676ms  </para><para>
    /// SpinWait(10ms)              waits: 00:00:00.0100008ms  </para><para>
    /// SpinWait(10ms, yield)       waits: 00:00:00.0100009ms  </para><para>
    /// DoEventsWait(10ms)          waits: 00:00:00.0100233ms  </para><para>
    /// StartDelayTask(10ms)        waits: 00:00:00.0101042ms  </para><para>
    /// ThreadClockDelayTask(10ms)  waits: 00:00:00.0226875ms  </para><para>
    /// SystemClockDelayTask(10ms)  waits: 00:00:00.0160262ms  </para><para>
    /// SpinWait(20ms)              waits: 00:00:00.0200008ms  </para><para>
    /// SpinWait(20ms, yield)       waits: 00:00:00.0200008ms  </para><para>
    /// DoEventsWait(20ms)          waits: 00:00:00.0200186ms  </para><para>
    /// StartDelayTask(20ms)        waits: 00:00:00.0201822ms  </para><para>
    /// ThreadClockDelayTask(20ms)  waits: 00:00:00.0316936ms  </para><para>
    /// SystemClockDelayTask(20ms)  waits: 00:00:00.0320374ms  </para><para>
    /// SpinWait(50ms)              waits: 00:00:00.0500011ms  </para><para>
    /// SpinWait(50ms, yield)       waits: 00:00:00.0500028ms  </para><para>
    /// DoEventsWait(50ms)          waits: 00:00:00.0500242ms  </para><para>
    /// StartDelayTask(50ms)        waits: 00:00:00.0501188ms  </para><para>
    /// ThreadClockDelayTask(50ms)  waits: 00:00:00.0552911ms  </para><para>
    /// SystemClockDelayTask(50ms)  waits: 00:00:00.0624929ms  </para><para>
    /// SpinWait(100ms)             waits: 00:00:00.1000353ms  </para><para>
    /// SpinWait(100ms, yield)      waits: 00:00:00.1000032ms  </para><para>
    /// DoEventsWait(100ms)         waits: 00:00:00.1000190ms  </para><para>
    /// StartDelayTask(100ms)       waits: 00:00:00.1135598ms  </para><para>
    /// Task Thread.Sleep(1ms)      waits: 00:00:00.0156630ms  </para><para>
    /// Task Thread.Sleep(2ms)      waits: 00:00:00.0156139ms  </para><para>
    /// Task Thread.Sleep(5ms)      waits: 00:00:00.0151129ms  </para><para>
    /// Task Thread.Sleep(10ms)     waits: 00:00:00.0160073ms  </para><para>
    /// Task Thread.Sleep(20ms)     waits: 00:00:00.0310864ms  </para><para>
    /// Task Thread.Sleep(50ms)     waits: 00:00:00.0640170ms  </para><para>
    /// Task Thread.Sleep(100ms)    waits: 00:00:00.1119477ms  </para><para>
    /// </remarks>
    [TestMethod()]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>" )]
    public void TimeSpanWaitTest()
    {
        System.Text.StringBuilder builder = new();
        TimeSpan expectedTimespan;
        TimeSpan maximumError;
        TimeSpan waitTimespan;
        TimeSpan actualTimespan;
        double[] testIntervals = [1, 2, 5, 10, 20, 50, 100];


        // initialize the underlying objects
        waitTimespan = TimeSpan.FromMilliseconds( 1 );
        waitTimespan.SpinWait();
        waitTimespan.StartDelayTask().Wait();
        waitTimespan.StartThreadClockDelayTask().Wait();
        waitTimespan.StartSystemClockDelayTask().Wait();

        Stopwatch sw = Stopwatch.StartNew();
        foreach ( double testInterval in testIntervals )
        {
            waitTimespan = TimeSpan.FromMilliseconds( testInterval );
            maximumError = waitTimespan.ExpectedWaitClockResolution();
            // using twice the resolution to account offset on the system clock.
            maximumError = maximumError.Add( maximumError );
            if ( testInterval == 100 )
                maximumError = maximumError.AddMicroseconds( 4 );
            expectedTimespan = waitTimespan;
            sw.Restart();
            waitTimespan.SpinWait();
            sw.Stop();
            actualTimespan = sw.Elapsed;
            _ = builder.AppendLine( $"SpinWait({testInterval}ms) waits: {actualTimespan}ms" );
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, maximumError,
                $"Spin Wait time interval should equal within error {maximumError}" );

            sw.Restart();
            waitTimespan.SpinWait( true );
            sw.Stop();
            actualTimespan = sw.Elapsed;
            _ = builder.AppendLine( $"SpinWait({testInterval}ms, yield) waits: {actualTimespan}ms" );
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, maximumError,
                $"Spin Wait( yield ) time interval should equal within error {maximumError}" );

            sw.Restart();
            waitTimespan.StartDelayTask().Wait();
            sw.Stop();
            actualTimespan = sw.Elapsed;
            _ = builder.AppendLine( $"StartDelayTask({testInterval}ms) waits: {actualTimespan}ms" );
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, maximumError,
                $"Delay time interval should equal within error {maximumError}" );

            sw.Restart();
            _ = waitTimespan.StartThreadClockDelayTask().Wait( ( int ) testInterval );
            sw.Stop();
            actualTimespan = sw.Elapsed;
            _ = builder.AppendLine( $"ThreadClockDelayTask({testInterval}ms) waits: {actualTimespan}ms" );
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, maximumError,
                $"Thread Clock Delay time interval should equal within error {maximumError}" );

            sw.Restart();
            waitTimespan.StartSystemClockDelayTask().Wait();
            sw.Stop();
            actualTimespan = sw.Elapsed;
            _ = builder.AppendLine( $"SystemClockDelayTask({testInterval}ms) waits: {actualTimespan}ms" );
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, maximumError,
                $"SystemClock Delay time interval should equal within error {maximumError}" );

            //             return Task.Factory.StartNew( () => Thread.Sleep( delay ) );
            //
            sw.Restart();
            System.Threading.Tasks.Task.Factory.StartNew( () => System.Threading.Thread.Sleep( ( int ) testInterval ) ).Wait();
            sw.Stop();
            actualTimespan = sw.Elapsed;
            _ = builder.AppendLine( $"Task Thread.Sleep({testInterval}ms) waits: {actualTimespan}ms" );
            Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, maximumError,
                $"Thread Sleep Delay time interval should equal within error {maximumError}" );

        }
        Trace.TraceInformation( builder.ToString() );
    }

    /// <summary>   (Unit Test Method) time span wait should delay. </summary>
    /// <remarks>   David, 2021-01-30. <para> 
    /// Yield = False </para><para>
    /// StartWaitTask(1ms)   waits: 00:00:00.0010772ms </para><para>
    /// StartWaitTask(2ms)   waits: 00:00:00.0020738ms </para><para>
    /// StartWaitTask(5ms)   waits: 00:00:00.0050905ms </para><para>
    /// StartWaitTask(10ms)  waits: 00:00:00.0101497ms </para><para>
    /// StartWaitTask(20ms)  waits: 00:00:00.0201509ms </para><para>
    /// StartWaitTask(50ms)  waits: 00:00:00.0501130ms </para><para>
    /// StartWaitTask(100ms) waits: 00:00:00.1001314ms </para><para>
    /// Yield = True </para><para>
    /// StartWaitTask(1ms)   waits: 00:00:00.0010824ms </para><para>
    /// StartWaitTask(2ms)   waits: 00:00:00.0020599ms </para><para>
    /// StartWaitTask(5ms)   waits: 00:00:00.0050710ms </para><para>
    /// StartWaitTask(10ms)  waits: 00:00:00.0100598ms </para><para>
    /// StartWaitTask(20ms)  waits: 00:00:00.0200864ms </para><para>
    /// StartWaitTask(50ms)  waits: 00:00:00.0500930ms </para><para>
    /// StartWaitTask(100ms) waits: 00:00:00.1000608ms </para><para>
    /// </remarks>
    [TestMethod()]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>" )]
    public void TimeSpanWaitShouldDelay()
    {
        System.Text.StringBuilder builder = new();
        TimeSpan expectedTimespan;
        TimeSpan maximumError;
        TimeSpan waitTimespan;
        TimeSpan actualTimespan;
        double[] testIntervals = [1, 2, 5, 10, 20, 50, 100];

        bool yield = true;
        // initialize
        TimeSpan.FromMilliseconds( 1 ).AsyncWait( yield );

        Stopwatch sw = Stopwatch.StartNew();
        foreach ( double testInterval in testIntervals )
        {
            waitTimespan = TimeSpan.FromMilliseconds( testInterval );
            maximumError = waitTimespan.ExpectedWaitClockResolution();
            expectedTimespan = waitTimespan;
            sw.Restart();
            waitTimespan.AsyncWait( yield );
            sw.Stop();
            actualTimespan = sw.Elapsed;
            _ = builder.AppendLine( $"StartWaitTask({testInterval}ms) waits: {actualTimespan}ms" );
            // Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, maximumError, $"Start Wait Task time interval should equal within error {maximumError}" );

        }
        Trace.TraceInformation( builder.ToString() );
    }

    /// <summary>   (Unit Test Method) time span time span wait should delay. </summary>
    /// <remarks>   David, 2021-01-30. <para> 
    /// Yield = False
    /// StartWaitTask(1ms)   waits: 00:00:00.0010939ms internal 00:00:00.0010000ms
    /// StartWaitTask(2ms)   waits: 00:00:00.0020608ms internal 00:00:00.0020000ms
    /// StartWaitTask(5ms)   waits: 00:00:00.0050608ms internal 00:00:00.0050000ms
    /// StartWaitTask(10ms)  waits: 00:00:00.0100781ms internal 00:00:00.0100000ms
    /// StartWaitTask(20ms)  waits: 00:00:00.0201004ms internal 00:00:00.0200000ms
    /// StartWaitTask(50ms)  waits: 00:00:00.0501085ms internal 00:00:00.0500000ms
    /// StartWaitTask(100ms) waits: 00:00:00.1002178ms internal 00:00:00.1000000ms
    /// Yield = True
    /// StartWaitTask(1ms)   waits: 00:00:00.0011354ms internal 00:00:00.0010000ms </para><para>
    /// StartWaitTask(2ms)   waits: 00:00:00.0020714ms internal 00:00:00.0020000ms </para><para>
    /// StartWaitTask(5ms)   waits: 00:00:00.0051486ms internal 00:00:00.0050000ms </para><para>
    /// StartWaitTask(10ms)  waits: 00:00:00.0100803ms internal 00:00:00.0100000ms </para><para>
    /// StartWaitTask(20ms)  waits: 00:00:00.0201377ms internal 00:00:00.0200000ms </para><para>
    /// StartWaitTask(50ms)  waits: 00:00:00.0501807ms internal 00:00:00.0500000ms </para><para>
    /// StartWaitTask(100ms) waits: 00:00:00.1000946ms internal 00:00:00.1000000ms </para><para>
    ///  </para></remarks>
    [TestMethod()]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>" )]
    public void TimeSpanTimeSpanWaitShouldDelay()
    {
        System.Text.StringBuilder builder = new();
        TimeSpan expectedTimespan;
        TimeSpan maximumError;
        TimeSpan waitTimespan;
        TimeSpan actualTimespan;
        TimeSpan internalTimespan;
        double[] testIntervals = [1, 2, 5, 10, 20, 50, 100];

        bool yield = true;

        // initialize
        _ = TimeSpan.FromMilliseconds( 1 ).AsyncWaitTimespan( yield );

        Stopwatch sw = Stopwatch.StartNew();
        foreach ( double testInterval in testIntervals )
        {
            waitTimespan = TimeSpan.FromMilliseconds( testInterval );
            maximumError = waitTimespan.ExpectedWaitClockResolution();
            expectedTimespan = waitTimespan;
            sw.Restart();
            internalTimespan = waitTimespan.AsyncWaitTimespan( yield );
            sw.Stop();
            actualTimespan = sw.Elapsed;
            _ = builder.AppendLine( $"StartWaitTask({testInterval}ms) waits: {actualTimespan}ms internal {internalTimespan}ms" );
            // Asserts.Instance.AreEqual( expectedTimespan, actualTimespan, maximumError, $"Start Wait Task time interval should equal within error {maximumError}" );

        }
        Trace.TraceInformation( builder.ToString() );
    }

    #endregion
}
