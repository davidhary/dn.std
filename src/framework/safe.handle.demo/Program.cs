using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Security;
using Microsoft.Win32.SafeHandles;

namespace SafeHandleDemo;

/// <summary>   my safe file handle. </summary>
/// <remarks>   David, 2020-05-01.
/// https://docs.Microsoft.com/en-us/dotnet/api/system.runtime.InteropServices.SafeHandle?view=NetFramework-4.8
/// when to use a safe handle:
/// https://StackOverflow.com/questions/155780/SafeHandle-in-c-sharp
/// </remarks>
/// </remarks>usage
internal sealed class MySafeFileHandle : SafeHandleZeroOrMinusOneIsInvalid
{
    // Create a SafeHandle, informing the base class
    // that this SafeHandle instance "owns" the handle,
    // and therefore SafeHandle should call
    // our ReleaseHandle method when the SafeHandle
    // is no longer in use.
    public MySafeFileHandle() : base( true )
    { }
    protected override bool ReleaseHandle()
    {
        // Here, we must obey all rules for constrained execution regions.
        return NativeMethods.CloseHandle( this.handle );// If ReleaseHandle failed, it can be reported via the// "releaseHandleFailed" managed debugging assistant (MDA).  This// MDA is disabled by default, but can be enabled in a debugger// or during testing to diagnose handle corruption problems.// We do not throw an exception because most code could not recover// from the problem.
    }
}

[SuppressUnmanagedCodeSecurity()]
internal static class NativeMethods
{
    // Win32 constants for accessing files.
    internal const int GENERIC_READ = unchecked(( int ) 0x80000000);

    // Allocate a file object in the kernel, then return a handle to it.
    [DllImport( "kernel32", SetLastError = true, CharSet = CharSet.Unicode )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Interoperability", "SYSLIB1054:Use 'LibraryImportAttribute' instead of 'DllImportAttribute' to generate P/Invoke marshalling code at compile time", Justification = "<Pending>" )]
    internal static extern MySafeFileHandle CreateFile( string fileName, int dwDesiredAccess,
                                                       FileShare dwShareMode,
                                                       IntPtr securityAttributes_MustBeZero,
                                                       FileMode dwCreationDisposition,
                                                       int dwFlagsAndAttributes, IntPtr hTemplateFile_MustBeZero );

    // Use the file handle.
    [DllImport( "kernel32", SetLastError = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Interoperability", "SYSLIB1054:Use 'LibraryImportAttribute' instead of 'DllImportAttribute' to generate P/Invoke marshalling code at compile time", Justification = "<Pending>" )]
    internal static extern int ReadFile( MySafeFileHandle handle, byte[] bytes,
                                         int numBytesToRead, out int numBytesRead, IntPtr overlapped_MustBeZero );

    // Free the kernel's file object (close the file).
    [DllImport( "kernel32", SetLastError = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Interoperability", "SYSLIB1054:Use 'LibraryImportAttribute' instead of 'DllImportAttribute' to generate P/Invoke marshalling code at compile time", Justification = "<Pending>" )]
    internal static extern bool CloseHandle( IntPtr handle );
}

// The MyFileReader class is a sample class that accesses an operating system
// resource and implements IDisposable. This is useful to show the types of
// transformation required to make your resource wrapping classes
// more resilient. Note the Dispose and Finalize implementations.
// Consider this a simulation of System.IO.FileStream.
public class MyFileReader : IDisposable
{
    // _handle is set to null to indicate disposal of this instance.
    private readonly MySafeFileHandle _handle;

    public MyFileReader( string fileName )
    {
        // Security permission check is not supported by the runtime.
        _ = Path.GetFullPath( fileName );

        // Open a file, and save its handle in _handle.
        // Note that the most optimized code turns into two processor
        // instructions: 1) a call, and 2) moving the return value into
        // the _handle field.  With SafeHandle, the CLR's platform invoke
        // marshaling layer will store the handle into the SafeHandle
        // object in an atomic fashion. There is still the problem
        // that the SafeHandle object may not be stored in _handle, but
        // the real operating system handle value has been safely stored
        // in a critical finalizable object, ensuring against leaking
        // the handle even if there is an asynchronous exception.

        MySafeFileHandle tmpHandle;
        tmpHandle = NativeMethods.CreateFile( fileName, NativeMethods.GENERIC_READ,
            FileShare.Read, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero );

        // An Async exception here will cause us to run our finalizer with
        // a null _handle, but MySafeFileHandle's ReleaseHandle code will
        // be invoked to free the handle.

        // This call to Sleep, run from the fault injection code in Main,
        // will help trigger a race. But it will not cause a handle leak
        // because the handle is already stored in a SafeHandle instance.
        // Critical finalization then guarantees that freeing the handle,
        // even during an unexpected AppDomain unload.
        Thread.Sleep( 500 );
        this._handle = tmpHandle;  // Makes _handle point to a critical finalizable object.

        // Determine if file is opened successfully.
        if ( this._handle.IsInvalid )
        {
            throw new Win32Exception( Marshal.GetLastWin32Error(), fileName );
        }
    }

    public void Dispose()  // Follow the Dispose pattern - public non-virtual.
    {
        this.Dispose( true );
        GC.SuppressFinalize( this );
    }

    // No finalizer is needed. The finalizer on SafeHandle
    // will clean up the MySafeFileHandle instance,
    // if it hasn't already been disposed.
    // However, there may be a need for a subclass to
    // introduce a finalizer, so Dispose is properly implemented here.
    protected virtual void Dispose( bool disposing )
    {
        // Note there are three interesting states here:
        // 1) CreateFile failed, _handle contains an invalid handle
        // 2) We called Dispose already, _handle is closed.
        // 3) _handle is null, due to an async exception before
        //    calling CreateFile. Note that the finalizer runs
        //    if the constructor fails.
        if ( this._handle != null && !this._handle.IsInvalid )
        {
            // Free the handle
            this._handle.Dispose();
        }
        // SafeHandle records the fact that we've called Dispose.
    }

    public byte[] ReadContents( int length )
    {
        ObjectDisposedException.ThrowIf( this._handle.IsInvalid, "FileReader is closed" );

        // This sample code will not work for all files.
        byte[] bytes = new byte[length];
        int r = NativeMethods.ReadFile( this._handle, bytes, length, out int numRead, IntPtr.Zero );
        // Since we removed MyFileReader's finalizer, we no longer need to
        // call GC.KeepAlive here.  Platform invoke will keep the SafeHandle
        // instance alive for the duration of the call.
        if ( r == 0 )
            throw new Win32Exception( Marshal.GetLastWin32Error() );
        if ( numRead < length )
        {
            byte[] newBytes = new byte[numRead];
            Array.Copy( bytes, newBytes, numRead );
            bytes = newBytes;
        }
        return bytes;
    }
}
/// <summary>   A program. </summary>
/// <remarks>
/// The following code example creates a custom safe handle for an operating system file handle,
/// deriving from SafeHandleZeroOrMinusOneIsInvalid
/// https://docs.Microsoft.com/en-us/dotnet/api/Microsoft.win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid?view=NetFramework-4.8
/// It reads bytes from a file and displays
/// their hexadecimal values. It also contains a fault testing harness that causes the thread to
/// abort, but the handle value is freed. When using an IntPtr to represent handles, the handle
/// is occasionally leaked due to the asynchronous thread abort.
/// 
/// You will need a text file in the same folder as the compiled application.Assuming that you
/// name the application "HexViewer", the command line usage is:
/// 
/// <see cref="HexViewer{filename}" />; -Fault
/// 
/// Optionally specify -Fault to intentionally attempt to leak the handle by aborting the thread
/// in a certain window. Use the Windows PERFMON.exe tool to monitor handle counts while injecting
/// faults.
/// https://docs.Microsoft.com/en-us/dotnet/api/system.runtime.InteropServices.SafeHandle?view=NetFramework-4.8
/// 
/// </remarks>
internal static class Program
{
    // Testing harness that injects faults.
    private static bool _printToConsole;
    private static bool _workerStarted;

    private static void Usage()
    {
        Console.WriteLine( "Usage:" );
        // Assumes that application is named SafeHandleDemo"
        Console.WriteLine( "SafeHandleDemo <fileName> [-fault]" );
        Console.WriteLine( " -fault Runs hex viewer repeatedly, injecting faults." );
    }

    private static void ViewInHex( object fileName )
    {
        _workerStarted = true;
        byte[] bytes;
        using ( MyFileReader reader = new( ( string ) fileName ) )
        {
            bytes = reader.ReadContents( 20 );
        }  // Using block calls Dispose() for us here.

        if ( _printToConsole )
        {
            // Print up to 20 bytes.
            int printNBytes = Math.Min( 20, bytes.Length );
            Console.WriteLine( "First {0} bytes of {1} in hex", printNBytes, fileName );
            for ( int i = 0; i < printNBytes; i++ )
                Console.Write( "{0:x} ", bytes[i] );
            Console.WriteLine();
        }
    }

    private static void Main( string[] args )
    {
        if ( args.Length == 0 || args.Length > 2 ||
            args[0] == "-?" || args[0] == "/?" )
        {
            Usage();
            return;
        }

        string fileName = args[0];
        bool injectFaultMode = args.Length > 1;
        if ( !injectFaultMode )
        {
            _printToConsole = true;
            ViewInHex( fileName );
        }
        else
        {
            Console.WriteLine( @"Injecting faults - watch handle count in PERFMON (press CTRL+C when done)
Open Performance monitor; under show select add counter; Select Process; Select Hex Viewer; Select Handle Count for the process." );
            int numIterations = 0;
            while ( true )
            {
                _workerStarted = false;
                Thread t = new( new ParameterizedThreadStart( ViewInHex! ) );
                t.Start( fileName );
                Thread.Sleep( 1 );
                while ( !_workerStarted )
                {
                    Thread.Sleep( 0 );
                }
                // t.Abort();  // Normal applications should not do this.
                numIterations++;
                if ( numIterations % 10 == 0 )
                    GC.Collect();
                if ( numIterations % 10000 == 0 )
                    Console.WriteLine( numIterations );
            }
        }
    }
}
