using cc.isr.Std.Threading;

namespace ReplacingThreadAbortDemo;
internal static class Program
{
    internal static void Main()
    {
        _processDuration = 1000;
        int msTimeout = _processDuration / 2;
        System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
        bool isExecuted = WaitFor.TryCallWithTimeout(
           ProcessMethod,
           TimeSpan.FromMilliseconds( msTimeout ), // Timeout < Process Duration => ProcessMethod() gets Canceled
                                                   // Timeout > Process Duration => ProcessMethod() gets Executed
           out int result );
        Console.WriteLine( $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_pollInterval}ms {(isExecuted ? "Executed" : "Cancelled")} after {sw.ElapsedMilliseconds}ms with result = {result}" );

        msTimeout = _processDuration * 2;
        sw.Restart();
        isExecuted = WaitFor.TryCallWithTimeout(
           ProcessMethod,
           TimeSpan.FromMilliseconds( msTimeout ), // Timeout < Process Duration => ProcessMethod() gets Canceled
                                                   // Timeout > Process Duration => ProcessMethod() gets Executed
           out result );
        Console.WriteLine();
        Console.WriteLine( $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_pollInterval}ms {(isExecuted ? "Executed" : "Cancelled")} after {sw.ElapsedMilliseconds}ms with result = {result}" );

        _processDuration = 200;
        msTimeout = _processDuration / 2;
        sw.Restart();
        isExecuted = WaitFor.TryCallWithTimeout(
           ProcessMethod,
           TimeSpan.FromMilliseconds( msTimeout ), // Timeout < Process Duration => ProcessMethod() gets Canceled
                                                   // Timeout > Process Duration => ProcessMethod() gets Executed
           out result );
        Console.WriteLine( $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_pollInterval}ms {(isExecuted ? "Executed" : "Cancelled")} after {sw.ElapsedMilliseconds}ms with result = {result}" );

        msTimeout = _processDuration * 2;
        sw.Restart();
        isExecuted = WaitFor.TryCallWithTimeout(
           ProcessMethod,
           TimeSpan.FromMilliseconds( msTimeout ), // Timeout < Process Duration => ProcessMethod() gets Canceled
                                                   // Timeout > Process Duration => ProcessMethod() gets Executed
           out result );
        Console.WriteLine();
        Console.WriteLine( $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_pollInterval}ms {(isExecuted ? "Executed" : "Cancelled")} after {sw.ElapsedMilliseconds}ms with result = {result}" );

    }

    private static int _pollInterval = 100;
    private static int _processDuration = 200;

    private static int ProcessMethod( CancellationToken ct )
    {
        _pollInterval = _processDuration / 10;
        System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
        while ( sw.ElapsedMilliseconds < _processDuration )
        {
            Thread.Sleep( _pollInterval );
            // co-operative cancellation implies periodically check IsCancellationRequested 
            if ( ct.IsCancellationRequested ) { throw new TaskCanceledException(); }
        }
        return 123; // the result 
    }

}
