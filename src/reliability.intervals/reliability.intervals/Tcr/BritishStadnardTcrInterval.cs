namespace cc.isr.Std.ReliabilityIntervals;
/// <summary>
/// British Standard (BS EN 60062) Temperature coefficient acceptance intervals.
/// </summary>
/// <remarks>
/// This class is sealed to ensure that the hash value of its elements is not used
/// by two instances with different hash value set. (c) 2014 Integrated Scientific
/// Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-05-19 </para>
/// </remarks>
public sealed class BritishStandardTcrInterval : AcceptanceInterval
{
    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="code"> The temperature coefficient code. </param>
    public BritishStandardTcrInterval( string code ) : this( EmptyCode, 0 )
    {
        if ( string.IsNullOrWhiteSpace( code ) )
        {
            this.Parsed = false;
        }
        else
        {
            BritishStandardTcrInterval info = BritishStandardTcrInterval.Empty;
            string details = string.Empty;
            this.Parsed = TryParse( code, ref info, ref details );
            if ( this.Parsed )
            {
                this.Code = code;
                this.RelativeInterval = new TcrReliabilityInterval( info.RelativeInterval );
                this.Caption = info.Caption;
                this.CompoundCaption = BuildCompoundCaptionFormat( code, this.Caption );
            }
        }
    }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="code">             The temperature coefficient code. </param>
    /// <param name="lowerCoefficient"> The lower Coefficient. </param>
    /// <param name="upperCoefficient"> The upper Coefficient. </param>
    /// <param name="caption">          The caption. </param>
    public BritishStandardTcrInterval( string code, double lowerCoefficient, double upperCoefficient, string caption ) : base( code, new TcrReliabilityInterval( lowerCoefficient, upperCoefficient ), caption )
    { }

    /// <summary> Specialized constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="value"> The value. </param>
    public BritishStandardTcrInterval( AcceptanceInterval value ) : this( value.Code, value.RelativeInterval, value.Caption )
    { }

    /// <summary> Constructor for symmetric range and standard caption. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="code">        The tolerance code. </param>
    /// <param name="coefficient"> The coefficient. </param>
    public BritishStandardTcrInterval( string code, ReliabilityInterval coefficient ) : base( code, coefficient, BritishStandardTcr.BuildCaption( coefficient.HighEndPoint ) )
    { }

    /// <summary> Constructor for symmetric range. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="code">        The temperature coefficient code. </param>
    /// <param name="coefficient"> The coefficient. </param>
    /// <param name="caption">     The caption. </param>
    public BritishStandardTcrInterval( string code, ReliabilityInterval coefficient, string caption ) : base( code, coefficient, caption )
    { }

    /// <summary> Constructor for symmetric range and standard caption. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="code">        The tolerance code. </param>
    /// <param name="coefficient"> The coefficient. </param>
    public BritishStandardTcrInterval( string code, double coefficient ) : this( code, coefficient, BritishStandardTcr.BuildCaption( coefficient ) )
    { }

    /// <summary> Constructor for symmetric range. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="code">        The temperature coefficient code. </param>
    /// <param name="coefficient"> The coefficient. </param>
    /// <param name="caption">     The caption. </param>
    public BritishStandardTcrInterval( string code, double coefficient, string caption ) : this( code, new ToleranceReliabilityInterval( coefficient ), caption )
    { }

    /// <summary> The clone Constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="value"> The value. </param>
    public BritishStandardTcrInterval( BritishStandardTcrInterval value ) : this( value.Code, value.RelativeInterval, value.Caption )
    { }

    /// <summary> Gets the empty value. </summary>
    /// <value> The empty. </value>
    public static BritishStandardTcrInterval Empty => new( EmptyCode, 0d );

    /// <summary> The dictionary. </summary>
    private static AcceptanceIntervalCollection _tcrIntervalCollection = [];

    /// <summary> Gets the <see cref="AcceptanceIntervalCollection">keyed collection</see>
    /// of British standard TCR intervals. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <returns> A Dictionary(Of String, CodedIntervalBase). </returns>
    public static AcceptanceIntervalCollection BritishStandardTcrIntervalCollection
    {
        get
        {
            if ( _tcrIntervalCollection is null || _tcrIntervalCollection.Count == 0 )
            {
                BuildBritishStandardTcrIntervalCollection();
            }
            return _tcrIntervalCollection!;
        }
    }

    /// <summary> Builds the keyed British standard TCR interval collection. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="values"> The values. </param>
    public static void BuildBritishStandardTcrIntervalCollection( AcceptanceIntervalCollection values )
    {
        _tcrIntervalCollection = [];
        _tcrIntervalCollection.Populate( values );
    }

    /// <summary> Builds the keyed British standard TCR interval collection. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    private static void BuildBritishStandardTcrIntervalCollection()
    {
        AcceptanceIntervalCollection dix = [];
        foreach ( KeyValuePair<string, double> kvp in BritishStandardTcr.BritishStandardTcrDictionary )
        {
            dix.Add( new BritishStandardTcrInterval( kvp.Key, kvp.Value ) );
        }

        BuildBritishStandardTcrIntervalCollection( dix );
    }

    /// <summary>   Query if 'code' is a known British standard tcr. </summary>
    /// <remarks>   2023-04-04. </remarks>
    /// <param name="code"> The code. </param>
    /// <returns>   True if known British standard tcr interval, false if not. </returns>
    public static bool IsKnownBritishStandardTcr( string code )
    {
        return BritishStandardTcr.BritishStandardTcrDictionary.ContainsKey( code );
    }

    /// <summary> Parses. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="code"> The code. </param>
    /// <returns> A British standard TCR Interval for the specified code or an <see cref="Empty"/> interval if the
    ///           code is not value.. </returns>
    public static BritishStandardTcrInterval Parse( string code )
    {
        AcceptanceIntervalCollection acceptanceIntervals = BritishStandardTcrIntervalCollection;
        return acceptanceIntervals.Contains( code )
                ? new BritishStandardTcrInterval( acceptanceIntervals[code] )
                : Empty;
    }

    /// <summary> Tries to parse a coded value. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="code">    The code. </param>
    /// <param name="value">   [in,out] The Margin Factor. </param>
    /// <param name="details"> [in,out] The details. </param>
    /// <returns> <c>true</c> if Margin Factor code can be parsed. </returns>
    public static bool TryParse( string code, ref BritishStandardTcrInterval value, ref string details )
    {
        bool affirmative = false;
        AcceptanceIntervalCollection acceptanceIntervals = BritishStandardTcrIntervalCollection;
        if ( acceptanceIntervals.Contains( code ) )
        {
            value = new BritishStandardTcrInterval( acceptanceIntervals[code] );
            affirmative = true;
        }
        else
        {
            details = $"{nameof( BritishStandardTcrInterval.GetType )}.{nameof( BritishStandardTcrInterval.Code )} '{code}' is unknown";
        }

        return affirmative;
    }

    /// <summary> Gets the sentinel indicating if the entered value is Empty defined. </summary>
    /// <value> <c>true</c> if unknown code. </value>
    public override bool IsEmptyCode => string.Equals( this.Code, EmptyCode, StringComparison.Ordinal );

    /// <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
    /// <value> <c>true</c> if unknown code. </value>
    public override bool IsUnknownCode => string.Equals( this.Code, UnknownCode, StringComparison.Ordinal );

    /// <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
    /// <value> <c>true</c> if unknown code. </value>
    public override bool IsUserCode => string.Equals( this.Code, UserCode, StringComparison.Ordinal );
}
