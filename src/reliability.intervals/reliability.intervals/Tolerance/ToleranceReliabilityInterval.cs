namespace cc.isr.Std.ReliabilityIntervals;

/// <summary> Tolerance reliability interval. </summary>
/// <remarks>
/// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2015-01-09 </para>
/// </remarks>
public class ToleranceReliabilityInterval : ReliabilityInterval
{
    #region " constructor"

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public ToleranceReliabilityInterval() : base() => this.Epsilon = DefaultEpsilon;

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="endPoint"> The end point of a symmetrical interval. </param>
    public ToleranceReliabilityInterval( double endPoint ) : base( -endPoint, endPoint, DefaultEpsilon )
    { }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="lowEndPoint">  The low end point. </param>
    /// <param name="highEndPoint"> The high end point. </param>
    public ToleranceReliabilityInterval( double lowEndPoint, double highEndPoint ) : base( lowEndPoint, highEndPoint, DefaultEpsilon )
    { }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="value"> The value. </param>
    public ToleranceReliabilityInterval( ToleranceReliabilityInterval value ) : base( value )
    { }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="value"> The value. </param>
    public ToleranceReliabilityInterval( ReliabilityInterval value ) : base( value )
    { }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="lowEndPoint">  The low end point. </param>
    /// <param name="highEndPoint"> The high end point. </param>
    public ToleranceReliabilityInterval( double? lowEndPoint, double? highEndPoint ) : base( lowEndPoint, highEndPoint, DefaultEpsilon )
    { }

    /// <summary> Creates a new object that is a copy of the current instance. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <returns> A new object that is a copy of this instance. </returns>
    public override object Clone()
    {
        return new ToleranceReliabilityInterval( this );
    }

    /// <summary> Gets the empty interval. </summary>
    /// <value> The empty. </value>
    public static new ReliabilityInterval Empty => new ToleranceReliabilityInterval( 0d, 0d );

    #endregion

    /// <summary> Gets the default minimum detectable difference. </summary>
    /// <value> The epsilon (1E-9). </value>
    public static double DefaultEpsilon { get; set; } = 0.000000001d;
}
