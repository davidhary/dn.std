# About

cc.isr.Std.ReliabilityIntervals is a .Net library supporting 
Reliability Intervals operations.

# How to Use

Parse a part number:
```
string partNumber = "SPD-6119-3831";
var parser = new PartSpecificationParser();
parser.Parse( partNumber );
string propertyName = nameof( PartSpecificationParser.Parsed );
Assert.IsTrue( parser.Parsed, $"{propertyName}" );
propertyName = nameof( PartSpecificationParser.ParseFailed );
Assert.IsFalse( parser.ParseFailed, $"{propertyName}" );
propertyName = nameof( PartSpecificationTrait.PartNumber );
Assert.AreEqual( partNumber, parser.Traits[PartSpecificationTrait.PartNumber], $"{propertyName} should match" );
```

Query historical reference date from the part historical data.
```
string fileName = "R:\\Ohmni\\Taper\\Files\\PartHistoricalReference.csv";
string partNumber = "PFC-W1206PC-03-2003-B";
DateTime date = cc.isr.Ohmni.Core.PartHistoricalReference.QueryPartHistoricalReference( partNumber, fileName ).HistoricalReferenceDate;
```

Query case code power from a CSV file.
```
double power = cc.isr.Ohmni.Core.CaseCodePower.QueryCaseCodePower( "1206, "R:\\Ohmni\\Taper\\Files\\CaseCodePower.csv" ).Power;
```

# Key Features

* Defines and tests values against acceptance and reliability intervals.

# Main Types

The main types provided by this library are:

* _AcceptanceInterval_ A sealed class defining the acceptance Interval.
* _NominalReliabilityInterval_ Nominal reliability interval.
* _ReliabilityInterval_ A reliability interval.
* _BritishStadnardTcr_ British Standard (BS EN 60062) Temperature coefficient acceptance intervals.
* _BritishStadnardTcrInterval_ British Standard (BS EN 60062) Temperature coefficient acceptance intervals.
* _TcrAcceptanceInterval_ Defines the Temperature Coefficient of Resistance (TCR) acceptance interval.
* _TcrReliabilityInterval_ TCR reliability interval.
* _ToleranceAcceptanceInterval_ A tolerance acceptance interval.
* _ToleranceReliabilityInterval_ Tolerance reliability interval.

# Feedback

cc.isr.Std.ReliabilityIntervals is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Standard Framework Repository].

[Standard Framework Repository]: https://bitbucket.org/davidhary/dn.std

