namespace cc.isr.Std.Testers;
internal sealed class Program
{
    public static void Main()
    {
        GenerateCombinationsTest();
    }

    private static void GenerateCombinationsTest()
    {
        List<List<int>> collectionOfSeries = [new( [1, 2, 3, 4, 5] ), new( [0, 1] ), new( [6, 3] ), new( [1, 3, 5] )];
        List<List<int>> combinations = cc.isr.Std.CombinationsExtensions.CombinationsExtensionMethods.GenerateCombinations( collectionOfSeries );
        Display( combinations );
    }

    private static void Display<T>( List<List<T>> generatedCombinations )
    {
        int index = 0;
        foreach ( List<T> generatedCombination in generatedCombinations )
        {
            Console.Write( "{0}\t:", System.Threading.Interlocked.Increment( ref index ) );
            foreach ( T i in generatedCombination )
                Console.Write( "{0,3}", i );
            Console.WriteLine();
        }

        _ = Console.ReadKey();
    }
}
