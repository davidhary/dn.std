using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Timers;

namespace cc.isr.Std.YieldCounters;

/// <summary> A yield counter using a stopwatch and optional timer. </summary>
/// <remarks>
/// Time, which is kept in Date Time Offset, is saved to bases as Date Time Offset where
/// supported. Otherwise, time is stored as Coordinated Universal Time (UTC). <para>
/// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-10-08 </para>
/// </remarks>
[DebuggerDisplay( "Total = {TotalCount}" )]
public class YieldStopwatch : YieldCounter
{
    #region " construction and cleanup "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public YieldStopwatch() : base()
    {
        this.ResetKnownStateThis();
        this.ClearKnownStateThis();
    }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    public YieldStopwatch( [NotNull] YieldStopwatch value ) : base( value )
    {
        this.ResetKnownStateThis();
        this.ClearKnownStateThis();
        this.InitializeKnownStateThis( value.StartTime );
        this.Interval = value.Interval;
        this.Stopwatch = new OffsetStopwatch( value.Stopwatch );
        if ( value.Stopwatch.IsRunning )
        {
        }
        else
        {
            this.Stopwatch.Reset();
        }
    }

    /// <summary> Creates a new Yield Stopwatch. </summary>
    /// <remarks> Helps implement CA2000. </remarks>
    /// <returns> A  <see cref="YieldStopwatch"/>. </returns>
    public static new YieldStopwatch Create()
    {
        YieldStopwatch result;
        try
        {
            result = new YieldStopwatch();
        }
        catch
        {
            throw;
        }

        return result;
    }

    #endregion

    #region " publish "

    /// <summary> Publishes this object. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public override void Publish()
    {
        base.Publish();
        this.NotifyPropertyChanged( nameof( YieldStopwatch.StartTime ) );
        this.NotifyPropertyChanged( nameof( YieldStopwatch.EndTime ) );
    }

    #endregion

    #region " reset and clear "

    /// <summary>
    /// Clears to known (clear) state; Clears select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [MemberNotNull( nameof( Stopwatch ) )]
    private void ClearKnownStateThis()
    {
        this.StartTime = DateTimeOffset.MinValue;
        this.Stopwatch = new OffsetStopwatch( TimeSpan.Zero );
    }

    /// <summary>
    /// Clears to known (clear) state; Clears select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public override void ClearKnownState()
    {
        base.ClearKnownState();
        // if clearing and value cleared, we need to publish to make sure displays get updated.
        this.ClearKnownStateThis();
        this.Publish();
    }

    /// <summary> Resets to known (default/instantiated) state. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [MemberNotNull( nameof( ElapsedTimer ) )]
    [MemberNotNull( nameof( Stopwatch ) )]
    private void ResetKnownStateThis()
    {
        this.ClearKnownStateThis();
        this.ElapsedTimer = new System.Timers.Timer();
        this.ElapsedTimer.Stop();
    }

    /// <summary> Resets the know state. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public override void ResetKnownState()
    {
        base.ResetKnownState();
        this.ResetKnownStateThis();
        this.Publish();
    }

    /// <summary>
    /// Initializes to known (Initialize) state; Initializes select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="startTime"> The start time. </param>
    private void InitializeKnownStateThis( DateTimeOffset startTime )
    {
        this.ClearKnownStateThis();
        this.StartTime = startTime;
    }

    /// <summary>
    /// Initializes to known (Initialize) state; Initializes select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="startTime"> The start time. </param>
    public void InitializeKnownState( DateTimeOffset startTime )
    {
        this.InitializeKnownStateThis( startTime );
    }

    /// <summary>
    /// Initializes to known (Initialize) state; Initializes select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void InitializeKnownState()
    {
        this.InitializeKnownState( DateTimeOffset.Now );
    }

    #endregion

    #region " time elements "

    /// <summary> Gets the stopwatch. </summary>
    /// <value> The stopwatch. </value>
    public OffsetStopwatch Stopwatch { get; set; }

    /// <summary> Gets the is started. </summary>
    /// <value> The is started. </value>
    public bool IsStarted => this.StartTime > DateTimeOffset.MinValue;

    /// <summary> Gets the start time. </summary>
    /// <value> The start time. </value>
    public DateTimeOffset StartTime { get; private set; }

    /// <summary> Gets the end time. </summary>
    /// <value> The end time. </value>
    public DateTimeOffset EndTime => this.StartTime.Add( this.Stopwatch.Elapsed() );

    #endregion

    #region " time actions "

    /// <summary> Resets and starts. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void Start()
    {
        this.InitializeKnownState();
        this.Stopwatch.Start();
        if ( this.Interval > TimeSpan.Zero )
        {
            this.ElapsedTimer.Start();
        }

        this.NotifyPropertyChanged( nameof( YieldStopwatch.StartTime ) );
    }

    /// <summary> Finishes measuring elapsed time. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void Finish()
    {
        this.ElapsedTimer.Stop();
        this.Stopwatch.Stop();
        this.NotifyPropertyChanged( nameof( YieldStopwatch.EndTime ) );
    }

    /// <summary> Resumes counting after a <see cref="Finish()">stop</see>. </summary>
    public void Resume()
    {
        if ( this.Interval > TimeSpan.Zero )
        {
            this.ElapsedTimer.Start();
        }

        if ( !this.Stopwatch.IsRunning )
        {
            this.Stopwatch.Start();
        }
    }

    #endregion

    #region " rate "

    /// <summary> Hourly rate. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> A Double. </returns>
    public double HourlyRate()
    {
        return this.HourlyRate( this.Stopwatch.Elapsed() );
    }

    #endregion

    #region " timer "

    private System.Timers.Timer _elapsedTimer = default!;

    /// <summary> Elapsed time Timer. </summary>
    /// <remarks>
    /// The synchronization context is captured as part of the property change and other event
    /// handlers and is no longer needed here.
    /// </remarks>
    private System.Timers.Timer ElapsedTimer
    {
        [MethodImpl( MethodImplOptions.Synchronized )]
        get => this._elapsedTimer;

        [MethodImpl( MethodImplOptions.Synchronized )]
        set
        {
            if ( this._elapsedTimer is not null )
                this._elapsedTimer.Elapsed -= this.Timer_Elapsed;

            this._elapsedTimer = value;
            if ( this._elapsedTimer is not null )
                this._elapsedTimer.Elapsed += this.Timer_Elapsed;
        }
    }

    /// <summary> Gets or sets the interval. </summary>
    /// <value> The interval. </value>
    public TimeSpan Interval
    {
        get => TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerMillisecond * this.ElapsedTimer.Interval) );
        set => this.ElapsedTimer.Interval = value.TotalMilliseconds;
    }

    /// <summary>   Event handler. Called by Timer for elapsed events. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Elapsed event information. </param>
    private void Timer_Elapsed( object? sender, ElapsedEventArgs e )
    {
        this.NotifyPropertyChanged( nameof( YieldStopwatch.ElapsedTimeCaption ) );
    }

    #endregion

    #region " format "

    /// <summary> Gets the ElapsedTime format. </summary>
    /// <value> The ElapsedTime format. </value>
    public string ElapsedTimeFormat { get; set; } = @"d\.hh\:mm\:ss\.fff";

    /// <summary> Gets the start time caption. </summary>
    /// <value> The start time caption. </value>
    public string ElapsedTimeCaption => this.Stopwatch.Elapsed().ToString( this.ElapsedTimeFormat );

    #endregion
}
