using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

namespace cc.isr.Std.YieldCounters;

/// <summary> A yield bin counter. </summary>
/// <remarks>
/// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-10-07 </para>
/// </remarks>
[DebuggerDisplay( "Total = {TotalCount}" )]
public class YieldLimitBinCounter : YieldCounter
{
    #region " construction "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public YieldLimitBinCounter() : base()
    {
        this.ResetKnownStateThis();
        this.ClearKnownStateThis();
    }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    public YieldLimitBinCounter( [NotNull] YieldLimitBinCounter value ) : base( value )
    {
        this.ResetKnownStateThis();
        this.ClearKnownStateThis();
        this.GoodBinNumber = value.GoodBinNumber;
        this.HighBinNumber = value.HighBinNumber;
        this.LowBinNumber = value.LowBinNumber;
        this.LowerLimit = value.LowerLimit;
        this.UpperLimit = value.UpperLimit;
        this._binNumber = value.BinNumber;
        foreach ( KeyValuePair<int, int> kvp in value.BinCountDictionary )
        {
            this.BinCountDictionary![kvp.Key] = kvp.Value;
            this._binNumber = kvp.Key;
        }
    }

    #endregion

    #region " reset and clear "

    /// <summary>
    /// Clears to known (clear) state; Clears select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [MemberNotNull( nameof( BinCountDictionary ) )]
    private void ClearKnownStateThis()
    {
        this.BinCountDictionary = new Dictionary<int, int>();
        for ( int i = 0; i <= 30; i++ )
        {
            this.BinCountDictionary.Add( i, 0 );
        }
    }

    /// <summary>
    /// Clears to known (clear) state; Clears select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public override void ClearKnownState()
    {
        this.ClearKnownStateThis();
        base.ClearKnownState();
    }

    /// <summary> Publishes this object. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public override void Publish()
    {
        this.NotifyPropertyChanged( nameof( YieldLimitBinCounter.BinNumber ) );
    }

    /// <summary> Resets the know state. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    private void ResetKnownStateThis()
    {
        this.GoodBinNumber = 0;
        this.HighBinNumber = 1;
        this.LowBinNumber = 2;
        this.LowerLimit = -1;
        this.UpperLimit = 1d;
    }

    /// <summary> Resets the know state. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public override void ResetKnownState()
    {
        this.ResetKnownStateThis();
        base.ResetKnownState();
    }

    #endregion

    #region " bin counter "

    /// <summary> Gets or sets a dictionary of bin counts. </summary>
    /// <value> A Dictionary of bin counts. </value>
    private IDictionary<int, int> BinCountDictionary { get; set; }

    /// <summary>
    /// Indexer to get or set items within this collection using array index syntax.
    /// </summary>
    /// <param name="binNumber">    The bin number. </param>
    /// <returns>   The indexed item. </returns>
    public int this[int binNumber]
    {
        get => this.BinCountDictionary[binNumber];
        set
        {
            this.BinCountDictionary[binNumber] = value;
            this.BinNumber = binNumber;
        }
    }

    /// <summary> Gets or sets the number of bins. </summary>
    /// <value> The number of bins. </value>
    public int GetBinCount( int binNumber )
    {
        return this.BinCountDictionary[binNumber];
    }

    /// <summary>   Sets bin count. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    /// <param name="binNumber">    The bin number. </param>
    /// <param name="value">        The value. </param>
    public void SetBinCount( int binNumber, int value )
    {
        this.BinCountDictionary[binNumber] = value;
        this.BinNumber = binNumber;
    }

    /// <summary> The bin number. </summary>
    private int _binNumber;

    /// <summary> Gets or sets the bin number. </summary>
    /// <value> The bin number. </value>
    public int BinNumber
    {
        get => this._binNumber;
        set => _ = this.SetProperty( ref this._binNumber, value );
    }

    /// <summary> Gets or sets the good bin number. </summary>
    /// <value> The good bin number. </value>
    public int GoodBinNumber { get; set; }

    /// <summary> Adds a value. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    public void AddValue( double? value )
    {
        if ( value.HasValue )
        {
            this.AddValue( value );
        }
        else
        {
            this.InvalidCount += 1;
            this.TotalCount += 1;
        }
    }

    #endregion

    #region " limit bins "

    /// <summary> Gets or sets the high bin number. </summary>
    /// <value> The high bin number. </value>
    public int HighBinNumber { get; set; }

    /// <summary> Gets or sets the low bin number. </summary>
    /// <value> The low bin number. </value>
    public int LowBinNumber { get; set; }

    /// <summary> Gets or sets the lower limit. </summary>
    /// <value> The lower limit. </value>
    public double LowerLimit { get; set; }

    /// <summary> Gets or sets the upper limit. </summary>
    /// <value> The upper limit. </value>
    public double UpperLimit { get; set; }

    /// <summary> Adds a value. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    public void AddValue( double value )
    {
        int bin = value > this.UpperLimit ? this.HighBinNumber : value < this.LowerLimit ? this.LowBinNumber : this.GoodBinNumber;
        this.BinCountDictionary[bin] += 1;
        this.TotalCount += 1;
    }

    #endregion
}
