
namespace cc.isr.Std.RedundancyCheck;

/// <summary> Static class to compute Cyclic Redundancy Check CRC-16, also called CRC-16-IBM, and CRC-ANSI. </summary>
/// <remarks>
/// David, 2016-05-07. Source: https://code.google.com/p/free-dotnet-ModBus/. Each Modbus RTU
/// message is terminated with two error checking bytes called a CRC or Cyclic Redundancy Check.
/// <para>
/// (c) 2013 Simone ASSUNTI. All rights reserved.</para><para>
/// Licensed under The MIT License.</para>
/// </remarks>
public sealed class CRC16
{
    /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
    /// <remarks> David, 2020-11-11. </remarks>
    private CRC16()
    { }
    #region " modulo functions "

    /// <summary>
    /// Base reversed polynomial representation.
    /// </summary>
    private const ushort POLY = 0xA001;

    /// <summary>
    /// CRC 16 table
    /// </summary>
    private static ushort[]? _table;

    /// <summary> Initialize the CRC table. </summary>
    /// <remarks> https://en.Wikipedia.org/wiki/Cyclic_redundancy_check. </remarks>
    private static void InitializeTable()
    {
        ushort crc, c;
        if ( _table is null )
        {
            _table = new ushort[256];
            for ( int ii = 0; ii <= 255; ii++ )
            {
                crc = 0;
                c = ( ushort ) ii;
                for ( int jj = 0; jj <= 7; jj++ )
                {
                    crc = ((crc ^ c) & 0x1) == 0x1 ? ( ushort ) ((crc >> 1) ^ POLY) : ( ushort ) (crc >> 1);
                    c = ( ushort ) (c >> 1);
                }

                _table[ii] = crc;
            }
        }
    }

    /// <summary> Updates a CRC value. </summary>
    /// <remarks> David, 2020-11-11. </remarks>
    /// <param name="crc"> Actual CRC value. </param>
    /// <param name="bt">  Data byte. </param>
    /// <returns> Computed CRC. </returns>
    private static ushort Update( ushort crc, byte bt )
    {
        ushort shortByte = ( ushort ) (0xFF & bt);
        InitializeTable();
        ushort tmp = ( ushort ) (crc ^ shortByte);
        crc = ( ushort ) ((crc >> 8) ^ _table![tmp & 0xFF]);
        return crc;
    }

    /// <summary> Calculated buffer CRC16. </summary>
    /// <remarks> David, 2020-11-11. </remarks>
    /// <param name="buffer"> Data Buffer. </param>
    /// <param name="offset"> Buffer offset. </param>
    /// <param name="length"> Data length. </param>
    /// <returns> Computed CRC. </returns>
    [CLSCompliant( false )]
    public static ushort Evaluate( byte[] buffer, int offset, int length )
    {
        ushort crc = 0xFFFF;
        for ( int ii = 0, loopTo = length - 1; ii <= loopTo; ii++ )
            crc = Update( crc, buffer[offset + ii] );
        return crc;
    }

    #endregion
}
