# About

cc.isr.Std.Primitive is a .Net library supporting Primitive objects such as line and range.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

cc.isr.Std.Primitive is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Standard Framework Repository].

[Standard Framework Repository]: https://bitbucket.org/davidhary/dn.std

