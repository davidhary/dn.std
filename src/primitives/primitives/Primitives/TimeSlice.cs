namespace cc.isr.Std.Primitives;

/// <summary> Time-Slice - a time span time keeper. </summary>
/// <remarks>
/// Time, which is kept in Date Time Offset, is saved to bases as Date Time Offset where
/// supported. Otherwise, time is stored as Coordinated Universal Time (UTC). <para>
/// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-09-11, </para><para>
/// David, 2014-11-19, 2.1.5436. </para>
/// </remarks>
public class TimeSlice
{
    #region " construction and cleanup "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="minimumTime"> The <see cref="StartTime"/> if started or the
    /// <see cref="MinimumTime"/> </param>
    public TimeSlice( DateTimeOffset minimumTime ) : base()
    {
        this.MinimumTime = minimumTime;
        this.ResetKnownStateThis();
    }

    /// <summary> Validated time slice. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="timeSlice"> The time slice. </param>
    /// <returns> A time slice. </returns>
    public static TimeSlice ValidatedTimeSlice( TimeSlice timeSlice )
    {
        return timeSlice is null ? throw new ArgumentNullException( nameof( timeSlice ) ) : timeSlice;
    }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    public TimeSlice( TimeSlice value ) : this( ValidatedTimeSlice( value ).MinimumTime )
    {
        if ( value is not null )
        {
            this.InitializeKnownStateThis( value.StartTime, value.EndTime );
        }
    }

    #endregion

    #region " reset and clear "

    /// <summary>
    /// Clears to known (clear) state; Clears select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    private void ClearKnownStateThis()
    {
        this.StartTime = this.MinimumTime;
        this.EndTime = new DateTimeOffset?();
    }

    /// <summary>
    /// Clears to known (clear) state; Clears select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void ClearKnownState()
    {
        this.ClearKnownStateThis();
    }

    /// <summary>
    /// Initializes to known (Initialize) state; Initializes select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="startTime"> The start time. </param>
    /// <param name="endTime">   The end time. </param>
    private void InitializeKnownStateThis( DateTimeOffset startTime, DateTimeOffset? endTime )
    {
        this.StartTime = startTime;
        this.EndTime = endTime;
    }

    /// <summary>
    /// Initializes to known (Initialize) state; Initializes select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="startTime"> The start time. </param>
    /// <param name="endTime">   The end time. </param>
    public void InitializeKnownState( DateTimeOffset startTime, DateTimeOffset? endTime )
    {
        this.InitializeKnownStateThis( startTime, endTime );
    }

    /// <summary>
    /// Initializes to known (Initialize) state; Initializes select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="startTime"> The start time. </param>
    public void InitializeKnownState( DateTimeOffset startTime )
    {
        this.InitializeKnownState( startTime, new DateTimeOffset?() );
    }

    /// <summary>
    /// Initializes to known (Initialize) state; Initializes select values to their initial state.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void InitializeKnownState()
    {
        this.InitializeKnownState( DateTimeOffset.Now );
    }

    /// <summary> Resets to known (default/instantiated) state. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    private void ResetKnownStateThis()
    {
        this.ClearKnownStateThis();
    }

    /// <summary> Resets to known (default/instantiated) state. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void ResetKnownState()
    {
        this.ResetKnownStateThis();
    }

    #endregion

    #region " time properties "

    /// <summary> Gets or sets the start time. </summary>
    /// <remarks>
    /// The time slice must have a start time. Start time can be set to <see cref="MinimumTime"/> to tag the
    /// time slice as not started. Start time can be <see cref="ResetStartTime()">reset</see>,
    /// <see cref="PostponeStartTime(DateTimeOffset)">postponed</see> or
    /// <see cref="AdvanceStartTime(DateTimeOffset)">advanced</see>.
    /// </remarks>
    /// <value> The start time. </value>
    public DateTimeOffset StartTime { get; set; }

    /// <summary> Gets or sets the End time. </summary>
    /// <value> The End time. </value>
    public DateTimeOffset? EndTime { get; set; }

    #endregion

    #region " time functions "

    /// <summary> Determines if the <see cref="TimeSlice"/> started. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> <c>true</c> if it started; otherwise <c>false</c> </returns>
    public bool HasStarted()
    {
        return this.StartTime > this.MinimumTime;
    }

    /// <summary> Query if time span has ended. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> <c>true</c> if time span has ended; otherwise <c>false</c> </returns>
    public bool HasEnded()
    {
        return this.EndTime.HasValue;
    }

    /// <summary> Query start time is later than end time or ended but not started. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> <c>true</c> if conflict; otherwise <c>false</c> </returns>
    public bool HasConflict()
    {
        return (this.HasEnded() && !this.HasStarted()) || (this.HasStarted() && this.HasEnded() && this.StartTime > this.EndTime!.Value);
    }

    /// <summary> Query if this time slice is active. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> <c>true</c> if active; otherwise <c>false</c>. </returns>
    public bool IsActive()
    {
        return this.HasStarted() && !this.HasEnded();
    }

    /// <summary>
    /// Gets the active start time. Defaults to <see cref="DateTimeOffset.MinValue"/>. Is set to SQL
    /// minimum time.
    /// </summary>
    /// <value> The <see cref="StartTime"/> if started or the <see cref="MinimumTime"/> </value>
    public DateTimeOffset MinimumTime { get; set; }

    /// <summary> Gets the time slice start time. </summary>
    /// <value> The time slice start time if started or the <see cref="MinimumTime"/> </value>
    public DateTimeOffset ActiveStartTime => this.HasStarted() ? this.StartTime : this.MinimumTime;

    /// <summary> Gets the active end time. </summary>
    /// <value> The <see cref="EndTime"/> if started or the <see cref="MinimumTime"/> </value>
    public DateTimeOffset ActiveEndTime => this.HasEnded() ? this.EndTime!.Value : this.HasStarted() ? DateTimeOffset.Now : this.MinimumTime;

    /// <summary> Gets the last elapsed time. </summary>
    /// <value> The last elapsed time. </value>
    protected TimeSpan LastElapsedTime { get; private set; }

    /// <summary> Gets the elapsed time span. </summary>
    /// <value> The elapsed time span. </value>
    public TimeSpan ElapsedTimespan
    {
        get
        {
            this.LastElapsedTime = this.ActiveEndTime.Subtract( this.ActiveStartTime );
            return this.LastElapsedTime;
        }
    }

    #endregion

    #region " time actions "

    /// <summary> Starts the <see cref="TimeSlice"/>. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void Start()
    {
        this.InitializeKnownState();
    }

    /// <summary> Starts the <see cref="TimeSlice"/>. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="dateTimeOffset"> The Date/Time. </param>
    public virtual void Start( DateTimeOffset dateTimeOffset )
    {
        this.InitializeKnownState( dateTimeOffset );
    }

    /// <summary> Ends the <see cref="TimeSlice"/>. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public virtual void Finish()
    {
        this.Finish( DateTimeOffset.Now );
    }

    /// <summary> Ends the <see cref="TimeSlice"/>. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    public virtual void Finish( DateTimeOffset value )
    {
        this.EndTime = value;
    }

    /// <summary> Opens the time slice after a <see cref="Finish()">stop</see>. </summary>
    public virtual void Resume()
    {
        this.EndTime = new DateTimeOffset?();
    }

    /// <summary> Resets the time slice start time to current time. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public virtual void ResetStartTime()
    {
        this.ResetStartTime( DateTimeOffset.Now );
    }

    /// <summary> Resets the time slice start time. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The new start time. </param>
    public virtual void ResetStartTime( DateTimeOffset value )
    {
        this.StartTime = value;
    }

    #endregion

    #region " time shifts "

    /// <summary> Advance start time to an earlier time. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    public void AdvanceStartTime( DateTimeOffset value )
    {
        if ( this.HasStarted() )
        {
            if ( this.StartTime > value )
            {
                this.StartTime = value;
            }
        }
        else
        {
            this.Start( value );
        }
    }

    /// <summary> Postpone start time. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    public void PostponeStartTime( DateTimeOffset value )
    {
        if ( this.HasStarted() )
        {
            if ( this.StartTime < value )
            {
                this.StartTime = value;
            }
        }
        else
        {
            this.Start( value );
        }
    }

    /// <summary> Sets start time to a later start time. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="startTime"> The start time. </param>
    public void StartLater( DateTimeOffset startTime )
    {
        if ( !this.HasStarted() || this.StartTime < startTime )
        {
            this.Start( startTime );
        }
    }

    /// <summary> Sets start time to an earlier start time. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="startTime"> The start time. </param>
    public void StartEarlier( DateTimeOffset startTime )
    {
        if ( !this.HasStarted() || this.StartTime > startTime )
        {
            this.Start( startTime );
        }
    }

    /// <summary> Postpone end time. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    public void PostponeEndTime( DateTimeOffset? value )
    {
        if ( value.HasValue )
        {
            if ( this.HasStarted() )
            {
                if ( this.HasEnded() )
                {
                    if ( this.EndTime!.Value < value.Value )
                    {
                        this.Finish( value.Value );
                    }
                }
                else
                {
                    this.EndTime = value;
                }
            }
            else
            {
                this.Finish( value.Value );
            }
        }
    }

    #endregion

    #region " formatting "

    /// <summary> Gets the default date time format. </summary>
    /// <value> The default date time format. </value>
    public static string DefaultDateTimeFormat { get; set; } = "yyyy/MM/dd HH:mm:ss K";

    /// <summary> Gets the default elapsed time format. </summary>
    /// <value> The default elapsed time format. </value>
    public static string DefaultElapsedTimeFormat { get; set; } = @"d\.hh\:mm\:ss\.fff";

    #endregion
}
/// <summary> A list of time-slices sorted by the Time-Slice start time. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-07-11 </para>
/// </remarks>
public class TimeSliceSortedList : SortedList<DateTimeOffset, TimeSlice>
{
    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="minimumTime"> The minimum time of the. </param>
    public TimeSliceSortedList( DateTimeOffset minimumTime ) : base() => this.MinimumTimeSlice = new TimeSlice( minimumTime ) { StartTime = minimumTime };

    /// <summary> Gets the minimum time slice. </summary>
    /// <value> The minimum time slice. </value>
    public TimeSlice MinimumTimeSlice { get; private set; }

    /// <summary> Gets the default start time. </summary>
    /// <value> The default start time. </value>
    public static DateTimeOffset DefaultStartTime { get; private set; }

    /// <summary> Adds item. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="item"> The item to add. </param>
    public void Add( TimeSlice item )
    {
        if ( item is not null )
        {
            this.Add( item.StartTime, item );
        }
    }

    /// <summary> Adds item. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="startTime"> The start time. </param>
    /// <param name="endTime">   The end time. </param>
    public void Add( DateTimeOffset startTime, DateTimeOffset? endTime )
    {
        this.Add( startTime, new TimeSlice( this.MinimumTimeSlice.MinimumTime ) { StartTime = startTime, EndTime = endTime } );
    }

    /// <summary> Updates this object. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="startTime"> The start time. </param>
    /// <param name="endTime">   The end time. </param>
    public void Update( DateTimeOffset startTime, DateTimeOffset? endTime )
    {
        if ( this.ContainsKey( startTime ) )
        {
            this[startTime].StartTime = startTime;
            this[startTime].EndTime = endTime;
        }
        else
        {
            this.Add( startTime, endTime );
        }
    }

    /// <summary> Gets the first. </summary>
    /// <value> The first. </value>
    public TimeSlice First => this.Values.Any() ? this.Values.First() : this.MinimumTimeSlice;

    /// <summary> Gets the last. </summary>
    /// <value> The last. </value>
    public TimeSlice Last => this.Values.Any() ? this.Values.Last() : this.MinimumTimeSlice;

    /// <summary> Gets the start time. </summary>
    /// <value> The start time. </value>
    public DateTimeOffset StartTime => this.First.ActiveStartTime;

    /// <summary> Gets the start time caption. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> A <see cref="string" />. </returns>
    public string StartTimeCaption()
    {
        return this.StartTime.ToString( this.DateTimeFormat );
    }

    /// <summary> Gets the end time. </summary>
    /// <value> The end time. </value>
    public DateTimeOffset EndTime => this.Last.ActiveEndTime;

    /// <summary> Elapsed time span. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> A TimeSpan. </returns>
    public TimeSpan ElapsedTimeSpan()
    {
        TimeSpan value = TimeSpan.Zero;
        foreach ( TimeSlice timeSlice in this.Values )
        {
            _ = value.Add( timeSlice.ElapsedTimespan );
        }

        return default;
    }

    /// <summary> Elapsed time caption. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> A <see cref="string" />. </returns>
    public string ElapsedTimeCaption()
    {
        return this.ElapsedTimeSpan().ToString( this.ElapsedTimeFormat );
    }

    /// <summary> Gets or sets the date time format. </summary>
    /// <value> The date time format. </value>
    public string DateTimeFormat { get; set; } = "yyyy/MM/dd HH:mm:ss K";

    /// <summary> Gets or sets the elapsed time format. </summary>
    /// <value> The elapsed time format. </value>
    public string ElapsedTimeFormat { get; set; } = @"d\.hh\:mm\:ss\.fff";
}
/// <summary> Dictionary of ordered list of time slices. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-08-31 </para>
/// </remarks>
public class OrderedTimeSliceDictionary : Dictionary<int, TimeSliceSortedList>
{
    /// <summary> Adds a dictionary to 'minimumTime'. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="key">         The key. </param>
    /// <param name="minimumTime"> The minimum time of the. </param>
    public void AddDictionary( int key, DateTimeOffset minimumTime )
    {
        if ( !this.ContainsKey( key ) )
        {
            this.MinimumTimeSlice = new TimeSlice( minimumTime );
            this.Add( key, new TimeSliceSortedList( minimumTime ) );
        }
    }

    /// <summary> Returns true if the reference dictionary has any elements. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="key"> The key. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    public bool Any( int key )
    {
        return this.ContainsKey( key ) && this[key].Any();
    }

    /// <summary> Gets or sets the minimum time slice. </summary>
    /// <value> The minimum time Slice. </value>
    public TimeSlice MinimumTimeSlice { get; private set; } = new TimeSlice( DateTimeOffset.MinValue );

    /// <summary> Gets the start time. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="key"> The key. </param>
    /// <returns> A DateTimeOffset. </returns>
    public DateTimeOffset StartTime( int key )
    {
        return this.Any( key ) ? this[key].StartTime : this.MinimumTimeSlice.StartTime;
    }

    /// <summary> Gets the start time caption. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="key"> The key. </param>
    /// <returns> A <see cref="string" />. </returns>
    public string StartTimeCaption( int key )
    {
        return this.Any( key ) ? this[key].StartTimeCaption() : this.MinimumTimeSlice.StartTime.ToString( this.DateTimeFormat );
    }

    /// <summary> Gets the end time. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="key"> The key. </param>
    /// <returns> A DateTimeOffset. </returns>
    public DateTimeOffset EndTime( int key )
    {
        return this.Any( key ) ? this[key].Last.ActiveEndTime : this.MinimumTimeSlice.ActiveEndTime;
    }

    /// <summary> Elapsed time span. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="key"> The key. </param>
    /// <returns> A TimeSpan. </returns>
    public TimeSpan ElapsedTimeSpan( int key )
    {
        TimeSpan value = TimeSpan.Zero;
        if ( this.Any( key ) )
        {
            value = this[key].ElapsedTimeSpan();
        }

        return value;
    }

    /// <summary> Elapsed time caption. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="key"> The key. </param>
    /// <returns> A <see cref="string" />. </returns>
    public string ElapsedTimeCaption( int key )
    {
        return this[key].ElapsedTimeCaption();
    }

    /// <summary> Gets or sets the date time format. </summary>
    /// <value> The date time format. </value>
    public string DateTimeFormat { get; set; } = "yyyy/MM/dd HH:mm:ss K";
}
