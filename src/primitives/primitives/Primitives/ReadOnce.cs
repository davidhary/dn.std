namespace cc.isr.Std.Primitives;

/// <summary> Read once class. </summary>
/// <remarks>
/// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para>
/// </remarks>
public class ReadOnce<T> : IEquatable<T> where T : struct
{
    /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public ReadOnce() : base() => this.HasValue = false;

    /// <summary> Gets or sets a value indicating whether this object has value. </summary>
    /// <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    public bool HasValue { get; set; }

    /// <summary> The value. </summary>
    private T _value;

    /// <summary> Gets or sets the value. </summary>
    /// <value> The value. </value>
    public T Value
    {
        get
        {
            this.HasValue = false;
            return this._value;
        }

        set
        {
            this._value = value;
            this.HasValue = true;
        }
    }

    /// <summary> Returns a string that represents the current object. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> A <see cref="string" /> that represents the current object. </returns>
    public override string ToString()
    {
        return $"{(this.HasValue ? "" : "?")}{this._value}";
    }

    #region " equals "

    /// <summary>
    /// Determines whether the specified <see cref="object" /> is equal to the current
    /// <see cref="object" />.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="obj"> The <see cref="object" /> to compare with the current
    /// <see cref="object" />. </param>
    /// <returns>
    /// <c>true</c> if the specified <see cref="object" /> is equal to the current
    /// <see cref="object" />; otherwise, <c>false</c>.
    /// </returns>
    public override bool Equals( object? obj )
    {
        return this.Equals( obj as ReadOnce<T> );
    }

    /// <summary>
    /// Indicates whether the current object is equal to another object of the same type.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="other"> An object to compare with this object. </param>
    /// <returns>
    /// <see langword="true" /> if the current object is equal to the <paramref name="other" />
    /// parameter; otherwise, <see langword="false" />.
    /// </returns>
    public bool Equals( T other )
    {
        return this.HasValue && other.Equals( this.Value );
    }

    /// <summary> Compares two ranges. </summary>
    /// <remarks>
    /// The two ranges are the same if the have the same minimum and maximum values.
    /// </remarks>
    /// <param name="other"> The other point to compare to this object. </param>
    /// <returns> A Boolean data type. </returns>
    public bool Equals( ReadOnce<T>? other )
    {
        return other is not null && other.Value.Equals( this.Value ) & other.HasValue.Equals( this.HasValue );
    }

    /// <summary> Implements the operator =. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    /// <returns> The result of the operation. </returns>
    public static bool operator ==( ReadOnce<T> left, ReadOnce<T> right )
    {
        return ReferenceEquals( left, right ) || (left is not null && left.Equals( right ));
    }

    /// <summary> Implements the operator !=. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    /// <returns> The result of the operator. </returns>
    public static bool operator !=( ReadOnce<T> left, ReadOnce<T> right )
    {
        return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
    }

    /// <summary> Creates a unique hash code. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> An <see cref="float">Single</see> value. </returns>
    public override int GetHashCode()
    {
        return (this.HasValue, this.Value).GetHashCode();
    }

    #endregion
}
