using System.Diagnostics.CodeAnalysis;

namespace cc.isr.Std.Primitives;

/// <summary> Implements a generic line class as a line between two generic. </summary>
/// <remarks>
/// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2006-04-10, 1.1.2291. </para>
/// </remarks>
/// <remarks>
/// Constructs a <see cref="Line{T}"/> instance by its origin and insertion points.
/// </remarks>
/// <remarks> David, 2020-09-22. </remarks>
/// <param name="origin">    Specifies the <see cref="Point{T}">origin</see> or start point of
/// the line. </param>
/// <param name="insertion"> Specifies the <see cref="Point{T}">insertion</see> or end point of
/// the line. </param>
public class Cord<T>( [DisallowNull] Point<T> origin, [DisallowNull] Point<T> insertion ) : Line<T>( origin.X, origin.Y, insertion.X, insertion.Y ) where T : IComparable<T>, IEquatable<T>, IFormattable
{
    #region " methods and properties "

    /// <summary> Gets or sets the insertion point of the line. </summary>
    /// <value> A <see cref="Point{T}">Point</see> property. </value>
    public Point<T> Insertion
    {
        get => new( this.X2, this.Y2 );

        [param: DisallowNull]
        set
        {
            this.X2 = value.X;
            this.Y2 = value.Y;
        }
    }

    /// <summary> Gets or sets the origin point of the line. </summary>
    /// <value> A <see cref="Point{T}">Point</see> property. </value>
    public Point<T> Origin
    {
        get => new( this.X1, this.Y1 );

        [param: DisallowNull]
        set
        {
            this.X1 = value.X;
            this.Y1 = value.Y;
        }
    }

    /// <summary> Sets the line coordinates. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="origin">    Specifies the origin point of the line. </param>
    /// <param name="insertion"> Specifies the insertion point of the line. </param>
    public void SetLine( [DisallowNull] Point<T> origin, [DisallowNull] Point<T> insertion )
    {
        this.Origin = new Point<T>( origin );
        this.Insertion = new Point<T>( insertion );
    }

    #endregion
}
