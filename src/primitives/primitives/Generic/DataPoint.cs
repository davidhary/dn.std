namespace cc.isr.Std.Primitives;

/// <summary> Implements a generic point structure. </summary>
/// <remarks>
/// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2006-04-10, 1.1.2291. </para>
/// </remarks>
/// <remarks> Initializes a new instance of the <see cref="DataPoint{T}" /> structure. </remarks>
/// <remarks> David, 2020-09-22. </remarks>
/// <param name="x"> Specifies the X coordinate of the point. </param>
/// <param name="y"> Specifies the Y coordinate of the point. </param>
public struct DataPoint<T>( T x, T y ) where T : IComparable<T>, IEquatable<T>, IFormattable
{
    #region " construction and cleanup "

    /// <summary>
    /// Initializes a new instance of the <see cref="DataPoint{T}" /> structure. The copy constructor.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="model"> The  <see cref="DataPoint{T}">Point</see> object from which to copy. </param>
    public DataPoint( DataPoint<T> model ) : this( model.X, model.Y )
    { }

    #endregion

    #region " static "

    /// <summary> Compares two points. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="left">  Specifies the point to compare. </param>
    /// <param name="right"> Specifies the point to compare with. </param>
    /// <returns> <c>true</c> if the points are equal. </returns>
    public static new bool Equals( object? left, object? right )
    {
        return (left is null && right is null) ||
            (left is not null && right is not null && Equals( ( DataPoint<T> ) left, ( DataPoint<T> ) right ));
    }

    /// <summary> Compares two points. </summary>
    /// <remarks> The two points are the same if they have the same X and Y coordinates. </remarks>
    /// <param name="left">  Specifies the point to compare. </param>
    /// <param name="right"> Specifies the point to compare with. </param>
    /// <returns> <c>true</c> if the points are equal. </returns>
    public static bool Equals( DataPoint<T> left, DataPoint<T> right )
    {
        return left.X.Equals( right.X ) && left.Y.Equals( right.Y );
    }

    /// <summary> Implements the operator =. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    /// <returns> The result of the operation. </returns>
    public static bool operator ==( DataPoint<T> left, DataPoint<T> right )
    {
        return Equals( left, right );
    }

    /// <summary> Implements the operator !=. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    /// <returns> The result of the operation. </returns>
    public static bool operator !=( DataPoint<T> left, DataPoint<T> right )
    {
        return !left.Equals( right );
    }

    /// <summary>
    /// Determines whether the specified <see cref="object" /> is equal to the current
    /// <see cref="object" />.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="obj"> The <see cref="object" /> to compare with the current
    /// <see cref="object" />. </param>
    /// <returns>
    /// <c>true</c> if the specified <see cref="object" /> is equal to the current
    /// <see cref="object" />; otherwise, false.
    /// </returns>
    public override readonly bool Equals( object? obj )
    {
        return obj is DataPoint<T> point && this.Equals( point );
    }

    /// <summary> Compares two points. </summary>
    /// <remarks> The two points are the same if they have the same X and Y coordinates. </remarks>
    /// <param name="other"> Specifies the other point to compare. </param>
    /// <returns> <c>true</c> if the points are equal. </returns>
    public readonly bool Equals( DataPoint<T> other )
    {
        return Equals( this, other );
    }

    /// <summary> Creates a unique hash code. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> An <see cref="int">integer</see> value. </returns>
    public override readonly int GetHashCode()
    {
#if NETSTANDARD2_1_OR_GREATER
        return System.HashCode.Combine( this.X, this.Y );
#else
        return (this.X, this.Y).GetHashCode();
#endif
    }

    #endregion

    #region " methods and properties "

    /// <summary> Holds the Y coordinate of the point. </summary>
    /// <value> The y coordinate. </value>
    public T Y { get; set; } = y;

    /// <summary> Holds the X coordinate of the point. </summary>
    /// <value> The x coordinate. </value>
    public T X { get; set; } = x;

    /// <summary> Sets the point based on the coordinates. </summary>
    /// <param name="x"> Specifies the X coordinate of the point. </param>
    /// <param name="y"> Specifies the Y coordinate of the point. </param>
    public void SetPoint( T x, T y )
    {
        this.X = x;
        this.Y = y;
    }

    /// <summary> Returns the default string representation of the point. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> The formatted string representation of the point, e.g., '(x,y)'. </returns>
    public override readonly string ToString()
    {
        return ToString( this.X, this.Y );
    }

    /// <summary> Returns the default string representation of the point. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="x"> Specifies the X coordinate of the point. </param>
    /// <param name="y"> Specifies the Y coordinate of the point. </param>
    /// <returns> The formatted string representation of the point, e.g., '(x,y)'. </returns>
    public static string ToString( T x, T y )
    {
        return $"({x},{y})";
    }

    #endregion
}
