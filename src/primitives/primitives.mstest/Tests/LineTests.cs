namespace cc.isr.Std.Primitives.Tests;

/// <summary> tests of equalities. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-10-10 </para>
/// </remarks>
[TestClass]
public class LineTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " line tests "

    /// <summary> (Unit Test Method) tests line equality. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void LineEqualityTest()
    {
        LineF? left = new( 0d, 0d );
        LineF? right = new( 0d, 0d );
        Assert.IsTrue( left == right, $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
        Assert.IsFalse( ReferenceEquals( left, right ), $"a. Object.ReferenceEquals(Left.{EqualityTests.InfoGetter( left )}, Right.{EqualityTests.InfoGetter( right )})" );
        Assert.IsFalse( ReferenceEquals( left, right ), $"b. (Object)Left.{EqualityTests.InfoGetter( left )} Is (Object)Right.{EqualityTests.InfoGetter( right )}" );
        EqualityTests.EqualityTest( left, right );
        left = null;
        right = null;
        Trace.TraceInformation( $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
        Assert.IsTrue( Equals( left, right ), $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
        Assert.IsTrue( ReferenceEquals( left, right ), $"a. Object.ReferenceEquals(Left.{EqualityTests.InfoGetter( left )}, Right.{EqualityTests.InfoGetter( right )})" );
        Assert.IsTrue( ReferenceEquals( left, right ), $"b. (Object)Left.{EqualityTests.InfoGetter( left )} Is (Object)Right.{EqualityTests.InfoGetter( right )}" );
        EqualityTests.EqualityTest( left, right );
    }

    /// <summary> (Unit Test Method) tests object inequality. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void LineInequalityTest()
    {
        LineF left = new( 0d, 0d );
        LineF? right = new( 1d, 0d );
        Trace.TraceInformation( $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
        Assert.IsFalse( left == right, $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
        EqualityTests.InequalityTest( left, right );
        left = new LineF( 0d, 0d );
        right = null;
        Trace.TraceInformation( $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
        Assert.IsFalse( Equals( left, right ), $"Left.{EqualityTests.InfoGetter( left )} == right.{EqualityTests.InfoGetter( right )}" );
        EqualityTests.InequalityTest( left, right );
    }

    #endregion
}
