
// [assembly: System.Reflection.AssemblyDescription( "Core Framework Library" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "cc.isr.Std.AsyncMSTest,PublicKey=" + cc.isr.Std.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "cc.isr.Std.AsyncTrials,PublicKey=" + cc.isr.Std.SolutionInfo.PublicKey )]
