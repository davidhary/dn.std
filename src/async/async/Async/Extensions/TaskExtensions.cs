using System.Diagnostics.CodeAnalysis;

namespace cc.isr.Std.Async.Extensions;

/// <summary>   A task extensions. </summary>
/// <remarks>   2023-03-29. <para>
/// Inspired by <see href="https://GitHub.com/HamidMolareza/AsyncSocket"/></para>
/// </remarks>
public static class TaskExtensions
{
    #region " execute async "

    /// <summary>
    /// Start the task and waits for its completion within the specified timeout. Throws a <see cref="TimeoutException"/>
    /// if the task failed to complete within the specified timeout.
    /// </summary>
    /// <remarks>   2023-03-29. </remarks>
    /// <typeparam name="TResult">  Generic type parameter. </typeparam>
    /// <param name="task">     The task that must be execute. </param>
    /// <param name="timeout">  . </param>
    /// <returns>   A <see cref="Task{TResult}"/>. </returns>
    public static Task<TResult> ExecuteAsync<TResult>( [NotNull] Task<TResult> task, int timeout )
    {
        task.Start();
        return WaitAsync( task, timeout );
    }

    /// <summary>
    /// Start the task and consider timeout.
    /// </summary>
    /// <param name="task">The task that must be execute.</param>
    /// <param name="timeout"></param>
    /// <exception cref="TimeoutException"></exception>
    public static Task ExecuteAsync( Task task, int timeout )
    {
        task.Start();
        return WaitAsync( task, timeout );
    }

    #endregion

    #region " wait async "

    /// <summary>
    /// Waits for the task to complete within the specified timeout and return the task result,
    /// otherwise or throw a <see cref="TimeoutException"/>
    /// </summary>
    /// <remarks>   2023-03-29. </remarks>
    /// <exception cref="TimeoutException"> Thrown when a Timeout error condition occurs. </exception>
    /// <typeparam name="TResult">    Generic type parameter. </typeparam>
    /// <param name="task">     The task to wait for. </param>
    /// <param name="timeout">  The timeout in milliseconds. </param>
    /// <returns>   A T. </returns>
    public static async Task<TResult> WaitAsync<TResult>( Task<TResult> task, int timeout )
    {
        return await Task.WhenAny( task, Task.Delay( timeout ) ) == task
            ? task.Result
            : throw new TimeoutException();
    }

    /// <summary>
    /// If the task is not completed within a specified timeout, throw timeout exception.
    /// </summary>
    /// <param name="task">The task that must be execute.</param>
    /// <param name="timeout"></param>
    /// <exception cref="TimeoutException"></exception>
    public static async Task WaitAsync( Task task, int timeout )
    {
        if ( await Task.WhenAny( task, Task.Delay( timeout ) ) == task )
            return;

        throw new TimeoutException();
    }

    #endregion

    #region " wait result "

    /// <summary>   Waits for the task completion or cancellation and returns the result. </summary>
    /// <remarks>   2023-03-29. </remarks>
    /// <typeparam name="TResult">    Generic type parameter. </typeparam>
    /// <param name="task">                 The task to wait for. </param>
    /// <param name="cancellationToken">    A token that allows processing to be cancelled. </param>
    /// <returns>   A T. </returns>
    public static TResult WaitResult<TResult>( Task<TResult> task, CancellationToken cancellationToken )
    {
        task.Wait( cancellationToken );
        return task.Result;
    }

    #endregion

    #region " validate "

    /// <summary>   Ensures that all tasks are stable. </summary>
    /// <remarks>   2023-03-29. </remarks>
    /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
    ///                                                 the required range. </exception>
    /// <param name="tasks">            The tasks. </param>
    /// <param name="stop">             True to stop. </param>
    /// <param name="running">          True to running. </param>
    /// <param name="createdOrWaiting"> True to created or waiting. </param>
    /// <param name="loopDelay">        (Optional) The loop delay in milliseconds. </param>
    public static void EnsureAllTasksAreStable( List<Task> tasks, bool stop, bool running, bool createdOrWaiting, int loopDelay = 5 )
    {
        foreach ( Task task in tasks )
            do
            {
                bool flag = task.Status switch
                {
                    TaskStatus.Canceled or TaskStatus.Faulted or TaskStatus.RanToCompletion => stop,
                    TaskStatus.Running => running,
                    TaskStatus.Created or TaskStatus.WaitingForActivation or TaskStatus.WaitingForChildrenToComplete or TaskStatus.WaitingToRun => createdOrWaiting,
                    _ => throw new ArgumentOutOfRangeException( nameof( tasks ) ),
                };
                if ( flag )
                    break;

                Task.Delay( loopDelay ).Wait();
            } while ( true );
    }

    #endregion

    #region " validate "

    /// <summary>   Executes the 'synchronously' operation. </summary>
    /// <remarks>   2023-03-29. </remarks>
    /// <param name="action">               The action. </param>
    /// <param name="cancellationToken">    A token that allows processing to be cancelled. </param>
    public static void RunSynchronously( Action action, CancellationToken cancellationToken )
    {
        Task mainHandlerTask = new( action );
        mainHandlerTask.RunSynchronously();
        mainHandlerTask.Wait( cancellationToken );
    }

    #endregion
}
