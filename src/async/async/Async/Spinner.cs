namespace cc.isr.Std.Async;

/// <summary>   A spinner. </summary>
/// <remarks>   David, 2021-06-17. </remarks>
public static class Spinner
{
#if false
    // Example
    static void Main()
    {
        int lifeValue = 42;
        int meaningOfLife = RunWithSpinner(() => LongLifeMethod(lifeValue), Spin);
        Console.WriteLine($"\r\nThe meaning of life is {meaningOfLife}");
    }
    private static void Spin( CancellationToken token, TimeSpan notificationPeriod )
    {
        while ( !token.IsCancellationRequested )
        {
            Console.Write( '.' );
            Thread.Sleep( notificationPeriod );
        }

    }
#endif

    /// <summary>
    /// Executes the <paramref name="longFunction"/> with <paramref name="spinner"/> notifications.
    /// </summary>
    /// <remarks>
    /// https://www.CodeProject.com/Tips/5305519/How-to-Action-a-Spinner-in-a-Console-Application.
    /// </remarks>
    /// <typeparam name="TResult">  Type of the result. </typeparam>
    /// <param name="longFunction">         The long function. </param>
    /// <param name="spinner">              The spinner. </param>
    /// <param name="notificationPeriod">   The notification period. </param>
    /// <returns>   A TResult. </returns>
    public static TResult RunWithSpinner<TResult>( Func<TResult> longFunction, Action<CancellationToken, TimeSpan> spinner, TimeSpan notificationPeriod )
    {
        CancellationTokenSource cts = new();
        //run the spinner on its own thread
        _ = Task.Run( () => spinner( cts.Token, notificationPeriod ) );
        TResult result;
        try
        {
            result = longFunction();
        }
        finally
        {
            //cancel the spinner
            cts.Cancel();
            cts.Dispose();
        }
        return result;
    }
}
