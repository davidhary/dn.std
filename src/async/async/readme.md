# About

cc.isr.Std.Async is a .Net library supporting async operations.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

cc.isr.Std.Async is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Standard Framework Repository].

# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

# [1.0.8484] - 2023-03-25
* Add Async Event Hanlders class.

[1.0.9083]: https://bitbucket.org/davidhary/dn.std/src/main/
[Standard Framework Repository]: https://bitbucket.org/davidhary/dn.std
