using System.ComponentModel;

namespace cc.isr.Std.Async.Extensions.Tests;

/// <summary>   Additional information for calculation completed events. </summary>
/// <remarks>   2023-05-29. </remarks>
/// <typeparam name="TResult">  Type of the result. </typeparam>
/// <remarks>   Constructor. </remarks>
/// <remarks>   2023-05-29. </remarks>
/// <param name="error">        The error. </param>
/// <param name="canceled">    True if canceled. </param>
/// <param name="userState">    State of the user. </param>
internal sealed class CalculationCompletedEventArgs<TResult>( Exception? error, bool canceled, object? userState ) : AsyncCompletedEventArgs( error, canceled, userState )
{
    /// <summary>   Constructor. </summary>
    /// <remarks>   2023-05-29. </remarks>
    /// <param name="result">   The result. </param>
    public CalculationCompletedEventArgs( TResult result ) : this( null, false, null ) => this.Result = result;

    /// <summary>   Gets the result. </summary>
    /// <value> The result. </value>
    public TResult? Result { get; }

}
