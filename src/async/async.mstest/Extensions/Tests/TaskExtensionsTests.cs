namespace cc.isr.Std.Async.Extensions.Tests;

[TestClass]
public class TaskExtensionsTests
{
    #region " construction and cleanup "

    /// <summary>   Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test
    /// in the class.
    /// </remarks>
    /// <param name="testContext">  Gets or sets the test context which provides information about
    ///                             and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            _ = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}";
            // moved to test initialize
            // Console.WriteLine( methodFullName );
        }
        catch ( Exception ex )
        {
            Console.WriteLine( $"Failed initializing fixture: {ex}" );
        }
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>											   
    [TestInitialize()]
    public virtual void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
    }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " sum "

    /// <summary>   Sum asynchronous. </summary>
    /// <remarks>   2023-05-30. 
    /// <see cref="https://devblogs.microsoft.com/dotnet/configureawait-faq/">configure await</see>
    /// </remarks>
    /// <param name="a">        An int to process. </param>
    /// <param name="b">        An int to process. </param>
    /// <param name="delay">    The delay. </param>
    /// <returns>   The sum. </returns>
    private static async Task<(int, TimeSpan)> SumAsync( int a, int b, int delay )
    {
        Stopwatch sw = Stopwatch.StartNew();
        await Task.Delay( delay ).ConfigureAwait( false );
        return (a + b, sw.Elapsed);
    }

    /// <summary>   Sum synchronize. </summary>
    /// <remarks>   2023-05-30. </remarks>
    /// <param name="a">        An int to process. </param>
    /// <param name="b">        An int to process. </param>
    /// <param name="delay">    The delay. </param>
    /// <param name="timeout">  The timeout. </param>
    /// <returns>   sum, total time. </returns>
    private static (int, TimeSpan) SumSync( int a, int b, int delay, int timeout )
    {
        Stopwatch sw = Stopwatch.StartNew();
        _ = Task.Delay( delay ).Wait( timeout );
        return (a + b, sw.Elapsed);
    }

    /// <summary>   Sum synchronize. </summary>
    /// <remarks>   2023-05-30. </remarks>
    /// <param name="a">        An int to process. </param>
    /// <param name="b">        An int to process. </param>
    /// <param name="delay">    The delay. </param>
    /// <returns>   sum, total time. </returns>
    private static (int, TimeSpan) SumSync( int a, int b, int delay )
    {
        Stopwatch sw = Stopwatch.StartNew();
        Task.Delay( delay ).Wait();
        return (a + b, sw.Elapsed);
    }

    private static int Sum( int a, int b, int delay )
    {
        Task.Delay( delay ).Wait();
        return a + b;
    }

    /// <summary>   Value task sum. </summary>
    /// <remarks>   2023-05-30. 
	/// Note that this cases the value task as a task which
	/// may defeat the allocation benefits of the value task.
    /// <see href="https://learn.microsoft.com/en-us/shows/on-net/understanding-how-to-use-task-and-valuetask"/>
    /// <see href="https://devblogs.microsoft.com/dotnet/understanding-the-whys-whats-and-whens-of-valuetask"/>
    /// <see href="https://devblogs.microsoft.com/dotnet/configureawait-faq/"/>
	/// </remarks>
    /// <param name="a">        An int to process. </param>
    /// <param name="b">        An int to process. </param>
    /// <param name="delay">    The delay. </param>
    /// <returns> <see cref="Runtime.CompilerServices.ConfiguredValueTaskAwaitable{int,TimeSpan}"/>. </returns>
    private static async Task<(int, TimeSpan)> ValueTaskSumAsync( int a, int b, int delay )
    {
        ValueTask<(int, TimeSpan)> valueTask = new( SumSync( a, b, delay ) );
        return await valueTask.ConfigureAwait( false );
    }

    #endregion

    #region " wait( timeout ) "

    /// <summary>   (Unit Test Method) task should not timeout. </summary>
    /// <remarks>   2023-04-04. <para>
    /// <code>
    /// Standard Output: 
    ///    Delay: 10 ms, Timeout: 30 ms, Elapsed: 15.4 ms
    ///    Delay: 20 ms, Timeout: 40 ms, Elapsed: 21.7 ms
    /// </code>
    /// </para></remarks>
    /// <param name="delay">    The delay. </param>
    /// <param name="timeout">  The timeout. </param>
    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( 10, 30 )]
    [DataRow( 20, 40 )]
    public void TaskShouldNotTimeout( int delay, int timeout )
    {
        int a = 2, b = 3, c = a + b;
        Task<(int, TimeSpan)> task = SumAsync( a, b, delay );
        bool timedOut = !task.Wait( timeout );
        Console.Write( $"Delay: {delay} ms, Timeout: {timeout} ms, Elapsed: {task.Result.Item2.TotalMilliseconds:0.0} ms" );
        Assert.IsFalse( timedOut, $"Task timed out; elapsed:{task.Result.Item2.TotalMilliseconds:0.0}ms" );
        Assert.AreEqual( c, task.Result.Item1 );
    }

    /// <summary>   (Unit Test Method) value task should not timeout. </summary>
    /// <remarks>   2023-05-29.
    /// <see href="https://devblogs.microsoft.com/dotnet/understanding-the-whys-whats-and-whens-of-valuetask"/>
    /// <code>
    ///  Standard Output: 
    ///    Delay: 10 ms, Timeout: 30 ms, Elapsed: 17.8 ms
    ///    Delay: 20 ms, Timeout: 40 ms, Elapsed: 24.6 ms
    /// </code>
    /// </remarks>
    /// <param name="delay">    The delay. </param>
    /// <param name="timeout">  The timeout. </param>
    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( 10, 30 )]
    [DataRow( 20, 40 )]
    public void ValueTaskShouldNotTimeout( int delay, int timeout )
    {
        int a = 2, b = 3, c = a + b;
        Task<(int, TimeSpan)> task = ValueTaskSumAsync( a, b, delay );
        bool timedOut = !task.Wait( timeout );
        Console.Write( $"Delay: {delay} ms, Timeout: {timeout} ms, Elapsed: {task.Result.Item2.TotalMilliseconds:0.0} ms" );
        Assert.IsFalse( timedOut, $"Task timed out; elapsed:{task.Result.Item2.TotalMilliseconds:0.0}ms" );
        Assert.AreEqual( c, task.Result.Item1 );
    }

    /// <summary>   (Unit Test Method) task should timeout. </summary>
    /// <remarks>   2023-04-04. <code>
    ///  TaskShouldTimeout (20,5)
    ///    Duration: 10 ms
    ///    Standard Output: 
    ///    2023-04-04 19:38:17.681,cc.isr.Std.Async.MSTest.Extensions.TaskExtensionsTests.TaskExtensionsTests
    ///    Delay: 20 Timeout: 5 Elapsed: 7
    ///  TaskShouldTimeout (10000,10)
    ///    Duration: 21 ms
    ///    Standard Output: 
    ///    Delay: 10000 Timeout: 10 Elapsed: 21
    ///
    /// </code> </remarks>
    /// <param name="delay">    The delay. </param>
    /// <param name="timeout">  The timeout. </param>
    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( 20, 5 )]
    [DataRow( 10000, 10 )]
    public void TaskShouldTimeout( int delay, int timeout )
    {
        int a = 2, b = 3, c = a + b;
        (int, TimeSpan) result = SumSync( a, b, delay, timeout );
        bool timedOut = result.Item2.TotalMicroseconds > timeout;
        Console.Write( $"Delay: {delay} ms, Timeout: {timeout} ms, Elapsed: {result.Item2.TotalMilliseconds:0.0} ms" );
        Assert.IsTrue( timedOut, $"Task timed out; elapsed:{result.Item2.TotalMilliseconds:0.0}ms" );
        Assert.AreEqual( c, result.Item1 );
    }

    #endregion

    #region " wait async "

    #region " async task<t> waitasync<t> (task<t> task, int timeout) "

    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( 5, 20, DisplayName = "Delay=5,Timeout=20" )]
    [DataRow( 10, 1000, DisplayName = "Delay=10,Timeout=1000" )]
    public async Task WaitAsyncTLessThanTimeoutTakesLongReturnAnswer( int delay, int timeout )
    {
        Task<int> task = Task.Run( () => Sum( 2, 3, delay ) );
        int expected = 5;
        int actual = await TaskExtensions.WaitAsync( task, timeout );
        Assert.AreEqual( expected, actual );
    }

    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( 20, 5 )]
    [DataRow( 10000, 10 )]
    public void WaitAsyncTMoreThanTimeoutTakesLongThrowTimeoutException( int delay, int timeout )
    {
        try
        {
            Task<int> task = Task.Run( () => Sum( 2, 3, delay ) );
            TaskExtensions.WaitAsync( task, timeout ).Wait();
        }
        catch ( AggregateException ex )
        {
            Assert.IsTrue( ex.InnerException is TimeoutException );
        }
    }

    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( -2 )]
    [DataRow( int.MaxValue * -1 )]
    public void WaitAsyncTOutOfRangeTimeoutThrowOutOfRangeException( int timeout )
    {
        try
        {
            //Fake Task
            Task<int> task = new( () => Sum( 2, 3, 20 ) );
            TaskExtensions.WaitAsync( task, timeout ).Wait();
        }
        catch ( AggregateException ex )
        {
            Assert.IsTrue( ex.InnerException is ArgumentOutOfRangeException );
        }
    }

    #endregion

    #region " async task waitasync (task task, int timeout) "

    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( 5, 20 )]
    [DataRow( 10, 1000 )]
    public async Task WaitAsyncLessThanTimeoutTakesLongReturnAnswer( int delay, int timeout )
    {
        Task task = Task.Run( () => Task.Delay( delay ).Wait() );
        await TaskExtensions.WaitAsync( task, timeout );
    }

    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( 20, 5 )]
    [DataRow( 10000, 10 )]
    public void WaitAsyncMoreThanTimeoutTakesLongThrowTimeoutException( int delay, int timeout )
    {
        try
        {
            Task task = Task.Run( () => Task.Delay( delay ).Wait() );
            TaskExtensions.WaitAsync( task, timeout ).Wait();
        }
        catch ( AggregateException ex )
        {
            Assert.IsTrue( ex.InnerException is TimeoutException );
        }
    }

    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( -2 )]
    [DataRow( int.MaxValue * -1 )]
    public void WaitAsyncOutOfRangeTimeoutThrowOutOfRangeException( int timeout )
    {
        try
        {
            //Fake Task
            Task task = Task.Run( () => { } );
            TaskExtensions.WaitAsync( task, timeout ).Wait();
        }
        catch ( AggregateException ex )
        {
            Assert.IsTrue( ex.InnerException is ArgumentOutOfRangeException );
        }
    }

    #endregion

    #endregion

    #region " execute async "

    #region " task<t> executeasync<t> (task<t> task, int timeout) "

    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( 5, 20 )]
    [DataRow( 10, 1000 )]
    public void ExecuteAsyncTLessThanTimeoutTakesLongReturnAnswer( int delay, int timeout )
    {
        Console.Write( $"Delay: {delay} Timeout: {timeout}" );
        Task<int> task = new( () => Sum( 2, 3, delay ) );
        Task<int> actual = TaskExtensions.ExecuteAsync( task, timeout );
        Stopwatch sw = Stopwatch.StartNew();
        actual.Wait();
        sw.Stop();
        Console.WriteLine( $" Elapsed: {sw.ElapsedMilliseconds}" );
        Assert.AreEqual( 5, actual.Result );
    }

    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( 20, 5 )]
    [DataRow( 10000, 10 )]
    public void ExecuteAsyncTMoreThanTimeoutTakesLongThrowTimeoutException( int delay, int timeout )
    {
        try
        {
            Task<int> task = new( () => Sum( 2, 3, delay ) );
            TaskExtensions.ExecuteAsync( task, timeout ).Wait();
        }
        catch ( AggregateException ex )
        {
            Assert.IsTrue( ex.InnerException is TimeoutException );
        }
    }

    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( -2 )]
    [DataRow( int.MaxValue * -1 )]
    public void ExecuteAsyncTOutOfRangeTimeoutThrowOutOfRangeException( int timeout )
    {
        try
        {
            //Fake Task
            Task<int> task = new( () => Sum( 2, 3, 20 ) );
            TaskExtensions.ExecuteAsync( task, timeout ).Wait();
        }
        catch ( AggregateException ex )
        {
            Assert.IsTrue( ex.InnerException is ArgumentOutOfRangeException );
        }
    }

    #endregion

    #region " task executeasync (task task, int timeout) "

    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( 5, 20 )]
    [DataRow( 10, 1000 )]
    public async Task ExecuteAsyncLessThanTimeoutTakesLongReturnAnswer( int delay, int timeout )
    {
        Task task = new( () => Task.Delay( delay ).Wait() );
        await TaskExtensions.ExecuteAsync( task, timeout );
    }

    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( 20, 5 )]
    [DataRow( 10000, 10 )]
    public void ExecuteAsyncMoreThanTimeoutTakesLongThrowTimeoutException( int delay, int timeout )
    {
        Stopwatch sw = Stopwatch.StartNew();
        try
        {
            Task task = new( () => Task.Delay( delay ).Wait() );
            TaskExtensions.ExecuteAsync( task, timeout ).Wait();
            Console.WriteLine( $"Delay: {delay} Timeout: {timeout}  Elapsed: {sw.ElapsedMilliseconds}" );
            // Assert.IsNotNull( () => cc.isr.Std.Async.Extensions.TaskExtensions.ExecuteAsync( task, timeout ) );
        }
        catch ( AggregateException ex )
        {
            Console.WriteLine( $"Delay: {delay} Timeout: {timeout}  Elapsed: {sw.ElapsedMilliseconds}" );
            Assert.IsTrue( ex.InnerException is TimeoutException );
        }
    }

    [TestMethod]
    [CLSCompliant( false )]
    [DataRow( -2 )]
    [DataRow( int.MaxValue * -1 )]
    public void ExecuteAsyncOutOfRangeTimeoutThrowOutOfRangeException( int timeout )
    {
        try
        {
            //Fake Task
            Task task = new( () => { } );
            TaskExtensions.ExecuteAsync( task, timeout ).Wait();
        }
        catch ( AggregateException ex )
        {
            Assert.IsTrue( ex.InnerException is ArgumentOutOfRangeException );
        }
    }

    #endregion

    #endregion
}
