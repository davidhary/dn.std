namespace cc.isr.Std.Async.Tests;

[TestClass]
public class AsyncEventHandlersTests
{
    #region " fixture construction and cleanup "

    /// <summary>   Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test
    /// in the class.
    /// </remarks>
    /// <param name="testContext">  Gets or sets the test context which provides information about
    ///                             and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            _ = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}";
            // Console.WriteLine( methodFullName );
        }
        catch ( Exception ex )
        {
            Console.WriteLine( $"Failed initializing the test class: {ex}" );

            // cleanup to meet strong guarantees

            try
            {
                CleanupTestClass();
            }
            finally
            {
            }
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    { }

    /// <summary> Initializes the test class instance before each test runs. </summary>											   
    [TestInitialize()]
    public virtual void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
    }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    /// <summary>   Event queue for all listeners interested in Event events. </summary>
    public event EventHandler<EventArgs>? Event;

    /// <summary>   (Unit Test Method) asynchronous event handler exception should be handled. </summary>
    /// <remarks>
    /// 2023-02-16.
    /// <code>
    /// Standard Output: 
    ///    2023-02-16 17:46:45.003,cc.isr.VXI11.MSTest.AsyncEventHandlersTests.AsyncEventHandlersTests
    ///    Entered the async callback method.
    ///    Started the Async task, which throws an exception on the next statement.
    ///    Entered the error handling method.
    ///    Success! Our exception handler caught: exception handled
    ///    Stack Trace:
    ///       at cc.isr.VXI11.MSTest.AsyncEventHandlersTests.!=c__DisplayClass10_0.&lt;AsyncEventHandlerExceptionShouldBeHandled&gt;b__2() in C:\my\lib\vs\iot\vxi\src\vxi\vxi.MSTest\AsyncEventHandlersTests.cs:line 67
    ///       at System.Threading.ExecutionContext.RunFromThreadPoolDispatchLoop(Thread threadPoolThread, ExecutionContext executionContext, ContextCallback callback, Object state)
    ///    --- End of stack trace from previous location ---
    ///       at System.Threading.ExecutionContext.RunFromThreadPoolDispatchLoop(Thread threadPoolThread, ExecutionContext executionContext, ContextCallback callback, Object state)
    ///       at System.Threading.Tasks.Task.ExecuteWithThreadLocal(Task&amp; currentTaskSlot, Thread threadPoolThread )
    ///    --- End of stack trace from previous location ---
    ///       at cc.isr.VXI11.MSTest.AsyncEventHandlersTests.!=c__DisplayClass10_0.&lt;&lt;AsyncEventHandlerExceptionShouldBeHandled&gt;b__0&gt;d.MoveNext() in C:\my\lib\vs\iot\vxi\src\vxi\vxi.MSTest\AsyncEventHandlersTests.cs:line 65
    ///    --- End of stack trace from previous location ---
    ///       at cc.isr.VXI11.AsyncEventHandlers.!=c__DisplayClass1_0`1.&lt;&lt;TryAsync&gt;b__0&gt;d.MoveNext() in C:\my\lib\vs\iot\vxi\src\vxi\vxi\VXI11\AsyncEventHandlers.cs:line 50
    ///    Exiting the error handling method.
    /// </code>
    /// </remarks>
    /// <exception cref="an">                           Thrown when an error condition occurs. </exception>
    /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
    ///                                                 invalid. </exception>
    [TestMethod]
    public void AsyncEventHandlerExceptionShouldBeHandled()
    {
        Exception? handledException = default;
        string actualExceptionMessage = string.Empty;
        string expectedExceptionMessage = "exception handled";
        EventHandler<EventArgs> handler = cc.isr.Std.Async.AsyncEventHandlers.TryAsync<EventArgs>(
                async ( sender, e ) =>
                {
                    Console.WriteLine( $"Entered the async callback method." );
                    await Task.Factory.StartNew( () =>
                    {
                        Console.WriteLine( $"Started the Async task, which throws an exception on the next statement." );
                        throw new InvalidOperationException( expectedExceptionMessage );
                    } );
                    Console.WriteLine( $"Exiting the async callback method." );
                },
                ( ex ) =>
                {
                    Console.WriteLine( $"Entered the error handling method." );
                    handledException = ex;
                    actualExceptionMessage = ex.Message;
                    Console.WriteLine( $"Success! Our exception handler caught: {ex.Message}\nStack Trace: \n{ex.StackTrace}" );
                    Console.WriteLine( $"Exiting the error handling method." );
                } );
        this.Event += handler;
        Event?.Invoke( this, EventArgs.Empty );

        TimeSpan timeout = TimeSpan.FromMilliseconds( 1000 );
        DateTime endTime = DateTime.Now.AddMilliseconds( 1000 );
        // wait for the event to get handled.
        while ( endTime > DateTime.Now && handledException is null )
            Task.Delay( 10 ).Wait();

        Assert.IsNotNull( handledException, "exception should be handled." );
        Assert.AreEqual( expectedExceptionMessage, actualExceptionMessage, "Exception message should be set when the exception is caught" );
    }

}
