using System.ComponentModel;
using cc.isr.Tracing;

namespace cc.isr.Std.Async.Tests;

/// <summary> A tasker tests. </summary>
/// <remarks> David, 2020-09-18. </remarks>
[TestClass]
public class TaskerTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " tasker tests  "

    /// <summary> Assert tasker exception. </summary>
    /// <remarks> David, 2020-08-06. </remarks>
    /// <exception cref="OperationCanceledException"> Thrown when an Operation Canceled error condition
    /// occurs. </exception>
    public static void AssertTaskerException()
    {
        Tasker tasker = new();
        // task should throw an exception
        string activity = "";
        activity = "Task started";
        try
        {
            activity = "Task action";
            tasker.StartAction( () => throw new OperationCanceledException( "thrown from withing the task action" ) );
            activity = "Task awaiting task idle";
            Assert.IsTrue( tasker.AwaitTaskIdle( TimeSpan.FromMilliseconds( 200d ) ), $"{nameof( Tasker.AwaitTaskIdle )} should timeout" );
        }
        catch ( Exception ex )
        {
            Console.Out.WriteLine( $"thrown from {activity}" );
            throw new OperationCanceledException( $"thrown from {activity}", ex );
        }
    }

    /// <summary>   Assert tasker asynchronous completed should report exception private. </summary>
    /// <remarks>   David, 2020-08-06. </remarks>
    /// <exception cref="OperationCanceledException">   Thrown when a thread cancels a running
    ///                                                 operation. </exception>
    private static void AssertTaskerAsyncCompletedShouldReportExceptionPrivate( bool expectsException = true )
    {
        DateTimeOffset startTime = default;
        TimeSpan captureTimestamp = default;
        TimeSpan exceptionTimestamp = default;
        TimeSpan actionExitTimestamp = TimeSpan.Zero;
        TimeSpan taskDoneTimestamp;
        bool handled = false;
        bool isFaulted = default;
        Exception? exception = null;
        Tasker tasker = new();
        tasker.AsyncCompleted += ( sender, e ) =>
        {
            handled = true;
            isFaulted = e.Error is not null;
            exception = e.Error;
            captureTimestamp = DateTimeOffset.Now.Subtract( startTime );
        };
        // task should throw an exception to be captured by the tasker before the wait occurred.
        string activity = "Task started";
        activity = "Task action";
        startTime = DateTimeOffset.Now;
        tasker.StartAction( () =>
        {
            exceptionTimestamp = DateTimeOffset.Now.Subtract( startTime );
            if ( expectsException )
                throw new OperationCanceledException( $"{activity} thrown from withing the task action" );
        } );
        activity = "Task awaiting task idle";
        Assert.IsTrue( tasker.AwaitTaskIdle( TimeSpan.FromMilliseconds( 400d ) ), $"{nameof( Tasker.AwaitTaskIdle )} should not timeout" );
        Assert.IsTrue( handled, $"{nameof( Tasker.AsyncCompleted )} should be handled" );
        Assert.AreEqual( expectsException, isFaulted, $"{nameof( Tasker.ActionTask )}.{nameof( Task.IsFaulted )} should match" );
        if ( expectsException )
        {
            Assert.IsNotNull( exception, $"{nameof( AsyncCompletedEventArgs )}.{nameof( AsyncCompletedEventArgs.Error )} should not be null" );
            Assert.IsNotNull( exception.InnerException as OperationCanceledException, $"{nameof( AsyncCompletedEventArgs )}.{nameof( AsyncCompletedEventArgs.Error )} should be {nameof( OperationCanceledException )}" );
        }
        else
        {
            Assert.IsNull( exception, $"{nameof( AsyncCompletedEventArgs )}.{nameof( AsyncCompletedEventArgs.Error )} should be null" );
        }
        Assert.IsNotNull( tasker.ActionTask );
        Assert.IsFalse( tasker.ActionTask.IsFaulted, $"{nameof( Tasker.ActionTask )}.{nameof( Task.IsFaulted )} should match as {nameof( Tasker.ActionTask )} does not register the exception" );
        taskDoneTimestamp = DateTimeOffset.Now.Subtract( startTime );
        // Timestamps: Exception 0; Capture: 1.3379; action exit 0; Done 1.3379
        Console.Out.WriteLine( $"Timestamps {(expectsException ? "with" : "without")} error: Exception {exceptionTimestamp.TotalMilliseconds} ms; Capture: {captureTimestamp.TotalMilliseconds} ms; action exit {actionExitTimestamp.TotalMilliseconds} ms; Done {taskDoneTimestamp.TotalMilliseconds} ms" );
    }

    /// <summary>   Assert tasker asynchronous completed should report exception. </summary>
    /// <remarks>   David, 2020-07-17. </remarks>
    public static void AssertTaskerAsyncCompletedShouldReportException()
    {
        Tasker tasker = new();
        TimeSpan timeout = TimeSpan.FromMilliseconds( 100d );
        TimeSpan delay = timeout.Subtract( TimeSpan.FromMilliseconds( 50d ) );
        tasker.StartAction( () => Thread.Sleep( delay ) );
        Assert.IsTrue( tasker.AwaitTaskIdle( timeout ), $"{nameof( Tasker.AwaitTaskIdle )} should not timeout" );
        delay = timeout.Add( TimeSpan.FromMilliseconds( 50d ) );
        tasker.StartAction( () => Thread.Sleep( delay ) );
        Assert.IsFalse( tasker.AwaitTaskIdle( timeout ), $"{nameof( Tasker.AwaitTaskIdle )} should timeout" );

        // MyAssertProperty.MyAssert.Throws(Of OperationCanceledException)(AddressOf AssertTaskerException)
        AssertTaskerAsyncCompletedShouldReportExceptionPrivate( true );
        AssertTaskerAsyncCompletedShouldReportExceptionPrivate( false );
    }

    /// <summary>
    /// (Unit Test Method) tasker asynchronous completed should report exception.
    /// </summary>
    /// <remarks>   David, 2020-07-17. </remarks>
    [TestMethod()]
    public void TaskerAsyncCompletedShouldReportException()
    {
        AssertTaskerAsyncCompletedShouldReportException();
    }

    /// <summary> Assert tasker of tuple. </summary>
    /// <remarks> David, 2020-07-17. </remarks>
    public static void AssertTaskerResultShouldEqualTuple()
    {
        Tasker<(bool Success, string Details)> tasker = new();
        TimeSpan timeout = TimeSpan.FromMilliseconds( 100d );
        TimeSpan delay = timeout.Subtract( TimeSpan.FromMilliseconds( 50d ) );
        (bool Success, string Details) expectedResult = (true, "Success");
        tasker.StartAction( () =>
        {
            Thread.Sleep( delay );
            return expectedResult;
        } );
        Assert.IsTrue( tasker.AwaitTaskIdle( timeout ), $"{nameof( Tasker.AwaitTaskIdle )} should not timeout" );
        (TaskStatus Status, (bool Success, string Details) Result) result = tasker.AwaitCompletion( timeout );
        Assert.AreEqual( TaskStatus.RanToCompletion, result.Status, $"{nameof( TaskStatus )} should match" );
        Assert.AreEqual( expectedResult.Details, result.Result.Details, $"{nameof( Tasker<(bool Success, string Details)> )} Result should match" );
        expectedResult = (false, "Failure");
        tasker.StartAction( () =>
        {
            Thread.Sleep( delay );
            return expectedResult;
        } );
        result = tasker.AwaitCompletion( timeout );
        Assert.AreEqual( TaskStatus.RanToCompletion, result.Status, $"{nameof( TaskStatus )} should match" );
        Assert.AreEqual( expectedResult.Details, result.Result.Details, $"{nameof( Tasker<(bool Success, string Details)> )} Result should match" );
    }

    /// <summary>   (Unit Test Method) tasker result should equal tuple. </summary>
    /// <remarks>   David, 2020-07-17. </remarks>
    [TestMethod()]
    public void TaskerResultShouldEqualTuple()
    {
        AssertTaskerResultShouldEqualTuple();
    }

    #endregion
}
