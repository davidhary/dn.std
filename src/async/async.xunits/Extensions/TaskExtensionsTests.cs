using System;
using System.Threading.Tasks;
using Xunit;

namespace cc.isr.Std.Async.XUnits.Extensions;
public class TaskExtensionsTests
{
    #region " wait async "

    #region " async task<t> waitasync<t> (task<t> task, int timeout) "

    [Theory]
    [InlineData( 5, 20 )]
    [InlineData( 10, 1000 )]
    public async Task WaitAsyncTLessThanTimeoutTakesLongReturnAnswer( int delay, int timeout )
    {
        Task<int> task = Task.Run( () => Sum( 2, 3, delay ) );
        int actual = await Async.Extensions.TaskExtensions.WaitAsync( task, timeout );
        Assert.Equal( 5, actual );
    }

    [Theory]
    [InlineData( 20, 5 )]
    [InlineData( 10000, 10 )]
    public async Task WaitAsyncTMoreThanTimeoutTakesLongThrowTimeoutException( int delay, int timeout )
    {
        Task<int> task = Task.Run( () => Sum( 2, 3, delay ) );
        _ = await Assert.ThrowsAsync<TimeoutException>( () => Async.Extensions.TaskExtensions.WaitAsync( task, timeout ) );
    }

    private static int Sum( int a, int b, int delay )
    {
        Task.Delay( delay ).Wait();
        return a + b;
    }

    [Fact]
    public async Task WaitAsyncTTaskIsNullThrowArgumentNullException()
    {
        _ = await Assert.ThrowsAsync<ArgumentNullException>( () => Async.Extensions.TaskExtensions.WaitAsync<int>( null, 500 ) );
    }

    [Theory]
    [InlineData( -1 )]
    [InlineData( int.MaxValue * -1 )]
    public async Task WaitAsyncTOutOfRangeTimeoutThrowOutOfRangeException( int timeout )
    {
        //Fake Task
        Task<int> task = new( () => Sum( 2, 3, 20 ) );

        _ = await Assert.ThrowsAsync<ArgumentOutOfRangeException>( () => Async.Extensions.TaskExtensions.WaitAsync( task, timeout ) );
    }

    #endregion

    #region " async task waitasync (task task, int timeout) "

    [Theory]
    [InlineData( 5, 20 )]
    [InlineData( 10, 1000 )]
    public async Task WaitAsyncLessThanTimeoutTakesLongReturnAnswer( int delay, int timeout )
    {
        Task task = Task.Run( () => Task.Delay( delay ).Wait() );
        await Async.Extensions.TaskExtensions.WaitAsync( task, timeout );
        Assert.True( true );
    }

    [Theory]
    [InlineData( 20, 5 )]
    [InlineData( 10000, 10 )]
    public async Task WaitAsyncMoreThanTimeoutTakesLongThrowTimeoutException( int delay, int timeout )
    {
        Task task = Task.Run( () => Task.Delay( delay ).Wait() );
        _ = await Assert.ThrowsAsync<TimeoutException>( () => Async.Extensions.TaskExtensions.WaitAsync( task, timeout ) );
    }

    [Fact]
    public async Task WaitAsyncTaskIsNullThrowArgumentNullException()
    {
        _ = await Assert.ThrowsAsync<ArgumentNullException>( () => Async.Extensions.TaskExtensions.WaitAsync<int>( null, 500 ) );
    }

    [Theory]
    [InlineData( -1 )]
    [InlineData( int.MaxValue * -1 )]
    public async Task WaitAsyncOutOfRangeTimeoutThrowOutOfRangeException( int timeout )
    {
        //Fake Task
        Task task = Task.Run( () => { } );

        _ = await Assert.ThrowsAsync<ArgumentOutOfRangeException>( () => Async.Extensions.TaskExtensions.WaitAsync( task, timeout ) );
    }

    #endregion

    #endregion

    #region " executeasync "

    #region " task<t> executeasync<t> (task<t> task, int timeout) "

    [Theory]
    [InlineData( 5, 20 )]
    [InlineData( 10, 1000 )]
    public async Task ExecuteAsyncTLessThanTimeoutTakesLongReturnAnswer( int delay, int timeout )
    {
        Task<int> task = new( () => Sum( 2, 3, delay ) );
        int actual = await Async.Extensions.TaskExtensions.ExecuteAsync( task, timeout );
        Assert.Equal( 5, actual );
    }

    [Theory]
    [InlineData( 20, 5 )]
    [InlineData( 10000, 10 )]
    public async Task ExecuteAsyncTMoreThanTimeoutTakesLongThrowTimeoutException( int delay, int timeout )
    {
        Task<int> task = new( () => Sum( 2, 3, delay ) );
        _ = await Assert.ThrowsAsync<TimeoutException>( () => Async.Extensions.TaskExtensions.ExecuteAsync( task, timeout ) );
    }

    [Fact]
    public async Task ExecuteAsyncTTaskIsNullThrowArgumentNullException()
    {
        _ = await Assert.ThrowsAsync<ArgumentNullException>( () => Async.Extensions.TaskExtensions.ExecuteAsync<int>( null, 500 ) );
    }

    [Theory]
    [InlineData( -1 )]
    [InlineData( int.MaxValue * -1 )]
    public async Task ExecuteAsyncTOutOfRangeTimeoutThrowOutOfRangeException( int timeout )
    {
        //Fake Task
        Task<int> task = new( () => Sum( 2, 3, 20 ) );

        _ = await Assert.ThrowsAsync<ArgumentOutOfRangeException>( () => Async.Extensions.TaskExtensions.ExecuteAsync( task, timeout ) );
    }

    #endregion

    #region " task executeasync (task task, int timeout) "

    [Theory]
    [InlineData( 5, 20 )]
    [InlineData( 10, 1000 )]
    public async Task ExecuteAsyncLessThanTimeoutTakesLongReturnAnswer( int delay, int timeout )
    {
        Task task = new( () => Task.Delay( delay ).Wait() );
        await Async.Extensions.TaskExtensions.ExecuteAsync( task, timeout );
        Assert.True( true );
    }

    [Theory]
    [InlineData( 20, 5 )]
    [InlineData( 10000, 10 )]
    public async Task ExecuteAsyncMoreThanTimeoutTakesLongThrowTimeoutException( int delay, int timeout )
    {
        Task task = new( () => Task.Delay( delay ).Wait() );
        _ = await Assert.ThrowsAsync<TimeoutException>( () => Async.Extensions.TaskExtensions.ExecuteAsync( task, timeout ) );
    }

    [Fact]
    public async Task ExecuteAsyncTaskIsNullThrowArgumentNullException()
    {
        _ = await Assert.ThrowsAsync<ArgumentNullException>( () => Async.Extensions.TaskExtensions.ExecuteAsync<int>( null, 500 ) );
    }

    [Theory]
    [InlineData( -1 )]
    [InlineData( int.MaxValue * -1 )]
    public async Task ExecuteAsyncOutOfRangeTimeoutThrowOutOfRangeException( int timeout )
    {
        //Fake Task
        Task task = new( () => { } );

        _ = await Assert.ThrowsAsync<ArgumentOutOfRangeException>( () => Async.Extensions.TaskExtensions.ExecuteAsync( task, timeout ) );
    }

    #endregion

    #endregion
}
