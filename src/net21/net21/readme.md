# About

cc.isr.Std.Net21 is a .Net library which adds .NET Standard 2.1 functions to .NET Standard 2.0.

# How to Use

```
<ItemGroup>
<ProjectReference Include="c:\my\lib\vs\std\src\net21\net21\cc.isr.Std.Net21.csproj" Condition="'$(TargetFramework)' == 'netstandard2.0'" />
</ItemGroup>
<ItemGroup>
<PackageReference Include="cc.isr.Std.Net21" Version="1.1.8955" Condition="'$(TargetFramework)' == 'netstandard2.0'" />
</ItemGroup>
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

cc.isr.Std.Electric is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Standard Framework Repository].

[Standard Framework Repository]: https://bitbucket.org/davidhary/dn.std

