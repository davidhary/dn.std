namespace System.Collections.Concurrent;

internal static class QueueExtensions
{
    /// <summary>
    /// Removes all objects from the System.Collections.Concurrent.ConcurrentQueue.
    /// </summary>
    /// <remarks>   2023-04-13. </remarks>
    /// <param name="queue">    The queue to act on. </param>
    public static void Clear<T>( this ConcurrentQueue<T> queue )
    {
        while ( queue.TryDequeue( out T _ ) )
        {
        }
    }

}
