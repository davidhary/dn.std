using System.Diagnostics;

namespace cc.isr.Std.Timers;

/// <summary> A micro timer. </summary>
/// <remarks>
/// (c) 2013 Ken Loveday. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-11-25 </para><para>
/// https://www.CodeProject.com/Articles/98346/Microsecond-and-Millisecond-NET-Timer. </para>
/// </remarks>
public class MicroTimer
{
    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public MicroTimer() : base()
    {
        this._timerThread = null;
        this._ignoreEventIfLateBy = long.MaxValue;
        this._interval = 0L;
        this._stopTimer = true;
    }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="microsecondsInterval"> The microseconds interval. </param>
    public MicroTimer( long microsecondsInterval ) : this() => this.Interval = microsecondsInterval;

    /// <summary> The interval. </summary>
    private long _interval;

    /// <summary> Gets or sets the interval in micro seconds. </summary>
    /// <value> The interval in micro seconds. </value>
    public long Interval
    {
        get => System.Threading.Interlocked.Read( ref this._interval );
        set => System.Threading.Interlocked.Exchange( ref this._interval, value );
    }

    /// <summary> Amount to ignore event if late by. </summary>
    private long _ignoreEventIfLateBy;

    /// <summary> Gets or sets the amount to ignore event if late by. </summary>
    /// <value> Amount to ignore event if late by. </value>
    public long IgnoreEventIfLateBy
    {
        get => System.Threading.Interlocked.Read( ref this._ignoreEventIfLateBy );
        set => System.Threading.Interlocked.Exchange( ref this._ignoreEventIfLateBy, value <= 0L ? long.MaxValue : value );
    }

    /// <summary> The timer thread. </summary>
    private Thread? _timerThread;

    /// <summary> Gets or sets the enabled. </summary>
    /// <value> The enabled. </value>
    public bool Enabled
    {
        get => this._timerThread is not null && this._timerThread.IsAlive;
        set
        {
            if ( value )
            {
                this.Start();
            }
            else
            {
                this.Stop();
            }
        }
    }

    /// <summary> Starts this object. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void Start()
    {
        if ( this.Enabled || this.Interval <= 0L ) return;

        this._stopTimer = false;
        void threadStart()
        {
            this.NotificationTimer( ref this._interval, ref this._ignoreEventIfLateBy, ref this._stopTimer );
        }

        this._timerThread = new Thread( threadStart ) { Priority = System.Threading.ThreadPriority.Highest };
        this._timerThread.Start();
    }

    /// <summary> True to stop timer. </summary>
    private bool _stopTimer;

    /// <summary> Stops the timer. </summary>
    public void Stop()
    {
        this._stopTimer = true;
    }

    /// <summary> Stops the timer. Waits for the current event to finish. Aborts
    /// the timer if the event does not finish the timeout, </summary>
    /// <param name="timeout"> The timeout. </param>
    public void Stop( TimeSpan timeout )
    {
        if ( !this.StopAndWait( timeout ) )
        {
            this.Abort();
        }
    }

    /// <summary> Stops and wait. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void StopAndWait()
    {
        _ = this.StopAndWait( System.Threading.Timeout.InfiniteTimeSpan );
    }

    /// <summary> Stops and wait. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="timeout"> The timeout. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    public bool StopAndWait( TimeSpan timeout )
    {
        this._stopTimer = true;
        return !this.Enabled || this._timerThread!.ManagedThreadId == System.Threading.Thread.CurrentThread.ManagedThreadId || this._timerThread.Join( timeout );
    }

    /// <summary> Aborts the timer (aborts the time thread). </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void Abort()
    {
        this._stopTimer = true;
#if false
        // thread abort is obsolete; is it necessary here?
        if ( this.Enabled )
        {
            this._timerThread.Abort();
        }
#endif
    }

    /// <summary> Notification timer. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="interval">            [in,out] The interval in micro seconds. </param>
    /// <param name="ignoreEventIfLateBy"> [in,out] Amount to ignore event if late by. </param>
    /// <param name="stopTimer">           [in,out] True to stop timer. </param>
    private void NotificationTimer( ref long interval, ref long ignoreEventIfLateBy, ref bool stopTimer )
    {
        int timerCount = 0;
        long nextNotification = 0L;
        MicroStopwatch microStopwatch = new();
        microStopwatch.Start();
        while ( !stopTimer )
        {
            long callbackFunctionExecutionTime = microStopwatch.ElapsedMicroseconds - nextNotification;
            long timerIntervalInMicroSecCurrent = System.Threading.Interlocked.Read( ref interval );
            long ignoreEventIfLateByCurrent = System.Threading.Interlocked.Read( ref ignoreEventIfLateBy );
            nextNotification += timerIntervalInMicroSecCurrent;
            timerCount += 1;
            long elapsedMicroseconds = microStopwatch.ElapsedMicroseconds;
            while ( elapsedMicroseconds < nextNotification )
            {
                System.Threading.Thread.SpinWait( 10 );
                elapsedMicroseconds = microStopwatch.ElapsedMicroseconds;
            }

            long timerLateBy = elapsedMicroseconds - nextNotification;
            if ( timerLateBy >= ignoreEventIfLateByCurrent )
            {
                continue;
            }

            MicroTimerEventArgs microTimerEventArgs = new( timerCount, elapsedMicroseconds, timerLateBy, callbackFunctionExecutionTime );
            MicroTimerElapsed?.Invoke( this, microTimerEventArgs );
        }

        microStopwatch.Stop();
    }

    /// <summary> Event queue for all listeners interested in MicroTimerElapsed events. </summary>
    public event EventHandler<MicroTimerEventArgs>? MicroTimerElapsed;
}
/// <summary> Additional information for micro timer events. </summary>
/// <remarks>
/// (c) 2013 Ken Loveday. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-11-25 </para><para>
/// https://www.CodeProject.com/Articles/98346/Microsecond-and-Millisecond-NET-Timer. </para>
/// </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 2020-09-22. </remarks>
/// <param name="timerCount">                    The number of timers. </param>
/// <param name="elapsedMicroseconds">           The elapsed microseconds. </param>
/// <param name="timerLateBy">                   Amount to timer late by. </param>
/// <param name="callbackFunctionExecutionTime"> The callback function execution time. </param>
public class MicroTimerEventArgs( int timerCount, long elapsedMicroseconds, long timerLateBy, long callbackFunctionExecutionTime ) : EventArgs()
{
    /// <summary>
    /// Gets Simple counter, number times timed event (callback function) executed.
    /// </summary>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <value> The number of timers. </value>
    public int TimerCount { get; private set; } = timerCount;

    /// <summary> Gets the time when timed event was called since timer started. </summary>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <value> The elapsed microseconds. </value>
    public long ElapsedMicroseconds { get; private set; } = elapsedMicroseconds;

    /// <summary>
    /// Gets the how late the timer was compared to when it should have been called.
    /// </summary>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <value> Amount to timer late by. </value>
    public long TimerLateBy { get; private set; } = timerLateBy;

    /// <summary>
    /// Gets the time it took to execute previous call to callback function (OnTimedEvent).
    /// </summary>
    /// <value> The callback function execution time. </value>
    public long CallbackFunctionExecutionTime { get; private set; } = callbackFunctionExecutionTime;
}
/// <summary> A micro stopwatch. </summary>
/// <remarks>
/// (c) 2013 Ken Loveday. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-11-25 </para><para>
/// https://www.CodeProject.com/Articles/98346/Microsecond-and-Millisecond-NET-Timer. </para>
/// </remarks>
public class MicroStopwatch : Stopwatch
{
    /// <summary> The micro security per tick. </summary>
    private readonly double _microSecPerTick = 1000000.0d / Frequency;

    /// <summary>
    /// Initializes a new instance of the <see cref="Stopwatch" /> class.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    public MicroStopwatch()
    {
        if ( !IsHighResolution )
        {
            throw new InvalidOperationException( "On this system the high-resolution performance counter is not available" );
        }
    }

    /// <summary> Gets the elapsed microseconds. </summary>
    /// <value> The elapsed microseconds. </value>
    public long ElapsedMicroseconds => ( long ) Math.Truncate( this.ElapsedTicks * this._microSecPerTick );
}
