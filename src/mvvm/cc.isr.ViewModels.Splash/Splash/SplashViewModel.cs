using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace cc.isr.ViewModels.Splash;

/// <summary>   A ViewModel for the splash. </summary>
/// <remarks>   2023-05-25. </remarks>
/// <remarks>   Constructor. </remarks>
/// <remarks>   2023-05-25. </remarks>
/// <param name="splashDisplay">    The splash display. </param>
public partial class SplashViewModel( IDisposable splashDisplay ) : ObservableObject
{
    /// <summary>   The message to display. </summary>
    [ObservableProperty]
    private string? _message;

    /// <summary>   The small application caption. </summary>
    [ObservableProperty]
    private string? _smallApplicationCaption;

    /// <summary>   The large application caption. </summary>
    [ObservableProperty]
    private string? _largeApplicationCaption;

    [ObservableProperty]
    [NotifyPropertyChangedFor( nameof( LargeApplicationCaption ) )]
    private int _width;

    [ObservableProperty]
    [NotifyPropertyChangedFor( nameof( LargeApplicationCaption ) )]
    private int _height;

    /// <summary>   True to top most. </summary>
    [ObservableProperty]
    private bool _topMost;

    /// <summary> Displays a message on the splash screen. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The message. </param>
    [RelayCommand( CanExecute = nameof( CanDisplay ), FlowExceptionsToTaskScheduler = true )]
    public async Task DisplayMessage( string value )
    {
        this.Message = value;
        await Task.Delay( 10 );
        // await Task.Factory.StartNew( () => { this.Message = value; } );
    }

    /// <summary>   Gets or sets the splash display. </summary>
    /// <value> The splash display. </value>
    public IDisposable? SplashDisplay { get; set; } = splashDisplay;

    /// <summary>   Determine if we can display. </summary>
    /// <remarks>   2023-05-25. </remarks>
    /// <returns>   True if we can display, false if not. </returns>
    public bool CanDisplay()
    {
        return this.SplashDisplay is not null && !this.IsDisposed( this.SplashDisplay );
    }

    /// <summary>   Query if 'disposable' is disposed. </summary>
    /// <remarks>   2023-05-25. </remarks>
    /// <param name="disposable">   The disposable. </param>
    /// <returns>   True if disposed, false if not. </returns>
    private bool IsDisposed( IDisposable disposable )
    {
        try
        {
            _ = disposable!.ToString();
            return false;
        }
        catch ( ObjectDisposedException )
        {
            return true;
        }
    }

}
