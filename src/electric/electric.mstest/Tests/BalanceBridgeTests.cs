namespace cc.isr.Std.Electric.Tests;

/// <summary> A balance bridge unit tests. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-09-23 </para>
/// </remarks>
[TestClass]
public class BalanceBridgeTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " construction tests "

    /// <summary> (Unit Test Method) tests build balance bridge. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void BuildBalanceBridgeTest()
    {
        SampleBridge? sampleBridge = BalanceBridges.LowTempZeroPressureBridge?.Clone();
        Assert.IsNotNull( sampleBridge );

        BalanceBridge? bridge = sampleBridge.BalanceBridge;
        Assert.IsNotNull( bridge );

        // test expected values
        double expectedResistance = 0d;
        if ( bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries )
        {
            expectedResistance = Resistor.ShuntConductor( bridge.NakedBridge.TopRight, bridge.BridgeBalance.Values[0] );
        }
        else if ( bridge.BridgeBalance.Layout == BalanceLayout.RightShuntRightSeries )
        {
            expectedResistance = Resistor.ShuntConductor( bridge.NakedBridge.TopRight, bridge.BridgeBalance.Values[0] ) + bridge.BridgeBalance.Values[1];
        }

        double actualResistance = bridge.TopRight;
        Assert.AreEqual( expectedResistance, actualResistance, BalanceBridges.ElementEpsilon );
        if ( bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries )
        {
            expectedResistance = Resistor.ToSeries( bridge.NakedBridge.BottomRight, bridge.BridgeBalance.Values[1] );
        }
        else if ( bridge.BridgeBalance.Layout == BalanceLayout.RightShuntRightSeries )
        {
            expectedResistance = Resistor.ShuntConductor( bridge.NakedBridge.BottomRight, bridge.BridgeBalance.Values[3] ) + bridge.BridgeBalance.Values[2];
        }

        actualResistance = bridge.BottomRight;
        Assert.AreEqual( expectedResistance, actualResistance, BalanceBridges.ElementEpsilon );
        if ( bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries )
        {
            expectedResistance = Resistor.ShuntConductor( bridge.NakedBridge.TopLeft, bridge.BridgeBalance.Values[3] );
        }
        else if ( bridge.BridgeBalance.Layout == BalanceLayout.RightShuntRightSeries )
        {
            expectedResistance = bridge.NakedBridge.TopLeft;
        }

        actualResistance = bridge.TopLeft;
        Assert.AreEqual( expectedResistance, actualResistance, BalanceBridges.ElementEpsilon );
        if ( bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries )
        {
            expectedResistance = Resistor.ToSeries( bridge.NakedBridge.BottomLeft, bridge.BridgeBalance.Values[2] );
        }
        else if ( bridge.BridgeBalance.Layout == BalanceLayout.RightShuntRightSeries )
        {
            expectedResistance = bridge.NakedBridge.BottomLeft;
        }

        actualResistance = bridge.BottomLeft;
        Assert.AreEqual( expectedResistance, actualResistance, BalanceBridges.ElementEpsilon );
    }

    /// <summary> (Unit Test Method) tests build balance bridge. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void BuildInitialBalanceBridgeTest()
    {
        SampleBridge? sampleBridge = BalanceBridges.LowTempZeroPressureBridge?.Clone();
        Assert.IsNotNull( sampleBridge );
        Assert.IsNotNull( sampleBridge.BalanceBridge );
        BalanceBridge bridge = new( sampleBridge.Bridge, sampleBridge.BalanceBridge.BridgeBalance.Layout );

        // test expected values
        double expectedResistance = 0d;
        if ( bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries )
        {
            expectedResistance = Resistor.ShuntConductor( bridge.NakedBridge.TopRight, bridge.BridgeBalance.Values[0] );
        }
        else if ( bridge.BridgeBalance.Layout == BalanceLayout.RightShuntRightSeries )
        {
            expectedResistance = Resistor.ShuntConductor( bridge.NakedBridge.TopRight, bridge.BridgeBalance.Values[0] ) + bridge.BridgeBalance.Values[1];
        }

        double actualResistance = bridge.TopRight;
        Assert.AreEqual( expectedResistance, actualResistance, BalanceBridges.ElementEpsilon );
        if ( bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries )
        {
            expectedResistance = Resistor.ToSeries( bridge.NakedBridge.BottomRight, bridge.BridgeBalance.Values[1] );
        }
        else if ( bridge.BridgeBalance.Layout == BalanceLayout.RightShuntRightSeries )
        {
            expectedResistance = Resistor.ShuntConductor( bridge.NakedBridge.BottomRight, bridge.BridgeBalance.Values[3] ) + bridge.BridgeBalance.Values[2];
        }

        actualResistance = bridge.BottomRight;
        Assert.AreEqual( expectedResistance, actualResistance, BalanceBridges.ElementEpsilon );
        if ( bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries )
        {
            expectedResistance = Resistor.ShuntConductor( bridge.NakedBridge.TopLeft, bridge.BridgeBalance.Values[3] );
        }
        else if ( bridge.BridgeBalance.Layout == BalanceLayout.RightShuntRightSeries )
        {
            expectedResistance = bridge.NakedBridge.TopLeft;
        }

        actualResistance = bridge.TopLeft;
        Assert.AreEqual( expectedResistance, actualResistance, BalanceBridges.ElementEpsilon );
        if ( bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries )
        {
            expectedResistance = Resistor.ToSeries( bridge.NakedBridge.BottomLeft, bridge.BridgeBalance.Values[2] );
        }
        else if ( bridge.BridgeBalance.Layout == BalanceLayout.RightShuntRightSeries )
        {
            expectedResistance = bridge.NakedBridge.BottomLeft;
        }

        actualResistance = bridge.BottomLeft;
        Assert.AreEqual( expectedResistance, actualResistance, BalanceBridges.ElementEpsilon );
    }

    /// <summary> (Unit Test Method) tests build invalid bridge. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void BuildInvalidBridgeTest()
    {
        SampleBridge? sampleBridge = BalanceBridges.LowTempZeroPressureBridge?.Clone();
        Assert.IsNotNull( sampleBridge );

        BalanceBridge? bridge = sampleBridge.BalanceBridge;
        Assert.IsNotNull( bridge );

        bool actualValidity = false;
        bool expectedValidity = false;
        Assert.AreEqual( expectedValidity, actualValidity );
        actualValidity = bridge.IsValid();
        expectedValidity = true;
        Assert.AreEqual( expectedValidity, actualValidity );
        bridge.BridgeBalance = bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries ? new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout ) : new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout );
        actualValidity = bridge.IsValid();
        expectedValidity = true;
        Assert.AreEqual( expectedValidity, actualValidity );
        bridge.BridgeBalance = bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries ? new BridgeBalance( new double[] { Conductor.ShortConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout ) : new BridgeBalance( new double[] { Conductor.ShortConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout );
        actualValidity = bridge.IsValid();
        expectedValidity = false;
        Assert.AreEqual( expectedValidity, actualValidity );
        bridge.BridgeBalance = bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries ? new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout ) : new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout );
        actualValidity = bridge.IsValid();
        expectedValidity = true;
        Assert.AreEqual( expectedValidity, actualValidity );
        bridge.BridgeBalance = bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries ? new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.OpenResistance, 1d / 52357d }, bridge.BridgeBalance.Layout ) : new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.OpenResistance, 1d / 52357d }, bridge.BridgeBalance.Layout );
        actualValidity = bridge.IsValid();
        expectedValidity = false;
        Assert.AreEqual( expectedValidity, actualValidity );
        bridge.BridgeBalance = bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries ? new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout ) : new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout );
        actualValidity = bridge.IsValid();
        expectedValidity = true;
        Assert.AreEqual( expectedValidity, actualValidity );
        bridge.BridgeBalance = bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries ? new BridgeBalance( new double[] { Conductor.OpenConductance, Resistor.OpenResistance, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout ) : new BridgeBalance( new double[] { Conductor.OpenConductance, Resistor.OpenResistance, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout );
        actualValidity = bridge.IsValid();
        expectedValidity = false;
        Assert.AreEqual( expectedValidity, actualValidity );
        bridge.BridgeBalance = bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries ? new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout ) : new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout );
        actualValidity = bridge.IsValid();
        expectedValidity = true;
        Assert.AreEqual( expectedValidity, actualValidity );
        bridge.BridgeBalance = bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries ? new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, Conductor.ShortConductance }, bridge.BridgeBalance.Layout ) : new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, Conductor.ShortConductance }, bridge.BridgeBalance.Layout );
        actualValidity = bridge.IsValid();
        expectedValidity = false;
        Assert.AreEqual( expectedValidity, actualValidity );
        bridge.BridgeBalance = bridge.BridgeBalance.Layout == BalanceLayout.TopShuntBottomSeries ? new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout ) : new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout );
        actualValidity = bridge.IsValid();
        expectedValidity = true;
        Assert.AreEqual( expectedValidity, actualValidity );
    }

    /// <summary> (Unit Test Method) tests null bridge exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void NullBridgeExceptionTest()
    {
        Wheatstone? wheatstone = null; // New Wheatstone(New Double() {369.59, 363.04, 357.69, 377.06}, WheatstoneLayout.Counterclockwise)
                                       // try build the bridge using a null bridge; should issue the expected exception
        _ = Assert.ThrowsException<ArgumentNullException>( () => new BalanceBridge( wheatstone!, [Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d], BalanceLayout.TopShuntBottomSeries ),
            "Null bridge inappropriately allowed" );
    }

    /// <summary>   (Unit Test Method) bridge values must include four elements. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestMethod()]
    public void BridgeValuesMustIncludeFourElements()
    {
        Wheatstone wheatstone = new( [369.59d, 363.04d, 357.69d, 377.06d], WheatstoneLayout.Counterclockwise );
        // try build the bridge using a null bridge; should issue the expected exception
        _ = Assert.ThrowsException<InvalidOperationException>( () => new BalanceBridge( wheatstone!, [], BalanceLayout.TopShuntBottomSeries ),
            "bridge values must have 4 elements" );
    }

    /// <summary> (Unit Test Method) tests invalid values exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void InvalidValuesExceptionTest()
    {
        Wheatstone wheatstone = new( [369.59d, 363.04d, 357.69d, 377.06d], WheatstoneLayout.Counterclockwise );
        // try build the bridge using a null bridge; should issue the expected exception
        _ = Assert.ThrowsException<InvalidOperationException>( () => new BalanceBridge( wheatstone, [Conductor.OpenConductance, 10.3d], BalanceLayout.TopShuntBottomSeries ),
            "Invalid balance values inappropriately allowed" );
    }

    #endregion

    #region " output tests "

    /// <summary> (Unit Test Method) tests low temporary zero pressure output. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void LowTempZeroPressureOutputTest()
    {
        SampleBridge? sampleBridge = BalanceBridges.LowTempZeroPressureBridge;
        Assert.IsNotNull( sampleBridge );
        Assert.IsNotNull( sampleBridge.BalanceBridge );

        BalanceBridge bridge = sampleBridge.BalanceBridge;

        double relativeOffset = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage;
        double expectedRelativeOffset = bridge.NakedBridge.Output();
        Assert.AreEqual( expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon );
        relativeOffset = bridge.Output();
        expectedRelativeOffset = 0d;
        Assert.AreEqual( expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon );
        bool expectedBalance = Math.Abs( bridge.Output() ) < bridge.OutputEpsilon;
        bool actualBalance = bridge.IsOutputBalanced;
        Assert.AreEqual( expectedBalance, actualBalance );
        double outputVoltage = bridge.NakedBridge.Output( sampleBridge.BridgeVoltage );
        double expectedVoltage = sampleBridge.OutputVoltage;
        Assert.AreEqual( expectedVoltage, outputVoltage, BalanceBridges.OutputVoltageEpsilon );
    }

    /// <summary> (Unit Test Method) tests high temporary zero pressure output. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void HighTempZeroPressureOutputTest()
    {
        SampleBridge sampleBridge = BalanceBridges.HighTempZeroPressureBridge;
        Assert.IsNotNull( sampleBridge.BalanceBridge );

        BalanceBridge bridge = sampleBridge.BalanceBridge;

        double relativeOffset = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage;
        double expectedRelativeOffset = bridge.NakedBridge.Output();
        Assert.AreEqual( expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon );
        relativeOffset = bridge.Output();
        expectedRelativeOffset = 0d;
        Assert.AreEqual( expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon );
        bool expectedBalance = Math.Abs( bridge.Output() ) < bridge.OutputEpsilon;
        bool actualBalance = bridge.IsOutputBalanced;
        Assert.AreEqual( expectedBalance, actualBalance );
        double outputVoltage = bridge.NakedBridge.Output( sampleBridge.BridgeVoltage );
        double expectedVoltage = sampleBridge.OutputVoltage;
        Assert.AreEqual( expectedVoltage, outputVoltage, BalanceBridges.OutputVoltageEpsilon );
    }

    /// <summary> (Unit Test Method) tests balance bridge change. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void BalanceBridgeChangeTest()
    {
        SampleBridge? sampleBridge = BalanceBridges.HighTempZeroPressureBridge.Clone();
        Assert.IsNotNull( sampleBridge );
        Assert.IsNotNull( sampleBridge.BalanceBridge );

        BalanceBridge bridge = sampleBridge.BalanceBridge;

        sampleBridge = BalanceBridges.LowTempZeroPressureBridge;
        Assert.IsNotNull( sampleBridge );

        bridge.NakedBridge = sampleBridge.Bridge;

        double relativeOffset = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage;
        double expectedRelativeOffset = bridge.NakedBridge.Output();
        Assert.AreEqual( expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon );
        relativeOffset = bridge.Output();
        expectedRelativeOffset = 0d;
        Assert.AreEqual( expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon );
        bool expectedBalance = Math.Abs( bridge.Output() ) < bridge.OutputEpsilon;
        bool actualBalance = bridge.IsOutputBalanced;
        Assert.AreEqual( expectedBalance, actualBalance );
        double outputVoltage = bridge.NakedBridge.Output( sampleBridge.BridgeVoltage );
        double expectedVoltage = sampleBridge.OutputVoltage;
        Assert.AreEqual( expectedVoltage, outputVoltage, BalanceBridges.OutputVoltageEpsilon );
    }

    /// <summary> (Unit Test Method) tests balance bridge values no change. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void BalanceBridgeValuesNoChangeTest()
    {
        SampleBridge sampleBridge = BalanceBridges.HighTempZeroPressureBridge.Clone();
        Assert.IsNotNull( sampleBridge.BalanceBridge );

        BalanceBridge bridge = sampleBridge.BalanceBridge;

        bridge.BridgeBalance = new BridgeBalance( new double[] { Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout );
        double relativeOffset = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage;
        double expectedRelativeOffset = bridge.NakedBridge.Output();
        Assert.AreEqual( expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon );
        relativeOffset = bridge.Output();
        expectedRelativeOffset = 0d;
        Assert.AreEqual( expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon );
        bool expectedBalance = Math.Abs( bridge.Output() ) < bridge.OutputEpsilon;
        bool actualBalance = bridge.IsOutputBalanced;
        Assert.AreEqual( expectedBalance, actualBalance );
        double outputVoltage = bridge.NakedBridge.Output( sampleBridge.BridgeVoltage );
        double expectedVoltage = sampleBridge.OutputVoltage;
        Assert.AreEqual( expectedVoltage, outputVoltage, BalanceBridges.OutputVoltageEpsilon );
    }

    /// <summary> (Unit Test Method) tests balance bridge values change. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void BalanceBridgeValuesChangeTest()
    {
        SampleBridge sampleBridge = BalanceBridges.HighTempZeroPressureBridge.Clone();
        Assert.IsNotNull( sampleBridge.BalanceBridge );

        BalanceBridge bridge = sampleBridge.BalanceBridge;

        bridge.BridgeBalance = new BridgeBalance( new double[] { Conductor.OpenConductance, 11.5d, Resistor.ShortResistance, 1d / 52357d }, bridge.BridgeBalance.Layout );
        double relativeOffset = sampleBridge.OutputVoltage / sampleBridge.BridgeVoltage;
        double expectedRelativeOffset = bridge.NakedBridge.Output();
        Assert.AreEqual( expectedRelativeOffset, relativeOffset, 0.001d * bridge.OutputEpsilon );
        relativeOffset = bridge.Output();
        expectedRelativeOffset = 0d;
        Assert.AreEqual( expectedRelativeOffset, relativeOffset, sampleBridge.RelativeOffsetEpsilon );
        bool expectedBalance = Math.Abs( bridge.Output() ) < bridge.OutputEpsilon;
        bool actualBalance = bridge.IsOutputBalanced;
        Assert.AreEqual( expectedBalance, actualBalance );
        double outputVoltage = bridge.NakedBridge.Output( sampleBridge.BridgeVoltage );
        double expectedVoltage = sampleBridge.OutputVoltage;
        Assert.AreEqual( expectedVoltage, outputVoltage, 0.001d * BalanceBridges.OutputVoltageEpsilon );
    }

    #endregion

    #region " compensation tests "

    /// <summary> Validates the compensation. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="sampleBridge"> The sample bridge. </param>
    /// <param name="bridge">       The bridge. </param>
    private static void ValidateCompensation( SampleBridge sampleBridge, BalanceBridge bridge )
    {
        double relativeOffset = bridge.Output();
        double expectedRelativeOffset = 0d;
        Assert.AreEqual( expectedRelativeOffset, relativeOffset, bridge.OutputEpsilon );
        bool expectedBalance = Math.Abs( bridge.Output() ) < bridge.OutputEpsilon;
        bool actualBalance = bridge.IsOutputBalanced;
        Assert.AreEqual( expectedBalance, actualBalance );
        double outputVoltage = bridge.Output( sampleBridge.BridgeVoltage );
        double expectedVoltage = 0d;
        Assert.AreEqual( expectedVoltage, outputVoltage, BalanceBridges.OutputVoltageEpsilon );
    }

    /// <summary> (Unit Test Method) tests low temporary zero pressure shunt compensation. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void LowTempZeroPressureShuntCompensationTest()
    {
        SampleBridge? sampleBridge = BalanceBridges.LowTempZeroPressureBridge?.Clone();
        Assert.IsNotNull( sampleBridge );
        Assert.IsNotNull( sampleBridge.BalanceBridge );

        BalanceBridge bridge = sampleBridge.BalanceBridge;

        // apply shunt balance
        bridge.ApplyShuntCompensation();
        ValidateCompensation( sampleBridge, bridge );
    }

    /// <summary> (Unit Test Method) tests low temporary zero pressure Series compensation. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void LowTempZeroPressureSeriesCompensationTest()
    {
        SampleBridge? sampleBridge = BalanceBridges.LowTempZeroPressureBridge?.Clone();
        Assert.IsNotNull( sampleBridge );
        Assert.IsNotNull( sampleBridge.BalanceBridge );

        BalanceBridge bridge = sampleBridge.BalanceBridge;

        // apply shunt balance
        bridge.ApplySeriesCompensation();
        ValidateCompensation( sampleBridge, bridge );
    }

    /// <summary> (Unit Test Method) tests high temporary zero pressure shunt compensation. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void HighTempZeroPressureShuntCompensationTest()
    {
        SampleBridge sampleBridge = BalanceBridges.HighTempZeroPressureBridge.Clone();
        Assert.IsNotNull( sampleBridge.BalanceBridge );

        BalanceBridge bridge = sampleBridge.BalanceBridge;

        // apply shunt balance
        bridge.ApplyShuntCompensation();
        ValidateCompensation( sampleBridge, bridge );
    }

    /// <summary> (Unit Test Method) tests high temporary zero pressure series compensation. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void HighTempZeroPressureSeriesCompensationTest()
    {
        SampleBridge sampleBridge = BalanceBridges.HighTempZeroPressureBridge.Clone();
        Assert.IsNotNull( sampleBridge.BalanceBridge );

        BalanceBridge bridge = sampleBridge.BalanceBridge;

        // apply Series balance
        bridge.ApplySeriesCompensation();
        ValidateCompensation( sampleBridge, bridge );
    }

    #endregion
}

#region " sample bridge "
/// <summary> A sample bridge. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-09-23 </para>
/// </remarks>
public class SampleBridge
{
    /// <summary> Constructor using counter clock wise notation. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="elements">         The Wheatstone Bridge Elements. </param>
    /// <param name="wheatstoneLayout"> The Wheatstone Bridge layout. </param>
    public SampleBridge( IEnumerable<double> elements, WheatstoneLayout wheatstoneLayout ) : base() => this.Bridge = new Wheatstone( elements, wheatstoneLayout );

    /// <summary> Validates the sample bridge described by value. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="value"> The value. </param>
    /// <returns> A SampleBridge. </returns>
    private static SampleBridge ValidateSampleBridge( SampleBridge value )
    {
        return value is null ? throw new ArgumentNullException( nameof( value ) ) : value;
    }

    /// <summary> Cloning Constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="value"> The value. </param>
    public SampleBridge( SampleBridge value ) : base()
    {
        Assert.IsNotNull( value );
        Assert.IsNotNull( value.BalanceBridge );

        this.Bridge = new Wheatstone( ValidateSampleBridge( value ).Bridge );

        this.BalanceBridge = new BalanceBridge( value.BalanceBridge );
        this.SupplyVoltage = value.SupplyVoltage;
        this.SupplyVoltageDrop = value.SupplyVoltage;
        this.SupplyVoltageDrop = value.SupplyVoltageDrop;
        this.OutputVoltage = value.OutputVoltage;
        this.WheatstoneLayout = value.WheatstoneLayout;
        this.EquivalentResistance = value.EquivalentResistance;
        this.RelativeOffsetEpsilon = value.RelativeOffsetEpsilon;
        this.NominalVoltage = value.NominalVoltage;
        this.VoltageSourceSeriesResistance = value.VoltageSourceSeriesResistance;
        this.VoltageSourceParallelResistance = value.VoltageSourceParallelResistance;
        this.NominalFullScaleVoltage = value.NominalFullScaleVoltage;
        this.NominalCurrent = value.NominalCurrent;
        this.CurrentSourceSeriesResistance = value.CurrentSourceSeriesResistance;
        this.CurrentSourceParallelResistance = value.CurrentSourceParallelResistance;
    }

    /// <summary> Makes a deep copy of this object. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <returns> A copy of this object. </returns>
    public SampleBridge Clone()
    {
        return new SampleBridge( this );
    }

    /// <summary> Gets the bridge. </summary>
    /// <value> The bridge. </value>
    public Wheatstone Bridge { get; set; }

    /// <summary> Gets the supply voltage. </summary>
    /// <value> The supply voltage. </value>
    public double SupplyVoltage { get; set; } = 5.02058d;

    /// <summary> Gets the supply voltage drop. </summary>
    /// <value> The supply voltage drop. </value>
    public double SupplyVoltageDrop { get; set; } = 3.67312d;

    /// <summary> Gets the bridge voltage. </summary>
    /// <value> The bridge voltage. </value>
    public double BridgeVoltage => this.SupplyVoltage - this.SupplyVoltageDrop;

    /// <summary> Gets or sets the output voltage. </summary>
    /// <value> The output voltage. </value>
    public double OutputVoltage { get; set; } = 0.011728d;

    /// <summary> Gets or sets the wheatstone layout. </summary>
    /// <value> The wheatstone layout. </value>
    public WheatstoneLayout WheatstoneLayout { get; set; } = WheatstoneLayout.Clockwise;

    /// <summary> Gets or sets the equivalent resistance. </summary>
    /// <value> The equivalent resistance. </value>
    public double EquivalentResistance { get; set; } = 782d;

    /// <summary> Gets or sets the relative offset epsilon. </summary>
    /// <value> The relative offset epsilon. </value>
    public double RelativeOffsetEpsilon { get; set; } = 0.005d;

    /// <summary> Gets or sets the nominal voltage. </summary>
    /// <value> The nominal voltage. </value>
    public double NominalVoltage { get; set; } = 5d;

    /// <summary> Gets or sets the voltage source series resistance. </summary>
    /// <value> The voltage source series resistance. </value>
    public double VoltageSourceSeriesResistance { get; set; } = 1538d;

    /// <summary> Gets or sets the voltage source parallel resistance. </summary>
    /// <value> The voltage source parallel resistance. </value>
    public double VoltageSourceParallelResistance { get; set; } = 1593d;

    /// <summary> Gets or sets the nominal full scale voltage. </summary>
    /// <value> The nominal full scale voltage. </value>
    public double NominalFullScaleVoltage { get; set; } = 0.02d;

    /// <summary> Gets or sets the nominal current. </summary>
    /// <value> The nominal current. </value>
    public double NominalCurrent { get; set; } = 0.004d;

    /// <summary> Gets or sets the current source series resistance. </summary>
    /// <value> The current source series resistance. </value>
    /// <remarks>  636 this is calculated based on 5v, 0.004 v and the above resistances, original value is 632. </remarks>
    public double CurrentSourceSeriesResistance { get; set; } = 636d;

    /// <summary> Gets or sets the current source parallel resistance. </summary>
    /// <value> The current source parallel resistance. </value>
    public double CurrentSourceParallelResistance { get; set; } = 150d;

    /// <summary> Gets or sets the balance bridge. </summary>
    /// <value> The balance bridge. </value>
    public BalanceBridge? BalanceBridge { get; set; }
}

#endregion

