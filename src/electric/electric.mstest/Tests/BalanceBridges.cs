namespace cc.isr.Std.Electric.Tests;

/// <summary> Provides bridges for testing. </summary>
/// <remarks> David, 2020-09-23. </remarks>
internal sealed class BalanceBridges
{
    /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    private BalanceBridges() : base()
    { }

    /// <summary> Gets the element epsilon. </summary>
    /// <value> The element epsilon. </value>
    public static double ElementEpsilon { get; set; } = 0.000001d;

    /// <summary> Gets the output voltage epsilon. </summary>
    /// <value> The output voltage epsilon. </value>
    public static double OutputVoltageEpsilon { get; set; } = 0.0001d;

    /// <summary> Gets the full scale voltage epsilon. </summary>
    /// <value> The full scale voltage epsilon. </value>
    public static double FullScaleVoltageEpsilon { get; set; } = 0.0001d;

    /// <summary> Gets source resistance tolerance. </summary>
    /// <value> The source resistance tolerance. </value>
    public static double SourceResistanceTolerance { get; set; } = 0.001d;

    private static SampleBridge? _lowTempZeroPressureBridge;

    /// <summary> Gets the low temporary zero pressure bridge. </summary>
    /// <value> The low temporary zero pressure bridge. </value>
    public static SampleBridge? LowTempZeroPressureBridge
    {
        get
        {
            // 636 this is calculated based on 5v, 0.004 v and the above resistances
            if ( _lowTempZeroPressureBridge is null )
            {
                _lowTempZeroPressureBridge = new SampleBridge( [369.59d, 363.04d, 357.69d, 377.06d], WheatstoneLayout.Counterclockwise )
                {
                    SupplyVoltage = 5.02058d,
                    SupplyVoltageDrop = 3.67312d,
                    OutputVoltage = 0.011728d
                };
                _lowTempZeroPressureBridge.BalanceBridge = new BalanceBridge( _lowTempZeroPressureBridge.Bridge, [Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d], BalanceLayout.TopShuntBottomSeries );
                _lowTempZeroPressureBridge.EquivalentResistance = 782d;
                _lowTempZeroPressureBridge.RelativeOffsetEpsilon = 0.005d;
                _lowTempZeroPressureBridge.NominalVoltage = 5d;
                _lowTempZeroPressureBridge.VoltageSourceSeriesResistance = 1538d;
                _lowTempZeroPressureBridge.VoltageSourceParallelResistance = 1593d;
                _lowTempZeroPressureBridge.NominalFullScaleVoltage = 0.02d;
                _lowTempZeroPressureBridge.NominalCurrent = 0.004d;
                _lowTempZeroPressureBridge.CurrentSourceSeriesResistance = 636d;
                _lowTempZeroPressureBridge.CurrentSourceParallelResistance = 150d;
            }

            return _lowTempZeroPressureBridge;
        }
    }

    private static SampleBridge? _lowTempFullScalePressureBridge;

    /// <summary> Gets the low temporary full scale pressure bridge. </summary>
    /// <value> The low temporary full scale pressure bridge. </value>
    public static SampleBridge? LowTempFullScalePressureBridge
    {
        get
        {
            // 636 this is calculated based on 5v, 0.004 v and the above resistances
            if ( _lowTempFullScalePressureBridge is null )
            {
                _lowTempFullScalePressureBridge = new SampleBridge( [381.78d, 356.88d, 369.62d, 370.89d], WheatstoneLayout.Counterclockwise )
                {
                    SupplyVoltage = 5.02075d,
                    SupplyVoltageDrop = 3.66533d,
                    OutputVoltage = 0.02001d
                };
                _lowTempFullScalePressureBridge.BalanceBridge = new BalanceBridge( _lowTempFullScalePressureBridge.Bridge, [Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d], BalanceLayout.TopShuntBottomSeries );
                _lowTempFullScalePressureBridge.EquivalentResistance = 782d;
                _lowTempFullScalePressureBridge.RelativeOffsetEpsilon = 0.005d;
                _lowTempFullScalePressureBridge.NominalVoltage = 5d;
                _lowTempFullScalePressureBridge.VoltageSourceSeriesResistance = 1538d;
                _lowTempFullScalePressureBridge.VoltageSourceParallelResistance = 1593d;
                _lowTempFullScalePressureBridge.NominalFullScaleVoltage = 0.02d;
                _lowTempFullScalePressureBridge.NominalCurrent = 0.004d;
                _lowTempFullScalePressureBridge.CurrentSourceSeriesResistance = 636d;
                _lowTempFullScalePressureBridge.CurrentSourceParallelResistance = 150d;
            }

            return _lowTempFullScalePressureBridge;
        }
    }

    private static SampleBridge? _highTempZeroPressureBridge;

    /// <summary> Gets the high temporary zero pressure bridge. </summary>
    /// <value> The high temporary zero pressure bridge. </value>
    public static SampleBridge HighTempZeroPressureBridge
    {
        get
        {
            if ( _highTempZeroPressureBridge is null )
            {
                _highTempZeroPressureBridge = new SampleBridge( [563.79d, 563.19d, 556.32d, 573.16d], WheatstoneLayout.Counterclockwise )
                {
                    SupplyVoltage = 5.02155d,
                    SupplyVoltageDrop = 3.21047d,
                    OutputVoltage = 0.013019d // possibly a transcription error: 0.011728
                };
                _highTempZeroPressureBridge.BalanceBridge = new BalanceBridge( _highTempZeroPressureBridge.Bridge, [Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d], BalanceLayout.TopShuntBottomSeries );
                _highTempZeroPressureBridge.EquivalentResistance = 782d;
                _highTempZeroPressureBridge.RelativeOffsetEpsilon = 0.006d;
                _highTempZeroPressureBridge.NominalVoltage = 5d;
                _highTempZeroPressureBridge.VoltageSourceSeriesResistance = 1538d;
                _highTempZeroPressureBridge.VoltageSourceParallelResistance = 1593d;
                _highTempZeroPressureBridge.NominalFullScaleVoltage = 0.02d;
                _highTempZeroPressureBridge.NominalCurrent = 0.004d;
                // 636 this is calculated based on 5v, 0.004 v and the above resistances
                _highTempZeroPressureBridge.CurrentSourceSeriesResistance = 636d;
                _highTempZeroPressureBridge.CurrentSourceParallelResistance = 150d;
            }

            return _highTempZeroPressureBridge;
        }
    }

    /// <summary> The high temporary full scale pressure bridge. </summary>
    private static SampleBridge? _highTempFullScalePressureBridge;

    /// <summary> Gets the high temporary full scale pressure bridge. </summary>
    /// <value> The high temporary full scale pressure bridge. </value>
    public static SampleBridge HighTempFullScalePressureBridge
    {
        get
        {
            if ( _highTempFullScalePressureBridge is null )
            {
                _highTempFullScalePressureBridge = new SampleBridge( [577.21d, 555.43d, 569.81d, 565.23d], WheatstoneLayout.Counterclockwise )
                {
                    SupplyVoltage = 5.02152d,
                    SupplyVoltageDrop = 3.20471d,
                    OutputVoltage = 0.02001d
                };
                _highTempFullScalePressureBridge.BalanceBridge = new BalanceBridge( _highTempFullScalePressureBridge.Bridge, [Conductor.OpenConductance, 10.3d, Resistor.ShortResistance, 1d / 52357d], BalanceLayout.TopShuntBottomSeries );
                _highTempFullScalePressureBridge.EquivalentResistance = 782d;
                _highTempFullScalePressureBridge.RelativeOffsetEpsilon = 0.005d;
                _highTempFullScalePressureBridge.NominalVoltage = 5d;
                _highTempFullScalePressureBridge.VoltageSourceSeriesResistance = 1538d;
                _highTempFullScalePressureBridge.VoltageSourceParallelResistance = 1593d;
                _highTempFullScalePressureBridge.NominalFullScaleVoltage = 0.02d;
                _highTempFullScalePressureBridge.NominalCurrent = 0.004d;
                // 636 this is calculated based on 5v, 0.004 v and the above resistances
                _highTempFullScalePressureBridge.CurrentSourceSeriesResistance = 636d;
                _highTempFullScalePressureBridge.CurrentSourceParallelResistance = 150d;
            }

            return _highTempFullScalePressureBridge;
        }
    }
}
