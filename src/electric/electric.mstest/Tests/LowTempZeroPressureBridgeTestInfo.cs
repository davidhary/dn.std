using cc.isr.Json.AppSettings.Models;

namespace cc.isr.Std.Electric.Tests;

/// <summary>   Information about the current source test. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-11-26 </para>
/// </remarks>
internal sealed class LowTempZeroPressureBridgeTestInfo : CommunityToolkit.Mvvm.ComponentModel.ObservableObject
{
    #region " construction "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   2023-05-15. </remarks>
    public LowTempZeroPressureBridgeTestInfo()
    { }

    #endregion

    #region " singleton "

    /// <summary>   Creates the instance. </summary>
    /// <remarks>   2023-05-15. </remarks>
    /// <returns>   The new instance. </returns>
    private static LowTempZeroPressureBridgeTestInfo CreateInstance()
    {
        // get assembly files using the .Logging suffix.

        AssemblyFileInfo ai = new( typeof( LowTempZeroPressureBridgeTestInfo ).Assembly, null, ".Settings", ".json" );

        // must copy application context settings here to clear any bad settings files.

        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.AllUsersAssemblyFilePath! );
        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.ThisUserAssemblyFilePath! );

        LowTempZeroPressureBridgeTestInfo ti = new();
        AppSettingsScribe.ReadSettings( ai.AllUsersAssemblyFilePath!, nameof( LowTempZeroPressureBridgeTestInfo ), ti );

        return ti;
    }

    /// <summary>   Gets the instance. </summary>
    /// <value> The instance. </value>
    public static LowTempZeroPressureBridgeTestInfo Instance => _instance.Value;

    private static readonly Lazy<LowTempZeroPressureBridgeTestInfo> _instance = new( CreateInstance, true );

    #endregion

    #region " configuration information "

    private TraceLevel _traceLevel = TraceLevel.Verbose;

    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    [System.ComponentModel.Description( "Sets the message level" )]
    public TraceLevel TraceLevel
    {
        get => this._traceLevel;
        set => _ = this.SetProperty( ref this._traceLevel, value );
    }

    private bool _enabled = true;

    /// <summary>   Gets or sets a value indicating whether this object is enabled. </summary>
    /// <value> True if enabled, false if not. </value>
    [System.ComponentModel.Description( "True if testing is enabled for this test class" )]
    public bool Enabled
    {
        get => this._enabled;
        set => this.SetProperty( ref this._enabled, value );
    }

    private bool _all = true;

    /// <summary> Gets or sets all. </summary>
    /// <value> all. </value>
    [System.ComponentModel.Description( "True if all testing is enabled for this test class" )]
    public bool All
    {
        get => this._all;
        set => this.SetProperty( ref this._all, value );
    }

    #endregion

    #region " test info "

    /// <summary>   Gets or sets the layout. </summary>
    /// <value> The layout. </value>
    public int Layout { get; set; }

    /// <summary>   Gets or sets the r 1. </summary>
    /// <value> The r 1. </value>
    public double R1 { get; set; }
    /// <summary>   Gets or sets the r 2. </summary>
    /// <value> The r 2. </value>
    public double R2 { get; set; }
    /// <summary>   Gets or sets the r 3. </summary>
    /// <value> The r 3. </value>
    public double R3 { get; set; }
    /// <summary>   Gets or sets the r 4. </summary>
    /// <value> The r 4. </value>
    public double R4 { get; set; }
    /// <summary>   Gets or sets the supply voltage. </summary>
    /// <value> The supply voltage. </value>
    public double SupplyVoltage { get; set; }
    /// <summary>   Gets or sets the supply voltage drop. </summary>
    /// <value> The supply voltage drop. </value>
    public double SupplyVoltageDrop { get; set; }
    /// <summary>   Gets or sets the output voltage. </summary>
    /// <value> The output voltage. </value>
    public double OutputVoltage { get; set; }
    /// <summary>   Gets or sets the 1 balance. </summary>
    /// <value> The r 1 balance. </value>
    public double R1Balance { get; set; }
    /// <summary>   Gets or sets the 2 balance. </summary>
    /// <value> The r 2 balance. </value>
    public double R2Balance { get; set; }
    /// <summary>   Gets or sets the 3 balance. </summary>
    /// <value> The r 3 balance. </value>
    public double R3Balance { get; set; }
    /// <summary>   Gets or sets the 4 balance. </summary>
    /// <value> The r 4 balance. </value>
    public double R4Balance { get; set; }
    /// <summary>   Gets or sets the equivalent resistance. </summary>
    /// <value> The equivalent resistance. </value>
    public double EquivalentResistance { get; set; }
    /// <summary>   Gets or sets the relative offset epsilon. </summary>
    /// <value> The relative offset epsilon. </value>
    public double RelativeOffsetEpsilon { get; set; }
    /// <summary>   Gets or sets the nominal voltage. </summary>
    /// <value> The nominal voltage. </value>
    public double NominalVoltage { get; set; }
    /// <summary>   Gets or sets the voltage source series resistance. </summary>
    /// <value> The voltage source series resistance. </value>
    public double VoltageSourceSeriesResistance { get; set; }
    /// <summary>   Gets or sets the voltage source parallel resistance. </summary>
    /// <value> The voltage source parallel resistance. </value>
    public double VoltageSourceParallelResistance { get; set; }
    /// <summary>   Gets or sets the nominal full scale voltage. </summary>
    /// <value> The nominal full scale voltage. </value>
    public double NominalFullScaleVoltage { get; set; }
    /// <summary>   Gets or sets the nominal current. </summary>
    /// <value> The nominal current. </value>
    public double NominalCurrent { get; set; }
    /// <summary>   Gets or sets the current source series resistance. </summary>
    /// <value> The current source series resistance. </value>
    public double CurrentSourceSeriesResistance { get; set; }
    /// <summary>   Gets or sets the current source parallel resistance. </summary>
    /// <value> The current source parallel resistance. </value>
    public double CurrentSourceParallelResistance { get; set; }

    #endregion
}
