using cc.isr.Json.AppSettings.Models;

namespace cc.isr.Std.Electric.Tests;

/// <summary> Information about the current source test. </summary>
/// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-11-26 </para></remarks>
internal sealed class CurrentSourceTestInfo : CommunityToolkit.Mvvm.ComponentModel.ObservableObject
{
    #region " constructions "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   2023-05-15. </remarks>
    public CurrentSourceTestInfo()
    { }

    #endregion

    #region " singleton "

    /// <summary>   Creates the instance. </summary>
    /// <remarks>   2023-05-15. </remarks>
    /// <returns>   The new instance. </returns>
    private static CurrentSourceTestInfo CreateInstance()
    {
        // get assembly files using the .Logging suffix.

        AssemblyFileInfo ai = new( typeof( CurrentSourceTestInfo ).Assembly, null, ".Settings", ".json" );

        // must copy application context settings here to clear any bad settings files.

        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.AllUsersAssemblyFilePath! );
        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.ThisUserAssemblyFilePath! );

        CurrentSourceTestInfo ti = new();
        AppSettingsScribe.ReadSettings( ai.AllUsersAssemblyFilePath!, nameof( CurrentSourceTestInfo ), ti );

        return ti;
    }

    /// <summary>   Gets the instance. </summary>
    /// <value> The instance. </value>
    public static CurrentSourceTestInfo Instance => _instance.Value;

    private static readonly Lazy<CurrentSourceTestInfo> _instance = new( CreateInstance, true );

    #endregion

    #region " configuration information "

    private TraceLevel _traceLevel = TraceLevel.Verbose;

    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    [System.ComponentModel.Description( "Sets the message level" )]
    public TraceLevel TraceLevel
    {
        get => this._traceLevel;
        set => _ = this.SetProperty( ref this._traceLevel, value );
    }

    private bool _enabled = true;

    /// <summary>   Gets or sets a value indicating whether this object is enabled. </summary>
    /// <value> True if enabled, false if not. </value>
    [System.ComponentModel.Description( "True if testing is enabled for this test class" )]
    public bool Enabled
    {
        get => this._enabled;
        set => this.SetProperty( ref this._enabled, value );
    }

    private bool _all = true;

    /// <summary> Gets or sets all. </summary>
    /// <value> all. </value>
    [System.ComponentModel.Description( "True if all testing is enabled for this test class" )]
    public bool All
    {
        get => this._all;
        set => this.SetProperty( ref this._all, value );
    }

    #endregion

    #region " current source test settings "

    /// <summary> Gets or sets the nominal current. </summary>
    /// <value> The nominal current. </value>
    public double NominalCurrent { get; set; }

    /// <summary> Gets or sets source conductance. </summary>
    /// <value> The source conductance. </value>
    public double SourceConductance { get; set; }

    /// <summary> Gets or sets the load resistance. </summary>
    /// <value> The load resistance. </value>
    public double LoadResistance { get; set; }

    /// <summary> Gets or sets the nominal voltage. </summary>
    /// <value> The nominal voltage. </value>
    public double NominalVoltage { get; set; }

    #endregion

    #region " attenueted current source test settings "

    /// <summary> Gets or sets the series resistance. </summary>
    /// <value> The series resistance. </value>
    public double SeriesResistance { get; set; }

    /// <summary> Gets or sets the parallel resistance. </summary>
    /// <value> The parallel resistance. </value>
    public double ParallelResistance { get; set; }

    /// <summary> Gets the parallel conductance. </summary>
    /// <value> The parallel conductance. </value>
    public double ParallelConductance => Resistor.ToConductance( this.ParallelResistance );

    #endregion
}

