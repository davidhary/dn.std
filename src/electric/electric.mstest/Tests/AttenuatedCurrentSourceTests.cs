namespace cc.isr.Std.Electric.Tests;

/// <summary> Unit tests for the attenuated current source. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-12-12 </para>
/// </remarks>
[TestClass]
public class AttenuatedCurrentSourceTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " construction tests "

    /// <summary> (Unit Test Method) tests build. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void BuildTest()
    {
        double attenuation = AttenuatedCurrentSource.ToAttenuation( CurrentSourceTestInfo.Instance.SeriesResistance, CurrentSourceTestInfo.Instance.ParallelConductance );
        double equivalentConductance = Conductor.SeriesResistor( CurrentSourceTestInfo.Instance.ParallelConductance, CurrentSourceTestInfo.Instance.SeriesResistance );


        // construct a current source 
        _ = new CurrentSource( CurrentSourceTestInfo.Instance.NominalCurrent, equivalentConductance );
        AttenuatedCurrentSource? attenuatedSource = null;
        double doubleEpsilon = 0.0000000001d;
        string attenuatedSourceTarget = string.Empty;
        string sourceTarget = string.Empty;
        for ( int i = 1; i <= 3; i++ )
        {
            CurrentSource currentSource;
            switch ( i )
            {
                case 1:
                    {
                        attenuatedSourceTarget = "nominal attenuated current source";
                        attenuatedSource = new AttenuatedCurrentSource( ( decimal ) CurrentSourceTestInfo.Instance.NominalCurrent, CurrentSourceTestInfo.Instance.SeriesResistance, CurrentSourceTestInfo.Instance.ParallelConductance );
                        sourceTarget = "current source from nominal";
                        break;
                    }

                case 2:
                    {
                        attenuatedSourceTarget = "equivalent attenuated current source";
                        attenuatedSource = new AttenuatedCurrentSource( CurrentSourceTestInfo.Instance.NominalCurrent / attenuation, CurrentSourceTestInfo.Instance.SeriesResistance, CurrentSourceTestInfo.Instance.ParallelConductance );
                        sourceTarget = "current source from equivalent";
                        break;
                    }

                case 3:
                    {
                        attenuatedSourceTarget = "converted attenuated current source";
                        currentSource = new CurrentSource( CurrentSourceTestInfo.Instance.NominalCurrent / attenuation, equivalentConductance );
                        attenuatedSource = CurrentSource.ToAttenuatedCurrentSource( currentSource, attenuation );
                        sourceTarget = "current source from converted";
                        break;
                    }

                default:
                    break;
            }

            currentSource = attenuatedSource!.ToCurrentSource();

            // test the attenuated voltage source
            string target = attenuatedSourceTarget;
            double actualValue = attenuatedSource.NominalCurrent;
            double expectedValue = CurrentSourceTestInfo.Instance.NominalCurrent;
            string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            string item = "nominal current";
            string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = attenuatedSourceTarget;
            actualValue = attenuatedSource.Current;
            expectedValue = CurrentSourceTestInfo.Instance.NominalCurrent / attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            item = "short load current";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = sourceTarget;
            actualValue = currentSource.Current;
            expectedValue = CurrentSourceTestInfo.Instance.NominalCurrent / attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            item = "short load current";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = attenuatedSourceTarget;
            actualValue = attenuatedSource.Conductance;
            expectedValue = equivalentConductance;
            item = "equivalent conductance";
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = sourceTarget;
            actualValue = attenuatedSource.Conductance;
            expectedValue = equivalentConductance;
            item = "equivalent conductance";
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = attenuatedSourceTarget;
            actualValue = attenuatedSource.SeriesResistance;
            expectedValue = CurrentSourceTestInfo.Instance.SeriesResistance;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            item = "series resistance";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = attenuatedSourceTarget;
            actualValue = attenuatedSource.Attenuation;
            expectedValue = attenuation;
            item = "attenuation";
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = attenuatedSourceTarget;
            actualValue = attenuatedSource.ParallelConductance;
            expectedValue = CurrentSourceTestInfo.Instance.ParallelConductance;
            item = "parallel conductance";
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
        }
    }

    #endregion

    #region " exception conversion tests "

    /// <summary>
    /// (Unit Test Method) tests exception for converting a current source to an attenuated current
    /// source with invalid attenuation.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ConvertAttenuationExceptionTest()
    {
        double attenuation = AttenuatedVoltageSource.MINIMUM_ATTENUATION - 0.01d;
        CurrentSource currentSource = new( CurrentSourceTestInfo.Instance.NominalCurrent, CurrentSourceTestInfo.Instance.SourceConductance );
        _ = Assert.ThrowsException<ArgumentOutOfRangeException>( () => CurrentSource.ToAttenuatedCurrentSource( currentSource, attenuation ),
            "Attenuation smaller than 1 inappropriately allowed" );
    }

    /// <summary>
    /// (Unit Test Method) tests exception for converting to an attenuated source using a null source.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ConvertNothingExceptionTest()
    {
        double attenuation = AttenuatedVoltageSource.MINIMUM_ATTENUATION + 0.01d;
        CurrentSource? currentSource = null;
        _ = Assert.ThrowsException<ArgumentNullException>( () => CurrentSource.ToAttenuatedCurrentSource( currentSource!, attenuation ),
            "Conversion with null source allowed" );
    }

    #endregion

    #region " load and property change tests "

    /// <summary> (Unit Test Method) tests loading an attenuated current source. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void LoadTest()
    {
        AttenuatedCurrentSource source = new( ( decimal ) CurrentSourceTestInfo.Instance.NominalCurrent, CurrentSourceTestInfo.Instance.SeriesResistance, CurrentSourceTestInfo.Instance.ParallelConductance );
        double doubleEpsilon = 0.0000000001d;

        // test the attenuated current source
        string target = "attenuated current source";

        // test open current
        string item = "open load current";
        double actualValue = source.LoadCurrent( Conductor.OpenConductance );
        double expectedValue = 0d;
        string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );
        item = "short load current";
        actualValue = source.LoadCurrent( Conductor.ShortConductance );
        expectedValue = ( double ) ( decimal ) CurrentSourceTestInfo.Instance.NominalCurrent / source.Attenuation;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test short voltage
        item = "short voltage";
        actualValue = source.LoadVoltage( 0d );
        expectedValue = 0d;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load current
        item = "load current";
        actualValue = source.LoadCurrent( Resistor.ToConductance( CurrentSourceTestInfo.Instance.LoadResistance ) );
        expectedValue = CurrentSourceTestInfo.Instance.NominalCurrent *
                        Conductor.OutputCurrent( CurrentSourceTestInfo.Instance.ParallelConductance,
                                                 Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Instance.LoadResistance,
                                                                                            CurrentSourceTestInfo.Instance.SeriesResistance ) ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load voltage
        item = "load voltage";
        actualValue = source.LoadVoltage( CurrentSourceTestInfo.Instance.LoadResistance );
        expectedValue = CurrentSourceTestInfo.Instance.NominalCurrent * CurrentSourceTestInfo.Instance.LoadResistance * Conductor.OutputCurrent( CurrentSourceTestInfo.Instance.ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Instance.LoadResistance, CurrentSourceTestInfo.Instance.SeriesResistance ) ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );
    }

    /// <summary>
    /// (Unit Test Method) tests changing properties of an attenuated voltage source.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void PropertiesChangeTest()
    {
        // test changing Attenuated voltage source properties
        AttenuatedCurrentSource source = new( ( decimal ) CurrentSourceTestInfo.Instance.NominalCurrent, CurrentSourceTestInfo.Instance.SeriesResistance, CurrentSourceTestInfo.Instance.ParallelConductance );
        double doubleEpsilon = 0.0000000001d;

        // test the attenuated voltage source
        string target = "attenuated current source";
        double currentGain = 1.5d;

        // test shot load current
        string item = $"short load current {currentGain}={nameof( currentGain )}";
        source.NominalCurrent *= currentGain;
        double actualValue = source.LoadCurrent( Conductor.ShortConductance );
        double expectedValue = currentGain * CurrentSourceTestInfo.Instance.NominalCurrent / source.Attenuation;
        string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test open current
        item = $"open current {currentGain}={nameof( currentGain )}";
        actualValue = source.LoadCurrent( Conductor.OpenConductance );
        expectedValue = 0d;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load current
        item = $"load current {currentGain}={nameof( currentGain )}";
        actualValue = source.LoadCurrent( Resistor.ToConductance( CurrentSourceTestInfo.Instance.LoadResistance ) );
        expectedValue = currentGain * CurrentSourceTestInfo.Instance.NominalCurrent * Conductor.OutputCurrent( CurrentSourceTestInfo.Instance.ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Instance.LoadResistance, CurrentSourceTestInfo.Instance.SeriesResistance ) ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );
        source.NominalCurrent /= currentGain;
        double seriesResistanceGain = 1.2d;
        source.SeriesResistance *= seriesResistanceGain;
        item = $"short load current {seriesResistanceGain}={nameof( seriesResistanceGain )}";
        actualValue = source.LoadCurrent( Conductor.ShortConductance );
        expectedValue = CurrentSourceTestInfo.Instance.NominalCurrent / source.Attenuation;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test short current
        item = $"open load current {seriesResistanceGain}={nameof( seriesResistanceGain )}";
        actualValue = source.LoadCurrent( Conductor.OpenConductance );
        expectedValue = 0d;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load current
        item = $"load current{seriesResistanceGain}={nameof( seriesResistanceGain )}";
        actualValue = source.LoadCurrent( Resistor.ToConductance( CurrentSourceTestInfo.Instance.LoadResistance ) );
        expectedValue = CurrentSourceTestInfo.Instance.NominalCurrent * Conductor.OutputCurrent( CurrentSourceTestInfo.Instance.ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Instance.LoadResistance, seriesResistanceGain * CurrentSourceTestInfo.Instance.SeriesResistance ) ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load voltage
        item = $"load voltage {seriesResistanceGain}={nameof( seriesResistanceGain )}";
        actualValue = source.LoadVoltage( CurrentSourceTestInfo.Instance.LoadResistance );
        expectedValue = CurrentSourceTestInfo.Instance.LoadResistance * CurrentSourceTestInfo.Instance.NominalCurrent * Conductor.OutputCurrent( CurrentSourceTestInfo.Instance.ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Instance.LoadResistance, seriesResistanceGain * CurrentSourceTestInfo.Instance.SeriesResistance ) ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );
        double parallelConductanceGain = 1.2d;
        source.SeriesResistance /= seriesResistanceGain;
        source.ParallelConductance *= parallelConductanceGain;

        // test short current
        item = $"Short load current {parallelConductanceGain}={nameof( parallelConductanceGain )}";
        actualValue = source.LoadCurrent( Conductor.ShortConductance );
        expectedValue = CurrentSourceTestInfo.Instance.NominalCurrent / source.Attenuation;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test open current
        item = $"open load current {parallelConductanceGain}={nameof( parallelConductanceGain )}";
        actualValue = source.LoadCurrent( Conductor.OpenConductance );
        expectedValue = 0d;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load voltage
        item = $"load voltage {parallelConductanceGain}={nameof( parallelConductanceGain )}";
        actualValue = source.LoadVoltage( CurrentSourceTestInfo.Instance.LoadResistance );
        expectedValue = CurrentSourceTestInfo.Instance.LoadResistance * CurrentSourceTestInfo.Instance.NominalCurrent * Conductor.OutputCurrent( parallelConductanceGain * CurrentSourceTestInfo.Instance.ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Instance.LoadResistance, CurrentSourceTestInfo.Instance.SeriesResistance ) ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load current
        item = $"load current {parallelConductanceGain}={nameof( parallelConductanceGain )}";
        actualValue = source.LoadCurrent( Resistor.ToConductance( CurrentSourceTestInfo.Instance.LoadResistance ) );
        expectedValue = CurrentSourceTestInfo.Instance.NominalCurrent * Conductor.OutputCurrent( parallelConductanceGain * CurrentSourceTestInfo.Instance.ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Instance.LoadResistance, CurrentSourceTestInfo.Instance.SeriesResistance ) ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );
        double attenuationChange = 1.2d;
        source.ParallelConductance /= parallelConductanceGain;
        double newAttenuation = source.Attenuation * attenuationChange;
        double parallelR = Conductor.ToResistance( source.Conductance * newAttenuation );
        double seriesR = (1d / source.Conductance) - parallelR;
        double newNominal = newAttenuation * CurrentSourceTestInfo.Instance.NominalCurrent / source.Attenuation;
        source.Attenuation *= attenuationChange;

        // test short current
        item = $"short load current {attenuationChange}={nameof( attenuationChange )}";
        actualValue = source.LoadCurrent( Conductor.ShortConductance );
        expectedValue = newNominal / source.Attenuation;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test open load current
        item = $"open load current {attenuationChange}={nameof( attenuationChange )}";
        actualValue = source.LoadCurrent( Conductor.OpenConductance );
        expectedValue = 0d;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load voltage
        item = $"load voltage {attenuationChange}={nameof( attenuationChange )}";
        actualValue = source.LoadVoltage( CurrentSourceTestInfo.Instance.LoadResistance );
        expectedValue = CurrentSourceTestInfo.Instance.LoadResistance * newNominal * Conductor.OutputCurrent( Resistor.ToConductance( parallelR ), Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Instance.LoadResistance, seriesR ) ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load current
        item = $"load current {attenuationChange}={nameof( attenuationChange )}";
        actualValue = source.LoadCurrent( Resistor.ToConductance( CurrentSourceTestInfo.Instance.LoadResistance ) );
        expectedValue = newNominal * Conductor.OutputCurrent( Resistor.ToConductance( parallelR ), Resistor.ToConductance( Resistor.ToSeries( CurrentSourceTestInfo.Instance.LoadResistance, seriesR ) ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );
    }

    #endregion
}
