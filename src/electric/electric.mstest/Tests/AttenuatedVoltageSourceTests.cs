namespace cc.isr.Std.Electric.Tests;

/// <summary> Unit tests for the attenuated voltage source. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-09-18 </para>
/// </remarks>
[TestClass]
public class AttenuatedVoltageSourceTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " construction tests "

    /// <summary> (Unit Test Method) tests building an attenuated voltage source. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void BuildTest()
    {
        double attenuation = AttenuatedVoltageSource.ToAttenuation( VoltageSourceTestInfo.Instance.SeriesResistance, VoltageSourceTestInfo.Instance.ParallelConductance );
        double equivalentResistance = Resistor.Parallel( VoltageSourceTestInfo.Instance.SeriesResistance, VoltageSourceTestInfo.Instance.ParallelResistance );

        // construct a voltage source 
        VoltageSource voltageSource;
        AttenuatedVoltageSource attenuatedSource = new( ( decimal ) VoltageSourceTestInfo.Instance.NominalVoltage, VoltageSourceTestInfo.Instance.SeriesResistance, VoltageSourceTestInfo.Instance.ParallelConductance );
        double doubleEpsilon = 0.0000000001d;
        string attenuatedSourceTarget = string.Empty;
        string sourceTarget = string.Empty;
        for ( int i = 1; i <= 3; i++ )
        {
            switch ( i )
            {
                case 1:
                    {
                        attenuatedSourceTarget = "nominal attenuated voltage source";
                        attenuatedSource = new AttenuatedVoltageSource( ( decimal ) VoltageSourceTestInfo.Instance.NominalVoltage, VoltageSourceTestInfo.Instance.SeriesResistance, VoltageSourceTestInfo.Instance.ParallelConductance );
                        sourceTarget = "voltage source from nominal";
                        break;
                    }

                case 2:
                    {
                        attenuatedSourceTarget = "equivalent attenuated voltage source";
                        attenuatedSource = new AttenuatedVoltageSource( VoltageSourceTestInfo.Instance.NominalVoltage / attenuation, VoltageSourceTestInfo.Instance.SeriesResistance, VoltageSourceTestInfo.Instance.ParallelConductance );
                        sourceTarget = "voltage source from equivalent";
                        break;
                    }

                case 3:
                    {
                        attenuatedSourceTarget = "converted attenuated voltage source";
                        voltageSource = new VoltageSource( VoltageSourceTestInfo.Instance.NominalVoltage / attenuation, equivalentResistance );
                        attenuatedSource = VoltageSource.ToAttenuatedVoltageSource( voltageSource, attenuation );
                        sourceTarget = "voltage source from converted";
                        break;
                    }

                default:
                    break;
            }

            voltageSource = attenuatedSource.ToVoltageSource();

            // test the attenuated voltage source
            string target = attenuatedSourceTarget;
            double actualValue = attenuatedSource.NominalVoltage;
            double expectedValue = VoltageSourceTestInfo.Instance.NominalVoltage;
            string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            string item = "nominal voltage";
            string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = attenuatedSourceTarget;
            actualValue = attenuatedSource.Voltage;
            expectedValue = VoltageSourceTestInfo.Instance.NominalVoltage / attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            item = "open load voltage";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = sourceTarget;
            actualValue = voltageSource.Voltage;
            expectedValue = VoltageSourceTestInfo.Instance.NominalVoltage / attenuation;
            item = "open load voltage";
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = attenuatedSourceTarget;
            actualValue = attenuatedSource.Resistance;
            expectedValue = equivalentResistance;
            item = "equivalent resistance";
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = sourceTarget;
            actualValue = voltageSource.Resistance;
            expectedValue = equivalentResistance;
            item = "equivalent resistance";
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = attenuatedSourceTarget;
            actualValue = attenuatedSource.SeriesResistance;
            expectedValue = VoltageSourceTestInfo.Instance.SeriesResistance;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            item = "series resistance";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = attenuatedSourceTarget;
            actualValue = attenuatedSource.Attenuation;
            expectedValue = attenuation;
            item = "attenuation";
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            target = attenuatedSourceTarget;
            actualValue = 1d / attenuatedSource.ParallelConductance;
            expectedValue = VoltageSourceTestInfo.Instance.ParallelResistance;
            item = "parallel resistance";
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
        }
    }

    /// <summary>
    /// (Unit Test Method) tests exception for converting a voltage source to an attenuated voltage
    /// source with invalid attenuation.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ConvertAttenuationExceptionTest()
    {
        double attenuation = AttenuatedVoltageSource.MINIMUM_ATTENUATION - 0.01d;
        VoltageSource voltageSource = new( VoltageSourceTestInfo.Instance.NominalVoltage, VoltageSourceTestInfo.Instance.SourceResistance );
        _ = Assert.ThrowsException<ArgumentOutOfRangeException>( () => VoltageSource.ToAttenuatedVoltageSource( voltageSource, attenuation ),
            "Attenuation smaller than 1 inappropriately allowed" );
    }

    /// <summary>
    /// (Unit Test Method) tests exception for converting to an attenuated source using a null source.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ConvertNothingExceptionTest()
    {
        double attenuation = AttenuatedVoltageSource.MINIMUM_ATTENUATION + 0.01d;
        VoltageSource? voltageSource = null;
        _ = Assert.ThrowsException<ArgumentNullException>( () => VoltageSource.ToAttenuatedVoltageSource( voltageSource!, attenuation ),
            "Conversion with null source allowed" );
    }

    #endregion

    #region " load and properties tests "

    /// <summary> (Unit Test Method) tests loading an attenuated voltage source. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void LoadTest()
    {
        AttenuatedVoltageSource source = new( ( decimal ) VoltageSourceTestInfo.Instance.NominalVoltage, VoltageSourceTestInfo.Instance.SeriesResistance, VoltageSourceTestInfo.Instance.ParallelConductance );
        double doubleEpsilon = 0.0000000001d;

        // test the attenuated voltage source
        string target = "attenuated voltage source";

        // test open load voltage
        string item = "open load voltage";
        double actualValue = source.LoadVoltage( Resistor.OpenResistance );
        double expectedValue = ( double ) ( decimal ) VoltageSourceTestInfo.Instance.NominalVoltage / source.Attenuation;
        string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test short load voltage
        item = "short load voltage";
        actualValue = source.LoadVoltage( 0d );
        expectedValue = 0d;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load voltage
        item = "load voltage";
        actualValue = source.LoadVoltage( VoltageSourceTestInfo.Instance.LoadResistance );
        expectedValue = source.NominalVoltage * Resistor.OutputVoltage( VoltageSourceTestInfo.Instance.SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Instance.LoadResistance, VoltageSourceTestInfo.Instance.ParallelResistance ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );


        // test load current
        item = "load current";
        actualValue = source.LoadCurrent( VoltageSourceTestInfo.Instance.LoadResistance );
        expectedValue = source.NominalVoltage * Resistor.OutputVoltage( VoltageSourceTestInfo.Instance.SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Instance.LoadResistance, VoltageSourceTestInfo.Instance.ParallelResistance ) ) / VoltageSourceTestInfo.Instance.LoadResistance;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );
    }

    /// <summary>
    /// (Unit Test Method) tests changing properties of an attenuated voltage source.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void PropertiesChangeTest()
    {
        // test changing Attenuated voltage source properties
        AttenuatedVoltageSource source = new( ( decimal ) VoltageSourceTestInfo.Instance.NominalVoltage, VoltageSourceTestInfo.Instance.SeriesResistance, VoltageSourceTestInfo.Instance.ParallelConductance );
        double doubleEpsilon = 0.0000000001d;

        // test the attenuated voltage source
        string target = "attenuated voltage source";
        double voltageGain = 1.5d;

        // test open voltage
        string item = $"open voltage {voltageGain}={nameof( voltageGain )}";
        source.NominalVoltage *= voltageGain;
        double actualValue = source.LoadVoltage( Resistor.OpenResistance );
        double expectedValue = voltageGain * VoltageSourceTestInfo.Instance.NominalVoltage / source.Attenuation;
        string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test short voltage
        item = $"short voltage {voltageGain}={nameof( voltageGain )}";
        actualValue = source.LoadVoltage( 0d );
        expectedValue = 0d;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load voltage
        item = $"load voltage {voltageGain}={nameof( voltageGain )}";
        actualValue = source.LoadVoltage( VoltageSourceTestInfo.Instance.LoadResistance );
        expectedValue = voltageGain * VoltageSourceTestInfo.Instance.NominalVoltage * Resistor.OutputVoltage( VoltageSourceTestInfo.Instance.SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Instance.LoadResistance, VoltageSourceTestInfo.Instance.ParallelResistance ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load current
        item = $"load current {voltageGain}={nameof( voltageGain )}";
        actualValue = source.LoadCurrent( VoltageSourceTestInfo.Instance.LoadResistance );
        expectedValue = voltageGain * VoltageSourceTestInfo.Instance.NominalVoltage * Resistor.OutputVoltage( VoltageSourceTestInfo.Instance.SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Instance.LoadResistance, VoltageSourceTestInfo.Instance.ParallelResistance ) ) / VoltageSourceTestInfo.Instance.LoadResistance;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );
        source.NominalVoltage /= voltageGain;
        double seriesResistanceGain = 1.2d;
        source.SeriesResistance *= seriesResistanceGain;
        item = $"open voltage {seriesResistanceGain}={nameof( seriesResistanceGain )}";
        actualValue = source.LoadVoltage( Resistor.OpenResistance );
        expectedValue = VoltageSourceTestInfo.Instance.NominalVoltage / source.Attenuation;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test short voltage
        item = $"short voltage {seriesResistanceGain}={nameof( seriesResistanceGain )}";
        actualValue = source.LoadVoltage( 0d );
        expectedValue = 0d;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load voltage
        item = $"load voltage {seriesResistanceGain}={nameof( seriesResistanceGain )}";
        actualValue = source.LoadVoltage( VoltageSourceTestInfo.Instance.LoadResistance );
        expectedValue = VoltageSourceTestInfo.Instance.NominalVoltage * Resistor.OutputVoltage( seriesResistanceGain * VoltageSourceTestInfo.Instance.SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Instance.LoadResistance, VoltageSourceTestInfo.Instance.ParallelResistance ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load current
        _ = source.LoadCurrent( VoltageSourceTestInfo.Instance.LoadResistance );
        _ = VoltageSourceTestInfo.Instance.NominalVoltage * Resistor.OutputVoltage( seriesResistanceGain * VoltageSourceTestInfo.Instance.SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Instance.LoadResistance, VoltageSourceTestInfo.Instance.ParallelResistance ) ) / VoltageSourceTestInfo.Instance.LoadResistance;
        item = $"load current {seriesResistanceGain}={nameof( seriesResistanceGain )}";
        actualValue = source.LoadCurrent( VoltageSourceTestInfo.Instance.LoadResistance );
        expectedValue = VoltageSourceTestInfo.Instance.NominalVoltage * Resistor.OutputVoltage( seriesResistanceGain * VoltageSourceTestInfo.Instance.SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Instance.LoadResistance, VoltageSourceTestInfo.Instance.ParallelResistance ) ) / VoltageSourceTestInfo.Instance.LoadResistance;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );
        double parallelConductanceGain = 1.2d;
        source.SeriesResistance /= seriesResistanceGain;
        source.ParallelConductance *= parallelConductanceGain;

        // test open voltage
        item = $"open voltage {parallelConductanceGain}={nameof( parallelConductanceGain )}";
        actualValue = source.LoadVoltage( Resistor.OpenResistance );
        expectedValue = VoltageSourceTestInfo.Instance.NominalVoltage / source.Attenuation;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test short voltage
        item = $"short voltage {parallelConductanceGain}={nameof( parallelConductanceGain )}";
        actualValue = source.LoadVoltage( 0d );
        expectedValue = 0d;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load voltage
        item = $"load voltage {parallelConductanceGain}={nameof( parallelConductanceGain )}";
        actualValue = source.LoadVoltage( VoltageSourceTestInfo.Instance.LoadResistance );
        expectedValue = VoltageSourceTestInfo.Instance.NominalVoltage * Resistor.OutputVoltage( VoltageSourceTestInfo.Instance.SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Instance.LoadResistance, VoltageSourceTestInfo.Instance.ParallelResistance / parallelConductanceGain ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load current
        item = $"load current {parallelConductanceGain}={nameof( parallelConductanceGain )}";
        actualValue = source.LoadCurrent( VoltageSourceTestInfo.Instance.LoadResistance );
        expectedValue = VoltageSourceTestInfo.Instance.NominalVoltage * Resistor.OutputVoltage( VoltageSourceTestInfo.Instance.SeriesResistance, Resistor.Parallel( VoltageSourceTestInfo.Instance.LoadResistance, VoltageSourceTestInfo.Instance.ParallelResistance / parallelConductanceGain ) ) / VoltageSourceTestInfo.Instance.LoadResistance;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );
        double attenuationChange = 1.2d;
        source.ParallelConductance /= parallelConductanceGain;
        double newAttenuation = source.Attenuation * attenuationChange;
        double seriesR = newAttenuation * source.Resistance;
        double parallelR = seriesR / (newAttenuation - 1d);
        double newNominal = newAttenuation * VoltageSourceTestInfo.Instance.NominalVoltage / source.Attenuation;
        source.Attenuation *= attenuationChange;

        // test open voltage
        item = $"open voltage {attenuationChange}={nameof( attenuationChange )}";
        actualValue = source.LoadVoltage( Resistor.OpenResistance );
        expectedValue = newNominal / source.Attenuation;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test short voltage
        item = $"short voltage {attenuationChange}={nameof( attenuationChange )}";
        actualValue = source.LoadVoltage( 0d );
        expectedValue = 0d;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load voltage
        item = $"load voltage {attenuationChange}={nameof( attenuationChange )}";
        actualValue = source.LoadVoltage( VoltageSourceTestInfo.Instance.LoadResistance );
        expectedValue = newNominal * Resistor.OutputVoltage( seriesR, Resistor.Parallel( VoltageSourceTestInfo.Instance.LoadResistance, parallelR ) );
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );

        // test load current
        item = $"load current {attenuationChange}={nameof( attenuationChange )}";
        actualValue = source.LoadCurrent( VoltageSourceTestInfo.Instance.LoadResistance );
        expectedValue = newNominal * Resistor.OutputVoltage( seriesR, Resistor.Parallel( VoltageSourceTestInfo.Instance.LoadResistance, parallelR ) ) / VoltageSourceTestInfo.Instance.LoadResistance;
        outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
        message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
        Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
        System.Diagnostics.Trace.TraceInformation( message );
    }

    #endregion
}
