namespace cc.isr.Std.Electric.Tests;

/// <summary> A conductance unit tests. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-09-16 </para>
/// </remarks>
[TestClass]
public class ConductorTests
{
    #region " construction and cleanup "

    /// <summary>   Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test
    /// in the class.
    /// </remarks>
    /// <param name="testContext">  Gets or sets the test context which provides information about
    ///                             and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            _ = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}";
            // moved to test initialize
            // Console.WriteLine( methodFullName );
        }
        catch ( Exception ex )
        {
            Console.WriteLine( $"Failed initializing fixture: {ex}" );
        }
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>											   
    [TestInitialize()]
    public virtual void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
    }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " test inputs "

    /// <summary> Gets or sets the left conductance. </summary>
    /// <value> The left conductance. </value>
    public static double LeftConductance { get; set; } = 0.001d;

    /// <summary> Gets or sets the right conductance. </summary>
    /// <value> The right conductance. </value>
    public static double RightConductance { get; set; } = 0.002d;

    /// <summary> Gets or sets the load conductance. </summary>
    /// <value> The load conductance. </value>
    public static double LoadConductance { get; set; } = 0.004d;

    /// <summary> Gets or sets the negative conductance. </summary>
    /// <value> The negative conductance. </value>
    public static double NegativeConductance { get; set; } = -0.001d;

    #endregion

    #region " parallel tests "

    /// <summary> (Unit Test Method) tests parallel. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ParallelTest()
    {
        double actualValue = Conductor.ToParallel( LeftConductance, RightConductance );
        double expectedValue = LeftConductance + RightConductance;
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests parallel short. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ParallelShortTest()
    {
        double actualValue = Conductor.ToParallel( Conductor.ShortConductance, RightConductance );
        double expectedValue = Conductor.ShortConductance;
        Assert.AreEqual( expectedValue, actualValue );
        actualValue = Conductor.ToParallel( LeftConductance, Conductor.ShortConductance );
        expectedValue = Conductor.ShortConductance;
        Assert.AreEqual( expectedValue, actualValue );
        actualValue = Conductor.ToParallel( Conductor.ShortConductance, Conductor.ShortConductance );
        expectedValue = Conductor.ShortConductance;
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests parallel open. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ParallelOpenTest()
    {
        double actualValue = Conductor.ToParallel( Conductor.OpenConductance, RightConductance );
        double expectedValue = RightConductance;
        Assert.AreEqual( expectedValue, actualValue );
        actualValue = Conductor.ToParallel( LeftConductance, Conductor.OpenConductance );
        expectedValue = LeftConductance;
        Assert.AreEqual( expectedValue, actualValue );
        actualValue = Conductor.ToParallel( Conductor.OpenConductance, Conductor.OpenConductance );
        expectedValue = Conductor.OpenConductance;
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests parallel negative left conductance exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ParallelNegativeLeftConductanceExceptionTest()
    {
        _ = Assert.ThrowsException<ArgumentOutOfRangeException>( () => Conductor.ToParallel( NegativeConductance, RightConductance ),
            "Negative conductance inappropriately allowed" );
    }

    /// <summary> (Unit Test Method) tests parallel negative right conductance exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ParallelNegativeRightConductanceExceptionTest()
    {
        _ = Assert.ThrowsException<ArgumentOutOfRangeException>( () => Conductor.ToParallel( LeftConductance, NegativeConductance ),
            "Negative conductance inappropriately allowed" );
    }

    #endregion

    #region " series tests "

    /// <summary> (Unit Test Method) tests series. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void SeriesTest()
    {
        double actualValue = Conductor.ToSeries( LeftConductance, RightConductance );
        double expectedValue = 1d / ((1d / LeftConductance) + (1d / RightConductance));
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests series short. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void SeriesShortTest()
    {
        double actualValue = Conductor.ToSeries( Conductor.ShortConductance, RightConductance );
        double expectedValue = RightConductance;
        Assert.AreEqual( expectedValue, actualValue );
        actualValue = Conductor.ToSeries( LeftConductance, Conductor.ShortConductance );
        expectedValue = LeftConductance;
        Assert.AreEqual( expectedValue, actualValue );
        actualValue = Conductor.ToSeries( Conductor.ShortConductance, Conductor.ShortConductance );
        expectedValue = Conductor.ShortConductance;
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests series open. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void SeriesOpenTest()
    {
        double actualValue = Conductor.ToSeries( Conductor.OpenConductance, RightConductance );
        double expectedValue = Conductor.OpenConductance;
        Assert.AreEqual( expectedValue, actualValue );
        actualValue = Conductor.ToSeries( LeftConductance, Conductor.OpenConductance );
        expectedValue = Conductor.OpenConductance;
        Assert.AreEqual( expectedValue, actualValue );
        actualValue = Conductor.ToSeries( Conductor.OpenConductance, Conductor.OpenConductance );
        expectedValue = Conductor.OpenConductance;
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests series negative left conductance exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void SeriesNegativeLeftConductanceExceptionTest()
    {
        _ = Assert.ThrowsException<ArgumentOutOfRangeException>( () => Conductor.ToSeries( NegativeConductance, RightConductance ),
            "Negative conductance inappropriately allowed" );
    }

    /// <summary> (Unit Test Method) tests series negative right conductance exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void SeriesNegativeRightConductanceExceptionTest()
    {
        _ = Assert.ThrowsException<ArgumentOutOfRangeException>( () => Conductor.ToSeries( LeftConductance, NegativeConductance ),
            "Negative conductance inappropriately allowed" );
    }

    #endregion

    #region " output current tests "

    /// <summary> (Unit Test Method) tests output current. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputCurrentTest()
    {
        double actualValue = Conductor.ToOutputCurrent( LeftConductance, RightConductance );
        double expectedValue = RightConductance / Conductor.Parallel( LeftConductance, RightConductance );
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests output current short. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputCurrentShortTest()
    {
        double actualValue = Conductor.ToOutputCurrent( Conductor.ShortConductance, RightConductance );
        double expectedValue = 0d;
        Assert.AreEqual( expectedValue, actualValue );
        actualValue = Conductor.ToOutputCurrent( LeftConductance, Conductor.ShortConductance );
        expectedValue = 1d;
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests output current short exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputCurrentShortsExceptionTest()
    {
        _ = Assert.ThrowsException<InvalidOperationException>( () => Conductor.ToOutputCurrent( Conductor.ShortConductance, Conductor.ShortConductance ),
            "Two shorts inappropriately allowed" );
    }

    /// <summary> (Unit Test Method) tests output current open. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputCurrentOpenTest()
    {
        double actualValue = Conductor.ToOutputCurrent( Conductor.OpenConductance, RightConductance );
        double expectedValue = 1d;
        Assert.AreEqual( expectedValue, actualValue );
        actualValue = Conductor.ToOutputCurrent( LeftConductance, Conductor.OpenConductance );
        expectedValue = 0d;
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests output current open exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputCurrentOpensExceptionTest()
    {
        _ = Assert.ThrowsException<InvalidOperationException>( () => Conductor.ToOutputCurrent( Conductor.OpenConductance, Conductor.OpenConductance ),
            "Two opens inappropriately allowed" );
    }

    /// <summary>
    /// (Unit Test Method) tests output current negative left conductance exception.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputCurrentNegativeLeftConductanceExceptionTest()
    {
        _ = Assert.ThrowsException<ArgumentOutOfRangeException>( () => Conductor.ToOutputCurrent( NegativeConductance, RightConductance ),
            "Negative conductance inappropriately allowed" );
    }

    /// <summary>
    /// (Unit Test Method) tests output current negative right conductance exception.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputCurrentNegativeRightConductanceExceptionTest()
    {
        _ = Assert.ThrowsException<ArgumentOutOfRangeException>( () => Conductor.ToOutputCurrent( LeftConductance, NegativeConductance ),
            "Negative conductance inappropriately allowed" );
    }

    #endregion

    #region " output voltage tests "

    /// <summary> (Unit Test Method) tests output voltage. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputVoltageTest()
    {
        double actualValue = Conductor.ToOutputVoltage( LeftConductance, RightConductance );
        double expectedValue = 1d / RightConductance * Conductor.ToSeries( LeftConductance, RightConductance );
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests output voltage short. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputVoltageShortTest()
    {
        double actualValue = Conductor.ToOutputVoltage( Conductor.ShortConductance, RightConductance );
        double expectedValue = 1d;
        Assert.AreEqual( expectedValue, actualValue );
        actualValue = Conductor.ToOutputVoltage( LeftConductance, Conductor.ShortConductance );
        expectedValue = 0d;
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests output voltage shorts exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputVoltageShortsExceptionTest()
    {
        _ = Assert.ThrowsException<InvalidOperationException>( () => Conductor.ToOutputVoltage( Conductor.ShortConductance, Conductor.ShortConductance ),
            "Two shorts inappropriately allowed" );
    }

    /// <summary> (Unit Test Method) tests output voltage open. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputVoltageOpenTest()
    {
        double actualValue = Conductor.ToOutputVoltage( Conductor.OpenConductance, RightConductance );
        double expectedValue = 0d;
        Assert.AreEqual( expectedValue, actualValue );
        actualValue = Conductor.ToOutputVoltage( LeftConductance, Conductor.OpenConductance );
        expectedValue = 1d;
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests output voltage opens exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputVoltageOpensExceptionTest()
    {
        _ = Assert.ThrowsException<InvalidOperationException>( () => Conductor.ToOutputVoltage( Conductor.OpenConductance, Conductor.OpenConductance ),
            "Two opens inappropriately allowed" );
    }

    /// <summary>
    /// (Unit Test Method) tests output voltage negative left conductance exception.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputVoltageNegativeLeftConductanceExceptionTest()
    {
        _ = Assert.ThrowsException<ArgumentOutOfRangeException>( () => Conductor.ToOutputVoltage( NegativeConductance, RightConductance ),
            "Negative conductance inappropriately allowed" );
    }

    /// <summary>
    /// (Unit Test Method) tests output voltage negative right conductance exception.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void OutputVoltageNegativeRightConductanceExceptionTest()
    {
        _ = Assert.ThrowsException<ArgumentOutOfRangeException>( () => Conductor.ToOutputVoltage( LeftConductance, NegativeConductance ),
            "Negative conductance inappropriately allowed" );
    }

    #endregion
}
