namespace cc.isr.Std.Electric.Tests;

/// <summary> Unit tests for testing attenuated voltage and current sources. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-09-18 </para>
/// </remarks>
[TestClass]
public class AttenuatedSourceTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " conversion tests "

    /// <summary> (Unit Test Method) tests convert attenuated current source. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ConvertAttenuatedCurrentSourceTest()
    {
        AttenuatedCurrentSource attenuatedCurrentSource = new( ( decimal ) CurrentSourceTestInfo.Instance.NominalCurrent,
                                                                   CurrentSourceTestInfo.Instance.SeriesResistance,
                                                                   CurrentSourceTestInfo.Instance.ParallelConductance );

        // create converted voltage sources using alternative conversions
        List<AttenuatedVoltageSource> sources = [];
        List<string> descriptions = [];
        sources.Add( attenuatedCurrentSource.ToAttenuatedVoltageSource( CurrentSourceTestInfo.Instance.NominalVoltage ) );
        descriptions.Add( "Voltage source from attenuated current source" );
        sources.Add( new VoltageSource( attenuatedCurrentSource ).ToAttenuatedVoltageSource( ( decimal ) CurrentSourceTestInfo.Instance.NominalVoltage ) );
        descriptions.Add( "Voltage source from voltage source" );
        sources.Add( attenuatedCurrentSource.ToVoltageSource().ToAttenuatedVoltageSource( ( decimal ) CurrentSourceTestInfo.Instance.NominalVoltage ) );
        descriptions.Add( "Voltage source from current source" );
        double doubleEpsilon = 0.0000000001d;
        AttenuatedVoltageSource source;
        for ( int i = 0, loopTo = sources.Count - 1; i <= loopTo; i++ )
        {
            source = sources[i];
            string description = descriptions[i];
            string item = $"nominal voltage";
            double actualValue = source.NominalVoltage;
            double expectedValue = CurrentSourceTestInfo.Instance.NominalVoltage;
            string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            string message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // the two sources have identical output resistance
            item = $"equivalent resistance";
            actualValue = source.Resistance;
            expectedValue = Conductor.ToResistance( attenuatedCurrentSource.Conductance );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // the sources have the same open load voltage
            item = $"open load voltage";
            actualValue = source.LoadVoltage( Resistor.OpenResistance );
            expectedValue = attenuatedCurrentSource.LoadVoltage( Resistor.OpenResistance );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // the sources have the same short load voltage
            item = $"short load voltage";
            actualValue = source.LoadVoltage( Resistor.ShortResistance );
            expectedValue = attenuatedCurrentSource.LoadVoltage( Resistor.ShortResistance );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // the sources have the same load voltage
            item = $"load voltage";
            actualValue = source.LoadVoltage( CurrentSourceTestInfo.Instance.LoadResistance );
            expectedValue = attenuatedCurrentSource.LoadVoltage( CurrentSourceTestInfo.Instance.LoadResistance );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
        }
    }

    /// <summary> (Unit Test Method) tests convert attenuated voltage source. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ConvertAttenuatedVoltageSourceTest()
    {
        double minimumImpedance = CurrentSourceTestInfo.Instance.NominalVoltage / CurrentSourceTestInfo.Instance.NominalCurrent;
        double impedance = AttenuatedVoltageSource.ToEquivalentResistance( CurrentSourceTestInfo.Instance.SeriesResistance, CurrentSourceTestInfo.Instance.ParallelConductance );
        double minImpedanceFactor = 2d * minimumImpedance / impedance;
        // make sure that the voltage source impedance is high enough for conversion from 5v to 4 ma.
        AttenuatedVoltageSource attenuatedVoltageSource = new( ( decimal ) CurrentSourceTestInfo.Instance.NominalVoltage, minImpedanceFactor * CurrentSourceTestInfo.Instance.SeriesResistance, minImpedanceFactor * CurrentSourceTestInfo.Instance.ParallelConductance );

        // create converted current sources using alternative conversions
        List<AttenuatedCurrentSource> sources = [];
        List<string> descriptions = [];
        sources.Add( attenuatedVoltageSource.ToAttenuatedCurrentSource( CurrentSourceTestInfo.Instance.NominalCurrent ) );
        descriptions.Add( "Current source from attenuated voltage source" );
        sources.Add( new CurrentSource( attenuatedVoltageSource ).ToAttenuatedCurrentSource( ( decimal ) CurrentSourceTestInfo.Instance.NominalCurrent ) );
        descriptions.Add( "Current source from current source" );
        sources.Add( attenuatedVoltageSource.ToCurrentSource().ToAttenuatedCurrentSource( ( decimal ) CurrentSourceTestInfo.Instance.NominalCurrent ) );
        descriptions.Add( "Current source from voltage source" );
        double doubleEpsilon = 0.0000000001d;
        AttenuatedCurrentSource source;
        for ( int i = 0, loopTo = sources.Count - 1; i <= loopTo; i++ )
        {
            source = sources[i];
            string description = descriptions[i];
            string item = $"nominal current";
            double actualValue = source.NominalCurrent;
            double expectedValue = CurrentSourceTestInfo.Instance.NominalCurrent;
            string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            string message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // the two sources have identical output resistance
            item = $"equivalent resistance";
            actualValue = Conductor.ToResistance( source.Conductance );
            expectedValue = attenuatedVoltageSource.Resistance;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // the sources have the same short load current
            item = $"short load current";
            actualValue = source.LoadCurrent( Conductor.ShortConductance );
            expectedValue = attenuatedVoltageSource.LoadCurrent( Resistor.ShortResistance );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // the sources have the same open load current
            item = $"open load current";
            actualValue = source.LoadCurrent( Conductor.OpenConductance );
            expectedValue = attenuatedVoltageSource.LoadCurrent( Resistor.OpenResistance );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // the sources have the same load current
            item = $"load current";
            actualValue = source.LoadCurrent( Resistor.ToConductance( CurrentSourceTestInfo.Instance.LoadResistance ) );
            expectedValue = attenuatedVoltageSource.LoadCurrent( CurrentSourceTestInfo.Instance.LoadResistance );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
        }
    }

    #endregion

    #region " exception conversion tests "

    /// <summary> (Unit Test Method) tests low nominal voltage source exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void LowNominalVoltageSourceExceptionTest()
    {
        AttenuatedCurrentSource currentSource = new( ( decimal ) CurrentSourceTestInfo.Instance.NominalCurrent,
            CurrentSourceTestInfo.Instance.SeriesResistance, CurrentSourceTestInfo.Instance.ParallelConductance );
        _ = Assert.ThrowsException<InvalidOperationException>( () => currentSource.ToAttenuatedVoltageSource( 0.99d * currentSource.LoadVoltage( Resistor.OpenResistance ) ),
            "Conversion from an attenuated current source to an attenuated voltage source with low nominal allowed" );
    }

    /// <summary> (Unit Test Method) tests low nominal current source exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void LowNominalCurrentSourceExceptionTest()
    {
        AttenuatedVoltageSource voltageSource = new( ( decimal ) CurrentSourceTestInfo.Instance.NominalVoltage, CurrentSourceTestInfo.Instance.SeriesResistance, CurrentSourceTestInfo.Instance.ParallelResistance );
        _ = Assert.ThrowsException<InvalidOperationException>( () => voltageSource.ToAttenuatedCurrentSource( 0.99d * voltageSource.LoadCurrent( Resistor.ShortResistance ) ),
            "Conversion from an attenuated voltage source to an attenuated current source with low nominal current allowed" );
    }

    /// <summary> (Unit Test Method) tests null attenuated current source exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void NullAttenuatedCurrentSourceExceptionTest()
    {
        AttenuatedCurrentSource currentSource = default!;
        AttenuatedVoltageSource source = new( ( decimal ) CurrentSourceTestInfo.Instance.NominalVoltage, CurrentSourceTestInfo.Instance.SeriesResistance, CurrentSourceTestInfo.Instance.ParallelConductance );
        _ = Assert.ThrowsException<ArgumentNullException>( () => source.FromAttenuatedCurrentSource( currentSource, ( decimal ) CurrentSourceTestInfo.Instance.NominalVoltage ),
            "Null Attenuated Current Source inappropriately allowed" );
    }

    /// <summary> (Unit Test Method) tests null attenuated voltage source exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void NullAttenuatedVoltageSourceExceptionTest()
    {
        AttenuatedVoltageSource voltageSource = default!;
        AttenuatedCurrentSource source = new( ( decimal ) CurrentSourceTestInfo.Instance.NominalCurrent, CurrentSourceTestInfo.Instance.SeriesResistance, CurrentSourceTestInfo.Instance.ParallelConductance );
        _ = Assert.ThrowsException<ArgumentNullException>( () => source.FromAttenuatedVoltageSource( voltageSource, ( double ) ( decimal ) CurrentSourceTestInfo.Instance.NominalCurrent ),
            "Null Attenuated Voltage Source inappropriately allowed" );
    }

    #endregion
}
