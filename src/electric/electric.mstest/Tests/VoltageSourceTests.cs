namespace cc.isr.Std.Electric.Tests;

/// <summary> A voltage source unit tests. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-09-18 </para>
/// </remarks>
[TestClass]
public class VoltageSourceTests
{
    #region " construction and cleanup "

    /// <summary>   Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test
    /// in the class.
    /// </remarks>
    /// <param name="testContext">  Gets or sets the test context which provides information about
    ///                             and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            _ = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}";
            // moved to test initialize
            // Console.WriteLine( methodFullName );
        }
        catch ( Exception ex )
        {
            Console.WriteLine( $"Failed initializing fixture: {ex}" );
        }
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>											   
    [TestInitialize()]
    public virtual void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
    }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " test inputs "

    /// <summary> Gets or sets the voltage of the voltage source. </summary>
    /// <value> The voltage. </value>
    public static double Voltage { get; set; } = 5d;

    /// <summary> Gets or sets the resistance of the voltage source. </summary>
    /// <value> The resistance. </value>
    public static double Resistance { get; set; } = 1000d;

    /// <summary> The load resistance. </summary>
    /// <value> The load resistance. </value>
    public static double LoadResistance { get; set; } = 500d;

    /// <summary> Gets or sets the open resistance. </summary>
    /// <value> The short resistance. </value>
    public static double OpenResistance { get; set; } = Resistor.OpenResistance;

    /// <summary> Gets or sets the current of the current source. </summary>
    /// <value> The current. </value>
    public static double Current { get; set; } = 0.004d;

    /// <summary> Gets or sets the conductance or the current source. </summary>
    /// <value> The conductance. </value>
    public static double Conductance { get; set; } = 0.001d;

    /// <summary> The voltage change scale. </summary>
    /// <value> The voltage change scale. </value>
    public static double VoltageChangeScale { get; set; } = 1.5d;

    /// <summary> The resistance change scale. </summary>
    /// <value> The resistance change scale. </value>
    public static double ResistanceChangeScale { get; set; } = 0.8d;

    /// <summary> Gets or sets the load voltage epsilon. </summary>
    /// <value> The load voltage epsilon. </value>
    public static double LoadVoltageEpsilon { get; set; } = 0.000000001d;

    /// <summary> Gets or sets the voltage to current level epsilon. </summary>
    /// <value> The voltage to current level epsilon. </value>
    public static double VoltageToCurrentLevelEpsilon { get; set; } = 0.000000001d;

    /// <summary> Gets or sets the voltage to current conductance epsilon. </summary>
    /// <value> The voltage to current conductance epsilon. </value>
    public static double VoltageToCurrentConductanceEpsilon { get; set; } = 0.000000001d;

    /// <summary> Gets or sets the voltage to current voltage level epsilon. </summary>
    /// <value> The voltage to current voltage level epsilon. </value>
    public static double VoltageToCurrentVoltageLevelEpsilon { get; set; } = 0.000000001d;

    /// <summary> Gets or sets the voltage to current resistance epsilon. </summary>
    /// <value> The voltage to current resistance epsilon. </value>
    public static double VoltageToCurrentResistanceEpsilon { get; set; } = 0.000000001d;

    #endregion

    #region " constuctor tests "

    /// <summary> (Unit Test Method) tests build voltage source. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void BuildVoltageSourceTest()
    {
        VoltageSource source = new( Voltage, Resistance );

        // test source voltage
        double actualValue = source.Voltage;
        double expectedValue = Voltage;
        Assert.AreEqual( expectedValue, actualValue );

        // test source resistance
        actualValue = source.Resistance;
        expectedValue = Resistance;
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) converts this object to a current source test. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ConvertToCurrentSourceTest()
    {
        // build voltage source
        VoltageSource voltageSource = new( CurrentSourceTests.Voltage, CurrentSourceTests.Resistance );

        // Convert to current source
        CurrentSource source = voltageSource.ToCurrentSource();

        // test source Current
        double actualValue = source.Current;
        double expectedValue = voltageSource.Voltage / voltageSource.Resistance;
        Assert.AreEqual( expectedValue, actualValue, VoltageToCurrentLevelEpsilon );

        // test source resistance
        actualValue = source.Conductance;
        expectedValue = 1d / voltageSource.Resistance;
        Assert.AreEqual( expectedValue, actualValue, VoltageToCurrentConductanceEpsilon );
        VoltageSource finalVoltageSource = source.ToVoltageSource();
        Assert.AreEqual( voltageSource.Voltage, finalVoltageSource.Voltage, VoltageToCurrentVoltageLevelEpsilon );
        Assert.AreEqual( voltageSource.Resistance, finalVoltageSource.Resistance, VoltageToCurrentResistanceEpsilon );
    }

    /// <summary> (Unit Test Method) tests null current source exception. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void NullCurrentSourceExceptionTest()
    {
        // use a null current source
        CurrentSource? currentSource = null;

        // convert to voltage source
        VoltageSource source = new( Voltage, Resistance );

        // should throw the expected exception
        _ = Assert.ThrowsException<ArgumentNullException>( () => source.FromCurrentSource( currentSource! ),
            "Null current source inappropriately allowed" );
    }

    #endregion

    #region " voltage tests "

    /// <summary> (Unit Test Method) tests voltage source. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void VoltageSourceTest()
    {
        VoltageSource source = new( Voltage, Resistance );

        // test open voltage
        double actualValue = source.LoadVoltage( Resistor.OpenResistance );
        double expectedValue = Voltage;
        Assert.AreEqual( expectedValue, actualValue );

        // test short voltage
        actualValue = source.LoadVoltage( Resistor.ShortResistance );
        expectedValue = 0d;
        Assert.AreEqual( expectedValue, actualValue );

        // test load voltage
        actualValue = source.LoadVoltage( LoadResistance );
        expectedValue = Voltage * Resistor.OutputVoltage( Resistance, LoadResistance );
        Assert.AreEqual( expectedValue, actualValue, LoadVoltageEpsilon );

        // test load current
        actualValue = source.LoadCurrent( LoadResistance );
        expectedValue = Voltage / Resistor.Series( Resistance, LoadResistance );
        Assert.AreEqual( expectedValue, actualValue );

        // test open voltage
        actualValue = source.LoadVoltage( OpenResistance );
        expectedValue = source.Voltage;
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) tests voltage source change. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void VoltageSourceChangeTest()
    {
        VoltageSource source = new( Voltage, Resistance );
        source.Voltage *= VoltageChangeScale;

        // test open voltage
        double actualValue = source.LoadVoltage( Resistor.OpenResistance );
        double expectedValue = VoltageChangeScale * Voltage;
        Assert.AreEqual( expectedValue, actualValue );

        // test short voltage
        actualValue = source.LoadVoltage( Resistor.ShortResistance );
        expectedValue = 0d;
        Assert.AreEqual( expectedValue, actualValue );

        // test load voltage
        actualValue = source.LoadVoltage( LoadResistance );
        expectedValue = VoltageChangeScale * Voltage * Resistor.OutputVoltage( Resistance, LoadResistance );
        Assert.AreEqual( expectedValue, actualValue );

        // test load current
        actualValue = source.LoadCurrent( LoadResistance );
        expectedValue = VoltageChangeScale * Voltage / Resistor.Series( Resistance, LoadResistance );
        Assert.AreEqual( expectedValue, actualValue );
        source.Voltage /= VoltageChangeScale;
        source.Resistance *= ResistanceChangeScale;

        // test open voltage
        actualValue = source.LoadVoltage( OpenResistance );
        expectedValue = source.Voltage;
        Assert.AreEqual( expectedValue, actualValue );

        // test short voltage
        actualValue = source.LoadVoltage( 0d );
        expectedValue = 0d;
        Assert.AreEqual( expectedValue, actualValue );

        // test load voltage
        actualValue = source.LoadVoltage( LoadResistance );
        expectedValue = Voltage * Resistor.OutputVoltage( ResistanceChangeScale * Resistance, LoadResistance );
        Assert.AreEqual( expectedValue, actualValue );

        // test load current
        actualValue = source.LoadCurrent( LoadResistance );
        expectedValue = Voltage / Resistor.Series( ResistanceChangeScale * Resistance, LoadResistance );
        Assert.AreEqual( expectedValue, actualValue );
    }

    #endregion
}
