using System.Diagnostics.CodeAnalysis;

namespace cc.isr.Std.Statistics;

/// <summary> Sample statistics. </summary>
/// <remarks>
/// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-11-19 </para>
/// </remarks>
public class SampleStatistics : ICloneable
{
    #region " constructor "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public SampleStatistics() : base()
    {
        this.ValuesList = [];
        this.ClearKnownStateThis();
    }

    /// <summary> The cloning constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="value"> The value. </param>
    public SampleStatistics( SampleStatistics value ) : this()
    {
        if ( value is not null )
        {
            this.ClearKnownState();
            this._mean = value.Mean;
            this._sigma = value.Sigma;
            this._sum = value.Sum;
            this._sumSquareDeviations = value.SumSquareDeviations;
            this._maximum = value.Maximum;
            this._minimum = value.Minimum;
            this._valuesArray = value.ValuesArray();
            this.AddValues( [.. value.ValuesList] );
        }
    }

    /// <summary> Creates a new object that is a copy of the current instance. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <returns> A new object that is a copy of this instance. </returns>
    public virtual object Clone()
    {
        return new SampleStatistics( this );
    }

    /// <summary> Clears values to their known (initial) state. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [MemberNotNull( nameof( _valuesArray ) )]
    private void ClearKnownStateThis()
    {
        this.Count = 0;
        this._mean = 0d;
        this._sigma = 0d;
        this._sum = 0d;
        this._sumSquareDeviations = 0d;
        this._maximum = double.MinValue;
        this._minimum = double.MaxValue;
        this._valuesArray = [];
        this.ValuesList.Clear();
    }

    /// <summary> Clears values to their known (initial) state. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public virtual void ClearKnownState()
    {
        this.ClearKnownStateThis();
    }

    #endregion

    #region " statistics "

    /// <summary> The maximum. </summary>
    private double _maximum;

    /// <summary> Gets the maximum. </summary>
    /// <value> The maximum value. </value>
    public double Maximum
    {
        get
        {
            if ( this.RangeRequired )
            {
                this.EvaluateRange();
            }

            return this._maximum;
        }
    }

    /// <summary> The minimum. </summary>
    private double _minimum;

    /// <summary> Gets the minimum. </summary>
    /// <value> The minimum value. </value>
    public double Minimum
    {
        get
        {
            if ( this.RangeRequired )
            {
                this.EvaluateRange();
            }

            return this._minimum;
        }
    }

    /// <summary> The mean. </summary>
    private double _mean;

    /// <summary> Gets the mean. </summary>
    /// <value> The mean value. </value>
    public double Mean
    {
        get
        {
            if ( this.MeanRequired )
            {
                _ = this.EvaluateMean();
            }

            return this._mean;
        }
    }

    /// <summary> The sigma. </summary>
    private double _sigma;

    /// <summary> Gets the sigma. </summary>
    /// <value> The sigma. </value>
    public double Sigma
    {
        get
        {
            if ( this.SigmaRequired )
                _ = this.EvaluateSigma( this.Mean );

            return this._sigma;
        }
    }

    /// <summary> Number of. </summary>
    private double _sum;

    /// <summary> Gets the number of. </summary>
    /// <value> The sum. </value>
    public double Sum
    {
        get
        {
            if ( this.MeanRequired )
                _ = this.EvaluateMean();

            return this._sum;
        }
    }

    /// <summary> The sum square deviations. </summary>
    private double _sumSquareDeviations;

    /// <summary> Gets the sum square deviations. </summary>
    /// <value> The total number of square deviations. </value>
    public double SumSquareDeviations
    {
        get
        {
            if ( this.SigmaRequired )
                _ = this.EvaluateSigma( this.Mean );

            return this._sumSquareDeviations;
        }
    }

    /// <summary> Returns true if the sample includes values. </summary>
    /// <value> any. </value>
    public bool Any => this.Count > 0;

    /// <summary> Gets or sets the number of values. </summary>
    /// <value> The count. </value>
    public int Count { get; protected set; }

    /// <summary> Gets the mean required. </summary>
    /// <value> The mean required. </value>
    protected bool MeanRequired { get; set; }

    /// <summary> Gets the statistics required. </summary>
    /// <value> The statistics required. </value>
    protected bool SigmaRequired { get; set; }

    /// <summary> Gets the range required. </summary>
    /// <value> The range required. </value>
    protected bool RangeRequired { get; set; }

    /// <summary> Gets the cast to array required. </summary>
    /// <value> The cast to array required. </value>
    protected bool CastToArrayRequired { get; set; }

    /// <summary>
    /// Gets the values changed indicating the values were either added or removed.
    /// </summary>
    /// <value> The values changed. </value>
    public bool ValuesChanged => this.CastToArrayRequired;

    #endregion

    #region " values "

    private double[] _valuesArray;

    /// <summary> Gets the array of values. </summary>
    /// <value> An array of values. </value>
    public double[] ValuesArray()
    {
        this.CastToArray();
        return this._valuesArray;
    }

    /// <summary> Gets or sets the values. </summary>
    /// <value> A list of values. </value>
    public IList<double> ValuesList { get; private set; }

    /// <summary> Adds a value. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="value"> The value. </param>
    public virtual void AddValue( double value )
    {
        this.MeanRequired = true;
        this.SigmaRequired = true;
        this.RangeRequired = true;
        this.CastToArrayRequired = true;
        this.ValuesList.Add( value );
        this.Count += 1;
    }

    /// <summary> Adds the values. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="values"> The values. </param>
    public void AddValues( double[] values )
    {
        if ( values is not null )
        {
            this.MeanRequired = true;
            this.SigmaRequired = true;
            this.RangeRequired = true;
            this.CastToArrayRequired = true;
            foreach ( double value in values )
            {
                this.ValuesList.Add( value );
                this.Count += 1;
            }
        }
    }

    /// <summary>   Adds the values. </summary>
    /// <remarks>   David, 2021-06-05. </remarks>
    /// <param name="values">           The values. </param>
    /// <param name="excludedIndexes">  The excluded indexes. </param>
    public void AddValues( double[] values, int[] excludedIndexes )
    {
        if ( values is not null )
        {
            if ( excludedIndexes is not null && excludedIndexes.Length > 0 )
            {
                this.MeanRequired = true;
                this.SigmaRequired = true;
                this.RangeRequired = true;
                this.CastToArrayRequired = true;
                int indexPointer = 0;
                int[] sortedIndexes = new int[excludedIndexes.Length];
                Array.Copy( excludedIndexes, sortedIndexes, excludedIndexes.Length );
                Array.Sort( sortedIndexes );
                int excludeIndex = sortedIndexes[indexPointer];
                for ( int i = 0; i < values.Length; i++ )
                {
                    if ( i == excludeIndex )
                    {
                        indexPointer += 1;
                        if ( indexPointer < sortedIndexes.Length )
                            excludeIndex = sortedIndexes[indexPointer];
                    }
                    else
                    {
                        this.ValuesList.Add( values[i] );
                        this.Count += 1;
                    }
                }
            }
            else
            {
                this.AddValues( values );
            }
        }
    }

    /// <summary> Removes the value at the index. </summary>
    /// <remarks> David, 2020-09-07. </remarks>
    /// <param name="index"> Zero-based index of the values. </param>
    public virtual void RemoveValueAt( int index )
    {
        this.MeanRequired = true;
        this.SigmaRequired = true;
        this.RangeRequired = true;
        this.CastToArrayRequired = true;
        this.ValuesList.RemoveAt( index );
        this.Count -= 1;
    }

    /// <summary>   Removes the values at the indexes. </summary>
    /// <remarks>   David, 2021-06-05. <para>
    /// <see cref="AddValues(double[], int[])"/> is faster.
    /// </para> </remarks>
    /// <param name="indexes">  The indexes. </param>
    public virtual void RemoveValuesAt( int[] indexes )
    {
        this.MeanRequired = true;
        this.SigmaRequired = true;
        this.RangeRequired = true;
        this.CastToArrayRequired = true;
        int[] sortedIndexes = new int[indexes.Length];
        Array.Copy( indexes, sortedIndexes, indexes.Length );
        Array.Sort( sortedIndexes );
        Array.Reverse( sortedIndexes );
        foreach ( int index in sortedIndexes )
        {
            this.ValuesList.RemoveAt( index );
            this.Count -= 1;
        }
    }

    #endregion

    #region " evaluate "

    /// <summary>
    /// Converts the internal <see cref="ValuesList">list of values</see> to an internal
    /// <see cref="ValuesArray">array of values</see>.
    /// </summary>
    /// <remarks>   David, 2020-10-09. </remarks>
    public void CastToArray()
    {
        if ( this.CastToArrayRequired )
        {
            this._valuesArray = [.. this.ValuesList];
            this.CastToArrayRequired = false;
        }
    }

    /// <summary> Evaluate mean. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <returns> A Double. </returns>
    public double EvaluateMean()
    {
        if ( this.CastToArrayRequired )
        {
            this._valuesArray = [.. this.ValuesList];
            this.CastToArrayRequired = false;
        }

        if ( this.MeanRequired )
        {
            this._sum = 0d;
            this._mean = 0d;
            if ( this.RangeRequired )
            {
                if ( this.Any )
                {
                    this._minimum = this._valuesArray[0];
                    this._maximum = this._minimum;
                    foreach ( double value in this._valuesArray )
                    {
                        if ( value > this._maximum )
                        {
                            this._maximum = value;
                        }
                        else if ( value < this._minimum )
                        {
                            this._minimum = value;
                        }

                        this._sum += value;
                    }
                }
                else
                {
                    this._maximum = double.MinValue;
                    this._minimum = double.MaxValue;
                }

                this.RangeRequired = false;
            }
            else if ( this.Any )
            {
                foreach ( double value in this._valuesArray )
                {
                    this._sum += value;
                }
            }

            this._mean = this._sum / this.Count;
            this.MeanRequired = false;
        }

        return this._mean;
    }

    /// <summary> Evaluate sigma. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="average"> The average. </param>
    /// <returns> A Double. </returns>
    public double EvaluateSigma( double average )
    {
        if ( this.CastToArrayRequired )
        {
            this._valuesArray = [.. this.ValuesList];
            this.CastToArrayRequired = false;
        }

        if ( this.SigmaRequired )
        {
            this._sigma = 0d;
            this._sumSquareDeviations = 0d;
            if ( this.Any && this.Count > 1 )
            {
                foreach ( double v in this._valuesArray )
                {
                    this._sumSquareDeviations += (v - average) * (v - average);
                }

                this._sigma = Math.Sqrt( this._sumSquareDeviations / (this.Count - 1) );
            }

            this.SigmaRequired = false;
        }

        return this._sigma;
    }

    /// <summary> Evaluates correlation coefficient. </summary>
    /// <remarks> Assumes that the function values already exist. </remarks>
    /// <param name="values"> The values. </param>
    /// <returns> The correlation coefficient or coefficient of multiple determination. </returns>
    public double EvaluateCorrelationCoefficient( double[] values )
    {
        SampleStatistics valuesSample = new();
        valuesSample.AddValues( values );
        double valuesSampleMean = valuesSample.Mean;
        double valuesSampleSSQ = valuesSample.SumSquareDeviations;
        double overallMean = this.Mean;
        double overallSSQ = this.SumSquareDeviations;
        double runningSSQ = 0d;
        for ( int i = 0, loopTo = this.Count - 1; i <= loopTo; i++ )
        {
            runningSSQ += (this._valuesArray[i] - overallMean) * (valuesSample._valuesArray[i] - valuesSampleMean);
        }

        return runningSSQ / (Math.Sqrt( valuesSampleSSQ ) * Math.Sqrt( overallSSQ ));
    }

    /// <summary> Evaluates this object. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public void Evaluate()
    {
        _ = this.EvaluateSigma( this.Mean );
        this.EvaluateRange();
    }

    /// <summary> Evaluate range. </summary>
    /// <remarks> David, 2020-09-07. </remarks>
    public void EvaluateRange()
    {
        if ( this.CastToArrayRequired )
        {
            this._valuesArray = [.. this.ValuesList];
            this.CastToArrayRequired = false;
        }

        if ( this.RangeRequired )
        {
            if ( this.Any )
            {
                this._minimum = this._valuesArray[0];
                this._maximum = this._minimum;
                foreach ( double value in this._valuesArray )
                {
                    if ( value > this._maximum )
                    {
                        this._maximum = value;
                    }
                    else if ( value < this._minimum )
                    {
                        this._minimum = value;
                    }
                }
            }
            else
            {
                this._maximum = double.MinValue;
                this._minimum = double.MaxValue;
            }

            this.RangeRequired = false;
        }
    }

    #endregion
}
