using System.Collections.Specialized;

namespace cc.isr.Std.Dictionaries.Tests;

/// <summary>   (Unit Test Class) a name value collection tests. </summary>
/// <remarks>   David, 2022-02-01. </remarks>
[TestClass]
public class NameValueCollectionTests
{

    #region " construction and cleanup "

    /// <summary>   Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test
    /// in the class.
    /// </remarks>
    /// <param name="testContext">  Gets or sets the test context which provides information about
    ///                             and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            _ = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}";
            // moved to test initialize
            // Console.WriteLine( methodFullName );
        }
        catch ( Exception ex )
        {
            Console.WriteLine( $"Failed initializing fixture: {ex}" );
        }
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>											   
    [TestInitialize()]
    public virtual void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
    }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    /// <summary>   (Unit Test Method) collection should add duplicate keys. </summary>
    /// <remarks>   David, 2022-02-01. </remarks>
    [TestMethod()]
    public void CollectionShouldAddDuplicateKeys()
    {
        string aKey = "a";
        int aKeyValue1 = 123;
        int aKeyIndex;
        int aKeyValue1Index;

        int aKeyValue2 = 456;
        int aKeyValue2Index;
        int aKeyCount = 2;

        string bKey = "b";
        int bKeyValue = 789;
        int bKeyCount = 1;
        int bKeyValueIndex;
        int bKeyIndex;


        NameValueCollection<int> collection = [];
        int keysCount = collection.Keys.Length;
        aKeyValue1Index = collection.Values.Length;
        aKeyIndex = collection.Count;
        collection.Add( aKey, aKeyValue1 );

        aKeyValue2Index = collection.Values.Length;
        collection.Add( aKey, aKeyValue2 ); // 123 and 456 will be inserted into the same key.

        bKeyValueIndex = collection.Values.Length;
        bKeyIndex = collection.Count;
        collection.Add( bKey, bKeyValue );  // 789 will be inserted into another key.

        int[] aKeyValues = collection.Get( aKey );   // contains 123 and 456.
        int[] bKeyValues = collection.Get( bKey );   // contains 789.

        // testing fetched size
        Assert.AreEqual( aKeyCount, aKeyValues.Length, $"number of {nameof( aKey )} values should be as expected" );
        Assert.AreEqual( bKeyCount, bKeyValues.Length, $"number of {nameof( bKey )} values should be as expected" );

        // testing fetched values
        Assert.IsTrue( Enumerable.SequenceEqual( aKeyValues, [aKeyValue1, aKeyValue2] ), $"Extracted data for the {nameof( aKey )} should match" );
        Assert.IsTrue( Enumerable.SequenceEqual( bKeyValues, [bKeyValue] ), $"Extracted data for the {nameof( bKey )} should match" );

        // getting values index
        Assert.AreEqual( aKeyValue1, collection.Values[aKeyValue1Index], $"value at {aKeyValue1Index} should be as expected" );
        Assert.AreEqual( aKeyValue2, collection.Values[aKeyValue2Index], $"value at {aKeyValue1Index} should be as expected" );
        Assert.AreEqual( bKeyValue, collection.Values[bKeyValueIndex], $"value at {bKeyValueIndex} should be as expected" );

        // getting keys index
        Assert.AreEqual( aKey, collection.Keys[aKeyIndex], $"Key at {aKeyIndex} should be as expected" );
        Assert.AreEqual( bKey, collection.Keys[bKeyIndex], $"Key at {bKeyIndex} should be as expected" );

        // Key index values
        Assert.AreEqual( 2, collection[aKeyIndex].Length, $"number of {nameof( NameValueCollection )} elements at collection key index {aKeyIndex} should be as expected" );
        Assert.AreEqual( 1, collection[bKeyIndex].Length, $"number of {nameof( NameValueCollection )} elements at collection key index {bKeyIndex} should be as expected" );

    }

}
