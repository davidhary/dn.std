namespace cc.isr.Std.Dictionaries.Tests;

/// <summary> tests of collections. </summary>
/// <remarks>
/// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2020-05-22 </para>
/// </remarks>
[TestClass]
public class CollectionTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " list dictionary  "

    /// <summary>   Assert contains. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    /// <param name="dix">          The dictionary. </param>
    /// <param name="keyValuePair"> The key value pair. </param>
    private static void AssertContains( ListDictionary<string, int> dix, KeyValuePair<string, int> keyValuePair )
    {
        Assert.AreEqual( keyValuePair.Value, dix[keyValuePair.Key] );
        Assert.IsTrue( dix.ContainsKey( keyValuePair.Key ) );
        Assert.IsTrue( dix.Keys.Contains( keyValuePair.Key ) );
        Assert.IsTrue( dix.Values.Contains( keyValuePair.Value ) );
    }

    /// <summary>   Assert first. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    /// <param name="dix">          The dictionary. </param>
    /// <param name="keyValuePair"> The key value pair. </param>
    private static void AssertFirst( ListDictionary<string, int> dix, KeyValuePair<string, int> keyValuePair )
    {
        Assert.AreEqual( keyValuePair.Key, dix.FirstKey );
        Assert.AreEqual( keyValuePair.Key, dix.Entries.First().Key );
        Assert.AreEqual( keyValuePair.Value, dix.Entries.First().Value );
    }

    /// <summary>   Assert last. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    /// <param name="dix">          The dictionary. </param>
    /// <param name="keyValuePair"> The key value pair. </param>
    private static void AssertLast( ListDictionary<string, int> dix, KeyValuePair<string, int> keyValuePair )
    {
        Assert.AreEqual( keyValuePair.Key, dix.LastKey );
        Assert.AreEqual( keyValuePair.Key, dix.Entries.Last().Key );
        Assert.AreEqual( keyValuePair.Value, dix.Entries.Last().Value );
    }

    /// <summary> (Unit Test Method) tests list dictionary. </summary>
    /// <remarks> David, 2020-05-22. </remarks>
    [TestMethod()]
    public void ListDictionaryTest()
    {
        ListDictionary<string, int> dix = new();
        int count = 0;
        KeyValuePair<string, int> firstKeyValuePair = new( "foo", 33 );
        dix[firstKeyValuePair.Key] = firstKeyValuePair.Value;
        count += 1;
        KeyValuePair<string, int> middleKeyValuePair = new( "art", 23 );
        dix[middleKeyValuePair.Key] = middleKeyValuePair.Value;
        count += 1;
        Assert.AreEqual( count, dix.Count );
        KeyValuePair<string, int> lastKeyValuePair = new( "bar", 13 );
        dix[lastKeyValuePair.Key] = lastKeyValuePair.Value;
        count += 1;
        Assert.AreEqual( count, dix.Count );
        AssertContains( dix, firstKeyValuePair );
        AssertFirst( dix, firstKeyValuePair );
        AssertContains( dix, middleKeyValuePair );
        AssertContains( dix, lastKeyValuePair );
        AssertLast( dix, lastKeyValuePair );
        Assert.IsFalse( dix.ContainsKey( "bar1" ) );

        // move first to last
        // firstKeyValuePair = New KeyValuePair(Of String, Integer)(middleKeyValuePair.Key, middleKeyValuePair.Value)
        // middleKeyValuePair = New KeyValuePair(Of String, Integer)(lastKeyValuePair.Key, lastKeyValuePair.Value)
        // lastKeyValuePair = New KeyValuePair(Of String, Integer)("foo", 42)

        firstKeyValuePair = new KeyValuePair<string, int>( "foo", 42 );
        dix[firstKeyValuePair.Key] = firstKeyValuePair.Value;
        AssertContains( dix, firstKeyValuePair );
        AssertFirst( dix, firstKeyValuePair );
        AssertContains( dix, middleKeyValuePair );
        AssertContains( dix, lastKeyValuePair );
        AssertLast( dix, lastKeyValuePair );
    }

    #endregion

    #region " sorted bucket collection test "

    public record FinanceTransaction( int No, DateTime Date, string Description, decimal Amount );

    /// <summary>   (Unit Test Method) tests sorted bucket collection. </summary>
    /// <remarks>   David, 2021-11-12. </remarks>
    [TestMethod()]
    public void SortedBucketCollectionTest()
    {
        //Constructing a SortedBucketCollection
        SortedBucketCollection<DateTime, int, FinanceTransaction> transactions =
          new( ft => ft.Date, ft => ft.No );
        DateTime date1 = DateTime.Now.Date;

        //Adding an item to SortedBucketCollection
        transactions.Add( new FinanceTransaction( 3, date1, "1.1", 1m ) );
        transactions.Add( new FinanceTransaction( 1, date1, "1.2", 2m ) );
        transactions.Add( new FinanceTransaction( 0, date1, "1.3", 3m ) );
        DateTime date2 = date1.AddDays( -1 );
        transactions.Add( new FinanceTransaction( 1, date2, "2.1", 4m ) );
        transactions.Add( new FinanceTransaction( 2, date2, "2.2", 5m ) );

        //Looping over all items in a SortedBucketCollection
        Console.WriteLine( "foreach over all transactions" );
        foreach ( FinanceTransaction transaction in transactions )
        {
            Console.Out.WriteLine( transaction.ToString() );
        }

        //Accessing one particular transaction
        DateTime selectedDate = date1;
        int transactionNumber = 1;

        FinanceTransaction? transaction12 = transactions[selectedDate, transactionNumber];
        Assert.IsNotNull( transaction12 );

        Assert.AreEqual( date1, transaction12.Date, $"Date of transaction selected by [{selectedDate},{transactionNumber}] should match the selected date" );

        //Removing  a transaction
        int initialCount = transactions.Count;
        _ = transactions.Remove( transaction12 );
        Assert.AreEqual( initialCount - 1, transactions.Count, "Number of transaction must be lower by 1 after removing the transaction" );

        bool exists = transactions.Contains( selectedDate, transactionNumber );
        Assert.IsFalse( exists, $"Transaction should not contain the removed [{selectedDate},{transactionNumber}] transaction" );

        //Accessing all items of one day
        Console.Out.WriteLine( $"foreach over transactions of one day @ {date1}" );
        foreach ( FinanceTransaction transaction in transactions[date1] )
        {
            Console.Out.WriteLine( transaction.ToString() );
        }

        #endregion

    }

}

#if OldCode
ListDictionary<string, int> dict = new ListDictionary<string, int>();

dict["foo"] = 1;
dict["bar"] = 2;

Assert.AreEqual( 1, dict["foo"] );
Assert.AreEqual( 2, dict["bar"] );

Assert.AreEqual( 2, dict.Count );

Assert.AreEqual( "foo", dict.FirstKey );

Assert.AreEqual( "foo", dict.Entries.First().Key );
Assert.AreEqual( 1, dict.Entries.First().Value );

Assert.AreEqual( "bar", dict.Entries.Last().Key );
Assert.AreEqual( 2, dict.Entries.Last().Value );

Assert.IsTrue( dict.Keys.Contains( "foo" ) );
Assert.IsTrue( dict.Values.Contains( 1 ) );

Assert.IsTrue( dict.Keys.Contains( "bar" ) );
Assert.IsTrue( dict.Values.Contains( 2 ) );

Assert.IsTrue( dict.ContainsKey( "foo" ) );
Assert.IsTrue( dict.ContainsKey( "bar" ) );
Assert.IsTrue( !dict.ContainsKey( "let" ) );

dict["foo"] = 42;
Assert.AreEqual( 42, dict["foo"] );

#endif
