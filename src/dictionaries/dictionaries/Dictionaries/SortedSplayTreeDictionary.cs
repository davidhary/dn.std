using System.Collections;

namespace cc.isr.Std.Dictionaries;
/// <summary>
/// An splay tree is a self-balancing binary search tree with the additional property that
/// recently accessed elements are quick to access again. It performs basic operations such as
/// insertion, look-up and removal in O(log n) amortized time.
/// </summary>
/// <remarks> David, 2019-09-16. </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 202-09-12. </remarks>
/// <param name="comparer"> The comparer. </param>
public class SortedSplayTreeDictionary<TKey, TValue>( IComparer<TKey> comparer ) : IDictionary<TKey, TValue>
{
    /// <summary> The root. </summary>
    private NodeThis? _root;

    /// <summary> The comparer. </summary>
    private readonly IComparer<TKey> _comparer = comparer ?? Comparer<TKey>.Default;

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public SortedSplayTreeDictionary() : this( default! )
    { }

    /// <summary> Gets the number of. </summary>
    /// <value> The count. </value>
    public int Count => this._root is null ? 0 : this.GetCountThis( this._root );

    /// <summary> Gets or sets the element with the specified key. </summary>
    /// <exception cref="KeyNotFoundException"> Thrown when a Key Not Found error condition occurs. </exception>
    /// <value> The element with the specified key. </value>
    public TValue this[TKey key]
    {
        get => this.TryGetValue( key, out TValue value ) ? value : throw new KeyNotFoundException();
        set
        {
            NodeThis? n = this.SplayThis( this._root, key );
            if ( n is not null )
            {
                n.Value = value;
            }
            else
            {
                this.Add( key, value );
            }
        }
    }

    /// <summary> Gets the is read only. </summary>
    /// <value> The is read only. </value>
    public bool IsReadOnly => false;

    /// <summary> Copies to. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="array">      The array. </param>
    /// <param name="arrayIndex"> Zero-based index of the array. </param>
    public void CopyTo( KeyValuePair<TKey, TValue>[] array, int arrayIndex )
    {
        CollectionExtensions.CollectionExtensionMethods.CopyTo( this, array, arrayIndex );// DictionaryUtility.CopyTo(Me, array, index)
    }

    /// <summary>
    /// Determines whether the <see cref="IDictionary{TKet,TValue}" /> contains an
    /// element with the specified key.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="key"> The key to locate in the
    /// <see cref="IDictionary{TKet,TValue}" />. </param>
    /// <returns>
    /// <see langword="true" /> if the <see cref="IDictionary{TKet,TValue}" />
    /// contains an element with the key; otherwise, <see langword="false" />.
    /// </returns>
    public bool ContainsKey( TKey key )
    {
        this._root = this.SplayThis( this._root, key );
        return this._root is not null && this._root.Key is not null && key is not null && 0 == this._comparer.Compare( this._root.Key, key );
    }

    /// <summary> Query if this object contains the given item. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="item"> The item to remove. </param>
    /// <returns> <c>true</c> if the object is in this collection; otherwise <c>false</c> </returns>
    public bool Contains( KeyValuePair<TKey, TValue> item )
    {
        return this.TryGetValue( item.Key, out TValue value ) && Equals( item.Value, value );
    }

    /// <summary> Gets the value associated with the specified key. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="key">   The key whose value to get. </param>
    /// <param name="value"> When this method returns, the value associated with the
    /// specified key, if the key is found; otherwise, the default value for
    /// the type of the <paramref name="value" /> parameter. This parameter
    /// is passed uninitialized.
    /// </param>
    /// <returns>
    /// <see langword="true" /> if the object that implements
    /// <see cref="IDictionary{TKet,TValue}" /> contains an element with the
    /// specified key; otherwise, <see langword="false" />.
    /// </returns>
    public bool TryGetValue( TKey key, out TValue value )
    {
        if ( this._root is not null && this._root.Key is not null && key is not null && 0 == this._comparer.Compare( this._root.Key, key ) )
        {
            value = this._root.Value;
            return true;
        }

        this._root = this.SplayThis( this._root, key );
        if ( this._root is not null && this._root.Key is not null && key is not null && 0 == this._comparer.Compare( this._root.Key, key ) )
        {
            value = this._root.Value;
            return true;
        }

        value = default!;
        return false;
    }

    /// <summary>
    /// Adds an element with the provided key and value to the
    /// <see cref="IDictionary{TKet,TValue}" />.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="key">   The object to use as the key of the element to add. </param>
    /// <param name="value"> The object to use as the value of the element to add. </param>
    public void Add( TKey key, TValue value )
    {
        this._root = this.AddThis( this._root, key, value );
    }

    /// <summary> Adds item. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="item"> The item to remove. </param>
    public void Add( KeyValuePair<TKey, TValue> item )
    {
        this.Add( item.Key, item.Value );
    }

    /// <summary>
    /// Removes the element with the specified key from the
    /// <see cref="IDictionary{TKet,TValue}" />.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="key"> The key of the element to remove. </param>
    /// <returns>
    /// <see langword="true" /> if the element is successfully removed; otherwise,
    /// <see langword="false" />.  This method also returns <see langword="false" /> if
    /// <paramref name="key" /> was not found in the original
    /// <see cref="IDictionary{TKet,TValue}" />.
    /// </returns>
    public bool Remove( TKey key )
    {
        if ( this.TryRemoveThis( this._root, key, out NodeThis? temp ) )
        {
            this._root = temp;
            return true;
        }

        return false;
    }

    /// <summary> Removes the given item. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="item"> The item to remove. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    public bool Remove( KeyValuePair<TKey, TValue> item )
    {
        return this.TryGetValue( item.Key, out TValue value ) && Equals( item.Value, value ) && this.Remove( item.Key );
    }

    /// <summary> Clears this object to its blank/initial state. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public void Clear()
    {
        this._root = null;
    }

    /// <summary> Creates a node. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="key">   The key. </param>
    /// <param name="value"> The value. </param>
    /// <returns> The new node. </returns>
    private static NodeThis CreateNodeThis( TKey key, TValue value )
    {
        NodeThis result = new()
        {
            Key = key,
            Value = value,
            Right = null,
            Left = null
        };
        return result;
    }

    /// <summary> Rotate right the given x coordinate. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="x"> A _node to process. </param>
    /// <returns> A _node. </returns>
    private static NodeThis RorThis( NodeThis x )
    {
        NodeThis? y = x.Left;
        x.Left = y!.Right;
        y.Right = x;
        return y;
    }

    /// <summary> Rotate left the given node. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="x"> A _node to process. </param>
    /// <returns> A _node. </returns>
    private static NodeThis RolThis( NodeThis x )
    {
        NodeThis? y = x.Right;
        x.Right = y!.Left;
        y.Left = x;
        return y;
    }

    /// <summary> Splays. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="root"> The root. </param>
    /// <param name="key">  The key. </param>
    /// <returns> A _node. </returns>
    private NodeThis? SplayThis( NodeThis? root, TKey key )
    {
        if ( root is null ) return root;

        int c = root is null ? 0 : this._comparer.Compare( root.Key, key );
        if ( c == 0 )
        {
            return root;
        }
        else if ( 0 < c )
        {
            if ( root?.Left is null )
            {
                return root;
            }

            c = this._comparer.Compare( root.Left.Key, key );
            if ( 0 < c )
            {
                root.Left.Left = this.SplayThis( root.Left.Left, key );
                root = RorThis( root );
            }
            else if ( 0 > c )
            {
                root.Left.Right = this.SplayThis( root.Left.Right, key );
                if ( root.Left.Right is not null )
                {
                    root.Left = RolThis( root.Left );
                }
            }

            return root.Left is null ? root : RorThis( root );
        }
        else if ( 0 > c )
        {
            if ( root!.Right is null )
            {
                return root;
            }

            c = this._comparer.Compare( root.Right.Key, key );
            if ( 0 < c )
            {
                root.Right.Left = this.SplayThis( root.Right.Left, key );
                if ( root.Right.Left is not null )
                {
                    root.Right = RorThis( root.Right );
                }
            }
            else if ( 0 > c )
            {
                root.Right.Right = this.SplayThis( root.Right.Right, key );
                root = RolThis( root );
            }

            return root.Right is null ? root : RolThis( root );
        }
        else
        {
            return root;
        }
    }

    /// <summary> Adds root. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    /// illegal values. </exception>
    /// <param name="root">  The root. </param>
    /// <param name="key">   The key. </param>
    /// <param name="value"> The value. </param>
    /// <returns> A _node. </returns>
    private NodeThis AddThis( NodeThis? root, TKey key, TValue value )
    {
        if ( root is null )
        {
            return CreateNodeThis( key, value );
        }

        root = this.SplayThis( root, key );
        int c = this._comparer.Compare( root!.Key, key );
        if ( 0 == c )
        {
            throw new ArgumentException( "An item with the specified key is already present in the dictionary.", nameof( key ) );
        }

        NodeThis newNode = CreateNodeThis( key, value );
        if ( 0 < c )
        {
            newNode.Right = root;
            newNode.Left = root.Left;
            root.Left = null;
        }
        else
        {
            newNode.Left = root;
            newNode.Right = root.Right;
            root.Right = null;
        }

        return newNode;
    }

    /// <summary> Try remove. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="root">   The root. </param>
    /// <param name="key">    The key. </param>
    /// <param name="result"> The out by reference result. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    private bool TryRemoveThis( NodeThis? root, TKey key, out NodeThis? result )
    {
        result = null;
        NodeThis temp;
        if ( root is null )
        {
            return false;
        }

        root = this.SplayThis( root, key )!;
        if ( 0 != this._comparer.Compare( key, root.Key ) )
        {
            result = root;
            return false;
        }

        if ( root.Left is null )
        {
            root = root.Right;
        }
        else
        {
            temp = root;
            root = this.SplayThis( root.Left, key )!;
            root.Right = temp.Right;
        }

        result = root;
        return true;
    }

    /// <summary>
    /// Gets an <see cref="ICollection{T}" /> containing the keys of the
    /// <see cref="IDictionary{TKet,TValue}" />.
    /// </summary>
    /// <value>
    /// An <see cref="ICollection{T}" /> containing the keys of the
    /// object that implements <see cref="IDictionary{TKet,TValue}" />.
    /// </value>
    public ICollection<TKey> Keys => CollectionExtensions.CollectionExtensionMethods.CreateKeys( this );// Return DictionaryUtility.CreateKeys(Me)

    /// <summary>
    /// Gets an <see cref="ICollection{T}" /> containing the values in
    /// the <see cref="IDictionary{TKet,TValue}" />.
    /// </summary>
    /// <value>
    /// An <see cref="ICollection{T}" /> containing the values in the
    /// object that implements <see cref="IDictionary{TKet,TValue}" />.
    /// </value>
    public ICollection<TValue> Values => CollectionExtensions.CollectionExtensionMethods.CreateValues( this );// Return DictionaryUtility.CreateValues(Me)

    /// <summary> Gets the enumerator. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <returns> The enumerator. </returns>
    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
    {
        foreach ( KeyValuePair<TKey, TValue> item in this.EnumNodesThis( this._root ) )
        {
            yield return item;
        }
    }

    /// <summary> Gets the enumerator. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <returns> The enumerator. </returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
        return this.GetEnumerator();
    }

    /// <summary> Enumerates enum nodes in this collection. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="root"> The root. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process enum nodes in this collection.
    /// </returns>
    private IEnumerable<KeyValuePair<TKey, TValue>> EnumNodesThis( NodeThis? root )
    {
        if ( root is not null )
        {
            foreach ( KeyValuePair<TKey, TValue> item in this.EnumNodesThis( root.Left ) )
            {
                yield return item;
            }

            foreach ( KeyValuePair<TKey, TValue> item in this.EnumNodesThis( root.Right ) )
            {
                yield return item;
            }

            yield return new KeyValuePair<TKey, TValue>( root.Key, root.Value );
        }
    }

    /// <summary> Gets a count. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="root"> The root. </param>
    /// <returns> The count. </returns>
    private int GetCountThis( NodeThis? root )
    {
        if ( root is null )
        {
            return 0;
        }

        int result = 1;
        result += this.GetCountThis( root.Left );
        result += this.GetCountThis( root.Right );
        return result;
    }

    /// <summary> A node. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    private sealed class NodeThis
    {
        /// <summary> Gets or sets the key. </summary>
        /// <value> The key. </value>
        public TKey Key { get; set; } = default!;

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public TValue Value { get; set; } = default!;

        /// <summary> Gets or sets the left. </summary>
        /// <value> The left. </value>
        public NodeThis? Left { get; set; }

        /// <summary> Gets or sets the right. </summary>
        /// <value> The right. </value>
        public NodeThis? Right { get; set; }
    }
}
