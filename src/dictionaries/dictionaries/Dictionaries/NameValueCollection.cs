using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Runtime.Serialization;
using static cc.isr.Std.Dictionaries.Extensions;
using System.Collections.Specialized;

namespace cc.isr.Std.Dictionaries;
/// <summary>
/// Represents a generic collection of associated <see cref="string" /> keys and
/// <typeparamref name="T"/> values that can be accessed either with the key or with the index.
/// </summary>
/// <remarks>
/// By Pavel BASHKARDIN, 2022-01-29 
/// https://www.CodeProject.com/Articles/5323395/A-Generic-Form-of-the-NameValueCollection.
/// </remarks>
/// <typeparam name="T">    Value type. </typeparam>
[Serializable]
[DebuggerDisplay( "Count = {Count.ToString()}" )]
[DebuggerTypeProxy( typeof( NameValueCollection<>.Enumerator ) )]
public class NameValueCollection<T> : NameObjectCollectionBase, IDictionary, IEnumerable<KeyValuePair<string, T>>
{
    private T[] _values = [];
    private string[] _keys = [];

    #region " constructor "

    /// <summary>
    /// Initializes a new instance of the <see cref="NameValueCollection{T}" /> class that is empty,
    /// has the default initial capacity and uses the default case-insensitive hash code provider
    /// and the default case-insensitive comparer.
    /// </summary>
    public NameValueCollection()
    { }

    /// <summary>
    /// Initializes a new instance of the <see cref="NameValueCollection{T}" /> class that is empty,
    /// has the default initial capacity and uses the specified hash code provider and the specified comparer.
    /// </summary>
    /// <param name="hashProvider">
    /// The <see cref="IHashCodeProvider" />
    /// that will supply the hash codes for all keys in the <see cref="NameValueCollection{T}" />.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="IComparer" /> to use to determine whether two keys are equal.
    /// </param>
    [Obsolete( "Please use NameValueCollection<T>(IEqualityComparer) instead." )]
    public NameValueCollection( IHashCodeProvider hashProvider, IComparer comparer )
        : base( hashProvider, comparer )
    { }

    /// <summary>
    /// Initializes a new instance of the <see cref="NameValueCollection{T}" /> class that is empty,
    /// has the specified initial capacity and uses the default case-insensitive hash code provider
    /// and the default case-insensitive comparer.
    /// </summary>
    /// <param name="capacity">
    /// The initial number of entries that the <see cref="NameValueCollection{T}" /> can contain.
    /// </param>
    /// <exception cref="ArgumentOutOfRangeException">
    /// <paramref name="capacity" /> is less than zero.
    /// </exception>
    public NameValueCollection( int capacity )
        : base( capacity )
    { }

    /// <summary>
    /// Initializes a new instance of the <see cref="NameValueCollection{T}" /> class that is empty,
    /// has the default initial capacity, and uses the specified <see cref="IEqualityComparer" /> object.
    /// </summary>
    /// <param name="equalityComparer">The <see cref="IEqualityComparer" /> object to use
    /// to determine whether two keys are equal and to generate hash codes for the keys in the collection.
    /// </param>
    public NameValueCollection( IEqualityComparer equalityComparer )
        : base( equalityComparer )
    { }

    /// <summary>
    /// Initializes a new instance of the <see cref="NameValueCollection{T}" /> class that is empty,
    /// has the specified initial capacity, and uses the specified <see cref="IEqualityComparer" /> object.
    /// </summary>
    /// <param name="capacity">
    /// The initial number of entries that the <see cref="NameValueCollection{T}" /> object can contain.
    /// </param>
    /// <param name="equalityComparer">
    /// The <see cref="IEqualityComparer" /> object to use to determine whether two keys are equal
    /// and to generate hash codes for the keys in the collection.
    /// </param>
    /// <exception cref="ArgumentOutOfRangeException">
    /// <paramref name="capacity" /> is less than zero.
    /// </exception>
    public NameValueCollection( int capacity, IEqualityComparer equalityComparer )
        : base( capacity, equalityComparer )
    { }

    /// <summary>
    /// Copies the entries from the specified <see cref="NameValueCollection{T}" /> to a new <see cref="NameValueCollection{T}" />
    /// with the specified initial capacity or the same initial capacity as the number of entries copied, whichever is greater,
    /// and using the default case-insensitive hash code provider and the default case-insensitive comparer.
    /// </summary>
    /// <param name="capacity">
    /// The initial number of entries that the <see cref="NameValueCollection{T}" /> can contain.
    /// </param>
    /// <param name="col">
    /// The <see cref="NameValueCollection{T}" /> to copy to the new <see cref="NameValueCollection{T}"/>.
    /// </param>
    /// <exception cref="ArgumentOutOfRangeException">
    /// <paramref name="capacity" /> is less than zero.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="col" /> is <see langword="null" />.
    /// </exception>
    public NameValueCollection( int capacity, NameValueCollection<T> col )
        : base( capacity )
    {
        if ( col == null ) throw new ArgumentNullException( nameof( col ) );
        this.Add( col );
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="NameValueCollection{T}" /> class that is empty,
    /// has the specified initial capacity and uses the specified hash code provider and the specified comparer.
    /// </summary>
    /// <param name="capacity">
    /// The initial number of entries that the <see cref="NameValueCollection{T}" /> can contain.
    /// </param>
    /// <param name="hashProvider">
    /// The <see cref="IHashCodeProvider" /> that will supply the hash codes
    /// for all keys in the <see cref="NameValueCollection{T}" />.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="IComparer" /> to use to determine whether two keys are equal.
    /// </param>
    /// <exception cref="ArgumentOutOfRangeException">
    /// <paramref name="capacity" /> is less than zero.</exception>
    [Obsolete( "Please use NameValueCollection<T>(Int32, IEqualityComparer) instead." )]
    public NameValueCollection( int capacity, IHashCodeProvider hashProvider, IComparer comparer )
        : base( capacity, hashProvider, comparer )
    { }

    /// <summary>
    /// Initializes a new instance of the <see cref="NameValueCollection{T}" /> class that is serializable
    /// and uses the specified <see cref="SerializationInfo" /> and <see cref="StreamingContext" />.
    /// </summary>
    /// <param name="info">
    /// A <see cref="SerializationInfo" /> object that contains the information
    /// required to serialize the new <see cref="NameValueCollection{T}"/>.
    /// </param>
    /// <param name="context">A <see cref="StreamingContext" /> object that contains the source
    /// and destination of the serialized stream associated with the new <see cref="NameValueCollection{T}"/>.
    /// </param>
    protected NameValueCollection( SerializationInfo info, StreamingContext context )
        : base( info, context )
    { }

    #endregion

    #region " methods "

    /// <summary>
    /// Resets the cached arrays of the collection to <see langword="null" />.
    /// </summary>
    protected void InvalidateCachedArrays()
    {
        this._values = [];
        this._keys = [];
    }

    /// <summary>
    /// Returns an array that contains all the values in the <see cref="NameValueCollection{T}"/>.
    /// </summary>
    /// <returns>
    /// An array that contains all the values in the <see cref="NameValueCollection{T}"/>.
    /// </returns>
    protected ArrayList GetAllValues()
    {
        int count = this.Count;
        ArrayList arrayList = new( count );
        for ( int i = 0; i < count; ++i )
        {
            arrayList.AddRange( this.Get( i ) );
        }
        return arrayList;
    }

    /// <summary>   Gets all values internal. </summary>
    /// <remarks>   David, 2022-02-01. </remarks>
    /// <returns>   An array of t. </returns>
    private T[] GetAllValuesInternal()
    {
        ArrayList arrayList = this.GetAllValues();
        return arrayList.Count == 0 ? [] : arrayList.AsArray<T>();
    }

    /// <summary>
    /// Gets collection of <see cref="DictionaryEntry"/> entries that contains
    /// all keys and values pairs in the <see cref="NameValueCollection{T}" />.
    /// </summary>
    /// <returns>
    /// A collection of <see cref="DictionaryEntry"/> that contains
    /// all the entries in the <see cref="NameValueCollection{T}"/>.
    /// </returns>
    protected IEnumerable<KeyValuePair<string, T>> GetAllEntries()
    {
        foreach ( string key in this.Keys )
        {
            foreach ( T value in this.Get( key ) )
            {
                yield return new KeyValuePair<string, T>( key, value );
            }
        }
    }

    /// <summary>Adds an entry with the specified name and value to the <see cref="NameValueCollection{T}" />.</summary>
    /// <param name="name">The <see cref="string" /> key of the entry to add. The key can be <see langword="null" />.</param>
    /// <param name="value">The value of the entry to add. The value can be <see langword="null" />.</param>
    /// <exception cref="NotSupportedException">The collection is read-only. </exception>
    public virtual void Add( string name, T value )
    {
        if ( this.IsReadOnly )
            throw new NotSupportedException( GetResourceString( "CollectionReadOnly" ) );
        this.InvalidateCachedArrays();
        ArrayList arrayList = ( ArrayList ) this.BaseGet( name );
        if ( arrayList == null )
        {
            arrayList = new ArrayList( 1 );
            if ( value is not null )
                _ = arrayList.Add( value );
            this.BaseAdd( name, arrayList );
        }
        else
        {
            if ( value == null )
                return;
            _ = arrayList.Add( value );
        }
    }

    /// <summary>
    /// Copies the entries in the specified <see cref="NameValueCollection{T}" />
    /// to the current <see cref="NameValueCollection{T}" />.
    /// </summary>
    /// <remarks>   2024-06-22. </remarks>
    /// <exception cref="ArgumentNullException">    <paramref name="collection" /> is <see langword="null" />.</exception>
    /// <param name="collection">   The <see cref="NameValueCollection{T}" /> to copy to the current <see cref="NameValueCollection{T}" />.</param>
    ///
    /// ### <exception cref="NotSupportedException">    The collection is read-only. </exception>
    public virtual void Add( NameValueCollection<T> collection )
    {
        if ( collection == null ) throw new ArgumentNullException( nameof( collection ) );
        this.InvalidateCachedArrays();
        int count = collection.Count;
        for ( int i = 0; i < count; i++ )
        {
            string key = collection.GetKey( i );
            T[] values = collection.Get( i );
            if ( values is not null )
            {
                foreach ( T? value in values )
                    this.Add( key, value );
            }
            else
                this.Add( key, default! );
        }
    }

    /// <summary>
    /// Gets the values associated with the specified key
    /// from the <see cref="NameValueCollection{T}" />.
    /// </summary>
    /// <param name="name">
    /// The <see cref="string"/> key of the entry that contains the values to get.
    /// The key can be <see langword="null" />.
    /// </param>
    /// <returns>
    /// An array that contains the values associated
    /// with the specified key from the <see cref="NameValueCollection{T}" />,
    /// if found; otherwise, <see langword="null" />.
    /// </returns>
    public virtual T[] Get( string name )
    {
        return (( ArrayList ) this.BaseGet( name )).AsArray<T>();
    }

    /// <summary>
    /// Gets the values at the specified index of the <see cref="NameValueCollection{T}" />.
    /// </summary>
    /// <param name="index">
    /// The zero-based index of the entry that contains the values to get from the collection.
    /// </param>
    /// <returns>
    /// An array that contains the values at the specified index of the <see cref="NameValueCollection{T}" />,
    /// if found; otherwise, <see langword="null" />.
    /// </returns>
    /// <exception cref="ArgumentOutOfRangeException">
    /// <paramref name="index" /> is outside the valid range of indexes for the collection.
    /// </exception>
    public virtual T[] Get( int index )
    {
        return (( ArrayList ) this.BaseGet( index )).AsArray<T>();
    }

    /// <summary>Sets the value of an entry in the <see cref="NameValueCollection{T}" />.
    /// </summary>
    /// <param name="name">
    /// The <see cref="string" /> key of the entry to add the new value to.
    /// The key can be <see langword="null" />.
    /// </param>
    /// <param name="value">
    /// The new value to add to the specified entry.
    /// The value can be <see langword="null" />.
    /// </param>
    /// <exception cref="NotSupportedException">
    /// The collection is read-only.
    /// </exception>
    public virtual void Set( string name, T value )
    {
        if ( this.IsReadOnly )
            throw new NotSupportedException( GetResourceString( "CollectionReadOnly" ) );
        this.InvalidateCachedArrays();
        this.BaseSet( name, new ArrayList( 1 ) { value } );
    }

    /// <summary>Sets the value of an entry in the <see cref="NameValueCollection{T}" />.
    /// </summary>
    /// <param name="name">
    /// The <see cref="string" /> key of the entry to add the new value to.
    /// The key can be <see langword="null" />.
    /// </param>
    /// <param name="values">
    /// New values to add to the specified entry.
    /// The value can be <see langword="null" />.
    /// </param>
    /// <exception cref="NotSupportedException">
    /// The collection is read-only.
    /// </exception>
    public virtual void Set( string name, params T[] values )
    {
        if ( this.IsReadOnly )
            throw new NotSupportedException( GetResourceString( "CollectionReadOnly" ) );
        this.InvalidateCachedArrays();
        this.BaseSet( name, new ArrayList( values ) );
    }

    /// <summary>
    /// Invalidates the cached arrays and removes all entries from the <see cref="NameValueCollection{T}" />.
    /// </summary>
    /// <exception cref="NotSupportedException">
    /// The collection is read-only.
    /// </exception>
    public virtual void Clear()
    {
        if ( this.IsReadOnly )
            throw new NotSupportedException( GetResourceString( "CollectionReadOnly" ) );
        this.InvalidateCachedArrays();
        this.BaseClear();
    }

    /// <summary>
    /// Copies the entire <see cref="NameValueCollection{T}" />
    /// to a compatible one-dimensional <see cref="Array" />,
    /// starting at the specified index of the target array.</summary>
    /// <param name="destination">The one-dimensional <see cref="Array" />
    /// that is the destination of the elements copied from <see cref="NameValueCollection{T}" />.
    /// The <see cref="Array" /> must have zero-based indexing.</param>
    /// <param name="index">
    /// The zero-based index in <paramref name="destination" /> at which copying begins.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="destination" /> is <see langword="null" />.
    /// </exception>
    /// <exception cref="ArgumentOutOfRangeException">
    /// <paramref name="index" /> is less than zero.
    /// </exception>
    /// <exception cref="ArgumentException">
    /// <paramref name="destination" /> is multidimensional.
    /// -or-
    /// The number of elements in the source <see cref="NameValueCollection{T}" />
    /// is greater than the available space from <paramref name="index" />
    /// to the end of the destination <paramref name="destination" />.
    /// </exception>
    /// <exception cref="InvalidCastException">
    /// The type of the source <see cref="NameValueCollection{T}" />
    /// cannot be cast automatically to the type of the destination <paramref name="destination" />.
    /// </exception>
    public virtual void CopyTo( T[] destination, int index )
    {
        if ( destination == null ) throw new ArgumentNullException( nameof( destination ) );
        if ( destination.Rank != 1 )
            throw new ArgumentException(
                GetResourceString( "Arg_MultiRank" ) );
        if ( index < 0 )
            throw new ArgumentOutOfRangeException( nameof( index ),
                GetResourceString( "IndexOutOfRange",
                       index.ToString( CultureInfo.CurrentCulture ) ) );
        this._values ??= this.GetAllValuesInternal(); // this.GetAllValues().AsArray<T>();
        int count = this._values.Length;
        if ( destination.Length - index < count )
            throw new ArgumentException( GetResourceString( "Arg_InsufficientSpace" ) );
        for ( int i = 0; i < count; i++ )
            destination.SetValue( this._values[i], i + index );
    }

    /// <summary>
    /// Removes the entries with the specified key from the <see cref="NameValueCollection{T}"/>.
    /// </summary>
    /// <param name="name">
    /// The <see cref="string" /> key of the entry to remove. The key can be <see langword="null" />.
    /// </param>
    /// <exception cref="NotSupportedException">
    /// The collection is read-only.
    /// </exception>
    public virtual void Remove( string name )
    {
        this.InvalidateCachedArrays();
        this.BaseRemove( name );
    }

    /// <summary>Gets a value indicating whether the <see cref="NameValueCollection{T}" />
    /// contains keys that are not <see langword="null" />.</summary>
    /// <returns>
    /// <see langword="true" /> if the <see cref="NameValueCollection{T}" />
    /// contains keys that are not <see langword="null" />; otherwise, <see langword="false" />.
    /// </returns>
    public bool HasKeys()
    {
        return this.BaseHasKeys();
    }

    /// <summary>
    /// Gets the key at the specified index of the <see cref="NameValueCollection{T}" />.
    /// </summary>
    /// <param name="index">
    /// The zero-based index of the key to get from the collection.
    /// </param>
    /// <returns>
    /// A <see cref="string" /> that contains the key at the specified index of the <see cref="NameValueCollection{T}" />,
    /// if found; otherwise, <see langword="null" />.
    /// </returns>
    /// <exception cref="ArgumentOutOfRangeException">
    /// <paramref name="index" /> is outside the valid range of indexes for the collection.
    /// </exception>
    public virtual string GetKey( int index )
    {
        return this.BaseGetKey( index );
    }

    #endregion

    #region " properties

    /// <summary>
    /// Gets all the keys in the <see cref="NameValueCollection{T}" />.
    /// </summary>
    /// <returns>
    /// A <see cref="string" /> array that contains all the keys of the <see cref="NameValueCollection{T}" />.
    /// </returns>
    public new string[] Keys
    {
        get
        {
            this._keys = this.BaseGetAllKeys();
            return this._keys;
        }
    }

    /// <summary>
    /// Gets all the values in the <see cref="NameValueCollection{T}" />.
    /// </summary>
    /// <returns>
    /// An array that contains all the values of the <see cref="NameValueCollection{T}" />.
    /// </returns>
    public virtual T[] Values
    {
        get
        {
            this._values = this.GetAllValuesInternal(); // this.GetAllValues().AsArray<T>();
            return this._values;
        }
    }

    /// <summary>
    /// Gets of sets the values at the specified index of the <see cref="NameValueCollection{T}" />.
    /// </summary>
    /// <param name="index">
    /// The zero-based index of the entry to locate in the collection.
    /// </param>
    /// <returns>
    /// An array that contains all the values at the specified index of the <see cref="NameValueCollection{T}" />.
    /// </returns>
    /// <exception cref="ArgumentOutOfRangeException">
    /// <paramref name="index" /> is outside the valid range of indexes for the collection.
    /// </exception>
    public T[] this[int index]
    {
        get => this.Get( index );
        set => this.BaseSet( index, new ArrayList( value ) );
    }

    /// <summary>
    /// Gets of sets the values at the specified name of the <see cref="NameValueCollection{T}" />.
    /// </summary>
    /// <param name="name">
    /// A <see cref="string" /> that contains the name of the entry to locate in the collection.
    /// </param>
    /// <returns>
    /// An array that contains all the values at the specified name of the <see cref="NameValueCollection{T}" />.
    /// </returns>
    /// <exception cref="ArgumentOutOfRangeException">
    /// <paramref name="name" /> is outside the valid range of names for the collection.
    /// </exception>
    public T[] this[string name]
    {
        get => this.Get( name );
        set => this.BaseSet( name, new ArrayList( value ) );
    }

    #endregion

    #region " idictionary, ienumerable "

    /// <inheritdoc cref="IDictionary.Values"/>
    ICollection IDictionary.Values => this.Values;

    /// <inheritdoc cref="IDictionary.IsReadOnly"/>
    public new bool IsReadOnly => base.IsReadOnly;

    /// <inheritdoc cref="IDictionary.IsFixedSize"/>
    public bool IsFixedSize => false;

    /// <inheritdoc cref="IDictionary.Keys"/>
    ICollection IDictionary.Keys => this.Keys;

    /// <inheritdoc cref="IDictionary.Contains"/>
    bool IDictionary.Contains( object key )
    {
        return key is string s && this.Keys.Contains( s );
    }

    /// <inheritdoc cref="IDictionary.Add"/>
    void IDictionary.Add( object key, object value )
    {
        string? k = key.CastTo<string>( new ArgumentException( GetResourceString( "Argument_InvalidKey" ), nameof( key ) ) );
        T? v = value.CastTo<T>( new ArgumentException( GetResourceString( "Argument_InvalidValue" ), nameof( key ) ) );
        if ( k is not null && v is not null )
            this.Add( k, v );
    }

    /// <inheritdoc cref="IEnumerable{T}.GetEnumerator"/>
    IEnumerator<KeyValuePair<string, T>> IEnumerable<KeyValuePair<string, T>>.GetEnumerator()
    {
        return new Enumerator( this );
    }

    /// <inheritdoc cref="IEnumerable.GetEnumerator"/>
    public override IEnumerator GetEnumerator()
    {
        return new Enumerator( this );
    }

    /// <inheritdoc cref="IDictionary.GetEnumerator"/>
    IDictionaryEnumerator IDictionary.GetEnumerator()
    {
        return new Enumerator( this );
    }

    /// <inheritdoc cref="IDictionary.Remove"/>
    void IDictionary.Remove( object key )
    {
        string? k = key.CastTo<string>( new ArgumentException( GetResourceString( "Argument_InvalidKey" ), nameof( key ) ) );
        if ( k is not null ) this.Remove( k );
    }

    /// <inheritdoc cref="IDictionary.this"/>
    object IDictionary.this[object key]
    {
        get
        {
            string? k = key.CastTo<string>( new ArgumentException( GetResourceString( "Argument_InvalidKey" ), nameof( key ) ) );
            return this.Get( k! );
        }
        set
        {
            string? k = key.CastTo<string>( new ArgumentException( GetResourceString( "Argument_InvalidKey" ), nameof( key ) ) );
            if ( value is IEnumerable<T> collection )
            {
                this.Set( k!, collection.AsArray() );
            }
            else
            {
                T? v = value.CastTo<T>( new ArgumentException( GetResourceString( "Argument_InvalidValue" ), nameof( key ) ) );
                this.Set( k!, v! );
            }
        }
    }

    #endregion

    #region " enumerator "

    private class Enumerator : IEnumerator<KeyValuePair<string, T>>, IDictionaryEnumerator
    {
        [DebuggerBrowsable( DebuggerBrowsableState.RootHidden )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private IEnumerable<KeyValuePair<string, T>> Entries { get; }

        [DebuggerBrowsable( DebuggerBrowsableState.Never )]
        private readonly IEnumerator<KeyValuePair<string, T>> _enumerator;

        public Enumerator( NameValueCollection<T> collection )
        {
            IEnumerable<KeyValuePair<string, T>> entries = collection.GetAllEntries().AsArray();
            this.Entries = entries;
            this._enumerator = entries.GetEnumerator();
        }
        [DebuggerBrowsable( DebuggerBrowsableState.Never )]
        KeyValuePair<string, T> IEnumerator<KeyValuePair<string, T>>.Current => this._enumerator.Current;

        [DebuggerBrowsable( DebuggerBrowsableState.Never )]
        object IEnumerator.Current
        {
            get
            {
                IEnumerator enumerator = this._enumerator;
                return enumerator.Current;
            }
        }

        [DebuggerBrowsable( DebuggerBrowsableState.Never )]
        object IDictionaryEnumerator.Key
        {
            get
            {
                IEnumerator<KeyValuePair<string, T>> enumerator = this;
                return enumerator.Current.Key;
            }
        }

        [DebuggerBrowsable( DebuggerBrowsableState.Never )]
        object IDictionaryEnumerator.Value
        {
            get
            {
                IEnumerator<KeyValuePair<string, T>> enumerator = this;
                return enumerator.Current.Value!;
            }
        }

        [DebuggerBrowsable( DebuggerBrowsableState.Never )]
        DictionaryEntry IDictionaryEnumerator.Entry
        {
            get
            {
                IEnumerator<KeyValuePair<string, T>> enumerator = this;
                return new DictionaryEntry( enumerator.Current.Key,
                    enumerator.Current.Value );
            }
        }

        public bool MoveNext()
        {
            return this._enumerator.MoveNext();
        }

        public void Reset()
        {
            this._enumerator.Reset();
        }

        public void Dispose()
        {
        }
    }

    #endregion
}
internal static class Extensions
{
    private static ResourceSet? _resourceSet = null;

    /// <summary>   Gets an MS Core Lib resource string. </summary>
    /// <remarks>   2023-03-31. </remarks>
    /// <param name="name"> The name. </param>
    /// <returns>   The resource string. </returns>
    internal static string GetResourceString( string name )
    {
        if ( _resourceSet == null )
        {
            Assembly a = Assembly.GetAssembly( typeof( object ) );
            string n = a.GetName().Name;
            ResourceManager m = new( n, a );
            _resourceSet = m.GetResourceSet( CultureInfo.CurrentUICulture, true, true );
        }

        return _resourceSet.GetString( name );
    }

    internal static string GetResourceString( string name, params object[] args )
    {
        return string.Format( GetResourceString( name ) ?? throw new ArgumentNullException( nameof( name ) ), args );
    }

    internal static T[] AsArray<T>( this ArrayList list )
    {
        int count;
        if ( list == null || (count = list.Count) == 0 )
            return [];
        T[] array = new T[count];
        list.CopyTo( 0, array, 0, count );
        return array;
    }

    internal static T[] AsArray<T>( this IEnumerable<T> collection )
    {
        T[] array = [];
        int length = 0;
        if ( collection is ICollection<T> elements )
        {
            length = elements.Count;
            if ( length > 0 )
            {
                array = new T[length];
                elements.CopyTo( array, 0 );
            }
        }
        else
        {
            foreach ( T element in collection )
            {
                if ( array == null || array.Length == 0 )
                    array = new T[4];
                else if ( array.Length == length )
                {
                    T[] tmpArray = new T[checked(length * 2)];
                    Array.Copy( array, 0, tmpArray, 0, length );
                    array = tmpArray;
                }
                array[length] = element;
                ++length;
            }
        }
        return array;
    }
    internal static T? CastTo<T>( this object? obj, Exception? e = null )
    {
        Type destinationType = typeof( T );
        return obj is null
            ? destinationType.IsValueType
                ? throw e ?? new InvalidOperationException( GetResourceString( "Arg_NullReferenceException" ) )
                : default( T )
            : obj is T t
                ? t
                : throw e ?? new InvalidOperationException( GetResourceString( "InvalidCast_FromTo", obj?.GetType().Name ?? "null", typeof( T ).Name ) );
    }
}

