using System.Collections;

namespace cc.isr.Std.Dictionaries;

/// <summary> A value collection. </summary>
/// <remarks>
/// David, 202-09-12.  <para>
/// (c) 2019 Honey the Code Witch, Inc. All rights reserved. </para><para>
/// https://www.CodeProject.com/Tips/5239264/Bee-A-Suite-of-Self-Balancing-Binary-Tree-Based-Di.
/// </para>
/// </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 202-09-12. </remarks>
/// <param name="parent"> The parent dictionary. </param>
public class ValuesCollection<TKey, TValue>( IDictionary<TKey, TValue> parent ) : object(), ICollection<TValue>
{
    /// <summary> The parent dictionary. </summary>
    private readonly IDictionary<TKey, TValue> _parent = parent;

    /// <summary>
    /// Determines whether the <see cref="ICollection{T}" /> contains a
    /// specific value.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="item"> The object to locate in the
    /// <see cref="ICollection{T}" />. </param>
    /// <returns>
    /// <see langword="true" /> if <paramref name="item" /> is found in the
    /// <see cref="ICollection{T}" />; otherwise,
    /// <see langword="false" />.
    /// </returns>
    public bool Contains( TValue item )
    {
        foreach ( KeyValuePair<TKey, TValue> kvp in this._parent )
        {
            if ( Equals( kvp.Value, item ) )
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Gets a value indicating whether the <see cref="ICollection{T}" />
    /// is read-only.
    /// </summary>
    /// <value>
    /// <see langword="true" /> if the <see cref="ICollection{T}" /> is
    /// read-only; otherwise, <see langword="false" />.
    /// </value>
    public bool IsReadOnly => true;

    /// <summary> Message describing the read only. </summary>
    private const string READ_ONLY_MESSAGE = "The collection is read-only.";

    /// <summary>
    /// Adds an item to the <see cref="ICollection{T}" />.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <param name="item"> The object to add to the
    /// <see cref="ICollection{T}" />. </param>
    public void Add( TValue item )
    {
        throw new InvalidOperationException( READ_ONLY_MESSAGE );
    }

    /// <summary>
    /// Removes the first occurrence of a specific object from the
    /// <see cref="ICollection{T}" />.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <param name="item"> The object to remove from the
    /// <see cref="ICollection{T}" />. </param>
    /// <returns>
    /// <see langword="true" /> if <paramref name="item" /> was successfully removed from the
    /// <see cref="ICollection{T}" />; otherwise,
    /// <see langword="false" />. This method also returns <see langword="false" /> if
    /// <paramref name="item" /> is not found in the original
    /// <see cref="ICollection{T}" />.
    /// </returns>
    public bool Remove( TValue item )
    {
        throw new InvalidOperationException( READ_ONLY_MESSAGE );
    }

    /// <summary>
    /// Removes all items from the <see cref="ICollection{T}" />.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    public void Clear()
    {
        throw new InvalidOperationException( READ_ONLY_MESSAGE );
    }

    /// <summary>
    /// Gets the number of elements contained in the
    /// <see cref="ICollection{T}" />.
    /// </summary>
    /// <value>
    /// The number of elements contained in the
    /// <see cref="ICollection{T}" />.
    /// </value>
    public int Count => this._parent.Count;

    /// <summary> Gets the enumerator. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <returns> The enumerator. </returns>
    public IEnumerator<TValue> GetEnumerator()
    {
        foreach ( KeyValuePair<TKey, TValue> item in this._parent )
        {
            yield return item.Value;
        }
    }

    /// <summary> Gets the enumerator. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <returns> The enumerator. </returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
        return this.GetEnumerator();
    }

    /// <summary>
    /// Copies the elements of the <see cref="ICollection{T}" /> to an
    /// <see cref="Array" />, starting at a particular <see cref="Array" /> index.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    /// are null. </exception>
    /// <exception cref="ArgumentException">           Thrown when one or more arguments have
    /// unsupported or illegal values. </exception>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="array">      The one-dimensional <see cref="Array" /> that is the
    /// destination of the elements copied from
    /// <see cref="ICollection{T}" />. The
    /// <see cref="Array" /> must have zero-based indexing. </param>
    /// <param name="arrayIndex"> The zero-based index in <paramref name="array" /> at which copying
    /// begins. </param>
    public void CopyTo( TValue[] array, int arrayIndex )
    {
        if ( array is null )
        {
            throw new ArgumentNullException( nameof( array ) );
        }

        int i = this._parent.Count;
        if ( i > array.Length )
        {
            throw new ArgumentException( "The array is not big enough to hold the dictionary values.", nameof( array ) );
        }

        if ( 0 > arrayIndex || i > array.Length + arrayIndex )
        {
            throw new ArgumentOutOfRangeException( nameof( arrayIndex ) );
        }

        i = 0;
        foreach ( KeyValuePair<TKey, TValue> item in this._parent )
        {
            array[i + arrayIndex] = item.Value;
            i += 1;
        }
    }
}
