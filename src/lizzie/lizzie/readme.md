# About

cc.isr.Std.Lizzie is a fork of the [lizzie] library.

# How to Use

See [lizzie]

# Feedback

cc.isr.Std.Lizzie is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Standard Framework Repository] repository.

# Open Source
Open source used by this software is described and licensed at the following sites:  
[lizzie]  
[Standard Framework Repository]  

[lizzie]: https://GitHub.com/polterguy/lizzie/
[Standard Framework Repository]: https://bitbucket.org/davidhary/dn.std
