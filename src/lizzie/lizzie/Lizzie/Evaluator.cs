namespace cc.isr.Std.Lizzie;

/// <summary> Class allowing you to evaluate a list of functions. </summary>
/// <remarks>
/// David, 2019-02-03, https://GitHub.com/polterguy/lizzie/. <para>
/// Copyright (c) 2018 Thomas Hansen - Thomas@GaiaSoul.com </para><para>
/// Licensed under The MIT License. </para>
/// </remarks>
public sealed class Evaluator<TResult>
{
    /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    private Evaluator() : base()
    { }

    /// <summary> Evaluates each function in order and returns the result to caller. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="functions"> Functions. </param>
    /// <returns> The sequence. </returns>
    public static IEnumerable<TResult> Sequentially( IEnumerable<Func<TResult>> functions )
    {
        // Sequentially execute each action on calling thread.
        foreach ( Func<TResult> ix in functions )
        {
            yield return ix();
        }
    }

    /// <summary>
    /// Evaluates each function in parallel blocking the calling thread until all functions are
    /// evaluated, and returns the results of the evaluation of each function to caller.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="functions"> Functions to evaluate. </param>
    /// <returns> The result of each function. </returns>
    public static IEnumerable<TResult> Parallel( IEnumerable<Func<TResult>> functions )
    {
        // Synchronizing access to return values.
        List<TResult> result = [];
        Synchronizer<List<TResult>> sync = new( result );

        // Creates and starts a new thread for each action.
        List<Thread> threads = functions.Select( ix => new Thread( new ThreadStart( () =>
        {
            TResult? res = ix();
            sync.Write( sh => sh.Add( res ) );
        } ) ) ).ToList();
        threads.ForEach( ix => ix.Start() );
        threads.ForEach( ix => ix.Join() );
        foreach ( TResult ix in result )
        {
            yield return ix;
        }
    }
}
