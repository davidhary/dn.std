namespace cc.isr.Std.Lizzie.Tests;

/// <summary> Functions tests. </summary>
/// <remarks>
/// David, 2019-02-03, <para> <see href="https://GitHub.com/polterguy/lizzie/"/>. </para><para>
/// Copyright (c) 2018 Thomas Hansen - Thomas@GaiaSoul.com </para><para>
/// Licensed under The MIT License.</para>
/// </remarks>
[TestClass]
public class ActionsTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " execute tests "

    /// <summary> (Unit Test Method) executes this object. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void Execute()
    {
        string result = string.Empty;
        Actions actions = [() => result += "Foo", () => result += "Bar"];
        actions.Execute();
        Assert.AreEqual( "FooBar", result );
    }

    /// <summary> (Unit Test Method) executes the arguments operation. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void ExecuteArgs()
    {
        Actions<Mutable<string>> sequence = [arg => arg.Value += "Foo", arg => arg.Value += "Bar"];
        Mutable<string> mutable = new( "initial_" );
        sequence.Execute( mutable );
        Assert.AreEqual( "initial_FooBar", mutable.Value );
    }

    /// <summary> Executes the parallel operation. </summary>
    /// <remarks>
    /// Creates two lambdas that run in parallel. If you look carefully at this code, you’ll notice
    /// that the result of the evaluation doesn't assume that any one of my lambdas is being executed
    /// before the other. The order of execution is not explicitly specified, and these lambdas are
    /// being executed on separate threads. This is because the Actions class in Figure 3lets you add
    /// delegates to it, so you can later decide if you want to execute the delegates in parallel or
    /// sequentially.
    /// </remarks>
    /// <param name="initialValue"> The initial value. </param>
    /// <param name="oneValue">     The one value. </param>
    /// <param name="otherValue">   The other value. </param>
    private static void ExecuteParallel( string initialValue, string oneValue, string otherValue )
    {
        string result = string.Empty;
        using ( Synchronizer<string, string, string> sync = new( initialValue ) )
        {
            Actions actions = [() => sync.Assign( res => res + oneValue ), () => sync.Assign( res => res + otherValue )];
            actions.ExecuteParallel();
            sync.Read( ( val ) => result = val );
        }

        Assert.AreEqual( true, ($"{initialValue}{oneValue}{otherValue}" ?? "") == (result ?? "") || (result ?? "") == ($"{initialValue}{otherValue}{oneValue}" ?? "") );
    }

    /// <summary> (Unit Test Method) executes the parallel test 1 operation. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void ExecuteParallelTest1()
    {
        ExecuteParallel( "initial_", "Foot", "Bar" );
        using Synchronizer<string, string, string> sync = new( "initial_" );
        Actions actions = [() => sync.Assign( res => res + "Foo" ), () => sync.Assign( res => res + "Bar" )];
        actions.ExecuteParallel();
        string result = string.Empty;
        sync.Read( ( val ) => result = val );
        Assert.AreEqual( true, result is "initial_FooBar" or "initial_BarFoo" );
    }

    /// <summary> (Unit Test Method) executes the parallel test 2 operation. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void ExecuteParallelTest2()
    {
        string result = string.Empty;
        ManualResetEvent wait = new( false );
        Actions actions =
        [
            () =>
            {
                result += "Foo";
                _ = wait.Set();
            },
            () =>
            {
                _ = wait.WaitOne();
                result += "Bar";
            }
        ];
        actions.ExecuteParallel();
        Assert.AreEqual( "FooBar", result );
    }

    /// <summary> (Unit Test Method) executes the parallel test 3 operation. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void ExecuteParallelTest3()
    {
        string result = string.Empty;
        ManualResetEvent wait = new( false );
        Actions actions =
        [
            () =>
            {
                _ = wait.WaitOne();
                result += "Foo";
            },
            () =>
            {
                result += "Bar";
                _ = wait.Set();
            }
        ];
        actions.ExecuteParallel();
        Assert.AreEqual( "BarFoo", result );
    }

    /// <summary> (Unit Test Method) executes the parallel unblocked operation. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void ExecuteParallelUnblocked()
    {
        string result = string.Empty;
        ManualResetEvent[] waits = [new( false ), new( false ), new( false )];
        Actions actions =
        [
            () =>
            {
                result += "1";
                _ = waits[0].Set();
            },
            () =>
            {
                result += "2";
                _ = waits[1].Set();
            },
            () =>
            {
                result += "3";
                _ = waits[2].Set();
            }
        ];
        actions.ExecuteParallelUnblocked();
        foreach ( ManualResetEvent e in waits )
            _ = e.WaitOne();
        // this fails on STA threads: WaitHandle.WaitAll(waits)
        bool outcome = result is "123" or "132" or "231" or "213" or "312" or "321";
        Assert.AreEqual( true, outcome, $"{result} unexpected" );
    }

    /// <summary> (Unit Test Method) executes the parallel unblocked argument operation. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void ExecuteParallelUnblockedArg()
    {
        EventWaitHandle[] waits = [ new( false, EventResetMode.ManualReset ),
            new( false, EventResetMode.ManualReset ),
            new( false, EventResetMode.ManualReset ) ];
        Actions<Synchronizer<string>> actions =
        [
            (input) =>
            {
                _ = waits[1].WaitOne();
                input.Assign(current => current + "1");
                _ = waits[0].Set();
            },
            (input) =>
            {
                _ = waits[2].WaitOne();
                input.Assign(current => current + "2");
                _ = waits[1].Set();
            },
            (input) =>
            {
                input.Assign(current => current + "3");
                _ = waits[2].Set();
            }
        ];
        using Synchronizer<string> sync = new( "initial_" );
        actions.ExecuteParallelUnblocked( sync );
        foreach ( EventWaitHandle e in waits )
            _ = e.WaitOne();
        // this fails on STA threads: WaitHandle.WaitAll(waits)
        string result = string.Empty;
        sync.Read( ( val ) => result = val );
        Assert.AreEqual( "initial_321", result );
    }
    #endregion
}
