namespace cc.isr.Std.Lizzie.Tests;

/// <summary> Functions tests. </summary>
/// <remarks>
/// David, 2019-02-03  <para> <see href="https://GitHub.com/polterguy/lizzie/"/>. </para><para>
/// Copyright (c) 2018 Thomas Hansen - Thomas@GaiaSoul.com </para><para>
/// Licensed under The MIT License.</para>
/// </remarks>
[TestClass]
public class FunctionsTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " evaluate tests "

    /// <summary> (Unit Test Method) evaluates this object. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void Evaluate()
    {
        Functions<string> functions = [() => "1", () => "2", () => "3"];
        string result = string.Empty;
        foreach ( string idx in functions.Evaluate() )
            result += idx;

        Assert.AreEqual( "123", result );
    }

    /// <summary> (Unit Test Method) evaluate parallel. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestMethod()]
    public void EvaluateParallel()
    {
        Functions<string> functions = [() => "1", () => "2", () => "3"];
        using Synchronizer<string> sync = new( "" );
        foreach ( string idx in functions.EvaluateParallel() )
            sync.Assign( input => input + idx );

        bool outcome = false;
        string result = string.Empty;
        sync.Read( x => result = x );
        outcome = result is "123" or "132" or "231" or "213" or "312" or "321";
        Assert.IsTrue( outcome );
    }
    #endregion
}
