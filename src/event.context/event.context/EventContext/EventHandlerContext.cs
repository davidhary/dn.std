namespace cc.isr.Std.EventContext;

/// <summary> A generic event handler context. </summary>
/// <remarks>
/// The <see cref="EventHandlerContext{TEventArgs}"/> and the associated custom event handlers make the
/// generic event fire on the SynchronizationContext of the listening code. <para>
/// The event declaration is modified to a custom event handler. </para><para>
/// As each handler may have a different context, the AddHandler block stores the handler being
/// passed in as well as the SynchronizationContext to be used later when raising the
/// event.</para> <para>
/// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-01-12 </para><para>
/// David, 2009-10-01 from Bill MCCARTHY  </para><para>
/// https://visualstudiomagazine.com/Articles/2009/10/01/Threading-and-the-UI.aspx?Page=2. </para>
/// </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 2020-09-15. </remarks>
/// <param name="handler"> The handler. </param>
public partial class EventHandlerContext<TEventArgs>( EventHandler<TEventArgs> handler ) where TEventArgs : EventArgs
{
    /// <summary> Gets or sets the synchronization context. </summary>
    /// <value> The context. </value>
    private SynchronizationContext Context { get; set; } = SynchronizationContext.Current;

    /// <summary> Gets or sets the handler. </summary>
    /// <value> The handler. </value>
    public EventHandler<TEventArgs> Handler { get; private set; } = handler;

    /// <summary>
    /// Executes the given operation on a different thread, and waits for the result.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Property changed event information. </param>
    public void UnsafeInvoke( object? sender, TEventArgs e )
    {
        this.Handler?.Invoke( sender, e );
    }

    /// <summary> Executes the asynchronous on a different thread, and waits for the result. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      T event information. </param>
    /// <returns> A Task. </returns>
    public async Task InvokeAsync( object? sender, TEventArgs e )
    {
        await Task.Run( () => this.Send( sender, e ) );
    }
}
/// <summary>
/// A generic thread-safe collection of <see cref="EventHandlerContext{TEventArgs}"/>
/// </summary>
/// <remarks>
/// <para>
/// David, 2018-12-11 </para>
/// </remarks>
public class EventHandlerContextCollection<TEventArgs> : List<EventHandlerContext<TEventArgs>> where TEventArgs : EventArgs
{
    /// <summary>
    /// Executes the given operation on a different thread, and waits for the result.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Property changed event information. </param>
    public void UnsafeInvoke( object? sender, TEventArgs e )
    {
        foreach ( EventHandlerContext<TEventArgs> handler in this )
        {
            handler?.UnsafeInvoke( sender, e );
        }
    }

    /// <summary> Executes the asynchronous on a different thread, and waits for the result. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      T event information. </param>
    /// <returns> A Task. </returns>
    public async Task InvokeAsync( object? sender, TEventArgs e )
    {
        await Task.Run( () => this.Send( sender, e ) );
    }

    /// <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Property changed event information. </param>
    public void Raise( object? sender, TEventArgs e )
    {
        foreach ( EventHandlerContext<TEventArgs> handler in this )
        {
            handler?.Send( sender, e );
        }
    }

    /// <summary> Executes (posts) the event handler action asynchronously (thread unsafe). </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      A T to process. </param>
    public void Post( object? sender, TEventArgs e )
    {
        foreach ( EventHandlerContext<TEventArgs> handler in this )
        {
            handler?.Post( sender, e );
        }
    }

    /// <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      A T to process. </param>
    public void Send( object? sender, TEventArgs e )
    {
        foreach ( EventHandlerContext<TEventArgs> handler in this )
        {
            handler?.Send( sender, e );
        }
    }

    /// <summary> Looks up a given key to find its associated last index. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> An Integer. </returns>
    private int LookupLastIndex( EventHandler<TEventArgs> value )
    {
        return this.ToList().FindLastIndex( x => x.Handler.Equals( value ) );
    }

    /// <summary> Removes the value described by value. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="value"> The value. </param>
    public void RemoveValue( EventHandler<TEventArgs> value )
    {
        if ( this.Any() )
        {
            int lastIndex = this.LookupLastIndex( value );
            if ( lastIndex >= 0 && lastIndex < this.Count && this.ElementAt( lastIndex ).Handler.Equals( value ) )
            {
                this.RemoveAt( lastIndex );
            }
        }
    }

    /// <summary> Removes all. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    public void RemoveAll()
    {
        while ( this.Count > 0 )
        {
            this.RemoveAt( 0 );
        }
    }
}
