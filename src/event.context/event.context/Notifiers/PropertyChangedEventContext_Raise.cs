using System.ComponentModel;

namespace cc.isr.Std.EventContext.Notifiers;
public partial class PropertyChangedEventContext
{
    #region " active context "

    /// <summary> Returns the current synchronization context. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
    /// null. </exception>
    /// <returns> A Threading.SynchronizationContext. </returns>
    private static SynchronizationContext CurrentEventContext()
    {
        if ( SynchronizationContext.Current is null )
        {
            SynchronizationContext.SetSynchronizationContext( new SynchronizationContext() );
        }

        return SynchronizationContext.Current is null
            ? throw new InvalidOperationException( "Current Synchronization Context not set;. Must be set before starting the thread." )
            : SynchronizationContext.Current;
    }

    /// <summary> Gets a context for the active. </summary>
    /// <value> The active context. </value>
    private SynchronizationContext ActiveContext => this.Context ?? CurrentEventContext();

    #endregion

    #region " send post "

    /// <summary>
    /// Asynchronously raises (Posts) the <see cref="PropertyChangedEventHandler"/>. It does all the
    /// checking to see if the SynchronizationContext is nothing or not, and invokes the delegate
    /// accordingly. Note that, unlike the synchronous Send action, although the event is posted on
    /// the synchronization context, cross thread exceptions still occurred, ostensibly, due to the
    /// changing of the sync context between the time the action was elicited and the time it was
    /// actually invoked.
    /// </summary>
    /// <remarks> David, 2020-07-27. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Property changed event information. </param>
    public void Post( object? sender, PropertyChangedEventArgs e )
    {
        PropertyChangedEventHandler? handler = this.Handler;
        if ( handler is not null )
        {
            this.ActiveContext.Post( ( object state ) => handler( sender, e ), null );
        }
    }

    /// <summary>
    /// Synchronously raises (sends) the <see cref="PropertyChangedEventHandler"/>. It does all the
    /// checking to see if the SynchronizationContext is nothing or not, and invokes the delegate
    /// accordingly.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Property changed event information. </param>
    public void Send( object? sender, PropertyChangedEventArgs e )
    {
        PropertyChangedEventHandler? handler = this.Handler;
        if ( handler is not null )
        {
            this.ActiveContext.Send( ( object state ) => handler( sender, e ), null );
        }
    }

    #endregion

    #region " post dynamic invoke "

    /// <summary> Posts a dynamic invoke of the event delegate. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Property changed event information. </param>
    public void PostDynamicInvoke( object? sender, PropertyChangedEventArgs e )
    {
        if ( sender is null || e is null ) return;
        SynchronizationContext context = this.Context ?? CurrentEventContext();
        this.PostDynamicInvoke( context, sender, e );
    }

    /// <summary> Posts a dynamic invoke of the event delegate. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="context"> The context. </param>
    /// <param name="sender">  Source of the event. </param>
    /// <param name="e">       Property changed event information. </param>
    private void PostDynamicInvoke( SynchronizationContext context, object? sender, PropertyChangedEventArgs e )
    {
        if ( sender is null || e is null || this.Handler is null ) return;
        PropertyChangedEventHandler? handler = this.Handler;
        if ( handler is not null )
        {
            foreach ( Delegate d in handler.GetInvocationList() )
            {
                context.Post( ee => d.DynamicInvoke( [sender, e] ), e );
            }
        }
    }

    #endregion
}
