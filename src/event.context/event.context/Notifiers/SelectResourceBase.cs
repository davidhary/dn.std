using cc.isr.Std.Async;

namespace cc.isr.Std.EventContext.Notifiers;

/// <summary> A resource selector base class with event notifications. </summary>
/// <remarks>
/// <para>
/// David, 2018-12-11 </para>
/// </remarks>
public abstract class SelectResourceBase : PropertyChangedNotifier
{
    #region " construction and cleanup "

    /// <summary> Specialized default constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    protected SelectResourceBase() : base()
    {
        this.ResourceNames = [];
        this._isOpen = false;
        this._searchable = true;
        this._searchToolTip = string.Empty;
        this._candidateResourceName = string.Empty;
        this._validatedResourceName = string.Empty;
    }

    #endregion

    #region " searchable "

    /// <summary> True if searchable. </summary>
    private bool _searchable;

    /// <summary>
    /// Gets or sets the condition determining if the control can be searchable. The elements can be
    /// searched only if not open.
    /// </summary>
    /// <value> The searchable. </value>
    public bool Searchable
    {
        get => this._searchable;
        set
        {
            if ( this.SetProperty( ref this._searchable, value ) )
                this.OnPropertyChanged( nameof( SelectResourceBase.SearchEnabled ) );
        }
    }

    /// <summary> Gets search enabled state. </summary>
    /// <value> The search enabled state. </value>
    public bool SearchEnabled => this.Searchable && !this.IsOpen;

    /// <summary> True if is open, false if not. </summary>
    private bool _isOpen;

    /// <summary> Gets or sets the open status. </summary>
    /// <remarks> the search should be disabled when the resource is open. </remarks>
    /// <value> The open status. </value>
    public bool IsOpen
    {
        get => this._isOpen;
        set
        {
            if ( this.SetProperty( ref this._isOpen, value ) )
                this.OnPropertyChanged( nameof( SelectResourceBase.SearchEnabled ) );
        }
    }

    /// <summary> The search tool tip. </summary>
    private string _searchToolTip;

    /// <summary> Gets or sets the Search tool tip. </summary>
    /// <value> The Search tool tip. </value>
    public string SearchToolTip
    {
        get => this._searchToolTip;
        set => _ = this.SetProperty( ref this._searchToolTip, value );
    }

    #endregion

    #region " resource name "

    /// <summary> Name of the candidate resource. </summary>
    private string _candidateResourceName;

    /// <summary> Gets or sets the name of the candidate resource. </summary>
    /// <value> The name of the candidate resource. </value>
    public string CandidateResourceName
    {
        get => this._candidateResourceName;
        set
        {
            value ??= string.Empty;
            _ = this.SetProperty( ref this._candidateResourceName, value.Trim() );
        }
    }

    /// <summary> Gets the resource found tool tip. </summary>
    /// <value> The resource found tool tip. </value>
    public string ResourceFoundToolTip { get; set; } = "Resource found";

    /// <summary> Gets the resource not found tool tip. </summary>
    /// <value> The resource not found tool tip. </value>
    public string ResourceNotFoundToolTip { get; set; } = "Resource not found";

    /// <summary> Gets existence status of the resource. </summary>
    /// <value> The resource name exists. </value>
    public bool SelectedResourceExists => !string.IsNullOrWhiteSpace( this.ValidatedResourceName );

    /// <summary> Name of the validated resource. </summary>
    private string _validatedResourceName;

    /// <summary> Returns the validated resource name. </summary>
    /// <value> The name of the validated resource. </value>
    public string ValidatedResourceName
    {
        get => this._validatedResourceName;
        set
        {
            value ??= string.Empty;
            if ( this.SetProperty( ref this._validatedResourceName, value.Trim() ) )
            {
                this.SearchToolTip = string.IsNullOrWhiteSpace( this.ValidatedResourceName ) ? this.ResourceFoundToolTip : this.ResourceNotFoundToolTip;
            }
        }
    }

    /// <summary> Queries resource exists. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="resourceName"> Name of the resource. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    public abstract bool QueryResourceExists( string resourceName );

    #endregion

    #region " validate resource  "

    /// <summary> True to enable, false to disable the validation. </summary>
    private bool _validationEnabled;

    /// <summary> Gets or sets Validation Enabled state. </summary>
    /// <remarks>
    /// Validation is disabled by default to facilitate resource selection in case of resource
    /// manager mismatch between VISA implementations.
    /// </remarks>
    /// <value> The Validation enabled. </value>
    public bool ValidationEnabled
    {
        get => this._validationEnabled;
        set => _ = this.SetProperty( ref this._validationEnabled, value );
    }

    /// <summary> Attempts to validate (e.g., check existence) the resource by name. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="resourceName"> Name of the resource. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    public abstract (bool Success, string Details) TryValidateResource( string resourceName );

    /// <summary> Used to notify of start of the validation of resource name. </summary>
    /// <remarks> David, 2020-07-20. </remarks>
    /// <param name="resourceName"> The name of the resource. </param>
    /// <param name="e">            Event information to send to registered event handlers. </param>
    protected abstract void OnValidatingResourceName( string resourceName, System.ComponentModel.CancelEventArgs e );

    /// <summary> Used to notify of validation of resource name. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="resourceName"> The name of the resource. </param>
    /// <param name="e">            Action event information. </param>
    protected abstract void OnResourceNameValidated( string resourceName, EventArgs e );

    /// <summary> Attempts to validate resource name from the given data. </summary>
    /// <remarks> David, 2020-07-20. </remarks>
    /// <param name="resourceName">        The name of the resource. </param>
    /// <param name="applyEnabledFilters"> True to apply filters if enabled. </param>
    /// <returns> The (Success As Boolean, Details As String) </returns>
    public (bool Success, string Details) TryValidateResourceName( string resourceName, bool applyEnabledFilters )
    {
        string activity = string.Empty;
        (bool Success, string Details) result;
        try
        {
            activity = "Selecting resource name";
            if ( string.IsNullOrWhiteSpace( resourceName ) )
            {
                result = (false, $"{activity} canceled because name is empty");
            }
            else
            {
                System.ComponentModel.CancelEventArgs args = new();
                activity = "Validating resource name";
                this.OnValidatingResourceName( resourceName, args );
                if ( args.Cancel )
                {
                    result = (false, $"{activity} canceled");
                }
                else
                {
                    activity = "Enumerating resource names";
                    _ = this.EnumerateResources( applyEnabledFilters );
                    activity = $"trying to select resource '{resourceName}'";
                    result = this.TryValidateResource( resourceName );
                    if ( result.Success )
                    {
                        this.OnResourceNameValidated( resourceName, EventArgs.Empty );
                    }
                }
            }
        }
        catch ( Exception ex )
        {
            result = (false, $"Exception {activity};. {ex}");
        }
        finally
        {
        }

        return result;
    }

    #endregion

    #region " enumerate resources "

    /// <summary> True if has resources, false if not. </summary>
    private bool _hasResources;

    /// <summary> Gets or sets the sentinel indication if resources were enumerated. </summary>
    /// <value> The has resources. </value>
    public bool HasResources
    {
        get => this._hasResources;
        set => _ = this.SetProperty( ref this._hasResources, value );
    }

    /// <summary> Gets or sets the resource names. </summary>
    /// <value> The resource names. </value>
    public System.ComponentModel.BindingList<string> ResourceNames { get; private set; }

    /// <summary> Enumerate resource names. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="names"> The names. </param>
    /// <returns>
    /// A =<see cref="System.ComponentModel.BindingList{T}">binding list</see>
    /// </returns>
    public System.ComponentModel.BindingList<string> EnumerateResources( string[] names )
    {
        // suspend binding while building the resource names
        this.ResourceNames.RaiseListChangedEvents = false;
        this.ResourceNames.Clear();
        if ( names?.Any() == true )
        {
            foreach ( string name in names )
            {
                this.ResourceNames.Add( name );
            }
        }
        // resume and reset binding 
        this.ResourceNames.RaiseListChangedEvents = true;
        this.ResourceNames.ResetBindings();
        this.SyncNotifyPropertyChanged( nameof( SelectResourceBase.ResourceNames ) );
        this.HasResources = this.ResourceNames.Any();
        return this.ResourceNames;
    }

    /// <summary> Enumerate resource names. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="names"> The names. </param>
    /// <returns>
    /// A =<see cref="System.ComponentModel.BindingList{T}">binding list</see>
    /// </returns>
    public System.ComponentModel.BindingList<string> EnumerateResources( IEnumerable<string> names )
    {
        // suspend binding while building the resource names
        this.ResourceNames.RaiseListChangedEvents = false;
        this.ResourceNames.Clear();
        if ( names?.Any() == true )
        {
            foreach ( string name in names )
            {
                this.ResourceNames.Add( name );
            }
        }
        // resume and reset binding 
        this.ResourceNames.RaiseListChangedEvents = true;
        this.ResourceNames.ResetBindings();
        this.SyncNotifyPropertyChanged( nameof( SelectResourceBase.ResourceNames ) );
        this.HasResources = this.ResourceNames.Any();
        return this.ResourceNames;
    }

    /// <summary> Enumerate resource names. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="names"> The names. </param>
    /// <returns>
    /// A =<see cref="System.ComponentModel.BindingList{T}">binding list</see>
    /// </returns>
    public System.ComponentModel.BindingList<string> EnumerateResources( IList<string> names )
    {
        // suspend binding while building the resource names
        this.ResourceNames.RaiseListChangedEvents = false;
        this.ResourceNames.Clear();
        if ( names?.Any() == true )
        {
            foreach ( string name in names )
            {
                this.ResourceNames.Add( name );
            }
        }
        // resume and reset binding 
        this.ResourceNames.RaiseListChangedEvents = true;
        this.ResourceNames.ResetBindings();
        this.SyncNotifyPropertyChanged( nameof( SelectResourceBase.ResourceNames ) );
        this.HasResources = this.ResourceNames.Any();
        return this.ResourceNames;
    }

    /// <summary> Enumerate resource names. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="applyEnabledFilters"> True to apply filters if enabled. </param>
    /// <returns> A list of <see cref="string"/>. </returns>
    public abstract System.ComponentModel.BindingList<string> EnumerateResources( bool applyEnabledFilters );

    #endregion

    #region " validate resource name async task "

    /// <summary> Gets or sets the default resource name validation task timeout. </summary>
    /// <value> The default resource name validation timeout. </value>
    public static TimeSpan DefaultResourceNameValidationTaskTimeout { get; set; } = TimeSpan.FromSeconds( 5d );

    /// <summary> Gets or sets the resource name validation tasker. </summary>
    /// <value> The resource name validation tasker. </value>
    public Tasker<(bool Success, string Details)>? ResourceNameValidationTasker { get; private set; }

    /// <summary> Starts task validating resource name. </summary>
    /// <remarks> David, 2020-07-20. </remarks>
    /// <param name="resourceName">        The name of the resource. </param>
    /// <param name="applyEnabledFilters"> True to apply filters if enabled. </param>
    public void StartTaskValidatingResourceName( string resourceName, bool applyEnabledFilters )
    {
        this.ResourceNameValidationTasker = new Tasker<(bool Success, string Details)>();
        this.ResourceNameValidationTasker.StartAction( () => this.TryValidateResourceName( resourceName, applyEnabledFilters ) );
    }

    /// <summary>   Await resource name selection. </summary>
    /// <remarks>   David, 2020-08-05. </remarks>
    /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
    ///                                                 failed. </exception>
    public void AwaitResourceNameSelection()
    {
        if ( this.ResourceNameValidationTasker is not null )
        {
            (TaskStatus status, (bool Success, string Details) result) = this.ResourceNameValidationTasker.AwaitCompletion( DefaultResourceNameValidationTaskTimeout );
            if ( System.Threading.Tasks.TaskStatus.RanToCompletion != status )
                throw new InvalidOperationException( $"resource name selection task existed with a {status} status; details: '{result.Details}'" );
        }
    }

    #endregion
}
