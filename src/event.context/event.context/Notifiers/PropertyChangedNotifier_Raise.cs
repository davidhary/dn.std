using System.ComponentModel;

namespace cc.isr.Std.EventContext.Notifiers;
public partial class PropertyChangedNotifier
{
    #region " notify defauilt: synchronized (using send) "

    /// <summary>
    /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
    /// threading; much faster.
    /// </summary>
    /// <remarks>
    /// David, 2020-09-21. <para>
    /// This method defaults to sync notification so as to quickly get the notifications for unit
    /// testing.
    /// </para>
    /// </remarks>
    /// <param name="e">            Property Changed event information. </param>
    /// <param name="syncNotify">   (Optional) True to synchronize notify. </param>
    public virtual void NotifyPropertyChanged( PropertyChangedEventArgs e, bool syncNotify = true )
    {
        if ( syncNotify )
            this._propertyChangedHandlers.Send( this, e );
        else
            this._propertyChangedHandlers.Post( this, e );

    }

    /// <summary>
    /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
    /// threading; much faster.
    /// </summary>
    /// <remarks>   David, 2020-09-21. </remarks>
    /// <param name="propertyName"> (Optional) caller member name. </param>
    /// <param name="syncNotify">   (Optional) True to synchronize notify. </param>
    public void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string propertyName = "", bool syncNotify = true )
    {
        if ( !string.IsNullOrWhiteSpace( propertyName ) )
            this.NotifyPropertyChanged( new PropertyChangedEventArgs( propertyName ), syncNotify );
    }

    #endregion

    #region " async notify: post "

    /// <summary>
    /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
    /// threading; much faster.
    /// </summary>
    /// <remarks>
    /// Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
    /// even with slow handler functions.
    /// </remarks>
    /// <param name="e"> Property Changed event information. </param>
    public virtual void AsyncNotifyPropertyChanged( PropertyChangedEventArgs e )
    {
        this.NotifyPropertyChanged( e, false );
    }

    /// <summary>
    /// Asynchronously notifies (posts) property change on a different thread. Unsafe for cross
    /// threading; much faster.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="propertyName"> (Optional) caller member name. </param>
    public void AsyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string propertyName = "" )
    {
        this.NotifyPropertyChanged( propertyName, false );
    }

    #endregion

    #region " sync notify: send "

    /// <summary>
    /// Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    /// </summary>
    /// <remarks>
    /// Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
    /// approach.
    /// </remarks>
    /// <param name="e"> Property Changed event information. </param>
    public virtual void SyncNotifyPropertyChanged( PropertyChangedEventArgs e )
    {
        this.NotifyPropertyChanged( e, true );
    }

    /// <summary>
    /// Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="propertyName"> (Optional) caller member name. </param>
    public void SyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string propertyName = "" )
    {
        this.NotifyPropertyChanged( propertyName, true );
    }

    #endregion

    #region " raise (sends) "

    /// <summary>
    /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
    /// cross thread exceptions.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="propertyName"> (Optional) caller member name. </param>
    public void RaisePropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string propertyName = "" )
    {
        if ( !string.IsNullOrWhiteSpace( propertyName ) )
        {
            this.RaisePropertyChanged( new PropertyChangedEventArgs( propertyName ) );
        }
    }

    /// <summary>
    /// Synchronously notifies property change on the synchronization thread using 'Send' to prevent
    /// cross thread exceptions.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    public virtual void RaisePropertyChanged( PropertyChangedEventArgs e )
    {
        this.OnPropertyChanged( this, e );
    }

    #endregion
}
