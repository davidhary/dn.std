namespace cc.isr.Std.EventContext.Notifiers;

/// <summary> Defines the contract that must be implemented by View Models. </summary>
/// <remarks>
/// <list type="bullet">View Model Notify implementation:<item>
/// Notify Function calls Synchronization Context Post;</item><item>
/// Async Notify Function calls Synchronization Context Post;</item><item>
/// Sync Notify function calls Synchronization Context Send;</item><item>
/// Raise event (custom implementation) calls Synchronization Context Post;</item><item>
/// Notify is used as the default in property set functions.</item></list>
/// <list type="bullet">Information on synchronization context can be found: <item>
/// https://www.CodeProject.com/Articles/31971/Understanding-SynchronizationContext-Part-I
/// </item></list> <para>
/// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2018-12-10, Created from property notifiers
/// </para>
/// </remarks>
public abstract partial class PropertyChangedNotifier
{
    /// <summary> Initializes a new instance of the <see cref="PropertyChangedNotifier" /> class. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    protected PropertyChangedNotifier() : base()
    { }
}
