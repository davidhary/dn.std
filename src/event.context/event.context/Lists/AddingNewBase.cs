using System.ComponentModel;

namespace cc.isr.Std.EventContext.Lists;

/// <summary> An adding new base. </summary>
/// <remarks> David, 2020-09-17. </remarks>
public partial class AddingNewBase
{
    #region " adding new event handler "

    /// <summary> Removes the Adding New event handlers. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    protected void RemoveAddingNewEventHandler()
    {
        this._addingNewHandlers.RemoveAll();
    }

    /// <summary> The adding new handlers. </summary>
    private readonly NotifyAddingNewEventContextCollection _addingNewHandlers = [];

    /// <summary> Event queue for all listeners interested in Adding New events. </summary>
    /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
    /// Using a custom Raise method lets you iterate through the delegate list.
    /// </remarks>

    public event AddingNewEventHandler? AddingNew
    {
        add
        {
            if ( value is not null ) this._addingNewHandlers.Add( new NotifyAddingNewEventContext( value ) );
        }

        remove
        {
            if ( value is not null ) this._addingNewHandlers.RemoveValue( value );
        }
    }

    /// <summary>   Raises the adding new event. </summary>
    /// <remarks>   2023-05-23. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information to send to registered event handlers. </param>
    protected void OnAddingNew( object? sender, AddingNewEventArgs e )
    {
        this._addingNewHandlers.Raise( sender, e );
    }

    /// <summary>   Raises the adding new event. </summary>
    /// <remarks>   2023-05-23. </remarks>
    /// <param name="e">    Event information to send to registered event handlers. </param>
    protected void OnAddingNew( AddingNewEventArgs e )
    {
        this._addingNewHandlers.Raise( this, e );
    }

    #endregion

    #region " notify "

    /// <summary>
    /// Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
    /// fast return of control to the invoking function.
    /// </summary>
    /// <remarks>
    /// 2020-09-21. <para> Async notify (Post) overhead is 7 to 10 times larger than naked raise
    /// event. While unsafe for cross threading, control is returned immediately to the invoking
    /// function. This has no advantage even with slow handler functions.  </para><para>
    /// The overhead of sync notify (Send) is 3 to 4 times larger than naked raise event. </para>
    /// </remarks>
    /// <param name="e">            Adding New event information. </param>
    /// <param name="syncNotify">   (Optional) True to synchronize notify. </param>
    protected virtual void NotifyAddingNew( AddingNewEventArgs e, bool syncNotify = true )
    {
        if ( syncNotify )
            this._addingNewHandlers.Send( this, e );
        else
            this._addingNewHandlers.Post( this, e );
    }

    #endregion
}
