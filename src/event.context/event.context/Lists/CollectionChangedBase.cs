using System.Collections.Specialized;
using System.ComponentModel;

namespace cc.isr.Std.EventContext.Lists;

/// <summary> A collection changed base. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-05-22 </para>
/// </remarks>
public class CollectionChangedBase : Notifiers.PropertyChangedNotifier, INotifyCollectionChanged, INotifyPropertyChanged
{
    /// <summary> Removes the Collection Changed event handlers. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    protected void RemoveCollectionChangedEventHandler()
    {
        this._collectionChangedHandlers.RemoveAll();
    }

    /// <summary> The collection changed handlers. </summary>
    private readonly NotifyCollectionChangedEventContextCollection _collectionChangedHandlers = [];

    /// <summary> Event queue for all listeners interested in Collection Changed events. </summary>
    /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
    /// Using a custom Raise method lets you iterate through the delegate list.
    /// </remarks>
    public event NotifyCollectionChangedEventHandler? CollectionChanged
    {
        add
        {
            if ( value is not null )
                this._collectionChangedHandlers.Add( new NotifyCollectionChangedEventContext( value ) );
        }

        remove
        {
            if ( value is not null )
                this._collectionChangedHandlers.RemoveValue( value );
        }

    }

    /// <summary>   Raises the notify collection changed event. </summary>
    /// <remarks>   2023-05-23. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information to send to registered event handlers. </param>
    protected void OnCollectionChanged( object? sender, NotifyCollectionChangedEventArgs e )
    {
        this._collectionChangedHandlers.Raise( sender, e );
    }

    /// <summary>   Raises the notify collection changed event. </summary>
    /// <remarks>   2023-05-23. </remarks>
    /// <param name="e">    Event information to send to registered event handlers. </param>
    protected void OnCollectionChanged( NotifyCollectionChangedEventArgs e )
    {
        this._collectionChangedHandlers.Raise( this, e );
    }

    /// <summary>
    /// Notifies collection change on the same (Send) or a different (Post) thread.
    /// </summary>
    /// <remarks>
    /// 2020-09-21. <para> Async notify (Post) overhead is 7 to 10 times larger than naked raise
    /// event. While unsafe for cross threading, control is returned immediately to the invoking
    /// function. This has no advantage even with slow handler functions.  </para><para>
    /// The overhead of sync notify (Send) is 3 to 4 times larger than naked raise event. </para>
    /// </remarks>
    /// <param name="e">            Collection Changed event information. </param>
    /// <param name="syncNotify">   (Optional) True to synchronize notify. </param>
    protected virtual void NotifyCollectionChanged( NotifyCollectionChangedEventArgs e, bool syncNotify = true )
    {
        if ( syncNotify )
            this._collectionChangedHandlers.Send( this, e );
        else
            this._collectionChangedHandlers.Post( this, e );
    }

}
