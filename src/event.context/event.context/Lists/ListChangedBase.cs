using System.ComponentModel;

namespace cc.isr.Std.EventContext.Lists;

/// <summary> A list changed base. </summary>
/// <remarks> David, 2020-09-17. </remarks>
public partial class ListChangedBase
{
    #region " list changed event handler "

    /// <summary> Removes the List Changed event handlers. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    protected void RemoveListChangedEventHandler()
    {
        this._listChangedHandlers.RemoveAll();
    }

    /// <summary> The list changed handlers. </summary>
    private readonly NotifyListChangedEventContextCollection _listChangedHandlers = [];

    /// <summary> Event queue for all listeners interested in List Changed events. </summary>
    /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
    /// Using a custom Raise method lets you iterate through the delegate list.
    /// </remarks>

    public event ListChangedEventHandler? ListChanged
    {
        add
        {
            if ( value is not null ) this._listChangedHandlers.Add( new NotifyListChangedEventContext( value ) );
        }

        remove
        {
            if ( value is not null )
                this._listChangedHandlers.RemoveValue( value );
        }
    }

    /// <summary>   Raises the list changed event. </summary>
    /// <remarks>   2023-05-23. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information to send to registered event handlers. </param>
    protected void OnListChanged( object? sender, ListChangedEventArgs e )
    {
        this._listChangedHandlers.Raise( sender, e );
    }

    /// <summary>   Raises the list changed event. </summary>
    /// <remarks>   2023-05-23. </remarks>
    /// <param name="e">        Event information to send to registered event handlers. </param>
    protected void OnListChanged( ListChangedEventArgs e )
    {
        this._listChangedHandlers.Raise( this, e );
    }

    #endregion

    #region " notify "

    /// <summary>
    /// Notifies (posts) change on a different thread. Unsafe for cross threading;
    /// fast return of control to the invoking function.
    /// </summary>
    /// <remarks>
    /// 2020-09-21. <para> Async notify (Post) overhead is 7 to 10 times larger than naked raise
    /// event. While unsafe for cross threading, control is returned immediately to the invoking
    /// function. This has no advantage even with slow handler functions.  </para><para>
    /// The overhead of sync notify (Send) is 3 to 4 times larger than naked raise event. </para>
    /// </remarks>
    /// <param name="e">            List Changed event information. </param>
    /// <param name="syncNotify">   (Optional) True to synchronize notify. </param>
    protected virtual void NotifyListChanged( ListChangedEventArgs e, bool syncNotify = true )
    {
        if ( syncNotify )
            this._listChangedHandlers.Send( this, e );
        else
            this._listChangedHandlers.Post( this, e );

    }

    #endregion
}
