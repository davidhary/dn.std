using System.Collections.Specialized;

namespace cc.isr.Std.EventContext.Lists;

/// <summary> A notify collection changed event context. </summary>
/// <remarks>
/// <para>
/// David, 2019-01-17 </para>
/// </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 2020-09-17. </remarks>
/// <param name="handler"> The handler. </param>
public class NotifyCollectionChangedEventContext( NotifyCollectionChangedEventHandler handler )
{
    /// <summary> Gets the synchronization context. </summary>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <value> The context. </value>
    private SynchronizationContext Context { get; set; } = SynchronizationContext.Current;

    /// <summary> Gets the handler. </summary>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <value> The handler. </value>
    public NotifyCollectionChangedEventHandler Handler { get; private set; } = handler;

    #region " invoke "

    /// <summary>
    /// Executes the given operation directly irrespective of the <see cref="Context"/>.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Collection Changed event information. </param>
    public void UnsafeInvoke( object? sender, NotifyCollectionChangedEventArgs e )
    {
        this.Handler?.Invoke( sender, e );
    }

    #endregion

    #region " active context "

    /// <summary> Returns the current synchronization context. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
    /// null. </exception>
    /// <returns> A Threading.SynchronizationContext. </returns>
    private static SynchronizationContext CurrentEventContext()
    {
        if ( SynchronizationContext.Current is null )
        {
            SynchronizationContext.SetSynchronizationContext( new SynchronizationContext() );
        }

        return SynchronizationContext.Current is null
            ? throw new InvalidOperationException( "Current Synchronization Context not set;. Must be set before starting the thread." )
            : SynchronizationContext.Current;
    }

    /// <summary> Gets a context for the active. </summary>
    /// <value> The active context. </value>
    private SynchronizationContext ActiveContext => this.Context ?? CurrentEventContext();

    #endregion

    #region " send post "

    /// <summary>
    /// Asynchronously raises (Posts) the <see cref="NotifyCollectionChangedEventHandler"/>. It does
    /// all the checking to see if the SynchronizationContext is nothing or not, and invokes the
    /// delegate accordingly. Note that, unlike the synchronous Send action, although the event is
    /// posted on the synchronization context, cross thread exceptions still occurred, ostensibly,
    /// due to the changing of the sync context between the time the action was elicited and the time
    /// it was actually invoked.
    /// </summary>
    /// <remarks> David, 2020-07-27. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Collection Changed event information. </param>
    public void Post( object? sender, NotifyCollectionChangedEventArgs e )
    {
        NotifyCollectionChangedEventHandler? handler = this.Handler;
        if ( handler is not null )
        {
            this.ActiveContext.Post( ( object state ) => handler( sender, e ), null );
        }
    }

    /// <summary>
    /// Synchronously raises (sends) the <see cref="NotifyCollectionChangedEventHandler"/>. It does
    /// all the checking to see if the SynchronizationContext is nothing or not, and invokes the
    /// delegate accordingly.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Collection Changed event information. </param>
    public void Send( object? sender, NotifyCollectionChangedEventArgs e )
    {
        NotifyCollectionChangedEventHandler? handler = this.Handler;
        if ( handler is not null )
        {
            this.ActiveContext.Send( ( object state ) => handler( sender, e ), null );
        }
    }

    #endregion
}
/// <summary> Collection of Notify Collection Changed event contexts. </summary>
/// <remarks>
/// <para>
/// David, 2018-12-11 </para>
/// </remarks>
public class NotifyCollectionChangedEventContextCollection : List<NotifyCollectionChangedEventContext>
{
    /// <summary>
    /// Executes the given operation directly irrespective of the
    /// <see cref="SynchronizationContext"/>.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Collection Changed event information. </param>
    public void UnsafeInvoke( object? sender, NotifyCollectionChangedEventArgs e )
    {
        foreach ( NotifyCollectionChangedEventContext handler in this )
        {
            handler?.UnsafeInvoke( sender, e );
        }
    }

    /// <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Collection Changed event information. </param>
    public void Raise( object? sender, NotifyCollectionChangedEventArgs e )
    {
        foreach ( NotifyCollectionChangedEventContext handler in this )
        {
            handler?.Send( sender, e );
        }
    }

    /// <summary> Executes (posts) the event handler action asynchronously (thread unsafe). </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Collection Changed event information. </param>
    public void Post( object? sender, NotifyCollectionChangedEventArgs e )
    {
        foreach ( NotifyCollectionChangedEventContext handler in this )
        {
            handler?.Post( sender, e );
        }
    }

    /// <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Collection Changed event information. </param>
    public void Send( object? sender, NotifyCollectionChangedEventArgs e )
    {
        foreach ( NotifyCollectionChangedEventContext handler in this )
        {
            handler?.Send( sender, e );
        }
    }

    /// <summary> Looks up a given key to find its associated last index. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> An Integer. </returns>
    private int LookupLastIndex( NotifyCollectionChangedEventHandler value )
    {
        return this.ToList().FindLastIndex( x => x.Handler.Equals( value ) );
    }

    /// <summary> Removes the value described by value. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="value"> The value. </param>
    public void RemoveValue( NotifyCollectionChangedEventHandler value )
    {
        if ( this.Any() )
        {
            int lastIndex = this.LookupLastIndex( value );
            if ( lastIndex >= 0 && lastIndex < this.Count && this.ElementAt( lastIndex ).Handler.Equals( value ) )
            {
                this.RemoveAt( lastIndex );
            }
        }
    }

    /// <summary> Removes all. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public void RemoveAll()
    {
        while ( this.Count > 0 )
        {
            this.RemoveAt( 0 );
        }
    }
}
