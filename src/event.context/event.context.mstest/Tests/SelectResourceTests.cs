using System.ComponentModel;
using cc.isr.Std.EventContext.Notifiers;

namespace cc.isr.Std.EventContext.Tests;

/// <summary>   (Unit Test Class) a select resource tests. </summary>
/// <remarks>   David, 2020-09-22. </remarks>
[TestClass]
public class SelectResourceTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " select resource tests "

    private sealed class Selector : SelectResourceBase
    {
        public override BindingList<string> EnumerateResources( bool applyEnabledFilters )
        {
            throw new NotImplementedException();
        }

        public override bool QueryResourceExists( string resourceName )
        {
            throw new NotImplementedException();
        }

        public override (bool Success, string Details) TryValidateResource( string resourceName )
        {
            throw new NotImplementedException();
        }

        protected override void OnResourceNameValidated( string resourceName, EventArgs e )
        {
            throw new NotImplementedException();
        }

        protected override void OnValidatingResourceName( string resourceName, CancelEventArgs e )
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>   (Unit Test Method) select resource should be constructed. </summary>
    /// <remarks>   David, 2020-10-10. </remarks>
    [TestMethod()]
    public void SelectResourceShouldBeConstructed()
    {
        _ = new Selector();
    }

    #endregion
}
