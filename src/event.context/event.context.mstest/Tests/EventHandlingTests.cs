using cc.isr.Std.EventContext.Notifiers;

namespace cc.isr.Std.EventContext.Tests;

/// <summary> Event handling tests. </summary>
/// <remarks> David, 2020-09-22. </remarks>
[TestClass]
public class EventHandlingTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " property change notifier tests "

    /// <summary> A property change. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    private sealed class PropertyChange : PropertyChangedNotifier
    {
        /// <summary> The expected value. </summary>
        private string? _expectedValue;

        /// <summary> Gets or sets the expected value. </summary>
        /// <value> The expected value. </value>
        public string? ExpectedValue
        {
            get => this._expectedValue;

            set
            {
                if ( !string.Equals( value, this.ExpectedValue, StringComparison.Ordinal ) )
                {
                    this._expectedValue = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the actual value. </summary>
        /// <value> The actual value. </value>
        public string? ActualValue { get; set; }
    }

    /// <summary> Handles the property changed. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Property changed event information. </param>
    private void HandlePropertyChanged( object? sender, System.ComponentModel.PropertyChangedEventArgs e )
    {
        if ( sender is not PropertyChange propertyChangeSender ) return;

        switch ( e?.PropertyName ?? "" )
        {
            case nameof( PropertyChange.ExpectedValue ):
                {
                    propertyChangeSender.ActualValue = propertyChangeSender.ExpectedValue;
                    break;
                }

            default:
                break;
        }
    }

    /// <summary>   (Unit Test Method) property change should be handled. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    [TestMethod()]
    public void PropertyChangeShouldBeHandled()
    {
        PropertyChange sender = new();
        sender.PropertyChanged += this.HandlePropertyChanged;
        sender.ExpectedValue = nameof( PropertyChange.ExpectedValue );
        Assert.AreEqual( sender.ExpectedValue, sender.ActualValue, "should equal after handling the synchronized event" );
    }

    #endregion
}
