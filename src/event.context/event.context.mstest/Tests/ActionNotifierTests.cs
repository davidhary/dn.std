using cc.isr.Std.EventContext.Notifiers;

namespace cc.isr.Std.EventContext.Tests;

/// <summary>   (Unit Test Class) an action notifier tests. </summary>
/// <remarks>   David, 2020-09-18. </remarks>
[TestClass]
public class ActionNotifierTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " action notifier test "

    /// <summary> The actual notified value. </summary>
    private string _actualNotifiedValue = string.Empty;

    /// <summary> Handles the action notify described by value. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="value"> The value. </param>
    private void HandleActionNotify( string value )
    {
        this._actualNotifiedValue = value;
    }

    /// <summary> Registers the action. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="sender">             Source of the event. </param>
    /// <param name="engineFailedAction"> The engine failed action. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
    private void RegisterAction( ActionNotifier<string> sender, Action<string> engineFailedAction )
    {
        sender.Register( engineFailedAction );
    }

    /// <summary> (Unit Test Method) tests action notifier handling. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void ActionNotifierShouldNotify()
    {
        ActionNotifier<string> sender = new();
        this.RegisterAction( sender, this.HandleActionNotify );
        string expectedNotificationValue = $"{nameof( ActionNotifier<string>.SyncInvoke )}";
        sender.SyncInvoke( expectedNotificationValue );
        Assert.AreEqual( expectedNotificationValue, this._actualNotifiedValue, "expects a notification value" );
    }

    #endregion
}
