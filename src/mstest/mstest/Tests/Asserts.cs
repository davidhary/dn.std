namespace cc.isr.Std.Tests;

/// <summary> Assert. </summary>
/// <remarks> David, 2016-12-12 <para>
/// <see href="https://StackOverflow.com/questions/933613/how-do-i-use-assert-to-verify-that-an-exception-has-been-thrown" />
/// </para>
/// </remarks>
public static class Asserts
{
    #region " time span "

    /// <summary> Are equal. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
    /// <param name="expected"> The expected. </param>
    /// <param name="actual">   The actual. </param>
    /// <param name="delta">    The delta. </param>
    /// <param name="message">  The message. </param>
    public static void AreEqual( TimeSpan expected, TimeSpan actual, TimeSpan delta, string message )
    {
        if ( actual < expected.Subtract( delta ) || actual > expected.Add( delta ) )
            throw new AssertFailedException( $"Expected {expected} <> actual {actual} by more than {delta}; {message}" );
    }

    /// <summary> Are equal. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="expected"> The expected. </param>
    /// <param name="actual">   The actual. </param>
    /// <param name="delta">    The delta. </param>
    /// <param name="format">   Describes the format to use. </param>
    /// <param name="args">     A variable-length parameters list containing arguments. </param>
    public static void AreEqual( TimeSpan expected, TimeSpan actual, TimeSpan delta, string format, params object[] args )
    {
        AreEqual( expected, actual, delta, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    #endregion

    #region " date time "

    /// <summary> Are equal. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
    /// <param name="expected"> The expected. </param>
    /// <param name="actual">   The actual. </param>
    /// <param name="delta">    The delta. </param>
    /// <param name="message">  The message. </param>
    public static void AreEqual( DateTime expected, DateTime actual, TimeSpan delta, string message )
    {
        if ( actual < expected.Subtract( delta ) || actual > expected.Add( delta ) )
            throw new AssertFailedException( $"Expected {expected} > actual {actual} by {expected.Subtract( actual ).Subtract( delta )} over {delta}; {message}" );
    }

    /// <summary> Are equal. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="expected"> The expected. </param>
    /// <param name="actual">   The actual. </param>
    /// <param name="delta">    The delta. </param>
    /// <param name="format">   Describes the format to use. </param>
    /// <param name="args">     A variable-length parameters list containing arguments. </param>
    public static void AreEqual( DateTime expected, DateTime actual, TimeSpan delta, string format, params object[] args )
    {
        AreEqual( expected, actual, delta, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    #endregion

    #region " date time offset "

    /// <summary> Are equal. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <exception cref="AssertFailedException"> Thrown when an Assert Failed error condition occurs. </exception>
    /// <param name="expected"> The expected. </param>
    /// <param name="actual">   The actual. </param>
    /// <param name="delta">    The delta. </param>
    /// <param name="message">  The message. </param>
    public static void AreEqual( DateTimeOffset expected, DateTimeOffset actual, TimeSpan delta, string message )
    {
        if ( actual < expected.Subtract( delta ) || actual > expected.Add( delta ) )
            throw new AssertFailedException( $"Expected {expected:o} <> actual {actual:o} by more than {delta}; {message}" );
    }

    /// <summary> Are equal. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="expected"> The expected. </param>
    /// <param name="actual">   The actual. </param>
    /// <param name="delta">    The delta. </param>
    /// <param name="format">   Describes the format to use. </param>
    /// <param name="args">     A variable-length parameters list containing arguments. </param>
    public static void AreEqual( DateTimeOffset expected, DateTimeOffset actual, TimeSpan delta, string format, params object[] args )
    {
        AreEqual( expected, actual, delta, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    #endregion

    #region " is single "

    /// <summary> Is single. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="items"> The items. </param>
    public static void IsSingle( IEnumerable<object> items )
    {
        Assert.IsTrue( items is object );
        Assert.IsTrue( items.Any() );
        Assert.AreEqual( 1, items.Count() );
    }

    /// <summary> Is single. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="items">  The items. </param>
    /// <param name="format"> Describes the format to use. </param>
    /// <param name="args">   A variable-length parameters list containing arguments. </param>
    public static void IsSingle( IEnumerable<object> items, string format, params object[] args )
    {
        Assert.IsTrue( items is object, format, args );
        Assert.IsTrue( items.Any(), format, args );
        Assert.AreEqual( 1, items.Count(), format, args );
    }

    #endregion

    #region " is empty "

    /// <summary> Is single. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="items"> The items. </param>
    public static void IsEmpty( IEnumerable<object> items )
    {
        if ( items is object )
            Assert.IsFalse( items.Any() );
    }

    /// <summary> Is single. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="items">  The items. </param>
    /// <param name="format"> Describes the format to use. </param>
    /// <param name="args">   A variable-length parameters list containing arguments. </param>
    public static void IsEmpty( IEnumerable<object> items, string format, params object[] args )
    {
        if ( items is object )
            Assert.IsFalse( items.Any(), format, args );
    }

    #endregion

    #region " throws "

    /// <summary>   Throws an exception to verify that an exception has been throw. </summary>
    /// <remarks>   David, 2020-09-18. </remarks>
    /// <exception cref="AssertFailedException">    Thrown when an Assert Failed error condition
    ///                                             occurs. </exception>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="function"> The function. </param>
    /// <returns>   A T. </returns>
    public static T Throws<T>( Action function ) where T : Exception
    {
        T? result = null;
        try
        {
            function?.Invoke();
        }
        catch ( T ex )
        {
            result = ex;
        }

        return result is null
            ? throw new AssertFailedException( $"An exception of type {typeof( T )} was expected, but not thrown" )
            : result;
    }

    /// <summary>   Throws an exception to verify that an exception has been throw. </summary>
    /// <remarks>   David, 2020-09-18. </remarks>
    /// <exception cref="AssertFailedException">    Thrown when an Assert Failed error condition
    ///                                             occurs. </exception>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="function"> The function. </param>
    /// <param name="message">  The message. </param>
    /// <returns>   A T. </returns>
    public static T Throws<T>( Action function, string message ) where T : Exception
    {
        T? result = null;
        try
        {
            function?.Invoke();
        }
        catch ( T ex )
        {
            result = ex;
        }

        return result is null
            ? throw new AssertFailedException( $"An exception of type {typeof( T )} was expected, but not thrown; {message}" )
            : result;
    }

    /// <summary> Throws an exception to verify that an exception has been throw. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="function">   The function. </param>
    /// <param name="format"> Describes the format to use. </param>
    /// <param name="args">   A variable-length parameters list containing arguments. </param>
    /// <returns> A T. </returns>
    public static T Throws<T>( Action function, string format, params object[] args ) where T : Exception
    {
        return Throws<T>( function, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
    }

    #endregion
}
