namespace cc.isr.Std.Tests;

/// <summary> A time zone tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-03-14 </para>
/// </remarks>
[TestClass]
public sealed class TimeZoneTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.Name}";
        try
        {
            TraceListener = new Tracing.TestWriterQueueTraceListener( $"{testContext.FullyQualifiedTestClassName}.TraceListener",
                SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
            Trace.WriteLine( "Initializing", methodFullName );
        }
        catch ( Exception ex )
        {
            Trace.WriteLine( $"Failed initializing the test class: {ex}", methodFullName );

            // cleanup to meet strong guarantees

            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        Console.WriteLine( $"Testing {typeof( TimeZoneTests ).Assembly.FullName}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " time zone tests "

    /// <summary> Query if 'timeZone' is daylight saving time. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="timeZone">  The time zone. </param>
    /// <param name="dateValue"> The date value. </param>
    /// <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
    public static bool IsDaylightSavingTime( string timeZone, string dateValue )
    {
        return TimeZoneInfo.FindSystemTimeZoneById( timeZone ).IsDaylightSavingTime( DateTimeOffset.Parse( dateValue, System.Globalization.CultureInfo.CurrentCulture ) );
    }

    /// <summary>
    /// (Unit Test Method) Arizona does not have a daylight saving time when California does.
    /// </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ArizonaDoesNotHaveDaylightSavingTimeWhenCaliforniaDoes()
    {
        string dayString = "2017-06-01";
        string timeZone = "US Mountain Standard Time";
        bool expected = false;
        bool actual = IsDaylightSavingTime( timeZone, dayString );
        Assert.AreEqual( expected, actual );
    }

    /// <summary>
    /// (Unit Test Method) Arizona does not have a daylight saving time when California does not.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ArizonaHasNoDaylightSavingTimeWhenCaliforniaDoesNot()
    {
        string dayString = "2017-01-01";
        string timeZone = "US Mountain Standard Time";
        bool expected = false;
        bool actual = IsDaylightSavingTime( timeZone, dayString );
        Assert.AreEqual( expected, actual );
    }

    /// <summary>   (Unit Test Method) date is pacific time zone daylight saving date. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestMethod()]
    public void DateIsPacificTimeZoneDaylightSavingDate()
    {
        string dayString = "2017-06-01";
        string timeZone = "Pacific Standard Time";
        bool expected = true;
        bool actual = IsDaylightSavingTime( timeZone, dayString );
        Assert.AreEqual( expected, actual );
    }

    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void DateIsNotPacificTimeZoneDaylightSavingDate()
    {
        string dayString = "2017-01-01";
        string timeZone = "Pacific Standard Time";
        bool expected = false;
        bool actual = IsDaylightSavingTime( timeZone, dayString );
        Assert.AreEqual( expected, actual );
    }

    /// <summary> (Unit Test Method) tests get Arizona time zone. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ArizonaTimeZoneIdIsExpected()
    {
        string expected = "US Mountain Standard Time";
        string actual = TimeZoneInfo.FindSystemTimeZoneById( expected ).Id;
        Assert.AreEqual( expected, actual );
    }

    /// <summary>   (Unit Test Method) settings is local pacific standard time. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestMethod()]
    public void SettingsIsLocalPacificStandardTime()
    {
        TimeZoneInfo tz = TimeZoneInfo.Local;
        string expected = TestSiteSettings.Instance.TimeZone();
        string actual = tz.Id;
        Assert.AreEqual( expected, actual );
    }

    /// <summary> Query if 'timeZone' is daylight saving time. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="dateValue"> The date value. </param>
    /// <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
    public static bool IsDaylightSavingTime( string dateValue )
    {
        TimeZoneInfo tz = TimeZoneInfo.Local;
        return tz.IsDaylightSavingTime( DateTimeOffset.Parse( dateValue, System.Globalization.CultureInfo.CurrentCulture ) );
    }

    /// <summary> Query if 'timeZone' is daylight saving time. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
    public static bool IsDaylightSavingTime()
    {
        TimeZoneInfo tz = TimeZoneInfo.Local;
        return tz.IsDaylightSavingTime( DateTimeOffset.Now );
    }

    /// <summary>   (Unit Test Method) date is not daylight savings. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestMethod()]
    public void DateIsNotDaylightSavings()
    {
        bool expectedBoolean = false;
        bool actualBoolean = IsDaylightSavingTime( "2017-01-01" );
        Assert.AreEqual( expectedBoolean, actualBoolean );
    }

    /// <summary>   (Unit Test Method) date is daylight savings. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestMethod()]
    public void DateIsDaylightSavings()
    {
        bool expectedBoolean = true;
        bool actualBoolean = IsDaylightSavingTime( "2017-06-01" );
        Assert.AreEqual( expectedBoolean, actualBoolean );
    }

    /// <summary>   (Unit Test Method) UTC time offset should equal settings. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestMethod()]
    public void UtcTimeOffsetShouldEqualSettings()
    {
        TimeSpan expected = TimeSpan.FromHours( IsDaylightSavingTime()
                        ? TestSiteSettings.Instance.TimeZoneOffset() + 1d
                        : TestSiteSettings.Instance.TimeZoneOffset() );
        TimeSpan actual = DateTimeOffset.Now.Offset;
        Assert.AreEqual( expected, actual );
    }

    /// <summary>   (Unit Test Method) UTC time can be calculated from date time offset. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestMethod()]
    public void UtcTimeCanBeCalculatedFromDateTimeOffset()
    {
        DateTime timeNow = DateTime.Now;
        DateTime expected = timeNow.ToUniversalTime();
        DateTime actual = timeNow.Subtract( DateTimeOffset.Now.Offset );
        Assert.AreEqual( expected, actual );
    }

    /// <summary>   (Unit Test Method) UTC time can be calculated. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestMethod()]
    public void UtcTimeCanBeCalculated()
    {
        DateTime timeNow = DateTime.Now;
        DateTime expected = timeNow.ToUniversalTime();
        DateTime actual = timeNow.Subtract( DateTimeOffset.Parse( timeNow.Date.ToShortDateString(), System.Globalization.CultureInfo.CurrentCulture ).Offset );
        Assert.AreEqual( expected, actual );
    }

    #endregion
}
