using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Collections;

namespace cc.isr.Std.CircularLists;
/// <summary>
/// (Serializable) queue of items of <typeparamref name="T" />. Internally it is implemented as
/// a circular buffer, so Enqueue can be O(n). Dequeue is O(1).
/// </summary>
/// <remarks> David, 2020-09-09. <para>
/// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
/// Licensed under The MIT License.</para>
/// </remarks>
[DebuggerTypeProxy( typeof( GenericQueue<>.GenericQueueDebugView ) )]
[DebuggerDisplay( "Count = {Count}" )]
public class GenericQueue<T> : ICollection<T>, IEnumerable<T>, ICollection, IEnumerable
{
    /// <summary>
    /// Creates a queue of items of <typeparamref name="T"/>. The default initial capacity and grow factor
    /// are used.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    public GenericQueue()
        : this( GenericQueue<T>.DEFAULT_CAPACTITY, GenericQueue<T>.DEFAULT_GROWTH_FACTOR )
    { }

    /// <summary>
    /// Creates a queue of items of <typeparamref name="T"/>. The default grow factor is used.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="capacity"> The capacity. </param>
    public GenericQueue( int capacity )
        : this( capacity, GenericQueue<T>.DEFAULT_GROWTH_FACTOR )
    { }

    /// <summary>
    /// Creates a queue of items of <typeparamref name="T"/>. When full, the new capacity is set to the old
    /// capacity * growFactor.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
    ///                                                 the required range. </exception>
    /// <param name="capacity">     The capacity. </param>
    /// <param name="growFactor">   100 == 1.0, 130 == 1.3, 200 == 2.0. </param>
    public GenericQueue( int capacity, float growFactor )
    {
        if ( capacity < 0 )
        {
            throw new ArgumentOutOfRangeException( nameof( capacity ), $"Capacity {capacity} must not be negative" );
        }

        if ( !(growFactor >= GenericQueue<T>._growFactorRange.Min && growFactor <= GenericQueue<T>._growFactorRange.Max) )
        {
            throw new ArgumentOutOfRangeException( nameof( growFactor ), $"Queue Grow Factor {growFactor} out of range of [{GenericQueue<T>._growFactorRange.Min},{GenericQueue<T>._growFactorRange.Max}]" );
        }

        Contract.EndContractBlock();

        this._array = new T[capacity];
        this.Head = 0;
        this.Tail = 0;
        this.Size = 0;
        this.GrowFactor = ( int ) (growFactor * 100);
    }

    /// <summary>
    /// Fills a Queue with the items of an ICollection.  Uses the enumerator to get each of the
    /// items.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="col">  The col. </param>
    public GenericQueue( ICollection<T> col ) : this( col == null ? GenericQueue<T>.DEFAULT_CAPACTITY : col.Count )
    {
        if ( col == null )
        {
            throw new ArgumentNullException( nameof( col ) );
        }

        Contract.EndContractBlock();

        IEnumerator<T> en = col.GetEnumerator();
        while ( en.MoveNext() )
        {
            this.Enqueue( en.Current );
        }
    }

    /// <summary>   Creates a new <see cref="GenericQueue{T}"/> that is a copy of the current instance. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <returns>   A new <see cref="GenericQueue{T}"/> that is a copy of this instance. </returns>
    public virtual GenericQueue<T> CreateCopy()
    {
        GenericQueue<T> q = new( this.Size ) { Size = this.Size };

        int numToCopy = this.Size;
        int firstPart = (this._array.Length - this.Head < numToCopy) ? this._array.Length - this.Head : numToCopy;
        Array.Copy( this._array, this.Head, q._array, 0, firstPart );
        numToCopy -= firstPart;
        if ( numToCopy > 0 )
        {
            Array.Copy( this._array, 0, q._array, this._array.Length - this.Head, numToCopy );
        }

        q.Version = this.Version;
        return q;
    }

    /// <summary>   Makes a deep copy of this <see cref="GenericQueue{T}"/> . </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <returns>   A copy of this <see cref="GenericQueue{T}"/> . </returns>
    public virtual object Clone()
    {
        {
            return this.CreateCopy();
        }
    }

    /// <summary>   The array. </summary>
    private T[] _array;

    /// <summary>   Gets or sets the head: First valid element in the queue. </summary>
    /// <value> The head. </value>
    public int Head { get; private set; }

    /// <summary>   Gets or sets the tail: Last valid element in the queue. </summary>
    /// <value> The tail. </value>
    public int Tail { get; private set; }

    /// <summary>   100 == 1.0, 130 == 1.3, 200 == 2.0. </summary>
    private int GrowFactor { get; set; }

    /// <summary>   The version. </summary>
    private long Version { get; set; }

    /// <summary>   Increments version. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="value">    The value. </param>
    private void IncrementVersion( int value )
    {
        this.Version = (this.Version + value) % int.MaxValue;
    }

    /// <summary>   The synchronize root. </summary>
    [NonSerialized]
    private object? _syncRoot;

    /// <summary>   The minimum growth. </summary>
    private const int MINIMUM_GROWTH = 4;

    /// <summary>   The default grow factor. </summary>
    private const float DEFAULT_GROWTH_FACTOR = 2f;

    /// <summary>   The default capacity. </summary>
    private const int DEFAULT_CAPACTITY = 32;

    /// <summary>   The grow factor range. </summary>
    private static (float Min, float Max) _growFactorRange = (1.0f, 10.0f);

    /// <summary>   Gets or sets the size: Number of items in the queue. </summary>
    /// <value> The size. </value>
    private int Size { get; set; }

    /// <summary>
    /// Gets the number of items contained in the <see cref="ICollection" />.
    /// </summary>
    /// <value>
    /// The number of items contained in the <see cref="ICollection" />.
    /// </value>
    public virtual int Count => this.Size;

    /// <summary>
    /// Gets a value indicating whether access to the <see cref="ICollection" />
    /// is synchronized (thread safe).
    /// </summary>
    /// <value>
    /// <see langword="true" /> if access to the <see cref="ICollection" /> is
    /// synchronized (thread safe); otherwise, <see langword="false" />.
    /// </value>
    public virtual bool IsSynchronized => false;

    /// <summary>
    /// Gets an object that can be used to synchronize access to the
    /// <see cref="ICollection" />.
    /// </summary>
    /// <value>
    /// An object that can be used to synchronize access to the
    /// <see cref="ICollection" />.
    /// </value>
    public virtual object SyncRoot
    {
        get
        {
            if ( this._syncRoot == null )
            {
                _ = System.Threading.Interlocked.CompareExchange( ref this._syncRoot, new object(), null );
            }
            return this._syncRoot;
        }
    }

    /// <summary>   Removes all items from the queue. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    public virtual void Clear()
    {
        if ( this.Head < this.Tail )
        {
            Array.Clear( this._array, this.Head, this.Size );
        }
        else
        {
            Array.Clear( this._array, this.Head, this._array.Length - this.Head );
            Array.Clear( this._array, 0, this.Tail );
        }

        this.Head = 0;
        this.Tail = 0;
        this.Size = 0;
        this.IncrementVersion( 1 );
    }

    /// <summary>   Gets the capacity. </summary>
    /// <value> The capacity. </value>
    public int Capacity => this._array.Length;

    /// <summary>
    /// CopyTo copies a collection into an Array, starting at a particular index into the array.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
    ///                                                 are null. </exception>
    /// <exception cref="ArgumentException">            Thrown when one or more arguments have
    ///                                                 unsupported or illegal values. </exception>
    /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
    ///                                                 the required range. </exception>
    /// <param name="array">    The one-dimensional <see cref="Array" /> that is the
    ///                         destination of the items copied from
    ///                         <see cref="ICollection" />. The
    ///                         <see cref="Array" /> must have zero-based indexing. </param>
    /// <param name="index">    The zero-based index in <paramref name="array" /> at which copying
    ///                         begins. </param>
    public virtual void CopyTo( T[] array, int index )
    {
        if ( array == null )
        {
            throw new ArgumentNullException( $"{nameof( array )}" );
        }

        if ( array.Rank != 1 )
        {
            throw new ArgumentException( $"Multi-dimensional array is not Not Supported" );
        }

        if ( index < 0 )
        {
            throw new ArgumentOutOfRangeException( $"{nameof( index )}", $"index {index} argument is out of range" );
        }

        Contract.EndContractBlock();

        int arrayLen = array.Length;
        if ( arrayLen - index < this.Size )
        {
            throw new ArgumentException( $"Invalid offset length; array length {arrayLen} - {index} < {this.Size}" );
        }

        int numToCopy = this.Size;
        if ( numToCopy == 0 ) return;

        int firstPart = (this._array.Length - this.Head < numToCopy) ? this._array.Length - this.Head : numToCopy;
        Array.Copy( this._array, this.Head, array, index, firstPart );
        numToCopy -= firstPart;
        if ( numToCopy > 0 )
        {
            Array.Copy( this._array, 0, array, index + this._array.Length - this.Head, numToCopy );
        }
    }

    /// <summary>   Adds an item to the tail of the queue. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="item">  The item to add. </param>
    public virtual void Enqueue( T item )
    {
        if ( this.Size == this._array.Length )
        {
            int newCapacity = ( int ) (this._array.Length * ( long ) this.GrowFactor / 100);
            if ( newCapacity < this._array.Length + MINIMUM_GROWTH )
            {
                newCapacity = this._array.Length + MINIMUM_GROWTH;
            }
            this.SetCapacity( newCapacity );
        }

        this._array[this.Tail] = item;
        this.Tail = (this.Tail + 1) % this._array.Length;
        this.Size++;
        this.IncrementVersion( 1 );
    }

    /// <summary>   Adds obj to the tail of the queue. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="source">   Source for the. </param>
    /// <returns>   An int. </returns>
    public int Enqueue( T[] source )
    {
        return this.Enqueue( source, 0, source.Length );
    }

    /// <summary>   Adds obj to the tail of the queue. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="source">   Source for the. </param>
    /// <param name="offset">   The offset. </param>
    /// <param name="count">    The number of items contained in the
    ///                         <see cref="ICollection" />. </param>
    /// <returns>   An int. </returns>
    public int Enqueue( T[] source, int offset, int count )
    {
        if ( count >= this._array.Length )
        {
            int newCapacity = count + MINIMUM_GROWTH;
            this.SetCapacity( newCapacity );
        }

        int srcIndex = offset;
        for ( int i = 0; i < count; i++, this.Tail++, srcIndex++ )
        {
            if ( this.Tail == this.Capacity )
            {
                this.Tail = 0;
            }
            this._array[this.Tail] = source[srcIndex];
        }
        this.Size = Math.Min( this.Size + count, this.Capacity );
        this.IncrementVersion( 1 );
        return count;
    }

    /// <summary>   Skips; Moves the head forward by the specified count. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="count">    Number of items to skip. </param>
    public void Skip( int count )
    {
        this.Head += count;
        if ( this.Head >= this.Capacity )
        {
            this.Head -= this.Capacity;
        }
    }

    /// <summary>
    /// GetEnumerator returns an IEnumerator over this Queue.  This Enumerator will support removing.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <returns>
    /// An <see cref="IEnumerator" /> that can be used to iterate through
    /// the collection.
    /// </returns>
    public virtual IEnumerator GetEnumerator()
    {
        return new QueueEnumerator( this );
    }

    /// <summary>
    /// Removes the item at the <see cref="Head"/> of the queue and returns it. If the queue is empty, this
    /// method simply returns null.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
    ///                                                 invalid. </exception>
    /// <returns>   The head item from this queue. </returns>
    public virtual T Dequeue()
    {
        if ( this.Count == 0 )
        {
            throw new InvalidOperationException( "Queue is empty" );
        }

        Contract.EndContractBlock();

        T removed = this._array[this.Head];
        // this._array[this.Head] = default;
        this.Head = (this.Head + 1) % this._array.Length;
        this.Size--;
        this.IncrementVersion( 1 );
        return removed;
    }

    /// <summary>   Removes the <see cref="Head"/> items from this queue. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="dst">  Destination for the. </param>
    /// <returns>   The <see cref="Head"/> items from this queue. </returns>
    public int Dequeue( T[] dst )
    {
        return this.Dequeue( dst, 0, dst.Length );
    }

    /// <summary>   Removes the <see cref="Head"/> items from this queue. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="dst">      Destination for the. </param>
    /// <param name="offset">   The offset. </param>
    /// <param name="count">    The number of items contained in the
    ///                         <see cref="ICollection" />. </param>
    /// <returns>   The head items from this queue. </returns>
    public int Dequeue( T[] dst, int offset, int count )
    {
        int realCount = Math.Min( count, this.Size );
        int dstIndex = offset;
        for ( int i = 0; i < realCount; i++, this.Head++, dstIndex++ )
        {
            if ( this.Head == this.Capacity )
            {
                this.Head = 0;
            }

            dst[dstIndex] = this._array[this.Head];
        }
        this.Size -= realCount;
        this.IncrementVersion( 1 );
        return realCount;
    }

    /// <summary>
    /// Returns the item at the <see cref="Head"/> of the queue. The item remains in the queue. If
    /// the queue is empty, this method throws an InvalidOperationException.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
    ///                                                 invalid. </exception>
    /// <returns>   The current item at the <see cref="Head"/> of the queue. </returns>
    public virtual T Peek()
    {
        if ( this.Count == 0 )
        {
            throw new InvalidOperationException( "Queue is empty" );
        }

        Contract.EndContractBlock();

        return this._array[this.Head];
    }

    /// <summary>
    /// Returns a synchronized Queue.  Returns a synchronized wrapper class around the queue - the
    /// caller must not use references to the original queue.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="queue">    The queue. </param>
    /// <returns>   An GenericQueue. </returns>
    public static GenericQueue<T> Synchronized( GenericQueue<T> queue )
    {
        if ( queue == null )
        {
            throw new ArgumentNullException( $"{nameof( queue )}" );
        }

        Contract.EndContractBlock();
        return new SynchronizedQueue( queue );
    }

    /// <summary>
    /// Returns true if the queue contains at least one item equal to <paramref name="item"/>.
    /// Equality is determined using obj.Equals().
    /// 
    /// Exceptions: ArgumentNullException if obj == null.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="item"> The item to test for containment. </param>
    /// <returns>   True if the item is in this collection, false if not. </returns>
    public virtual bool Contains( T item )
    {
        int index = this.Head;
        int count = this.Size;

        while ( count-- > 0 )
        {
            if ( item == null )
            {
                if ( this._array[index] == null )
                {
                    return true;
                }
            }
            else if ( this._array[index] != null && item.Equals( this._array[index] ) )
            {
                return true;
            }
            index = (index + 1) % this._array.Length;
        }

        return false;
    }

    /// <summary>   Gets an element. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="i">    Zero-based index of the. </param>
    /// <returns>   The element. </returns>
    internal T GetElement( int i )
    {
        return this._array[(this.Head + i) % this._array.Length];
    }

    /// <summary>
    /// Iterates over the items in the queue, returning an array of the items in the Queue, or an
    /// empty array if the queue is empty. The order of items in the array is first in to last in,
    /// the same order produced by successive calls to Dequeue.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <returns>   An array that represents the data in this Queue. </returns>
    public virtual T[] ToArray()
    {
        T[] arr = new T[this.Size];
        if ( this.Size == 0 )
        {
            return arr;
        }

        if ( this.Head < this.Tail )
        {
            Array.Copy( this._array, this.Head, arr, 0, this.Size );
        }
        else
        {
            Array.Copy( this._array, this.Head, arr, 0, this._array.Length - this.Head );
            Array.Copy( this._array, 0, arr, this._array.Length - this.Head, this.Tail );
        }

        return arr;
    }

    /// <summary>
    /// Iterates over the items in the queue, returning an array of the items in the Queue, or an
    /// empty array if the queue is empty. The order of items in the array is first in to last in,
    /// the same order produced by successive calls to Dequeue.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="count">    The number of items contained in the
    ///                         <see cref="ICollection" />. </param>
    /// <returns>   An array that represents the data in this Queue. </returns>
    public virtual T[] ToArray( int count )
    {
        T[] arr = new T[count];
        if ( this.Size == 0 )
        {
            return arr;
        }

        if ( this.Head < this.Tail )
        {
            Array.Copy( this._array, this.Head, arr, 0, count );
        }
        else
        {
            int num = this._array.Length - this.Head;
            if ( count <= num )
            {
                Array.Copy( this._array, this.Head, arr, 0, count );
            }
            else
            {
                Array.Copy( this._array, this.Head, arr, 0, num );
                Array.Copy( this._array, 0, arr, num, this.Tail );
            }
        }

        return arr;
    }

    /// <summary>
    /// Grows or shrinks the buffer to hold the capacity if items. Capacity must be greater than <see cref="Size"/>.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="capacity"> The capacity. </param>
    private void SetCapacity( int capacity )
    {
        T[] newArray = new T[capacity];
        if ( this.Size > 0 )
        {
            if ( this.Head < this.Tail )
            {
                Array.Copy( this._array, this.Head, newArray, 0, this.Size );
            }
            else
            {
                Array.Copy( this._array, this.Head, newArray, 0, this._array.Length - this.Head );
                Array.Copy( this._array, 0, newArray, this._array.Length - this.Head, this.Tail );
            }
        }

        this._array = newArray;
        this.Head = 0;
        this.Tail = (this.Size == capacity) ? 0 : this.Size;
        this.IncrementVersion( 1 );
    }

    /// <summary>   Trim to size. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    public virtual void TrimToSize()
    {
        this.SetCapacity( this.Size );
    }

    #region " icollection members "

    /// <summary>
    /// Copies the elements of the <see cref="ICollection" /> to an
    /// <see cref="Array" />, starting at a particular <see cref="Array" /> index.
    /// </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    /// <param name="array">        The one-dimensional <see cref="Array" /> that is the
    ///                             destination of the elements copied from
    ///                             <see cref="ICollection" />. The
    ///                             <see cref="Array" /> must have zero-based indexing. </param>
    /// <param name="arrayIndex">   The zero-based index in <paramref name="array" /> at which
    ///                             copying begins. </param>
    public void CopyTo( Array array, int arrayIndex )
    {
        this.CopyTo( ( T[] ) array, arrayIndex );
    }

    #endregion

    #region " icollection<t> members "

    /// <summary>
    /// Gets the number of elements contained in the
    /// <see cref="ICollection{T}" />.
    /// </summary>
    /// <typeparamref name="T">    Generic type parameter. </typeparamref>
    /// <value>
    /// The number of elements contained in the
    /// <see cref="ICollection{T}" />.
    /// </value>
    int ICollection<T>.Count => this.Size;

    /// <summary>
    /// Gets a value indicating whether the <see cref="ICollection{T}" />
    /// is read-only.
    /// </summary>
    /// <typeparamref name="T">    Generic type parameter. </typeparamref>
    /// <value>
    /// <see langword="true" /> if the <see cref="ICollection{T}" /> is
    /// read-only; otherwise, <see langword="false" />.
    /// </value>
    bool ICollection<T>.IsReadOnly => false;

    /// <summary>
    /// Adds an item to the <see cref="ICollection{T}" />.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <typeparamref name="T">    Generic type parameter. </typeparamref>
    /// <param name="item"> The object to add to the
    ///                     <see cref="ICollection{T}" />. </param>
    void ICollection<T>.Add( T item )
    {
        this.Enqueue( item );
    }

    /// <summary>
    /// Removes the first occurrence of a specific object from the
    /// <see cref="ICollection{T}" />.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <typeparamref name="T">    Generic type parameter. </typeparamref>
    /// <param name="item"> The object to remove from the
    ///                     <see cref="ICollection{T}" />. </param>
    /// <returns>
    /// <see langword="true" /> if <paramref name="item" /> was successfully removed from the
    /// <see cref="ICollection{T}" />; otherwise,
    /// <see langword="false" />. This method also returns <see langword="false" /> if
    /// <paramref name="item" /> is not found in the original
    /// <see cref="ICollection{T}" />.
    /// </returns>
    bool ICollection<T>.Remove( T item )
    {
        if ( this.Size == 0 )
        {
            return false;
        }

        _ = this.Dequeue();
        return true;
    }

    /// <summary>
    /// Copies the elements of the <see cref="ICollection{T}" /> to an
    /// <see cref="Array" />, starting at a particular <see cref="Array" /> index.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="array">    The one-dimensional <see cref="Array" /> that is the
    ///                         destination of the elements copied from
    ///                         <see cref="ICollection{T}" />. The
    ///                         <see cref="Array" /> must have zero-based indexing. </param>
    public void CopyTo( T[] array )
    {
        this.CopyTo( array, 0 );
    }

    #endregion

    #region " ienumerable<t> members "

    /// <summary>   Returns an enumerator that iterates through the collection. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <typeparamref name="T">    Generic type parameter. </typeparamref>
    /// <returns>   An enumerator that can be used to iterate through the collection. </returns>
    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
        int bufferIndex = this.Head;
        for ( int i = 0; i < this.Size; i++, bufferIndex++ )
        {
            if ( bufferIndex == this.Capacity )
            {
                bufferIndex = 0;
            }
            yield return this._array[bufferIndex];
        }
    }

    #endregion

    /// <summary>   Implements a synchronization wrapper around a queue. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    private class SynchronizedQueue : GenericQueue<T>
    {
        /// <summary>   The q. </summary>
        private readonly GenericQueue<T> _q;

        /// <summary>   The synchronizing root. </summary>
        private readonly object _root;

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="q">    An encapsulated <see cref="GenericQueue{T}"/>. </param>
        internal SynchronizedQueue( GenericQueue<T> q )
        {
            this._q = q;
            this._root = this._q.SyncRoot;
        }

        /// <summary>
        /// Gets a value indicating whether access to the <see cref="ICollection" />
        /// is synchronized (thread safe).
        /// </summary>
        /// <value>
        /// <see langword="true" /> if access to the <see cref="ICollection" /> is
        /// synchronized (thread safe); otherwise, <see langword="false" />.
        /// </value>
        public override bool IsSynchronized => true;

        /// <summary>   Gets the synchronize root. </summary>
        /// <value> The synchronize root. </value>
        public override object SyncRoot => this._root;

        /// <summary>
        /// Gets the number of items contained in the <see cref="ICollection" />.
        /// </summary>
        /// <value>
        /// The number of items contained in the <see cref="ICollection" />.
        /// </value>
        public override int Count
        {
            get
            {
                lock ( this._root )
                {
                    return this._q.Count;
                }
            }
        }

        /// <summary>   Clears this <see cref="GenericQueue{T}"/> to its blank/initial state. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        public override void Clear()
        {
            lock ( this._root )
            {
                this._q.Clear();
            }
        }

        /// <summary>   Creates a new <see cref="GenericQueue{T}"/> that is a copy of the current instance. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   A new <see cref="GenericQueue{T}"/> that is a copy of this instance. </returns>
        public SynchronizedQueue CreateCopyNew()
        {
            lock ( this._root )
            {
                return new SynchronizedQueue( this._q.CreateCopy() );
            }
        }

        /// <summary>   Creates a new <see cref="GenericQueue{T}"/> that is a copy of the current instance. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   A new <see cref="GenericQueue{T}"/> that is a copy of this instance. </returns>
        public override object Clone()
        {
            lock ( this._root )
            {
                return new SynchronizedQueue( this._q.CreateCopy() );
            }
        }

        /// <summary>
        /// Returns true if the queue contains at least one item equal to <paramref name="item"/>.
        /// Equality is determined using obj.Equals().
        /// 
        /// Exceptions: ArgumentNullException if obj == null.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="item"> The item to test for containment. </param>
        /// <returns>   True if the item is in this collection, false if not. </returns>
        public override bool Contains( T item )
        {
            lock ( this._root )
            {
                return this._q.Contains( item );
            }
        }

        /// <summary>
        /// Copies the items of the <see cref="ICollection" /> to an
        /// <see cref="Array" />, starting at a particular <see cref="Array" /> index.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="array">        The one-dimensional <see cref="Array" /> that is the
        ///                             destination of the items copied from
        ///                             <see cref="ICollection" />. The
        ///                             <see cref="Array" /> must have zero-based indexing. </param>
        /// <param name="arrayIndex">   The zero-based index in <paramref name="array" /> at which
        ///                             copying begins. </param>
        public override void CopyTo( T[] array, int arrayIndex )
        {
            lock ( this._root )
            {
                this._q.CopyTo( array, arrayIndex );
            }
        }

        /// <summary>   Adds an item to the tail of the queue. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="value">    The item to add. </param>
        public override void Enqueue( T value )
        {
            lock ( this._root )
            {
                this._q.Enqueue( value );
            }
        }

        /// <summary>
        /// Removes the item at the <see cref="P:isr.Std.CircularListsQueue`1.Head" /> of the queue
        /// and returns it. If the queue is empty, this method simply returns null.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   The head item from this queue. </returns>
        public override T Dequeue()
        {
            lock ( this._root )
            {
                return this._q.Dequeue();
            }
        }

        /// <summary>   Returns an enumerator that iterates through a collection. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>
        /// An <see cref="IEnumerator" /> that can be used to iterate through
        /// the collection.
        /// </returns>
        public override IEnumerator GetEnumerator()
        {
            lock ( this._root )
            {
                return this._q.GetEnumerator();
            }
        }

        /// <summary>
        /// Returns the item at the <see cref="Head"/> of the queue. The item remains in the queue. If
        /// the queue is empty, this method throws an InvalidOperationException.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>
        /// The current item at the <see cref="P:isr.Std.CircularListsQueue`1.Head" /> of the queue.
        /// </returns>
        public override T Peek()
        {
            lock ( this._root )
            {
                return this._q.Peek();
            }
        }

        /// <summary>
        /// Iterates over the items in the queue, returning an array of the items in the Queue, or an
        /// empty array if the queue is empty. The order of items in the array is first in to last in,
        /// the same order produced by successive calls to Dequeue.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   An array that represents the data in this <see cref="GenericQueue{T}"/>. </returns>
        public override T[] ToArray()
        {
            lock ( this._root )
            {
                return this._q.ToArray( this._q.Count );
            }
        }

        /// <summary>
        /// Iterates over the items in the queue, returning an array of the items in the Queue, or an
        /// empty array if the queue is empty. The order of items in the array is first in to last in,
        /// the same order produced by successive calls to Dequeue.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="count">    The number of items contained in the
        ///                         <see cref="ICollection" />. </param>
        /// <returns>   An array that represents the data in this Queue. </returns>
        public override T[] ToArray( int count )
        {
            lock ( this._root )
            {
                return this._q.ToArray( count );
            }
        }

        /// <summary>   Trim to size. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        public override void TrimToSize()
        {
            lock ( this._root )
            {
                this._q.TrimToSize();
            }
        }
    }

    /// <summary>
    /// (Serializable) a queue enumerator. Implements an enumerator for a Queue.  The enumerator uses
    /// the internal version number of the list to ensure that no modifications are made to the list
    /// while an enumeration is in progress.
    /// </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    private class QueueEnumerator : IEnumerator, ICloneable
    {
        /// <summary>   The q. </summary>
        private readonly GenericQueue<T> _q;

        /// <summary>   Zero-based index of the. </summary>
        private int _index;

        /// <summary>   The version. </summary>
        private readonly long _version;

        /// <summary>   The current element. </summary>
        private T? _currentElement;

        /// <summary>   State of the enumerator. </summary>
        private EnumeratorState _state;

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="q">    An ObjectQueue to process. </param>
        internal QueueEnumerator( GenericQueue<T> q )
        {
            this._q = q;
            this._version = this._q.Version;
            this.Reset();
        }

        /// <summary>   Creates a new object that is a copy of the current instance. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   A new object that is a copy of this instance. </returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>   Advances the enumerator to the next element of the collection. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <returns>
        /// <see langword="true" /> if the enumerator was successfully advanced to the next element;
        /// <see langword="false" /> if the enumerator has passed the end of the collection.
        /// </returns>
        public virtual bool MoveNext()
        {
            if ( this._version != this._q.Version )
            {
                throw new InvalidOperationException( $"Queue enumerator version {this._version} must equal queue version {this._q.Version}" );
            }

            this._index++;
            if ( this._index == this._q.Size )
            {
                this._state = EnumeratorState.Ended;
                return false;
            }
            else
            {
                this._currentElement = this._q.GetElement( this._index );
                return true;
            }
        }

        private enum EnumeratorState
        {
            None,
            Started,
            Ended
        }


        /// <summary>
        /// Gets the element in the collection at the current position of the enumerator.
        /// </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The element in the collection at the current position of the enumerator. </value>
        public virtual object Current
        {
            get
            {
                if ( this._state == EnumeratorState.Started )
                {
                    return this._currentElement!;
                }
                else
                {
                    if ( this._state == EnumeratorState.None )
                    {
                        throw new InvalidOperationException( "Enumerator not started" );
                    }
                    else
                    {
                        throw new InvalidOperationException( "Enumerator ended" );
                    }
                }
            }
        }

        /// <summary>
        /// Sets the enumerator to its initial position, which is before the first element in the
        /// collection.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        public virtual void Reset()
        {
            if ( this._version != this._q.Version )
            {
                throw new InvalidOperationException( $"Queue enumerator version {this._version} must equal queue version {this._q.Version}" );
            }

            this._index = -1;
            // this._currentElement = default;
            this._state = this._q.Size == 0 ? EnumeratorState.None : EnumeratorState.Started;
        }
    }

    /// <summary>   (Serializable) a queue debug view. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    internal class GenericQueueDebugView
    {
        /// <summary>   The queue. </summary>
        private readonly GenericQueue<T> _queue;

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="queue">    The queue. </param>
        public GenericQueueDebugView( GenericQueue<T> queue )
        {
            Contract.EndContractBlock();

            this._queue = queue ?? throw new ArgumentNullException( $"{nameof( queue )}" );
        }

        /// <summary>   Gets the items. </summary>
        /// <value> The items. </value>
        [DebuggerBrowsable( DebuggerBrowsableState.RootHidden )]
        public T[] Items => this._queue.ToArray( this._queue.Count );
    }
}



