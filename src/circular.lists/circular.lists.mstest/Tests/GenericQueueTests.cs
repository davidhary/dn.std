using System.Collections;
using cc.isr.Std.CircularLists;

namespace cc.isr.Std.CircularLists.Tests;

/// <summary>   (Unit Test Class) a generic queue tests. </summary>
/// <remarks>   David, 2020-09-10. </remarks>
[TestClass]
public class GenericQueueTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    # region " random "

    /// <summary>   The random. </summary>
    private static readonly Random? _randomNumberGenerator = new();

    /// <summary>   Generates a random byte array. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="length">   The length. </param>
    /// <returns>   An array of byte. </returns>
    protected static byte[] GenerateRandomBytes( int length )
    {
        byte[] bytes = new byte[length];
        _randomNumberGenerator!.NextBytes( bytes );
        return bytes;
    }

    #endregion

    #region " test methods "

    /// <summary>   (Unit Test Method) empty queue. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    [TestMethod()]
    public void EmptyQueue()
    {
        byte[] data = GenerateRandomBytes( 100 );
        GenericQueue<byte> queue = new( data.Length );
        _ = queue.Enqueue( data );
        byte[] ret = new byte[queue.Count];
        _ = queue.Dequeue( ret );
        CollectionAssert.AreEqual( data, ret );
        Assert.IsTrue( queue.Count == 0 );
    }

    /// <summary>   (Unit Test Method) fill queue. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    [TestMethod()]
    public void FillQueue()
    {
        byte[] data = GenerateRandomBytes( 100 );
        GenericQueue<byte> queue = new( data.Length );
        _ = queue.Enqueue( data );
        CollectionAssert.AreEqual( data, queue.ToArray() );
    }

    /// <summary>   (Unit Test Method) wrap around queue. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    [TestMethod()]
    public void WrapAroundQueue()
    {
        byte[] data = GenerateRandomBytes( 100 );
        GenericQueue<byte> queue = new( data.Length );
        _ = queue.Enqueue( GenerateRandomBytes( data.Length / 2 ) );
        _ = queue.Enqueue( data );
        queue.Skip( data.Length - (data.Length / 2) );
        CollectionAssert.AreEqual( data, queue.ToArray( data.Length ) );
    }

    /// <summary>   (Unit Test Method) queue iteration. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    /// <param name="queue">    The queue. </param>
    /// <param name="bytes">    The bytes. </param>
    private static void QueueIteration( ICollection queue, byte[] bytes )
    {
        int i = 0;
        foreach ( byte item in queue )
        {
            Assert.AreEqual( item, bytes[i] );
            i++;
        }
    }

    /// <summary>   (Unit Test Method) queue iteration. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    /// <param name="dummy">    The dummy. </param>
    /// <param name="queue">    The queue. </param>
    /// <param name="bytes">    The bytes. </param>
    private static void QueueIteration( int dummy, ICollection<byte> queue, byte[] bytes )
    {
        int i = dummy == 0 ? dummy : 0;
        foreach ( byte item in queue )
        {
            Assert.AreEqual( item, bytes[i] );
            i++;
        }
    }

    /// <summary>   (Unit Test Method) queue iteration. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    /// <param name="bytes">    The bytes. </param>
    /// <param name="queue">    The queue. </param>
    private static void QueueIteration( byte[] bytes, IEnumerable queue )
    {
        int i = 0;
        foreach ( byte item in queue )
        {
            Assert.AreEqual( item, bytes[i] );
            i++;
        }
    }

    /// <summary>   (Unit Test Method) queue iteration. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    /// <param name="dummy">    The dummy. </param>
    /// <param name="bytes">    The bytes. </param>
    /// <param name="queue">    The queue. </param>
    private static void QueueIteration( int dummy, byte[] bytes, IEnumerable<byte> queue )
    {
        int i = dummy == 0 ? dummy : 0;
        foreach ( byte item in queue )
        {
            Assert.AreEqual( item, bytes[i] );
            i++;
        }
    }

    /// <summary>   (Unit Test Method) queue iteration. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    [TestMethod()]
    public void QueueIteration()
    {
        byte[] data = GenerateRandomBytes( 100 );
        GenericQueue<byte> queue = new( data.Length );
        _ = queue.Enqueue( data );
        int i = 0;
        foreach ( object? item in queue )
        {
            Assert.AreEqual( ( byte ) item, data[i] );
            i++;
        }
        GenericQueueTests.QueueIteration( queue, data );
        GenericQueueTests.QueueIteration( data, queue );
        GenericQueueTests.QueueIteration( 0, queue, data );
        GenericQueueTests.QueueIteration( 0, data, queue );
    }

    #endregion
}
