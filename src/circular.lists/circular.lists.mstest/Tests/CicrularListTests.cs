using System.Collections;
using cc.isr.Std.CircularLists;

namespace cc.isr.Std.CircularLists.Tests;

/// <summary>   (Unit Test Class) a circular list tests. </summary>
/// <remarks>   David, 2020-09-10. </remarks>
[TestClass]
public class CircularListTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    # region " random "

    /// <summary>   The random. </summary>
    private static readonly Random? _randomNumberGenerator = new();

    /// <summary>   Generates a random byte array. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="length">   The length. </param>
    /// <returns>   An array of byte. </returns>
    protected static byte[] GenerateRandomBytes( int length )
    {
        byte[] bytes = new byte[length];
        _randomNumberGenerator?.NextBytes( bytes );
        return bytes;
    }

    #endregion

    #region " test methods "

    /// <summary>   (Unit Test Method) fill list. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    [TestMethod()]
    public void FillList()
    {
        byte[] data = GenerateRandomBytes( 100 );
        CircularList<byte> list = new( data.Length ) {
            data
        };
        CollectionAssert.AreEqual( data, list.ToArray() );
    }

    /// <summary>   (Unit Test Method) overflow list. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    [TestMethod()]
    public void OverflowList()
    {
        byte[] data = GenerateRandomBytes( 100 );
        CircularList<byte> list = new( data.Length ) {
            data
        };
        data = GenerateRandomBytes( 100 );
        list.Add( data );
        CollectionAssert.AreEqual( data, list.ToArray() );
    }

    /// <summary>   (Unit Test Method) list iteration. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    /// <param name="list">     The list. </param>
    /// <param name="bytes">    The bytes. </param>
    private static void ListIteration( ICollection list, byte[] bytes )
    {
        int i = 0;
        foreach ( byte item in list )
        {
            Assert.AreEqual( item, bytes[i] );
            i++;
        }
    }

    /// <summary>   (Unit Test Method) list iteration. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    /// <param name="dummy">    The dummy. </param>
    /// <param name="list">     The list. </param>
    /// <param name="bytes">    The bytes. </param>
    private static void ListIteration( int dummy, IReadOnlyCollection<byte> list, byte[] bytes )
    {
        int i = dummy == 0 ? dummy : 0;
        foreach ( byte item in list )
        {
            Assert.AreEqual( item, bytes[i] );
            i++;
        }
    }

    /// <summary>   (Unit Test Method) list iteration. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    /// <param name="bytes">    The bytes. </param>
    /// <param name="list">     The list. </param>
    private static void ListIteration( byte[] bytes, IEnumerable list )
    {
        int i = 0;
        foreach ( byte item in list )
        {
            Assert.AreEqual( item, bytes[i] );
            i++;
        }
    }

    /// <summary>   (Unit Test Method) list iteration. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    /// <param name="dummy">    The dummy. </param>
    /// <param name="bytes">    The bytes. </param>
    /// <param name="list">     The list. </param>
    private static void ListIteration( int dummy, byte[] bytes, IEnumerable<byte> list )
    {
        int i = dummy == 0 ? dummy : 0;
        foreach ( byte item in list )
        {
            Assert.AreEqual( item, bytes[i] );
            i++;
        }
    }

    /// <summary>   (Unit Test Method) list iteration. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    [TestMethod()]
    public void ListIteration()
    {
        byte[] data = GenerateRandomBytes( 100 );
        CircularList<byte> list = new( data.Length ) {
            data
        };
        int i = 0;
        foreach ( byte item in list )
        {
            Assert.AreEqual( item, data[i] );
            i++;
        }
        CircularListTests.ListIteration( list, data );
        CircularListTests.ListIteration( data, list );
        CircularListTests.ListIteration( 0, list, data );
        CircularListTests.ListIteration( 0, data, list );
    }

    #endregion
}
