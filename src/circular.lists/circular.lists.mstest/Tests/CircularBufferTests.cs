using cc.isr.Std.CircularLists;

namespace cc.isr.Std.CircularLists.Tests;

/// <summary>   (Unit Test Class) a circular buffer tests. </summary>
/// <remarks>   David, 2020-09-09.
/// From: https://archive.CodePlex.com/?p=CircularBuffer </remarks>
/// <license> (c) 2012 Alex REGUEIRO.<para>
/// Licensed under The MIT License. </para><para>
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
/// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
/// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
/// </para> </license>
[TestClass]
public class CircularBufferTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    # region " random "

    /// <summary>   The random. </summary>
    private static readonly Random? _randomNumberGenerator = new();

    /// <summary>   Generates a random data. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    /// <param name="length">   The length. </param>
    /// <returns>   An array of byte. </returns>
    protected static byte[] GenerateRandomData( int length )
    {
        byte[] data = new byte[length];
        _randomNumberGenerator?.NextBytes( data );
        return data;
    }

    #endregion 

    # region " test methods "

    /// <summary>   (Unit Test Method) empty buffer. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    [TestMethod()]
    public void EmptyBuffer()
    {
        byte[] data = GenerateRandomData( 100 );
        CircularBuffer<byte> buffer = new( data.Length );
        _ = buffer.Put( data );
        byte[] ret = new byte[buffer.Size];
        _ = buffer.Get( ret );
        CollectionAssert.AreEqual( data, ret );
        Assert.IsTrue( buffer.Size == 0 );
    }

    /// <summary>   (Unit Test Method) fill buffer. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    [TestMethod()]
    public void FillBuffer()
    {
        byte[] data = GenerateRandomData( 100 );
        CircularBuffer<byte> buffer = new( data.Length );
        _ = buffer.Put( data );
        CollectionAssert.AreEqual( data, buffer.ToArray() );
    }

    /// <summary>   (Unit Test Method) wrap around buffer. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    [TestMethod()]
    public void WrapAroundBuffer()
    {
        byte[] data = GenerateRandomData( 100 );
        CircularBuffer<byte> buffer = new( data.Length, true );
        _ = buffer.Put( GenerateRandomData( data.Length / 2 ) );
        _ = buffer.Put( data );
        buffer.Skip( data.Length - (data.Length / 2) );
        CollectionAssert.AreEqual( data, buffer.ToArray() );
    }

    /// <summary>   (Unit Test Method) overflow buffer. </summary>
    /// <remarks>   David, 2020-09-09. </remarks>
    [TestMethod()]
    public void OverflowBuffer()
    {
        byte[] data = GenerateRandomData( 100 );
        CircularBuffer<byte> buffer = new( data.Length - 1, false );
        _ = Assert.ThrowsException<InvalidOperationException>( () => _ = buffer.Put( data ) );
    }

    #endregion 

}
