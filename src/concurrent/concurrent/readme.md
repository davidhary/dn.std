# About

cc.isr.Std.Concurrent is a .Net library supporting Concurrent operations.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

cc.isr.Std.Concurrent is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Standard Framework Repository].

[Standard Framework Repository]: https://bitbucket.org/davidhary/dn.std

