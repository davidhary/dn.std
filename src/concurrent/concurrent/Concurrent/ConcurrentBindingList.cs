using System.Collections;
using System.ComponentModel;
using System.Diagnostics;

namespace cc.isr.Std.Concurrent;

/// <summary> A thread safe binding list <see cref="BindingList{T}"/> </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-03-06 </para>
/// </remarks>
[DebuggerDisplay( "Count = {Count}" )]
public class ConcurrentBindingList<T> : BindingList<T>, IDisposable
{
    #region " construction and cleanup "

    /// <summary>
    /// Initializes a new instance of the <see cref="BindingList{T}" /> class
    /// using default values.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public ConcurrentBindingList() : base()
    { }

    /// <summary> Gets the is disposed. </summary>
    /// <value> The is disposed. </value>
    protected bool IsDisposed { get; private set; }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="ICollection{T}" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected virtual void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.ItemsLocker?.Dispose();
            }
        }
        catch
        {
        }
        finally
        {
            this.IsDisposed = true;
        }
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    /// resources.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void Dispose()
    {
        // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        this.Dispose( true );
        // uncomment the following line if Finalize() is overridden above.
        GC.SuppressFinalize( this );
    }

    /// <summary>
    /// This destructor will run only if the Dispose method does not get called. It gives the base
    /// class the opportunity to finalize. Do not provide destructors in types derived from this
    /// class.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    ~ConcurrentBindingList()
    {
        // Do not re-create Dispose clean-up code here.
        // Calling Dispose(false) is optimal for readability and maintainability.
        this.Dispose( false );
    }

    #endregion

    #region " thread sync management  "

    /// <summary> Gets the items locker. </summary>
    /// <value> The items locker. </value>
    protected ReaderWriterLockSlim ItemsLocker { get => this._itemsLocker; private set => this._itemsLocker = value; }

    [NonSerialized]
    private ReaderWriterLockSlim _itemsLocker = new();

    [NonSerialized]
    private object? _syncRoot;

    /// <summary> Gets the synchronization root. </summary>
    /// <remarks>
    /// The Sync root helps super classes synchronously access the items. For details see reply by
    /// Roman ZAVALOV in
    /// https://StackOverflow.com/questions/728896/whats-the-use-of-the-SyncRoot-pattern.
    /// </remarks>
    /// <value> The synchronization root. </value>
    public object SyncRoot
    {
        get
        {
            if ( this._syncRoot is null )
            {
                this.ItemsLocker.EnterReadLock();
                try
                {
                    if ( this.Items is ICollection c )
                    {
                        this._syncRoot = c.SyncRoot;
                    }
                    else
                    {
                        _ = Interlocked.CompareExchange<object?>( ref this._syncRoot, new object(), null );
                    }
                }
                finally
                {
                    this.ItemsLocker.ExitReadLock();
                }
            }

            return this._syncRoot;
        }
    }

    #endregion

    #region " check methods  "

    /// <summary>
    /// Returns <c>true</c> if the collection size is fixed or the collection is read only.
    /// </summary>
    /// <value> The size of the is fixed. </value>
    public bool IsFixedSize => this.Items is IList list ? list.IsFixedSize : this.Items.IsReadOnly;

    /// <summary> Gets the is read only. </summary>
    /// <value> The is read only. </value>
    public bool IsReadOnly => this.Items.IsReadOnly;

    /// <summary> Throws an exception if index is out of range. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="index"> Zero-based index of the. </param>
    private void EnsureIndexRange( int index )
    {
        if ( index < 0 || index >= this.Items.Count )
        {
            throw new ArgumentOutOfRangeException( nameof( index ), $"Must be between {0} and {this.Items.Count - 1}" );
        }
    }

    /// <summary> Ensures that not empty. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    private void EnsureNotEmpty()
    {
        if ( !this.Any() )
        {
            throw new NotSupportedException( "Collection is empty" );
        }
    }

    /// <summary> Throws an exception if read only. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    private void EnsureNotReadOnly()
    {
        if ( this.Items.IsReadOnly )
        {
            throw new NotSupportedException( "Collection is read only" );
        }
    }

    /// <summary> Query if 'value' is compatible object. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> <c>true</c> if compatible object; otherwise <c>false</c> </returns>
    public static bool IsCompatibleObject( object value )
    {
        // Non-null values are fine.  Only accept nulls if T is a class or Nullable<U>.
        // Note that default(T) is not equal to null for value types except when T is Nullable<U>. 
        return value is T or null;
    }

    #endregion

    #region " public overloads "

    /// <summary> Gets any. </summary>
    /// <value> any. </value>
    public bool SafeAny
    {
        get
        {
            this.ItemsLocker.EnterReadLock();
            try
            {
                return this.Any();
            }
            finally
            {
                this.ItemsLocker.ExitReadLock();
            }
        }
    }

    /// <summary> Gets the number of elements. </summary>
    /// <value> The count. </value>
    public new int Count
    {
        get
        {
            this.ItemsLocker.EnterReadLock();
            try
            {
                return this.Items.Count;
            }
            finally
            {
                this.ItemsLocker.ExitReadLock();
            }
        }
    }

    /// <summary>
    /// Provides direct access to the base class item to permit synchronization for the super class.
    /// </summary>
    /// <value> The base item. </value>
    protected T GetBaseItem( int index )
    {
        return base[index];
    }

    /// <summary>   Sets base item. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    /// <param name="index">    Zero-based index of the. </param>
    /// <param name="value">    The new value for the item at the specified index. The value can be
    ///                         <see langword="null" /> for reference types. </param>
    protected void SetBaseItem( int index, T value )
    {
        base[index] = value;
    }

    /// <summary>
    /// Indexer to get or set items within this collection using array index syntax.
    /// </summary>
    /// <param name="index">    Zero-based index of the entry to access. </param>
    /// <returns>   The indexed item. </returns>
    public new T this[int index]
    {
        get
        {
            this.ItemsLocker.EnterReadLock();
            try
            {
                this.EnsureIndexRange( index );
                return base[index];
            }
            finally
            {
                this.ItemsLocker.ExitReadLock();
            }
        }
        set
        {
            this.ItemsLocker.EnterWriteLock();
            try
            {
                this.EnsureNotReadOnly();
                this.EnsureIndexRange( index );
                base.SetItem( index, value );
            }
            finally
            {
                this.ItemsLocker.ExitWriteLock();
            }
        }
    }

    /// <summary> Gets or sets the element at the specified index. </summary>
    /// <value> The element at the specified index. </value>
    public T GetItem( int index )
    {
        this.ItemsLocker.EnterReadLock();
        try
        {
            this.EnsureIndexRange( index );
            return base[index];
        }
        finally
        {
            this.ItemsLocker.ExitReadLock();
        }
    }

    /// <summary>   Replaces the item at the specified index with the specified item. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    /// <param name="index">    The zero-based index of the item to replace. </param>
    /// <param name="value">    The new value for the item at the specified index. The value can be
    ///                         <see langword="null" /> for reference types. </param>
    public void ItemSetter( int index, T value )
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            this.EnsureIndexRange( index );
            base.SetItem( index, value );
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary>
    /// Adds an item to the <see cref="ICollection{T}" />
    /// .
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="item"> The object to add to the
    /// <see cref="ICollection{T}" />
    /// . </param>
    public new void Add( T item )
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            base.Add( item );
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary>
    /// Copies the elements of the <see cref="ICollection{T}" />
    /// to an <see cref="Array" />
    /// , starting at a particular <see cref="Array" />
    /// index.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="array"> The one-dimensional <see cref="Array" />
    /// that is the destination of the elements copied from
    /// <see cref="ICollection{T}" />
    /// . The <see cref="Array" />
    /// must have zero-based indexing. </param>
    /// <param name="index"> The zero-based index in <paramref name="array" />
    /// at which copying begins. </param>
    public new void CopyTo( T[] array, int index )
    {
        this.ItemsLocker.EnterReadLock();
        try
        {
            base.CopyTo( array, index );
        }
        finally
        {
            this.ItemsLocker.ExitReadLock();
        }
    }

    /// <summary>
    /// Determines whether the <see cref="ICollection{T}" />
    /// contains a specific value.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="item"> The object to locate in the
    /// <see cref="ICollection{T}" />
    /// . </param>
    /// <returns>
    /// true if <paramref name="item" />
    /// is found in the <see cref="ICollection{T}" />
    /// ; otherwise, false.
    /// </returns>
    public new bool Contains( T item )
    {
        this.ItemsLocker.EnterReadLock();
        try
        {
            return this.Items.Contains( item );
        }
        finally
        {
            this.ItemsLocker.ExitReadLock();
        }
    }

    /// <summary> Returns an enumerator that iterates through the collection. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> An enumerator that can be used to iterate through the collection. </returns>
    public new IEnumerator<T> GetEnumerator()
    {
        this.ItemsLocker.EnterReadLock();
        try
        {
            return this.Items.ToList().GetEnumerator();
        }
        finally
        {
            this.ItemsLocker.ExitReadLock();
        }
    }

    /// <summary>
    /// Determines the index of a specific item in the
    /// <see cref="IList{T}" />
    /// .
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="item"> The object to locate in the
    /// <see cref="IList{T}" />
    /// . </param>
    /// <returns>
    /// The index of <paramref name="item" />
    /// if found in the list; otherwise, -1.
    /// </returns>
    public new int IndexOf( T item )
    {
        this.ItemsLocker.EnterReadLock();
        try
        {
            return this.Items.IndexOf( item );
        }
        finally
        {
            this.ItemsLocker.ExitReadLock();
        }
    }

    #endregion

    #region " protected overrides "

    /// <summary> Removes all items from the collection. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    protected override void ClearItems()
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            base.ClearItems();
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary> Inserts an item into the collection at the specified index. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="index"> The zero-based index at which <paramref name="item" />
    /// should be inserted. </param>
    /// <param name="item">  The object to insert. </param>
    protected override void InsertItem( int index, T item )
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            this.EnsureIndexRange( index );
            base.InsertItem( index, item );
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary> Base remove item. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="index"> Zero-based index of the. </param>
    protected void BaseRemoveItem( int index )
    {
        base.RemoveItem( index );
    }

    /// <summary> Removes the item at the specified index of the collection. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="index"> The zero-based index of the element to remove. </param>
    protected override void RemoveItem( int index )
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            this.EnsureIndexRange( index );
            base.RemoveItem( index );
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary> Replaces the element at the specified index. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="index"> The zero-based index of the element to replace. </param>
    /// <param name="item">  The new value for the element at the specified index. </param>
    protected override void SetItem( int index, T item )
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            this.EnsureIndexRange( index );
            base.SetItem( index, item );
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    #endregion

    #region " queue and stack operations "

    /// <summary> Adds an object onto the end of this queue. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="item"> The item. </param>
    public void Enqueue( T item )
    {
        this.Add( item );
    }

    /// <summary> Removes the head object from this queue. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> The head object from this queue. </returns>
    public T Dequeue()
    {
        this.ItemsLocker.EnterUpgradeableReadLock();
        try
        {
            this.EnsureNotEmpty();
            T? item = this.GetBaseItem( 0 );
            try
            {
                this.ItemsLocker.EnterWriteLock();
                this.BaseRemoveItem( 0 );
                return item;
            }
            finally
            {
                this.ItemsLocker.ExitWriteLock();
            }
        }
        finally
        {
            this.ItemsLocker.ExitUpgradeableReadLock();
        }
    }

    /// <summary> Returns the top-of-stack object without removing it. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> The current top-of-stack object. </returns>
    public T Peek()
    {
        return this.GetItem( 0 );
    }

    /// <summary> Pushes an object onto this stack. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="item"> The object to add to the
    /// <see cref="ICollection{T}" />
    /// . </param>
    public void Push( T item )
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            if ( this.Any() )
            {
                base.InsertItem( 0, item );
            }
            else
            {
                base.Add( item );
            }
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary> Removes and returns the top-of-stack object. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> The previous top-of-stack object. </returns>
    public T Pop()
    {
        return this.Dequeue();
    }

    #endregion

    #region " bulk operations "

    /// <summary>
    /// Removes all items from the <see cref="ICollection{T}" />.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public new void Clear()
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.RaiseListChangedEvents = false;
            this.EnsureNotReadOnly();
            base.ClearItems();
        }
        finally
        {
            this.RaiseListChangedEvents = true;
            this.ResetBindings();
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary> Adds an object onto the end of this queue. </summary>
    /// <remarks> David, 2020-09-05. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="items"> The items. </param>
    public void Enqueue( IList<T> items )
    {
        if ( items is null )
        {
            throw new ArgumentNullException( nameof( items ) );
        }

        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.RaiseListChangedEvents = false;
            this.EnsureNotReadOnly();
            foreach ( T item in items )
            {
                base.Add( item );
            }
        }
        finally
        {
            this.RaiseListChangedEvents = true;
            this.ResetBindings();
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary> Adds items onto the end of this queue. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="items"> The items. </param>
    public void Enqueue( IEnumerable<T> items )
    {
        if ( items is null )
        {
            throw new ArgumentNullException( nameof( items ) );
        }

        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.RaiseListChangedEvents = false;
            this.EnsureNotReadOnly();
            foreach ( T item in items )
            {
                base.Add( item );
            }
        }
        finally
        {
            this.RaiseListChangedEvents = true;
            this.ResetBindings();
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary> Pushes an object onto this stack. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="items"> The items. </param>
    public void Push( IList<T> items )
    {
        if ( items is null )
        {
            throw new ArgumentNullException( nameof( items ) );
        }

        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.RaiseListChangedEvents = false;
            this.EnsureNotReadOnly();
            foreach ( T item in items )
            {
                if ( this.Any() )
                {
                    base.Add( item );
                }
                else
                {
                    base.InsertItem( 0, item );
                }
            }
        }
        finally
        {
            this.RaiseListChangedEvents = true;
            this.ResetBindings();
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary> Pushes an object onto this stack. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="items"> The items. </param>
    public void Push( IEnumerable<T> items )
    {
        if ( items is null )
        {
            throw new ArgumentNullException( nameof( items ) );
        }

        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.RaiseListChangedEvents = false;
            this.EnsureNotReadOnly();
            foreach ( T item in items )
            {
                if ( this.Any() )
                {
                    base.Add( item );
                }
                else
                {
                    base.InsertItem( 0, item );
                }
            }
        }
        finally
        {
            this.RaiseListChangedEvents = true;
            this.ResetBindings();
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary> Removes the head objects from this queue. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="count"> The count. </param>
    /// <returns> The head object from this queue. </returns>
    public IList<T> Dequeue( int count )
    {
        this.ItemsLocker.EnterUpgradeableReadLock();
        List<T> l = [];
        try
        {
            this.EnsureNotEmpty();
            try
            {
                this.ItemsLocker.EnterWriteLock();
                this.RaiseListChangedEvents = false;
                while ( this.Any() && l.Count < count )
                {
                    l.Add( this.GetBaseItem( 0 ) );
                    this.BaseRemoveItem( 0 );
                }
            }
            finally
            {
                this.RaiseListChangedEvents = true;
                this.ResetBindings();
                this.ItemsLocker.ExitWriteLock();
            }
        }
        finally
        {
            this.ItemsLocker.ExitUpgradeableReadLock();
        }

        return l;
    }

    /// <summary> Removes and returns the top-of-stack objects. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="count"> The count. </param>
    /// <returns> The previous top-of-stack object. </returns>
    public IList<T> Pop( int count )
    {
        return this.Dequeue( count );
    }

    /// <summary> Removes items from the head of the queue (top of the tack). </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="count"> The count. </param>
    /// <returns> The number of removed items. </returns>
    public int TrimStart( int count )
    {
        this.ItemsLocker.EnterUpgradeableReadLock();
        int result = 0;
        try
        {
            if ( this.Any() )
            {
                try
                {
                    this.ItemsLocker.EnterWriteLock();
                    this.RaiseListChangedEvents = false;
                    while ( this.Any() && result < count )
                    {
                        this.BaseRemoveItem( 0 );
                        result += 1;
                    }
                }
                finally
                {
                    this.RaiseListChangedEvents = true;
                    this.ResetBindings();
                    this.ItemsLocker.ExitWriteLock();
                }
            }
        }
        finally
        {
            this.ItemsLocker.ExitUpgradeableReadLock();
        }

        return result;
    }

    /// <summary> Removes items from the tail of the queue (bottom of the tack). </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="count"> The count. </param>
    /// <returns> The number of removed items. </returns>
    public int TrimEnd( int count )
    {
        this.ItemsLocker.EnterUpgradeableReadLock();
        int result = 0;
        try
        {
            if ( this.Any() )
            {
                try
                {
                    this.ItemsLocker.EnterWriteLock();
                    this.RaiseListChangedEvents = false;
                    while ( this.Any() && result < count )
                    {
                        this.BaseRemoveItem( this.Items.Count - 1 );
                        result += 1;
                    }
                }
                finally
                {
                    this.RaiseListChangedEvents = true;
                    this.ResetBindings();
                    this.ItemsLocker.ExitWriteLock();
                }
            }
        }
        finally
        {
            this.ItemsLocker.ExitUpgradeableReadLock();
        }

        return result;
    }

    #endregion

    #region " thread safe event handlers "

    /// <summary> Gets the context. </summary>
    /// <value> The context. </value>
    private static SynchronizationContext Ctx => SynchronizationContext.Current;

    /// <summary>
    /// Raises the <see cref="System.ComponentModel.BindingList{T}.AddingNew" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="e"> An <see cref="AddingNewEventArgs" /> that contains
    /// the event data. </param>
    protected override void OnAddingNew( AddingNewEventArgs e )
    {
        if ( Ctx is null )
        {
            this.BaseAddingNew( e );
        }
        else
        {
            Ctx.Send( ( object state ) => this.BaseAddingNew( e ), null );
        }
    }

    /// <summary> Base adding new. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="e"> Adding new event information. </param>
    private void BaseAddingNew( AddingNewEventArgs e )
    {
        base.OnAddingNew( e );
    }

    /// <summary>
    /// Raises the <see cref="System.ComponentModel.BindingList{T}.ListChanged" /> event.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="e"> A <see cref="ListChangedEventArgs" /> that contains
    /// the event data. </param>
    protected override void OnListChanged( ListChangedEventArgs e )
    {
        if ( Ctx is null )
        {
            this.BaseListChanged( e );
        }
        else
        {
            Ctx.Send( ( object state ) => this.BaseListChanged( e ), null );
        }
    }

    /// <summary> Base list changed. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="e"> List changed event information. </param>
    private void BaseListChanged( ListChangedEventArgs e )
    {
        base.OnListChanged( e );
    }

    #endregion
}
