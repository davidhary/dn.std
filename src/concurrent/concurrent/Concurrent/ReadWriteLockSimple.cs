namespace cc.isr.Std.Concurrent;

/// <summary>   A read write lock simple. This class cannot be inherited. </summary>
/// <remarks>
/// https://www.CodeProject.com/Tips/5323262/The-Simplest-Implementation-of-a-Reader-Writer-Loc
/// The standard <see cref="Monitor.Wait(object)"/> and
/// <see cref="Monitor.PulseAll(object)"/> methods
/// are rarely used today, but still could provide powerful capabilities for thread
/// synchronization. Technically, this code splits incoming requests into two large groups of
/// writers and readers, with the former taking precedence. Therefore, we need an extra call to
/// Monitor.Enter(syncWrite) to ensure that only one writer can access at a given time.
/// </remarks>
public sealed class ReadWriteLockSimple
{
    private readonly object _syncRoot = new();
    private readonly object _syncWrite = new();

    /// <summary>   The number of active readers. </summary>
    private int _readersCount = 0;

    /// <summary>   A total number of pending and active writers. </summary>
    private int _writersCount = 0;

    /// <summary>   Enter read lock. </summary>
    /// <remarks>
    /// The <see cref="EnterReadLock"/> method allows the reader to continue only when the resource is not being
    /// accessed by writers. 
    /// </remarks>
    public void EnterReadLock()
    {
        lock ( this._syncRoot )
        {
            while ( this._writersCount > 0 )
            {
                _ = Monitor.Wait( this._syncRoot ); // Wait till all writers are done.
            }

            this._readersCount++; // Notify there is an active reader.
        }
    }

    /// <summary>   Exit read lock. </summary>
    /// <remarks>
    /// The last reader or writer in a row just wakes up all waiting threads so they can continue the
    /// race.
    /// </remarks>
    public void ExitReadLock()
    {
        lock ( this._syncRoot )
        {
            if ( --this._readersCount == 0 && this._writersCount > 0 )
            {
                Monitor.PulseAll( this._syncRoot ); // Notify writers waiting.
            }
        }
    }

    /// <summary>   Enter write lock. </summary>
    /// <remarks>   
    /// The <see cref="EnterWriteLock"/> immediately notifies the presence of
    /// the writer, and then waits until readers release the lock.
    ///  </remarks>
    public void EnterWriteLock()
    {
        lock ( this._syncRoot )
        {
            this._writersCount++; // Notify that there is a pending writer.

            while ( this._readersCount > 0 )
            {
                _ = Monitor.Wait( this._syncRoot ); // Wait till all readers are done.
            }
        }

        Monitor.Enter( this._syncWrite );
    }

    /// <summary>   Exit write lock. </summary>
    /// <remarks>
    /// The last writer in a row wakes up all waiting threads so they can continue the
    /// race.
    /// </remarks>
    public void ExitWriteLock()
    {
        Monitor.Exit( this._syncWrite );

        lock ( this._syncRoot )
        {
            if ( --this._writersCount == 0 )
            {
                Monitor.PulseAll( this._syncRoot ); // Notify readers waiting.
            }
        }
    }
}
