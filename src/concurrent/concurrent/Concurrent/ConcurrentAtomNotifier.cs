using System.ComponentModel;

namespace cc.isr.Std.Concurrent;
/// <summary>
/// Implements a thread safe notifier based on the
/// <see cref="Synchronizer{TImpl, TIRead, TIWrite}"/> class.
/// </summary>
/// <remarks>
/// <para>
/// Regardless of how many threads are accessing the property, no Write method will be invoked as
/// long as another Read or Write method is being executed. However, multiple Read methods can be
/// invoked simultaneously, without having to scatter your code with multiple try/catch/finally
/// statements, or repeating the same code over and over.
/// </para><para>
/// For the record, consuming it with a simple string is meaningless, because System.String is
/// immutable.
/// </para><para>
/// The basic idea Is that all methods that can modify the state Of your instance must be added
/// To the IWriteToShared Interface. At the same time, all methods that only read from your
/// instance should be added To the IReadFromShared Interface. By separating your concerns Like
/// this into two distinct interfaces, And implementing both interfaces On your underlying type,
/// you can Then use the Synchronizer Class To synchronize access To your instance. Just Like
/// that, the art Of synchronizing access To your code becomes much simpler, And you can Do it
/// For the most part In a much more declarative manner.
/// </para><para>
/// When it comes to multi-threaded programming, syntactic sugar might be the difference between
/// success And failure. Debugging multi-threaded code Is often extremely difficult, And creating
/// unit tests for synchronization objects can be an exercise in futility.
/// </para><para>
/// If you want, you can create an overloaded type With only one generic argument, inheriting
/// from the original Synchronizer Class And passing On its Single generic argument As the type
/// argument three times To its base Class. Doing this, you won't need the read or write
/// interfaces, since you can simply use the concrete implementation of your type. However, this
/// approach requires that you manually take care of those parts that need to use either the
/// Write or Read method. It’s also slightly less safe, but does allow you to wrap classes you
/// cannot change into a Synchronizer instance.</para> <para>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-02-03 </para>
/// </remarks>
public class ConcurrentAtomNotifier<T> : IDisposable where T : IComparable<T>, IEquatable<T>, INotifyPropertyChanged
{
    #region " construction and cleanup "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public ConcurrentAtomNotifier() : base()
    { }

    /// <summary> Gets or sets the is disposed. </summary>
    /// <value> The is disposed. </value>
    protected bool IsDisposed { get; private set; }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    /// resources.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="disposing"> True to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected virtual void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this._sync?.Dispose();
            }
        }
        finally
        {
            this.IsDisposed = true;
        }
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    /// resources.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void Dispose()
    {
        // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        this.Dispose( true );
        // uncomment the following line if Finalize() is overridden above.
        GC.SuppressFinalize( this );
    }

    /// <summary>
    /// This destructor will run only if the Dispose method does not get called. It gives the base
    /// class the opportunity to finalize. Do not provide destructors in types derived from this
    /// class.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    ~ConcurrentAtomNotifier()
    {
        // Do not re-create Dispose clean-up code here.
        // Calling Dispose(false) is optimal for readability and maintainability.
        this.Dispose( false );
    }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="TValue">   Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<TValue>( ref TValue backingField, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<TValue>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="TValue">   Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( ref TValue prop, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        if ( EqualityComparer<TValue>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TValue">   Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( TValue oldValue, TValue newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif

        if ( EqualityComparer<TValue>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion

    #region " value "

    /// <summary> The value. </summary>
    private T? _value;

    /// <summary> Gets or sets the value. </summary>
    /// <value> The value. </value>
    public T Value
    {
        get
        {
            this._sync.Read( x => this._value = x.GetValue() );
            return this._value!;
        }

        set => _ = this.SetProperty( this._value, value, () => this._sync.Write( x => x.SetValue( value ) ) );
    }

    #endregion

    #region " synchronizer "

    /// <summary> The synchronize. </summary>
    private readonly Synchronizer<MySharedClass, IReadFromShared, IWriteToShared> _sync = new( new MySharedClass() );

    /// <summary> Interface for read from shared. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    private interface IReadFromShared
    {
        /// <summary> Gets the value. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The value. </returns>
        T? GetValue();
    }

    /// <summary> Interface for write to shared. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    internal interface IWriteToShared
    {
        /// <summary> Sets a value. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        void SetValue( T value );
    }

    /// <summary> my shared class. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    private class MySharedClass : IReadFromShared, IWriteToShared
    {
        /// <summary> The foo. </summary>
        private T? _foo;

        /// <summary> Gets the value. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The value. </returns>
        public T? GetValue()
        {
            return this._foo;
        }

        /// <summary> Sets a value. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        public void SetValue( T value )
        {
            this._foo = value;
        }
    }

    #endregion
}
