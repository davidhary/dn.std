namespace cc.isr.Std.ObservableLists;

/// <summary> A thread safe monitor. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-07-29 </para>
/// </remarks>
public sealed class ThreadSafeMonitor : IDisposable
{
    /// <summary> Number of busies. </summary>
    private int _busyCount;

    /// <summary> Returns True if busy. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <returns> <c>true</c> if busy; otherwise <c>false</c> </returns>
    public bool Busy()
    {
        return this._busyCount > 0;
    }

    /// <summary> Increments the busy count. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void Enter()
    {
        _ = System.Threading.Interlocked.Increment( ref this._busyCount );
    }

    /// <summary>
    /// Increments the busy count and return an instance of the <see cref="ThreadSafeMonitor"/>.
    /// </summary>
    /// <remarks>
    /// Use the following code to block re-entry
    /// <code>
    /// Private Monitor as New ThreadSafeMonitor
    /// Private Sub OnCollectionChanged(e As NotifyCollectionChangedEventArgs)
    /// Dim handler As NotifyCollectionChangedEventHandler = Me.CollectionChangedEvent
    /// If handler IsNot Nothing Then
    /// Using Me.Monitor.SyncMonitor()
    /// Me.Context.Send(Sub(state) handler(Me, e), Nothing)
    /// End Using
    /// End If
    /// End Sub
    /// </code>
    /// </remarks>
    /// <returns> An IDisposable. </returns>
    public IDisposable SyncMonitor()
    {
        _ = System.Threading.Interlocked.Increment( ref this._busyCount );
        return this;
    }

    /// <summary> Gets or sets the sentinel to detect redundant calls. </summary>
    /// <value> The sentinel to detect redundant calls. </value>
    private bool IsDisposed { get; set; }

    /// <summary>
    /// Releases the unmanaged resources used by the cc.isr.Std.Models.ThreadSafeToken{T} and
    /// optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="disposing"> True to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    private void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                _ = System.Threading.Interlocked.Decrement( ref this._busyCount );
            }
        }
        finally
        {
            this.IsDisposed = true;
        }
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    /// resources.
    /// </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public void Dispose()
    {
        // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        this.Dispose( true );
    }
}
