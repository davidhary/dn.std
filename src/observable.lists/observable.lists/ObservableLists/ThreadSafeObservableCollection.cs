using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;

namespace cc.isr.Std.ObservableLists;
/// <summary>
/// A thread safe observable collection based upon
/// <see cref="System.Collections.ObjectModel.ObservableCollection{T}"/>
/// </summary>
/// <remarks>
/// David, 2016-12-13. <para>
/// TO_DO: Modify based on Observable Keyed Collection. </para>
/// </remarks>
[DebuggerDisplay( "Count = {Count}" )]
public partial class ThreadSafeObservableCollection<T> : System.Collections.ObjectModel.ObservableCollection<T>, IDisposable
{
    #region " construction and cleanup "

    /// <summary>
    /// Initializes a new instance of the
    /// <see cref="System.Collections.ObjectModel.ObservableCollection{T}" /> class.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public ThreadSafeObservableCollection() : base()
    { }

    /// <summary>
    /// Initializes a new instance of the
    /// <see cref="System.Collections.ObjectModel.ObservableCollection{T}" /> class that contains
    /// elements copied from the specified collection.
    /// </summary>
    /// <remarks> David, 2020-09-05. </remarks>
    /// <param name="items"> The collection from which the elements are copied. </param>
    public ThreadSafeObservableCollection( IEnumerable<T> items ) : base( items )
    { }

    /// <summary> Gets the is disposed. </summary>
    /// <value> The is disposed. </value>
    protected bool IsDisposed { get; set; }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    /// resources.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public void Dispose()
    {
        this.Dispose( true );
        GC.SuppressFinalize( this );
    }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="ICollection{T}" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="disposing"> true to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected virtual void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.ItemsLocker?.Dispose();
            }
        }
        catch
        {
        }
        finally
        {
            this.IsDisposed = true;
        }
    }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        base.OnPropertyChanged( new PropertyChangedEventArgs( propertyName ) );
    }

    #endregion

    #region " collection change notifications "

    /// <summary> Event queue for all listeners interested in Collection Changed events. </summary>
    public new event NotifyCollectionChangedEventHandler? CollectionChanged;

    /// <summary>
    /// Synchronously notifies collection change. Unsafe for cross threading.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="e"> Collection Changed event information. </param>
    protected virtual void NotifyCollectionChanged( NotifyCollectionChangedEventArgs e )
    {
        using ( this.BlockReentrancy() )
        {
            this.CollectionChanged?.Invoke( this, e );
        }
    }

    /// <summary>
    /// Raises the
    /// <see cref="System.Collections.ObjectModel.ObservableCollection{T}.CollectionChanged" />
    /// event with the provided arguments.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="e"> Arguments of the event being raised. </param>
    protected override void OnCollectionChanged( NotifyCollectionChangedEventArgs e )
    {
        this.NotifyCollectionChanged( e );
    }

    #endregion

    #region " thread sync management  "

    /// <summary> Gets the items locker. </summary>
    /// <value> The items locker. </value>
    protected ReaderWriterLockSlim ItemsLocker { get; private set; } = new ReaderWriterLockSlim();

    /// <summary> The synchronize root. </summary>
    [NonSerialized]
    private object _syncRoot = default!;

    /// <summary> Gets the synchronization root. </summary>
    /// <remarks>
    /// The Sync root helps super classes synchronously access the items. For details see reply by
    /// Roman ZAVALOV in
    /// https://StackOverflow.com/questions/728896/WHATS-the-use-of-the-SyncRoot-pattern.
    /// </remarks>
    /// <value> The synchronization root. </value>
    public object SyncRoot
    {
        get
        {
            if ( this._syncRoot is null )
            {
                this.ItemsLocker.EnterReadLock();
                try
                {
                    if ( this.Items is ICollection c )
                    {
                        this._syncRoot = c.SyncRoot;
                    }
                    else
                    {
                        _ = Interlocked.CompareExchange<object>( ref this._syncRoot!, new object(), default! );
                    }
                }
                finally
                {
                    this.ItemsLocker.ExitReadLock();
                }
            }

            return this._syncRoot;
        }
    }

    #endregion

    #region " check methods  "

    /// <summary> Gets the size of the is fixed. </summary>
    /// <value> The size of the is fixed. </value>
    public bool IsFixedSize => this.Items is IList list ? list.IsFixedSize : this.Items.IsReadOnly;

    /// <summary> Gets the is read only. </summary>
    /// <value> The is read only. </value>
    public bool IsReadOnly => this.Items.IsReadOnly;

    /// <summary> Throws an exception if index is out of range. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    /// the required range. </exception>
    /// <param name="index"> Zero-based index of the. </param>
    private void EnsureIndexRange( int index )
    {
        if ( index < 0 || index >= this.Items.Count )
        {
            throw new ArgumentOutOfRangeException( nameof( index ), FormattableString.Invariant( $"Must be between {0} and {this.Items.Count}" ) );
        }
    }

    /// <summary> Throws an exception if read only. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    private void EnsureNotReadOnly()
    {
        if ( this.Items.IsReadOnly )
        {
            throw new NotSupportedException( "Collection is read only" );
        }
    }

    /// <summary> Query if 'value' is compatible object. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> <c>true</c> if compatible object; otherwise <c>false</c> </returns>
    public static bool IsCompatibleObject( object value )
    {
        // Non-null values are fine.  Only accept nulls if T is a class or Nullable<U>.
        // Note that default(T) is not equal to null for value types except when T is Nullable<U>. 
        return value is T or null;
    }

    #endregion

    #region " public overloads "

    /// <summary> Gets the number of elements. </summary>
    /// <value> The count. </value>
    public new int Count
    {
        get
        {
            this.ItemsLocker.EnterReadLock();
            try
            {
                return this.Items.Count;
            }
            finally
            {
                this.ItemsLocker.ExitReadLock();
            }
        }
    }

    /// <summary>
    /// Provides direct access to the base class item to permit synchronization for the super class.
    /// </summary>
    /// <value> The base item. </value>
    protected T GetBaseItem( int index )
    {
        return base[index];
    }

    /// <summary>   Sets base item. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    /// <param name="index">    Zero-based index of the. </param>
    /// <param name="value">    The value. </param>
    protected void SetBaseItem( int index, T value )
    {
        base[index] = value;
    }

    /// <summary>
    /// Indexer to get or set items within this collection using array index syntax.
    /// </summary>
    /// <param name="index">    Zero-based index of the entry to access. </param>
    /// <returns>   The indexed item. </returns>
    public new T this[int index]
    {
        get => this.GetItemThis( index );
        set => this.SetItemThis( index, value );
    }

    /// <summary> Gets or sets the element at the specified index. </summary>
    /// <value> The element at the specified index. </value>
    protected T GetItemThis( int index )
    {
        this.ItemsLocker.EnterReadLock();
        try
        {
            this.EnsureIndexRange( index );
            return base[index];
        }
        finally
        {
            this.ItemsLocker.ExitReadLock();
        }
    }

    private void SetItemThis( int index, T value )
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            this.EnsureIndexRange( index );
            base[index] = value;
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary>
    /// Adds an item to the <see cref="ICollection{T}" />
    /// .
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="item"> The object to add to the
    /// <see cref="ICollection{T}" />
    /// . </param>
    public new void Add( T item )
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            base.Add( item );
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary>
    /// Copies the elements of the <see cref="ICollection{T}" />
    /// to an <see cref="Array" />
    /// , starting at a particular <see cref="Array" />
    /// index.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="array"> The one-dimensional <see cref="Array" />
    /// that is the destination of the elements copied from
    /// <see cref="ICollection{T}" />
    /// . The <see cref="Array" />
    /// must have zero-based indexing. </param>
    /// <param name="index"> The zero-based index in <paramref name="array" />
    /// at which copying begins. </param>
    public new void CopyTo( T[] array, int index )
    {
        this.ItemsLocker.EnterReadLock();
        try
        {
            base.CopyTo( array, index );
        }
        finally
        {
            this.ItemsLocker.ExitReadLock();
        }
    }

    /// <summary>
    /// Determines whether the <see cref="ICollection{T}" />
    /// contains a specific value.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="item"> The object to locate in the
    /// <see cref="ICollection{T}" />
    /// . </param>
    /// <returns>
    /// true if <paramref name="item" />
    /// is found in the <see cref="ICollection{T}" />
    /// ; otherwise, false.
    /// </returns>
    public new bool Contains( T item )
    {
        this.ItemsLocker.EnterReadLock();
        try
        {
            return this.Items.Contains( item );
        }
        finally
        {
            this.ItemsLocker.ExitReadLock();
        }
    }

    /// <summary> Returns an enumerator that iterates through the collection. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <returns> An enumerator that can be used to iterate through the collection. </returns>
    public new IEnumerator<T> GetEnumerator()
    {
        this.ItemsLocker.EnterReadLock();
        try
        {
            return this.Items.ToList().GetEnumerator();
        }
        finally
        {
            this.ItemsLocker.ExitReadLock();
        }
    }

    /// <summary>
    /// Determines the index of a specific item in the
    /// <see cref="IList{T}" />
    /// .
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="item"> The object to locate in the
    /// <see cref="IList{T}" />
    /// . </param>
    /// <returns>
    /// The index of <paramref name="item" />
    /// if found in the list; otherwise, -1.
    /// </returns>
    public new int IndexOf( T item )
    {
        this.ItemsLocker.EnterReadLock();
        try
        {
            return this.Items.IndexOf( item );
        }
        finally
        {
            this.ItemsLocker.ExitReadLock();
        }
    }

    #endregion

    #region " protected overrides "

    /// <summary> Removes all items from the collection. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    protected override void ClearItems()
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            base.ClearItems();
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary> Inserts an item into the collection at the specified index. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="index"> The zero-based index at which <paramref name="item" />
    /// should be inserted. </param>
    /// <param name="item">  The object to insert. </param>
    protected override void InsertItem( int index, T item )
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            this.EnsureIndexRange( index );
            base.InsertItem( index, item );
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary> Base remove item. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="index"> Zero-based index of the. </param>
    protected void BaseRemoveItem( int index )
    {
        base.RemoveItem( index );
    }

    /// <summary> Removes the item at the specified index of the collection. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="index"> The zero-based index of the element to remove. </param>
    protected override void RemoveItem( int index )
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            this.EnsureIndexRange( index );
            base.RemoveItem( index );
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    /// <summary> Replaces the element at the specified index. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="index"> The zero-based index of the element to replace. </param>
    /// <param name="item">  The new value for the element at the specified index. </param>
    protected override void SetItem( int index, T item )
    {
        this.ItemsLocker.EnterWriteLock();
        try
        {
            this.EnsureNotReadOnly();
            this.EnsureIndexRange( index );
            base.SetItem( index, item );
        }
        finally
        {
            this.ItemsLocker.ExitWriteLock();
        }
    }

    #endregion
}
