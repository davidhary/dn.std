using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;

namespace cc.isr.Std.ObservableLists;

/// <summary> Implements a synchronized read only observable keyed collection. </summary>
/// <remarks>
/// David, 2017-05-02.  <para>
/// (c) 2011 MULJADI BUDIMAN. All rights reserved. </para><para>
/// http://geekswithblogs.net/NewThingsILearned/archive/2010/01/12/make-keyedcollectionlttkey-titemgt-to-work-properly-with-wpf-data-binding.aspx
/// </para>
/// </remarks>
[DebuggerDisplay( "Count = {Count}" )]
public class SynchronizedReadOnlyObservableCollection<T> : ReadOnlyCollection<T>, INotifyCollectionChanged, INotifyPropertyChanged
{
    #region " constrction and cleanup "

    /// <summary> Constructor. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="list"> The list. </param>
    public SynchronizedReadOnlyObservableCollection( SynchronizedObservableCollection<T> list ) : base( list )
    {
        (( INotifyCollectionChanged ) this.Items).CollectionChanged += this.HandleCollectionChanged;
        (( INotifyPropertyChanged ) this.Items).PropertyChanged += this.HandlePropertyChanged;
    }
    #endregion

    #region " public events "

    /// <summary> Event queue for all listeners interested in CollectionChanged events. </summary>
    public event NotifyCollectionChangedEventHandler? CollectionChanged;

    /// <summary>
    /// Notifies collection change event. Not thread safe.
    /// </summary>
    /// <param name="e"> Collection Changed event information. </param>
    protected virtual void NotifyCollectionChanged( NotifyCollectionChangedEventArgs e )
    {
        this.CollectionChanged?.Invoke( this, e );
    }

    /// <summary>   Raises the collection changed event. </summary>
    /// <remarks>   David, 2021-08-16. </remarks>
    /// <param name="e">    Event information to send to registered event handlers. </param>
    protected void OnCollectionChanged( NotifyCollectionChangedEventArgs e )
    {
        this.NotifyCollectionChanged( e );
    }

    /// <summary> Handles the collection changed. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Notify collection changed event information. </param>
    private void HandleCollectionChanged( object? sender, NotifyCollectionChangedEventArgs e )
    {
        this.OnCollectionChanged( e );
    }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary> Handles the property changed event. </summary>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Property changed event information. </param>
    private void HandlePropertyChanged( object? sender, PropertyChangedEventArgs e )
    {
        if ( sender is not null && e is not null )
            PropertyChanged?.Invoke( this, e );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="TProperty">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<TProperty>( ref TProperty backingField, TProperty value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<TProperty>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged1( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="TProperty">   Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TProperty>( ref TProperty prop, TProperty value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        if ( EqualityComparer<TProperty>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TProperty">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TProperty>( TProperty oldValue, TProperty newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif

        if ( EqualityComparer<TProperty>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion
}
