namespace cc.isr.Std.MovingFilters.Tests;

/// <summary> Moving average tests. </summary>
/// <remarks>
/// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-01-27 </para>
/// </remarks>
[TestClass]
public class MovingAverageTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    /// <summary>   Evaluate statistics. </summary>
    /// <remarks>   David, 2021-03-20. </remarks>
    /// <param name="values">   The values. </param>
    /// <returns>   A Tuple. </returns>
    private static (double Mean, double Minimum, double Maximum) EvaluateStats( IEnumerable<double> values )
    {
        double min = double.MaxValue;
        double max = double.MinValue;
        double sum = 0;
        foreach ( double value in values )
        {
            if ( value < min )
                min = value;
            if ( value > max )
                max = value;
            sum += value;
        }
        return (sum / values.Count(), min, max);
    }

    /// <summary> The default window length. </summary>
    private const int DEFAULT_WINDOW_LENGTH = 5;

    /// <summary>   Assert moving average. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    /// <param name="count">            Number of. </param>
    /// <param name="movingAverage">    The moving average. </param>
    private static void AssertMovingAverage( int count, MovingAverage movingAverage )
    {
        (double mean, double minimum, double maximum) = EvaluateStats( movingAverage.GetValues() );
        if ( minimum != movingAverage.Minimum )
            Assert.AreEqual( minimum, movingAverage.Minimum, $"Minimum after adding {count} values" );
        if ( maximum != movingAverage.Maximum )
            Assert.AreEqual( maximum, movingAverage.Maximum, $"Maximum after adding {count} values" );
        Assert.AreEqual( mean, movingAverage.Mean, 10e-15, $"Mean after adding {count} values" );
    }

    /// <summary> Adds a value. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="rng">           The random number generator. </param>
    /// <param name="queue">         The queue. </param>
    /// <param name="movingAverage"> The moving average. </param>
    private static void AddValue( Random rng, Queue<double> queue, MovingAverage movingAverage )
    {
        movingAverage.AddValue( rng.NextDouble() );
        queue.Enqueue( movingAverage.LastAveragedReading );
        while ( queue.Count > movingAverage.Length )
            _ = queue.Dequeue();
    }

    /// <summary>   A TimeSpan extension method that starts wait task. </summary>
    /// <remarks>   David, 2021-02-28. </remarks>
    /// <param name="delayTime">    The delayTime to act on. </param>
    /// <returns>   A TimeSpan. </returns>
    private static Task<TimeSpan> StartWaitTask( TimeSpan delayTime )
    {
        static TimeSpan stopWatchWait( TimeSpan delay )
        {
            Stopwatch sw = Stopwatch.StartNew();
            while ( sw.Elapsed < delay )
            {
            }
            return sw.Elapsed;
        }
        return Task<TimeSpan>.Factory.StartNew( () => stopWatchWait( delayTime ) );
    }

    /// <summary> (Unit Test Method) moving average test multiple times. </summary>
    /// <remarks>
    /// Added because the moving average failed updating the range correctly when a adding a new
    /// value.
    /// </remarks>
    [TestMethod()]
    public void MovingAverageTestMultipleTimes()
    {
        for ( int i = 1; i <= 100; i++ )
        {
            this.MovingAverageTest();
            StartWaitTask( TimeSpan.FromMilliseconds( 1 ) ).Wait();
        }
    }

    /// <summary> (Unit Test Method) tests moving average. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void MovingAverageTest()
    {
        MovingAverage movingAverage = new( DEFAULT_WINDOW_LENGTH );
        Random rng = new( DateTimeOffset.Now.Second );
        Queue<double> queue = new();
        int count = 0;
        count += 1;
        AddValue( rng, queue, movingAverage );
        AssertMovingAverage( count, movingAverage );

        count += 1;
        AddValue( rng, queue, movingAverage );
        AssertMovingAverage( count, movingAverage );

        count += 1;
        AddValue( rng, queue, movingAverage );
        AssertMovingAverage( count, movingAverage );

        count += 1;
        AddValue( rng, queue, movingAverage );
        AssertMovingAverage( count, movingAverage );

        count += 1;
        AddValue( rng, queue, movingAverage );
        AssertMovingAverage( count, movingAverage );

        count += 1;
        AddValue( rng, queue, movingAverage );
        AssertMovingAverage( count, movingAverage );
    }
}
