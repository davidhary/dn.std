namespace cc.isr.Std.MovingFilters.Tests;

/// <summary> A moving window test. </summary>
/// <remarks>
/// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-01-27 </para>
/// </remarks>
[TestClass]
public class MovingWindowTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    /// <summary> The default window length. </summary>
    private const int DEFALUT_WINDOW_LENGTH = 5;

    /// <summary> A test for Moving Window. </summary>
    /// <remarks> Passed 2016-01-27. </remarks>
    [TestMethod()]
    public void MovingWindowTest()
    {
        MovingWindow mw = new( DEFALUT_WINDOW_LENGTH )
        {
            RelativeWindow = new FilterWindow<double>( -0.05d, 0.05d ),
            ConformityRange = new FilterWindow<double>( 0d, 10d ),
            OverflowRange = new FilterWindow<double>( -100, 100d ),
            MaximumConsecutiveFailureCount = 3,
            MaximumFailureCount = 10,
            UpdateRule = MovingWindowUpdateRule.StopOnWithinWindow
        };
        mw.AddValue( 0.9d );
        mw.AddValue( 1.1d );
        mw.AddValue( 0.95d );
        mw.AddValue( 1.05d );
        mw.AddValue( 1.0d );
        int actualCount = mw.Count;
        int expectedCount = 5;
        Assert.AreEqual( expectedCount, actualCount );
        double actualMean = mw.Mean;
        double expectedMean = 1d;
        Assert.AreEqual( expectedMean, actualMean, "Mean after adding the initial values" );
        double actualMax = mw.Maximum;
        double expectedMax = 1.1d;
        Assert.AreEqual( expectedMax, actualMax, "Maximum after adding the initial values" );
        double actualMin = mw.Minimum;
        double expectedMin = 0.9d;
        Assert.AreEqual( expectedMin, actualMin, "Minimum after adding the initial values" );

        // adding a value within the window, is not added to the moving window 
        mw.AddValue( 1.01d );
        MovingWindowStatus actualOutcome = mw.Status;
        MovingWindowStatus expectedOutcome = MovingWindowStatus.Within;
        Assert.AreEqual( expectedOutcome, actualOutcome );

        // the moving window can stop because it is full and the new value falls within the window.
        bool actualStopStatus = mw.IsStopStatus();
        bool expectedStopStatus = true;
        Assert.AreEqual( expectedStopStatus, actualStopStatus, $"Expected stop status after {MovingWindowStatus.Within}" );

        // adding a value within the window, is not added to the moving window: the mean states unaffected.
        actualMean = mw.Mean;
        expectedMean = 1d;
        Assert.AreEqual( expectedMean, actualMean, "Mean after adding a new value within the window" );
        mw.AddValue( 1.2d );
        actualOutcome = mw.Status;
        expectedOutcome = MovingWindowStatus.Above;
        Assert.AreEqual( expectedOutcome, actualOutcome, $"Expected status after {MovingWindowStatus.Above} value" );
        actualStopStatus = mw.IsStopStatus();
        expectedStopStatus = false;
        Assert.AreEqual( expectedStopStatus, actualStopStatus, $"Expected stop status after {MovingWindowStatus.Above}" );
        actualMean = mw.Mean;
        expectedMean = 1.06d;
        Assert.AreEqual( expectedMean, actualMean, "Mean after adding a large value" );
        actualMax = mw.Maximum;
        expectedMax = 1.2d;
        Assert.AreEqual( expectedMax, actualMax );
        actualMin = mw.Minimum;
        expectedMin = 0.95d;
        Assert.AreEqual( expectedMin, actualMin );
        mw.AddValue( 0.96d );
        mw.AddValue( 1.03d );
        actualOutcome = mw.Status;
        expectedOutcome = MovingWindowStatus.Within;
        Assert.AreEqual( expectedOutcome, actualOutcome, "Expected status before failures are added" );
        actualStopStatus = mw.IsStopStatus();
        expectedStopStatus = true;
        Assert.AreEqual( expectedStopStatus, actualStopStatus, $"Expected stop status before failures {MovingWindowStatus.Above}" );
        actualCount = mw.ConsecutiveFailureCount;
        expectedCount = 0;
        Assert.AreEqual( expectedCount, actualCount, "Consecutive failure count before consecutive failures" );
        actualCount = mw.FailureCount;
        expectedCount = 0;
        Assert.AreEqual( expectedCount, actualCount, "Failure count before failures" );
        mw.AddValue( mw.ConformityRange.Max + 1d );
        actualCount = mw.ConsecutiveFailureCount;
        expectedCount = 0;
        Assert.AreEqual( expectedCount, actualCount, "Consecutive failure count before consecutive failures" );
        actualCount = mw.FailureCount;
        expectedCount = 1;
        Assert.AreEqual( expectedCount, actualCount, "Failure count after one over range failure" );
        actualOutcome = mw.Status;
        expectedOutcome = MovingWindowStatus.Filling;
        Assert.AreEqual( expectedOutcome, actualOutcome, $"Status after {ReadingStatus.NonConformal} failure" );
        mw.AddValue( new double?(), TimeSpan.Zero );
        actualStopStatus = mw.IsStopStatus();
        expectedStopStatus = false;
        Assert.AreEqual( expectedStopStatus, actualStopStatus, $"Expected stop status after {ReadingStatus.Indeterminable} failure" );
        actualCount = mw.ConsecutiveFailureCount;
        expectedCount = 0;
        Assert.AreEqual( expectedCount, actualCount, "Consecutive failure count before consecutive failures" );
        actualCount = mw.FailureCount;
        expectedCount = 2;
        Assert.AreEqual( expectedCount, actualCount, $"Failure count after one {ReadingStatus.NonConformal} and one {ReadingStatus.Indeterminable} failure" );
        actualOutcome = mw.Status;
        expectedOutcome = MovingWindowStatus.Filling;
        Assert.AreEqual( expectedOutcome, actualOutcome, $"Status after {ReadingStatus.Indeterminable} failure" );
        mw.AddValue( new double?(), TimeSpan.Zero );
        actualCount = mw.ConsecutiveFailureCount;
        expectedCount = 2;
        Assert.AreEqual( expectedCount, actualCount, $"Consecutive failure count after two {ReadingStatus.Indeterminable} consecutive failures" );
        actualCount = mw.FailureCount;
        expectedCount = 3;
        Assert.AreEqual( expectedCount, actualCount, $"Failure count after one {ReadingStatus.NonConformal} and two {ReadingStatus.Indeterminable} failure" );
        actualOutcome = mw.Status;
        expectedOutcome = MovingWindowStatus.Filling;
        Assert.AreEqual( expectedOutcome, actualOutcome, $"Status after {ReadingStatus.Indeterminable} failure" );
        mw.AddValue( mw.ConformityRange.Min - 1d );
        actualCount = mw.ConsecutiveFailureCount;
        expectedCount = 0;
        Assert.AreEqual( expectedCount, actualCount, $"Consecutive failure count after reset of consecutive failures" );
        actualCount = mw.FailureCount;
        expectedCount = 4;
        Assert.AreEqual( expectedCount, actualCount, $"Failure count after two {ReadingStatus.NonConformal} and two {ReadingStatus.Indeterminable} failure" );
        actualOutcome = mw.Status;
        expectedOutcome = MovingWindowStatus.Filling;
        Assert.AreEqual( expectedOutcome, actualOutcome, $"Status after {ReadingStatus.NonConformal} failure" );
        mw.AddValue( mw.ConformityRange.Min - 2d );
        actualCount = mw.ConsecutiveFailureCount;
        expectedCount = 2;
        Assert.AreEqual( expectedCount, actualCount, $"Consecutive failure count after two {ReadingStatus.NonConformal} consecutive failures" );
        actualCount = mw.FailureCount;
        expectedCount = 5;
        Assert.AreEqual( expectedCount, actualCount, $"Failure count after three {ReadingStatus.NonConformal} and two {ReadingStatus.Indeterminable} failure" );
        actualOutcome = mw.Status;
        expectedOutcome = MovingWindowStatus.Filling;
        Assert.AreEqual( expectedOutcome, actualOutcome, $"Status after {ReadingStatus.NonConformal} failure" );
        actualStopStatus = mw.IsStopStatus();
        expectedStopStatus = false;
        Assert.AreEqual( expectedStopStatus, actualStopStatus, $"Expected stop status after two consecutive {ReadingStatus.NonConformal} failures" );
        mw.AddValue( mw.ConformityRange.Min - 3d );
        actualCount = mw.ConsecutiveFailureCount;
        expectedCount = 3;
        Assert.AreEqual( expectedCount, actualCount, $"Consecutive failure count after three {ReadingStatus.NonConformal} consecutive failures" );
        actualStopStatus = mw.IsStopStatus();
        expectedStopStatus = true;
        Assert.AreEqual( expectedStopStatus, actualStopStatus, $"Expected stop status after three consecutive {ReadingStatus.NonConformal} failures" );
        mw.AddValue( mw.OverflowRange.Min );
        actualOutcome = mw.Status;
        expectedOutcome = MovingWindowStatus.Filling;
        Assert.AreEqual( expectedOutcome, actualOutcome, $"Status after {ReadingStatus.Overflow} failure" );
        actualCount = mw.ConsecutiveFailureCount;
        expectedCount = 0;
        Assert.AreEqual( expectedCount, actualCount, $"Consecutive failure count after first {ReadingStatus.Overflow} failure" );
        actualCount = mw.FailureCount;
        expectedCount = 7;
        Assert.AreEqual( expectedCount, actualCount, $"Failure count after four {ReadingStatus.NonConformal}, two {ReadingStatus.Indeterminable} and one {ReadingStatus.NonConformal} failure" );
        actualStopStatus = mw.IsStopStatus();
        expectedStopStatus = false;
        Assert.AreEqual( expectedStopStatus, actualStopStatus, $"Expected stop status after first (min) {ReadingStatus.Overflow} failure" );
        mw.AddValue( mw.OverflowRange.Max );
        actualOutcome = mw.Status;
        expectedOutcome = MovingWindowStatus.Filling;
        Assert.AreEqual( expectedOutcome, actualOutcome, $"Status after second (max) {ReadingStatus.Overflow} failure" );
        actualCount = mw.ConsecutiveFailureCount;
        expectedCount = 2;
        Assert.AreEqual( expectedCount, actualCount, $"Consecutive failure count after second (max) {ReadingStatus.Overflow} failure" );
        actualCount = mw.FailureCount;
        expectedCount = 8;
        Assert.AreEqual( expectedCount, actualCount, $"Failure count after four {ReadingStatus.NonConformal}, two {ReadingStatus.Indeterminable} and two {ReadingStatus.NonConformal} failures" );
        actualStopStatus = mw.IsStopStatus();
        expectedStopStatus = false;
        Assert.AreEqual( expectedStopStatus, actualStopStatus, $"Expected stop status after second (max) {ReadingStatus.Overflow} failure" );
        mw.ClearKnownState();
        actualMean = mw.Mean;
        expectedMean = 0d;
        Assert.AreEqual( expectedMean, actualMean, "Mean after clear known state" );
        actualMax = mw.Maximum;
        expectedMax = double.MinValue;
        Assert.AreEqual( expectedMax, actualMax, "Maximum after clear known state" );
        actualMin = mw.Minimum;
        expectedMin = double.MaxValue;
        Assert.AreEqual( expectedMin, actualMin, "Minimum after clear known state" );
        actualCount = mw.ConsecutiveFailureCount;
        expectedCount = 0;
        Assert.AreEqual( expectedCount, actualCount, $"Consecutive failure count after clear known state" );
        actualCount = mw.FailureCount;
        expectedCount = 0;
        Assert.AreEqual( expectedCount, actualCount, $"Failure count after clear known state" );
        actualStopStatus = mw.IsStopStatus();
        expectedStopStatus = false;
        Assert.AreEqual( expectedStopStatus, actualStopStatus, $"Expected stop status after clear known state" );
        actualOutcome = mw.Status;
        expectedOutcome = MovingWindowStatus.None;
        Assert.AreEqual( expectedOutcome, actualOutcome, $"Status after clear known state" );
    }
}
