using System.Diagnostics.CodeAnalysis;

namespace cc.isr.Std.MovingFilters;

/// <summary> Moving average. </summary>
/// <remarks>
/// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-11-19 </para>
/// </remarks>
public class MovingAverage : ICloneable
{
    #region " constructor "

    /// <summary> The minimum length of the filter. </summary>
    private const int MINIMUM_LENGTH = 2;

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="length"> The length. </param>
    public MovingAverage( int length ) : base()
    {
        this.Length = length < MINIMUM_LENGTH ? MINIMUM_LENGTH : length;
        this.ClearKnownStateThis();
    }

    /// <summary> The cloning constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="value"> The value. </param>
    public MovingAverage( MovingAverage value ) : this( value is null ? MINIMUM_LENGTH : value.Length < MINIMUM_LENGTH ? MINIMUM_LENGTH : value.Length )
    {
        this.ClearKnownStateThis();
        if ( value is not null )
        {
            this._headIndex = value._headIndex;
            this._tailIndex = value._tailIndex;
            this.Mean = value.Mean;
            this.Sum = value.Sum;
            this.Maximum = value.Maximum;
            this.Minimum = value.Minimum;
            List<double> l = new( value.Values );
            this.Values = [.. l];
        }
    }

    /// <summary> Creates a new object that is a copy of the current instance. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <returns> A new object that is a copy of this instance. </returns>
    public virtual object Clone()
    {
        return new MovingAverage( this );
    }

    #endregion

    #region " moving average "

    private double[] Values { get; set; }

    /// <summary> Gets or sets the length of the moving average. </summary>
    /// <value> The length. </value>
    public int Length { get; set; }

    /// <summary>   Gets the number of elements.  </summary>
    /// <value> The number of elements that were added. </value>
    public int Count { get; private set; }

    /// <summary>   Gets the mean. </summary>
    /// <value> The mean value. </value>
    public double Mean { get; private set; }

    /// <summary>   Gets the minimum. </summary>
    /// <value> The minimum value. </value>
    public double Minimum { get; private set; }

    /// <summary>   Gets the maximum. </summary>
    /// <value> The maximum value. </value>
    public double Maximum { get; private set; }

    /// <summary>   Gets or sets the number of.  </summary>
    /// <value> The sum. </value>
    private double Sum { get; set; }

    /// <summary>   Zero-based index of the head of the circular buffer. </summary>
    private int _headIndex;

    /// <summary>   The circular buffer tail index. </summary>
    private int _tailIndex;

    /// <summary> Gets the last averaged reading. </summary>
    /// <value> The last averaged reading. </value>
    public double LastAveragedReading => this.Count > 0 ? this.Values[this._headIndex] : double.NaN;

    /// <summary>   Gets the get values. </summary>
    /// <value> The get values. </value>
    public double[] GetValues()
    {
        List<double> l = [];
        for ( int i = 0; i < this.Count; i++ )
        {
            l.Add( this.Values[(this._tailIndex + i) % this.Length] );
        }
        return [.. l];
    }

    /// <summary>   Clears values to their known (initial) state. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [MemberNotNull( nameof( Values ) )]
    private void ClearKnownStateThis()
    {
        this._headIndex = -1;
        this._tailIndex = 0;
        this.Count = 0;
        this.Mean = 0d;
        this.Sum = 0d;
        this.Maximum = double.MinValue;
        this.Minimum = double.MaxValue;
        this.Values = new double[this.Length];
    }

    /// <summary>   Clears the known state. </summary>
    /// <remarks>   David, 2021-03-18. </remarks>
    public virtual void ClearKnownState()
    {
        this.ClearKnownStateThis();
    }

    /// <summary>   Updates the minimum. </summary>
    /// <remarks>   David, 2021-03-20. </remarks>
    private void UpdateMinimum()
    {
        // if the new value is neither the new minimum or maximum, lookup for a new minimax.
        double min = this.Values[0];
        foreach ( double v in this.Values )
        {
            if ( v < min )
            {
                min = v;
            }
        }
        this.Minimum = min;
    }

    private void UpdateMaximum()
    {
        // if the new value is neither the new minimum or maximum, lookup for a new minimax.
        double max = this.Values[0];
        double min = this.Values[0];
        foreach ( double v in this.Values )
        {
            if ( v < min )
            {
                min = v;
            }
            else if ( v > max )
            {
                max = v;
            }
        }
        this.Minimum = min;
        this.Maximum = max;
    }

    /// <summary> Adds a value. </summary>
    /// <param name="value"> The value. </param>
    public virtual void AddValue( double value )
    {
        if ( this.Count < this.Length )
        {
            this._headIndex += 1;
            this.Values[this._headIndex] = value;
            this.Sum += value;
            this.Count += 1;
            if ( value > this.Maximum )
                this.Maximum = value;
            if ( value < this.Minimum )
                this.Minimum = value;
        }
        else
        {
            // remember the previous tail value, which is replaced.
            double firstValue = this.Values[this._tailIndex];

            // shift the pointers.
            this._headIndex = this._tailIndex;
            this._tailIndex = (this._tailIndex + 1) % this.Length;

            // save the new head value
            this.Values[this._headIndex] = value;

            // update the sum replacing the previous fist value with the new value
            this.Sum = this.Sum - firstValue + value;
            if ( value > this.Maximum )
            {
                this.Maximum = value;
                if ( firstValue >= this.Minimum )
                    this.UpdateMinimum();
            }
            else if ( value < this.Minimum )
            {
                this.Minimum = value;
                if ( firstValue >= this.Maximum )
                    this.UpdateMaximum();
            }
            else if ( firstValue <= this.Minimum )
            {
                this.UpdateMinimum();
            }
            else if ( firstValue >= this.Maximum )
            {
                this.UpdateMaximum();
            }
        }
        this.Mean = this.Sum / this.Count;
    }

    #endregion
}
