# About

cc.isr.Std.SyncContext is a .Net library for Event Handler Context notifiers.
These context notifiers have been replaced by simpler and faster but thread 
unsafe handlers that require checking for invoke required on forms and controls
to prevent cross thread exceptions.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

cc.isr.Std.SyncContext is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Standard Framework Repository].

[Standard Framework Repository]: https://bitbucket.org/davidhary/dn.std

