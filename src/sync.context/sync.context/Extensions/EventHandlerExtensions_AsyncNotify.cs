using System.Collections.Specialized;
using System.ComponentModel;

namespace cc.isr.Std.SyncContext;

/// <summary>   An event handler extensions. </summary>
/// <remarks>   David, 2020-09-15. </remarks>
public static partial class EventHandlerExtensions
{
    /// <summary>
    /// Asynchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="handler"> The event handler. </param>
    /// <param name="sender">  The sender of the event. </param>
    public static void AsyncNotify( this EventHandler<EventArgs> handler, object sender )
    {
        EventHandler<EventArgs>? theHandler = handler;
        if ( theHandler is not null )
        {
            AsyncNotifyThis( theHandler, CurrentSyncContext(), sender, EventArgs.Empty );
        }
    }

    /// <summary>
    /// Asynchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="handler"> The handler. </param>
    /// <param name="context"> The synchronization context. </param>
    /// <param name="sender">  The sender of the event. </param>
    /// <param name="e">       T event information. </param>
    public static void AsyncNotify<TEventArgs>( this EventHandler<TEventArgs> handler, SynchronizationContext context, object sender, TEventArgs e ) where TEventArgs : EventArgs
    {
        EventHandler<TEventArgs>? theHandler = handler;
        if ( theHandler is not null )
        {
            AsyncNotifyThis( theHandler, context, sender, e );
        }
    }

    /// <summary>
    /// Asynchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <typeparam name="TEventArgs">   Type of the event arguments. </typeparam>
    /// <param name="handler">  The handler. </param>
    /// <param name="context">  The synchronization context. </param>
    /// <param name="sender">   The sender of the event. </param>
    /// <param name="e">        T event information. </param>
    private static void AsyncNotifyThis<TEventArgs>( EventHandler<TEventArgs> handler, SynchronizationContext context, object sender, TEventArgs e ) where TEventArgs : EventArgs
    {
        context.Post( ( object status ) => handler( sender, e ), null );
    }

    /// <summary>
    /// Asynchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="handler"> The handler. </param>
    /// <param name="sender">  The sender of the event. </param>
    /// <param name="e">       T event information. </param>
    public static void AsyncNotify( this PropertyChangedEventHandler handler, object sender, PropertyChangedEventArgs e )
    {
        PropertyChangedEventHandler? theHandler = handler;
        if ( theHandler is not null )
        {
            AsyncNotifyThis( theHandler, CurrentSyncContext(), sender, e );
        }
    }

    /// <summary>
    /// Asynchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="handler"> The handler. </param>
    /// <param name="context"> The synchronization context. </param>
    /// <param name="sender">  The sender of the event. </param>
    /// <param name="e">       Property changed event information. </param>
    public static void AsyncNotify( this PropertyChangedEventHandler handler, SynchronizationContext context, object sender, PropertyChangedEventArgs e )
    {
        PropertyChangedEventHandler? theHandler = handler;
        if ( theHandler is not null )
        {
            AsyncNotifyThis( theHandler, context, sender, e );
        }
    }

    /// <summary>
    /// Asynchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="handler">  The handler. </param>
    /// <param name="context">  The synchronization context. </param>
    /// <param name="sender">   The sender of the event. </param>
    /// <param name="e">        Property changed event information. </param>
    private static void AsyncNotifyThis( PropertyChangedEventHandler handler, SynchronizationContext context, object sender, PropertyChangedEventArgs e )
    {
        context.Post( ( object status ) => handler( sender, e ), null );
    }

    /// <summary>
    /// Asynchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="handler"> The handler. </param>
    /// <param name="sender">  The sender of the event. </param>
    /// <param name="e">       T event information. </param>
    public static void AsyncNotify( this PropertyChangingEventHandler handler, object sender, PropertyChangingEventArgs e )
    {
        PropertyChangingEventHandler? theHandler = handler;
        if ( theHandler is not null )
        {
            AsyncNotifyThis( theHandler, CurrentSyncContext(), sender, e );
        }
    }

    /// <summary>
    /// Asynchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="handler"> The handler. </param>
    /// <param name="context"> The synchronization context. </param>
    /// <param name="sender">  The sender of the event. </param>
    /// <param name="e">       Property Changing event information. </param>
    public static void AsyncNotify( this PropertyChangingEventHandler handler, SynchronizationContext context, object sender, PropertyChangingEventArgs e )
    {
        PropertyChangingEventHandler? theHandler = handler;
        if ( theHandler is not null )
        {
            AsyncNotifyThis( theHandler, context, sender, e );
        }
    }

    /// <summary>
    /// Asynchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="handler">  The handler. </param>
    /// <param name="context">  The synchronization context. </param>
    /// <param name="sender">   The sender of the event. </param>
    /// <param name="e">        Property Changing event information. </param>
    private static void AsyncNotifyThis( PropertyChangingEventHandler handler, SynchronizationContext context, object sender, PropertyChangingEventArgs e )
    {
        context.Post( ( object status ) => handler( sender, e ), null );
    }

    /// <summary>
    /// Asynchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="handler"> The handler. </param>
    /// <param name="sender">  The sender of the event. </param>
    /// <param name="e">       T event information. </param>
    public static void AsyncNotify( this NotifyCollectionChangedEventHandler handler, object sender, NotifyCollectionChangedEventArgs e )
    {
        NotifyCollectionChangedEventHandler? theHandler = handler;
        if ( theHandler is not null )
        {
            AsyncNotifyThis( theHandler, CurrentSyncContext(), sender, e );
        }
    }

    /// <summary>
    /// Asynchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="handler"> The handler. </param>
    /// <param name="context"> The synchronization context. </param>
    /// <param name="sender">  The sender of the event. </param>
    /// <param name="e">       Property changed event information. </param>
    public static void AsyncNotify( this NotifyCollectionChangedEventHandler handler, SynchronizationContext context, object sender, NotifyCollectionChangedEventArgs e )
    {
        NotifyCollectionChangedEventHandler? theHandler = handler;
        if ( theHandler is not null )
        {
            AsyncNotifyThis( theHandler, context, sender, e );
        }
    }

    /// <summary>
    /// Asynchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="handler">  The handler. </param>
    /// <param name="context">  The synchronization context. </param>
    /// <param name="sender">   The sender of the event. </param>
    /// <param name="e">        Property changed event information. </param>
    private static void AsyncNotifyThis( NotifyCollectionChangedEventHandler handler, SynchronizationContext context, object sender, NotifyCollectionChangedEventArgs e )
    {
        context.Post( ( object status ) => handler( sender, e ), null );
    }
}
