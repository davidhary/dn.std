namespace cc.isr.Std.SyncContext;

/// <summary>   An event handler extensions. </summary>
/// <remarks>   David, 2020-09-15. </remarks>
public static partial class EventHandlerExtensions
{
    /// <summary> Returns the current synchronization context. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
    /// null. </exception>
    /// <returns> A Threading.SynchronizationContext. </returns>
    private static SynchronizationContext CurrentSyncContext()
    {
        if ( System.Threading.SynchronizationContext.Current is null )
        {
            System.Threading.SynchronizationContext.SetSynchronizationContext( new SynchronizationContext() );
        }

        return System.Threading.SynchronizationContext.Current is null
            ? throw new InvalidOperationException( "Current Synchronization Context not set;. Must be set before starting the thread." )
            : System.Threading.SynchronizationContext.Current;
    }
}
