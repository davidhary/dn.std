using System.ComponentModel;

namespace cc.isr.Std.SyncContext.Lists;

/// <summary> A List changed base class using synchronization context notification. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-05-22 </para>
/// </remarks>
public abstract class ListChangedBase : PropertyChangedNotifier, INotifyPropertyChanged
{
    /// <summary>   Specialized constructor for use only by derived class. </summary>
    /// <remarks>   David, 2021-02-25. </remarks>
    protected ListChangedBase() : base()
    { }

    /// <summary>   Event queue for all listeners interested in List Changed events. </summary>
    public event ListChangedEventHandler? ListChanged;

    /// <summary>   Raises the notify List changed event. </summary>
    /// <remarks>   David, 2021-02-27. </remarks>
    /// <param name="e">                    Event information to send to registered event handlers. </param>
    /// <param name="notificationLevel">    (Optional) The notification level [<see cref="NotificationLevel.Synchronous"/>
    ///                                     ]. </param>
    protected void OnListChanged( ListChangedEventArgs e, NotificationLevel notificationLevel = NotificationLevel.Synchronous )
    {
        if ( notificationLevel == NotificationLevel.Asynchronous )
            this.AsyncNotifyListChanged( e );
        else
            this.SyncNotifyListChanged( e );
    }

    /// <summary>   Notifies List changed. </summary>
    /// <remarks>   David, 2020-09-21. </remarks>
    /// <param name="e">                    List Changed event information. </param>
    /// <param name="notificationLevel">    (Optional) The notification level [<see cref="NotificationLevel.Synchronous"/>
    ///                                     ]. </param>
    protected virtual void NotifyListChanged( ListChangedEventArgs e, NotificationLevel notificationLevel = NotificationLevel.Synchronous )
    {
        this.OnListChanged( e, notificationLevel );
    }

    /// <summary>
    /// Asynchronously notifies (posts) List change on a different thread. Unsafe for cross
    /// threading; fast return of control to the invoking function.
    /// </summary>
    /// <remarks>
    /// Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
    /// even with slow handler functions.
    /// </remarks>
    /// <param name="e"> List Changed event information. </param>
    protected virtual void AsyncNotifyListChanged( ListChangedEventArgs e )
    {
        ListChangedEventHandler? handler = this.ListChanged;
        if ( handler is not null )
            this.ActiveContext.Post( ( object state ) => handler( this, e ), null );
    }

    /// <summary>
    /// Synchronously notifies (send) List change on a different thread. Safe for cross
    /// threading.
    /// </summary>
    /// <param name="e"> List Changed event information. </param>
    protected virtual void SyncNotifyListChanged( ListChangedEventArgs e )
    {
        ListChangedEventHandler? handler = this.ListChanged;
        if ( handler is not null )
            this.ActiveContext.Send( ( object state ) => handler( this, e ), null );
    }

}
