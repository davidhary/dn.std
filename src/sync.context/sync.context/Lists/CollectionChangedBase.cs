using System.Collections.Specialized;
using System.ComponentModel;

namespace cc.isr.Std.SyncContext.Lists;

/// <summary> A collection changed base using synchronization context notifications. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-05-22 </para>
/// </remarks>
public abstract class CollectionChangedBase : PropertyChangedNotifier, INotifyCollectionChanged, INotifyPropertyChanged
{
    /// <summary>   Specialized constructor for use only by derived class. </summary>
    /// <remarks>   David, 2021-02-25. </remarks>
    protected CollectionChangedBase() : base()
    { }

    ///<summary>   Event queue for all listeners interested in Collection Changed events. </summary>
    public event NotifyCollectionChangedEventHandler? CollectionChanged;

    /// <summary>   Raises the notify collection changed event. </summary>
    /// <remarks>   David, 2021-02-27. </remarks>
    /// <param name="e">                    Event information to send to registered event handlers. </param>
    /// <param name="notificationLevel">    (Optional) The notification level [<see cref="NotificationLevel.Synchronous"/>
    ///                                     ]. </param>
    protected void OnCollectionChanged( NotifyCollectionChangedEventArgs e, NotificationLevel notificationLevel = NotificationLevel.Synchronous )
    {
        if ( notificationLevel == NotificationLevel.Asynchronous )
            this.AsyncNotifyCollectionChanged( e );
        else
            this.SyncNotifyCollectionChanged( e );
    }

    /// <summary>   Notifies collection changed. </summary>
    /// <remarks>   David, 2020-09-21. </remarks>
    /// <param name="e">                    Collection Changed event information. </param>
    /// <param name="notificationLevel">    (Optional) The notification level [<see cref="NotificationLevel.Synchronous"/>
    ///                                     ]. </param>
    protected virtual void NotifyCollectionChanged( NotifyCollectionChangedEventArgs e, NotificationLevel notificationLevel = NotificationLevel.Synchronous )
    {
        this.OnCollectionChanged( e, notificationLevel );
    }

    /// <summary>
    /// Asynchronously notifies (posts) collection change on a different thread. Unsafe for cross
    /// threading; fast return of control to the invoking function.
    /// </summary>
    /// <remarks>
    /// Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
    /// even with slow handler functions.
    /// </remarks>
    /// <param name="e"> Collection Changed event information. </param>
    protected virtual void AsyncNotifyCollectionChanged( NotifyCollectionChangedEventArgs e )
    {
        NotifyCollectionChangedEventHandler? handler = this.CollectionChanged;
        if ( handler is not null )
            this.ActiveContext.Post( ( object state ) => handler( this, e ), null );
    }

    /// <summary>
    /// Synchronously notifies (send) collection change on a different thread. Safe for cross
    /// threading.
    /// </summary>
    /// <param name="e"> Collection Changed event information. </param>
    protected virtual void SyncNotifyCollectionChanged( NotifyCollectionChangedEventArgs e )
    {
        NotifyCollectionChangedEventHandler? handler = this.CollectionChanged;
        if ( handler is not null )
            this.ActiveContext.Send( ( object state ) => handler( this, e ), null );
    }

}
