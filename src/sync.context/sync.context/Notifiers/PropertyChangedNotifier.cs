using System.ComponentModel;

namespace cc.isr.Std.SyncContext;

/// <summary>   A property changed notifier using the synchronization context. </summary>
/// <remarks>   David, 2021-02-25. </remarks>
public abstract class PropertyChangedNotifier : INotifyPropertyChanged
{
    #region " construction "

    /// <summary>   Specialized default constructor for use only by derived class. </summary>
    /// <remarks>   David, 2021-02-25. </remarks>
    protected PropertyChangedNotifier() => this.Context = SynchronizationContext.Current;

    #endregion

    #region " synchronization context "

    /// <summary> Gets or sets the synchronization context. </summary>
    /// <value> The context. </value>
    private SynchronizationContext Context { get; set; }

    /// <summary> Returns the current synchronization context. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
    /// null. </exception>
    /// <returns> A Threading.SynchronizationContext. </returns>
    private static SynchronizationContext CurrentSyncContext()
    {
        if ( SynchronizationContext.Current is null )
        {
            SynchronizationContext.SetSynchronizationContext( new SynchronizationContext() );
        }

        return SynchronizationContext.Current is null
            ? throw new InvalidOperationException( "Current Synchronization Context not set;. Must be set before starting the thread." )
            : SynchronizationContext.Current;
    }

    /// <summary> Gets a context for the active. </summary>
    /// <value> The active context. </value>
    protected SynchronizationContext ActiveContext => this.Context ?? CurrentSyncContext();

    #endregion

    #region " event handling "

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="propertyName">         (Optional) Name of the property. </param>
    /// <param name="notificationLevel">    (Optional) The notification level [<see cref="NotificationLevel.Synchronous"/>
    ///                                     ]. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "",
        NotificationLevel notificationLevel = NotificationLevel.Synchronous )
    {
        if ( notificationLevel == NotificationLevel.Synchronous )
            this.SyncNotifyPropertyChanged( propertyName );
        else if ( notificationLevel == NotificationLevel.Asynchronous )
            this.AsyncNotifyPropertyChanged( propertyName );
    }

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.OnPropertyChanged( propertyName );
    }

    /// <summary>   Synchronously notify property changed described by propertyName. </summary>
    /// <remarks>   David, 2021-02-25. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void SyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Asynchronously notify property changed described by propertyName. </summary>
    /// <remarks>   David, 2021-02-25. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void AsyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
            this.ActiveContext.Post( ( object state ) => handler( this, new PropertyChangedEventArgs( propertyName ) ), null );
    }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <remarks>   2023-04-04. </remarks>
    /// <param name="propertyName">         Name of the property. </param>
    /// <param name="notificationLevel">    (Optional) The notification level [<see cref="NotificationLevel.Synchronous"/>
    ///                                     ]. </param>
    protected virtual void OnPropertyChanged( string? propertyName, NotificationLevel notificationLevel = NotificationLevel.Synchronous )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
        {
            if ( notificationLevel == NotificationLevel.Synchronous )
                this.SyncNotifyPropertyChanged( propertyName! );
            else if ( notificationLevel == NotificationLevel.Asynchronous )
                this.AsyncNotifyPropertyChanged( propertyName! );
        }
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<T>( ref T backingField, T value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<T>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<T>( ref T prop, T value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        if ( EqualityComparer<T>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<T>( T oldValue, T newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif

        if ( EqualityComparer<T>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion
}
/// <summary>   Values that represent notification levels. </summary>
/// <remarks>   David, 2021-02-25. </remarks>
public enum NotificationLevel
{
    /// <summary>   An enum constant representing the none option. </summary>
    [Description( "None" )]
    None,
    /// <summary>   An enum constant representing the synchronous option. </summary>
    [Description( "Synchronous" )]
    Synchronous,
    /// <summary>   An enum constant representing the asynchronous option. </summary>
    [Description( "Asynchronous" )]
    Asynchronous
}
