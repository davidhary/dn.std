namespace cc.isr.Std.Notifiers;

/// <summary> Action notifier. </summary>
/// <remarks>
/// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2017-04-26 </para>
/// </remarks>
public class ActionNotifier<T>
{
    #region " construction "

    /// <summary>   Constructor. </summary>
    /// <remarks>   David, 2021-03-22. </remarks>
    public ActionNotifier() => this.NotifyAsyncActions = [];

    #endregion

    #region " notifiers "

    /// <summary> Event queue for all listeners interested in Notified events. </summary>
    public event Action<T>? Notified;

    /// <summary>   Executes the 'notified' action. </summary>
    /// <remarks>   David, 2021-02-27. </remarks>
    /// <param name="argument"> The action argument. </param>
    protected virtual void OnNotified( T argument )
    {
        this.Invoke( argument );
    }

    /// <summary>
    /// Executes the given operation on a different thread, and waits for the result.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="argument"> The action argument. </param>
    public void Invoke( T argument )
    {
        this.Notified?.Invoke( argument );
    }

    /// <summary> Registers an action to invoke. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="action"> The action. </param>
    public void Register( Action<T> action )
    {
        Notified += action;
    }

    /// <summary> Unregisters an action. </summary>
    /// <remarks> David, 2020-08-26. </remarks>
    /// <param name="action"> The action. </param>
    public void Unregister( Action<T> action )
    {
        Notified -= action;
    }

    #endregion

    #region " async task notifiers "

    /// <summary> Gets or sets the notify asynchronous actions. </summary>
    /// <value> The notify asynchronous actions. </value>
    private List<Func<T, Task>> NotifyAsyncActions { get; set; } = [];

    /// <summary> Executes the asynchronous on a different thread, and waits for the result. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="argument"> The action argument. </param>
    /// <returns> A Task. </returns>
    public async Task InvokeAsync( T argument )
    {
        this.Invoke( argument );
        foreach ( Func<T, Task> callback in this.NotifyAsyncActions )
        {
            await callback( argument );
        }
    }

    /// <summary> Registers this object. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="action"> The action. </param>
    public void Register( Func<T, Task> action )
    {
        this.NotifyAsyncActions.Add( action );
    }

    #endregion
}
