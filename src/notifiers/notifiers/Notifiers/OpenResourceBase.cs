using System.ComponentModel;

namespace cc.isr.Std.Notifiers;

/// <summary> A resource Opener base class with event notifications. </summary>
/// <remarks>
/// <para>
/// David, 2018-12-11 </para>
/// </remarks>
public abstract class OpenResourceBase : INotifyPropertyChanged
{
    #region " construction and cleanup "

    /// <summary> Specialized default constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    protected OpenResourceBase() : base()
    {
        this._openable = true;
        this._clearable = true;
        this._clearToolTip = string.Empty;
        this._identity = string.Empty;
        this._openResourceName = string.Empty;
        this._openResourceModel = string.Empty;
        this._openToolTip = string.Empty;
        this._openToolTip = string.Empty;
        this._resourceClosedCaption = "<closed>";
        this._resourceNameCaption = string.Empty;
        this._resourceModelCaption = string.Empty;
        this._validatedResourceName = string.Empty;
        this._candidateResourceName = string.Empty;
        this._candidateResourceModel = string.Empty;

    }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<TValue>( ref TValue backingField, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<TValue>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( ref TValue prop, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        if ( EqualityComparer<TValue>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( TValue oldValue, TValue newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif

        if ( EqualityComparer<TValue>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion

    #region " clearable "

    private bool _clearable;

    /// <summary>
    /// Gets or sets the value indicating if the clear button is visible and can be enabled. An item
    /// can be cleared only if it is Opened.
    /// </summary>
    /// <value> The clearable. </value>
    public bool Clearable
    {
        get => this._clearable;
        set => _ = this.SetProperty( ref this._clearable, value );
    }

    private string _clearToolTip;

    /// <summary> Gets or sets the Clear tool tip. </summary>
    /// <value> The Clear tool tip. </value>
    public string ClearToolTip
    {
        get => this._clearToolTip;
        set => _ = this.SetProperty( ref this._clearToolTip, (value ?? string.Empty).Trim() );
    }

    #endregion

    #region " openable "

    /// <summary> True if openable. </summary>
    private bool _openable;

    /// <summary>
    /// Gets or sets the value indicating if the Open button is visible and can be enabled. An item
    /// can be Opened only if it is selected.
    /// </summary>
    /// <value> The Openable. </value>
    public bool Openable
    {
        get => this._openable;
        set
        {
            if ( this.SetProperty( ref this._openable, value ) )
                this.OpenEnabled = this.Openable && !string.IsNullOrWhiteSpace( this.ValidatedResourceName );
        }
    }

    /// <summary> True to enable, false to disable the open. </summary>
    private bool _openEnabled;

    /// <summary> Gets or sets Open Enabled state. </summary>
    /// <value> The Open enabled. </value>
    public bool OpenEnabled
    {
        get => this._openEnabled;
        set => _ = this.SetProperty( ref this._openEnabled, value );
    }

    /// <summary> True to enable, false to disable the validation. </summary>
    private bool _validationEnabled;

    /// <summary> Gets or sets Validation Enabled state. </summary>
    /// <remarks>
    /// Validation is disabled by default to facilitate resource selection in case of resource
    /// manager mismatch between VISA implementations.
    /// </remarks>
    /// <value> The Validation enabled. </value>
    public bool ValidationEnabled
    {
        get => this._validationEnabled;
        set => _ = this.SetProperty( ref this._validationEnabled, value );
    }

    /// <summary> Name of the validated resource. </summary>
    private string _validatedResourceName;

    /// <summary> Returns the validated resource name. </summary>
    /// <value> The name of the validated resource. </value>
    public string ValidatedResourceName
    {
        get => this._validatedResourceName;
        set
        {
            if ( this.SetProperty( ref this._validatedResourceName, (value ?? string.Empty).Trim() ) )
            {
                this.CandidateResourceNameValidated = !string.IsNullOrWhiteSpace( this.ValidatedResourceName );
                this.OpenEnabled = this.Openable && this.CandidateResourceNameValidated;
            }
        }
    }

    /// <summary> True if candidate resource name validated. </summary>
    private bool _candidateResourceNameValidated;

    /// <summary> Gets or sets the candidate resource name validated. </summary>
    /// <value> The candidate resource name validated. </value>
    public virtual bool CandidateResourceNameValidated
    {
        get => this._candidateResourceNameValidated;
        set => _ = this.SetProperty( ref this._candidateResourceNameValidated, value );
    }

    private string _openToolTip;

    /// <summary> Gets or sets the Open tool tip. </summary>
    /// <value> The Open tool tip. </value>
    public string OpenToolTip
    {
        get => this._openToolTip;
        set => _ = this.SetProperty( ref this._openToolTip, (value ?? string.Empty).Trim() );
    }

    #endregion

    #region " open fields "

    /// <summary> Gets the Open status. </summary>
    /// <value> The Open status. </value>
    public abstract bool IsOpen { get; }

    /// <summary> Gets the close status. </summary>
    /// <value> The Close status. </value>
    public bool IsClose => !this.IsOpen;

    #endregion

    #region " name  "

    /// <summary> Gets the name of the designated resource. </summary>
    /// <value> The name of the designated resource. </value>
    public string DesignatedResourceName => this.CandidateResourceNameValidated ? this.ValidatedResourceName : this.CandidateResourceName;

    private string _candidateResourceName;

    /// <summary> Gets or sets the name of the candidate resource. </summary>
    /// <value> The name of the candidate resource. </value>
    public virtual string CandidateResourceName
    {
        get => this._candidateResourceName;
        set => _ = this.SetProperty( ref this._candidateResourceName, (value ?? string.Empty).Trim() );
    }

    private string _openResourceName;

    /// <summary> Gets or sets the name of the open resource. </summary>
    /// <value> The name of the open resource. </value>
    public virtual string OpenResourceName
    {
        get => this._openResourceName;
        set => _ = this.SetProperty( ref this._openResourceName, (value ?? string.Empty).Trim() );
    }

    #endregion

    #region " title "

    private string _candidateResourceModel;

    /// <summary> Gets or sets the candidate resource model. </summary>
    /// <value> The candidate resource model. </value>
    public virtual string CandidateResourceModel
    {
        get => this._candidateResourceModel;
        set => _ = this.SetProperty( ref this._candidateResourceModel, (value ?? string.Empty).Trim() );
    }

    private string _openResourceModel;

    /// <summary> Gets or sets a short title for the device. </summary>
    /// <value> The short title of the device. </value>
    public virtual string OpenResourceModel
    {
        get => this._openResourceModel;
        set => _ = this.SetProperty( ref this._openResourceModel, (value ?? string.Empty).Trim() );
    }

    private string _resourceModelCaption;

    /// <summary> Gets or sets the Title caption. </summary>
    /// <value> The Title caption. </value>
    public virtual string ResourceModelCaption
    {
        get => this._resourceModelCaption;
        set => _ = this.SetProperty( ref this._resourceModelCaption, (value ?? string.Empty).Trim() );
    }

    #endregion

    #region " caption "

    private string _resourceClosedCaption;

    /// <summary> Gets or sets the default resource name closed caption. </summary>
    /// <value> The resource closed caption. </value>
    public virtual string ResourceClosedCaption
    {
        get => this._resourceClosedCaption;
        set => _ = this.SetProperty( ref this._resourceClosedCaption, (value ?? "<closed>").Trim() );
    }

    private string _resourceNameCaption;

    /// <summary> Gets or sets the resource name caption. </summary>
    /// <value>
    /// The <see cref="OpenResourceName"/> or <see cref="CandidateResourceName"/> resource names.
    /// </value>
    public virtual string ResourceNameCaption
    {
        get => this._resourceNameCaption;
        set => _ = this.SetProperty( ref this._resourceNameCaption, (value ?? string.Empty).Trim() );
    }

    #endregion

    #region " opening "

    /// <summary> Allows taking actions before opening. </summary>
    /// <remarks>
    /// This override should occur as the first call of the overriding method. After this call, the
    /// parent class adds the subsystems.
    /// </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected virtual void OnOpening( CancelEventArgs e )
    {
        if ( e is null )
        {
            throw new ArgumentNullException( nameof( e ) );
        }

        if ( !e.Cancel )
        {
            this.IsInitialized = false;
            this.NotifyOpening( e );
        }
    }

    /// <summary> Event queue for all listeners interested in Opening events. </summary>
    public event EventHandler<CancelEventArgs>? Opening;

    /// <summary>
    /// Synchronously invokes the <see cref="Opening">Opening Event</see>. Not thread safe.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="e"> The <see cref="EventArgs" /> instance containing the event data. </param>
    protected void NotifyOpening( CancelEventArgs e )
    {
        this.Opening?.Invoke( this, e );
    }

    /// <summary>   Removes the opening event handlers. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    protected void RemoveOpeningEventHandlers()
    {
        EventHandler<CancelEventArgs>? handler = this.Opening;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( EventHandler<CancelEventArgs> ) item;
            }
        }
    }

    #endregion

    #region " opened "

    /// <summary> Notifies an open changed. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    public virtual void NotifyOpenChanged()
    {
        this.OpenToolTip = $"Click to {(this.IsOpen ? "Close" : "Open")}";
        this.NotifyPropertyChanged( nameof( OpenResourceBase.IsOpen ) );
        this.NotifyPropertyChanged( nameof( OpenResourceBase.IsClose ) );
        if ( this.IsOpen )
        {
            this.ResourceModelCaption = this.OpenResourceModel;
            this.CandidateResourceModel = this.OpenResourceModel;
            this.ResourceNameCaption = $"{this.OpenResourceModel}.{this.OpenResourceName}";
        }
        else
        {
            this.ResourceModelCaption = $"{this.OpenResourceModel}.{this.ResourceClosedCaption}";
            this.ResourceNameCaption = $"{this.OpenResourceModel}.{this.OpenResourceName}.{this.ResourceClosedCaption}";
        }
    }

    /// <summary> Notifies of the opened event. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected virtual void OnOpened( EventArgs e )
    {
        this.NotifyOpenChanged();
        this.NotifyOpened( e );
    }

    /// <summary> Event queue for all listeners interested in Opened events. </summary>
    public event EventHandler<EventArgs>? Opened;

    /// <summary>
    /// Synchronously invokes the <see cref="Opened">Opened Event</see>. not thread safe.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="e"> The <see cref="EventArgs" /> instance containing the event data. </param>
    protected void NotifyOpened( EventArgs e )
    {
        this.Opened?.Invoke( this, e );
    }

    /// <summary>   Removes the opened event handlers. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    protected void RemoveOpenedEventHandlers()
    {
        EventHandler<EventArgs>? handler = this.Opened;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( EventHandler<EventArgs> ) item;
            }
        }
    }

    #endregion

    #region " closing "

    /// <summary> Allows taking actions before closing. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected virtual void OnClosing( CancelEventArgs e )
    {
        if ( e is null )
        {
            throw new ArgumentNullException( nameof( e ) );
        }

        if ( !e.Cancel )
        {
            this.IsInitialized = false;
            this.NotifyClosing( e );
        }
    }

    /// <summary> Event queue for all listeners interested in Closing events. </summary>
    public event EventHandler<CancelEventArgs>? Closing;

    /// <summary>
    /// Synchronously invokes the <see cref="Closing">Closing Event</see>. Not thread safe.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="e"> The <see cref="EventArgs" /> instance containing the event data. </param>
    protected void NotifyClosing( CancelEventArgs e )
    {
        this.Closing?.Invoke( this, e );
    }

    /// <summary>   Removes the Closing event handlers. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    protected void RemoveClosingEventHandlers()
    {
        EventHandler<CancelEventArgs>? handler = this.Closing;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( EventHandler<CancelEventArgs> ) item;
            }
        }
    }

    #endregion

    #region " closed "

    /// <summary> Notifies of the closed event. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected virtual void OnClosed( EventArgs e )
    {
        this.NotifyOpenChanged();
        this.NotifyClosed( e );
    }

    /// <summary> Event queue for all listeners interested in Closed events. </summary>
    public event EventHandler<EventArgs>? Closed;

    /// <summary>
    /// Synchronously invokes the <see cref="Closed">Closed Event</see>. not thread safe.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="e"> The <see cref="EventArgs" /> instance containing the event data. </param>
    protected void NotifyClosed( EventArgs e )
    {
        this.Closed?.Invoke( this, e );
    }

    /// <summary>   Removes the Closed event handlers. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    protected void RemoveClosedEventHandlers()
    {
        EventHandler<EventArgs>? handler = this.Closed;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( EventHandler<EventArgs> ) item;
            }
        }
    }

    #endregion

    #region " initializing "

    /// <summary> Allows taking actions before Initializing. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected virtual void OnInitializing( CancelEventArgs e )
    {
        if ( e is null )
        {
            throw new ArgumentNullException( nameof( e ) );
        }

        if ( e is not null && !e.Cancel )
        {
            this.NotifyInitializing( e );
        }
    }

    /// <summary> Event queue for all listeners interested in Initializing events. </summary>
    public event EventHandler<CancelEventArgs>? Initializing;

    /// <summary>
    /// Synchronously invokes the <see cref="Initializing">Initializing Event</see>. Not thread safe.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="e"> The <see cref="EventArgs" /> instance containing the event data. </param>
    protected void NotifyInitializing( CancelEventArgs e )
    {
        this.Initializing?.Invoke( this, e );
    }

    /// <summary>   Removes the Initializing event handlers. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    protected void RemoveInitializingEventHandlers()
    {
        EventHandler<CancelEventArgs>? handler = this.Initializing;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( EventHandler<CancelEventArgs> ) item;
            }
        }
    }

    #endregion

    #region " initialized "

    /// <summary> True if is initialized, false if not. </summary>
    private bool _isInitialized;

    /// <summary>
    /// Gets or sets the Initialized sentinel of the device. The device is ready after it is
    /// initialized.
    /// </summary>
    /// <value> <c>true</c> if hardware device is Initialized; <c>false</c> otherwise. </value>
    public virtual bool IsInitialized
    {
        get => this._isInitialized;
        set => _ = this.SetProperty( ref this._isInitialized, value );
    }

    /// <summary> Notifies of the Initialized event. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected virtual void OnInitialized( EventArgs e )
    {
        this.IsInitialized = true;
        this.NotifyInitialized( e );
    }

    /// <summary> Event queue for all listeners interested in Initialized events. </summary>
    public event EventHandler<EventArgs>? Initialized;

    /// <summary>
    /// Synchronously invokes the <see cref="Initialized">Initialized Event</see>. Not thread safe.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="e"> The <see cref="EventArgs" /> instance containing the event data. </param>
    protected void NotifyInitialized( EventArgs e )
    {
        this.Initialized?.Invoke( this, e );
    }

    /// <summary>   Removes the Initialized event handlers. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    protected void RemoveInitializedEventHandlers()
    {
        EventHandler<EventArgs>? handler = this.Initialized;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( EventHandler<EventArgs> ) item;
            }
        }
    }

    #endregion

    #region " identity "

    private string _identity = string.Empty;

    /// <summary> Gets or sets the Identity. </summary>
    /// <value> The Identity. </value>
    public string Identity
    {
        get => this._identity;
        set => _ = this.SetProperty( ref this._identity, (value ?? string.Empty).Trim() );
    }

    #endregion

    #region " commands "

    /// <summary> Opens a resource. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="resourceName">  The name of the resource. </param>
    /// <param name="resourceModel"> The resource model. </param>
    public virtual void OpenResource( string resourceName, string resourceModel )
    {
        if ( this.IsOpen )
        {
            this.ValidatedResourceName = resourceName;
            this.CandidateResourceNameValidated = string.Equals( this.ValidatedResourceName, this.CandidateResourceName, StringComparison.Ordinal );
            this.OpenResourceName = resourceName;
            this.OpenResourceModel = resourceModel;
            this.CandidateResourceModel = resourceModel;
        }
    }

    /// <summary>   Opens a resource. </summary>
    /// <remarks>   David, 2021-02-27. </remarks>
    public virtual void OpenResource()
    {
        this.OpenResource( this.ValidatedResourceName, this.CandidateResourceModel );
    }

    /// <summary> Attempts to open resource from the given data. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="resourceName">  The name of the resource. </param>
    /// <param name="resourceModel"> The resource model. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    public abstract (bool success, string Details) TryOpen( string resourceName, string resourceModel );

    /// <summary> Attempts to open resource from the given data. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    public virtual (bool success, string Details) TryOpen()
    {
        return this.TryOpen( this.ValidatedResourceName, this.CandidateResourceModel );
    }

    /// <summary> Closes the resource. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    public abstract void CloseResource();

    /// <summary> Attempts to close resource from the given data. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    public abstract (bool success, string Details) TryClose();

    /// <summary> Applies default settings and clears the resource active state. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    public abstract void ClearActiveState();

    /// <summary>   Attempts to clear active state. </summary>
    /// <remarks>   David, 2020-09-21. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <returns>   (Success: True if success; false otherwise, Details)  </returns>
    public abstract (bool success, string Details) TryClearActiveState();

    #endregion
}
