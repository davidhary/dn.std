using System.ComponentModel;

namespace cc.isr.Std.Notifiers;

/// <summary> Comparable and equitable value Notifier. </summary>
/// <remarks>
/// This single-value class can be used to raise property change events. This could be used, for
/// example, to send a message. <para>
/// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-10-10 </para>
/// </remarks>
public class AtomNotifier<T> : INotifyPropertyChanged where T : IComparable<T>, IEquatable<T>
{
    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    public AtomNotifier() : base()
    { }

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary> The value. </summary>
    private T? _value;

    /// <summary> Gets or sets the value. </summary>
    /// <value> The value. </value>
    public T? Value
    {
        get => this._value;
        set
        {
            if ( !EqualityComparer<T>.Default.Equals( (value, this._value) ) )
            {
                this._value = value;
                this.NotifyPropertyChanged();
            }
        }
    }

}
