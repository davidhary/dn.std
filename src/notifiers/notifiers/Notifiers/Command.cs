using System.Windows.Input;

namespace cc.isr.Std.Notifiers;

/// <summary> Implements <see cref="ICommand"/>. </summary>
/// <remarks>
/// About <see cref="ICommand"/> implementation <para>
/// One can add one's own functionality relatively easily with attached behaviors (pure MVVM):
/// you can subscribe to certain View events (you can do it in the View code-behind, but that
/// would be dirty MVVM) to call certain methods of ViewModel (directly, via interface or using
/// reflection). ICommand was a bare minimum what WPF needs, namely it solves problem when menu
/// items/buttons become disabled e.g. for Copy/Paste scenario. Nothing more. In fact
/// ButtonBase.Command can be a simple Click event handler, you can right away call ViewModel
/// method there </para><para>
/// Both approaches are correct, they follow MVVM principles And works without ICommand, but As
/// you can see, neither Is As elegant As ICommand</para><para>
/// For additional references see:
/// https://StackOverflow.com/questions/1685088/binding-to-commands-in-WinForms  </para><para>
/// https://StackOverflow.com/questions/42113388/what-is-the-reason-for-iCommand-in-MVVM
/// </para><para>
/// iCommand serves these purposes:</para><para>
/// <list type="bullet"><item>
/// It wraps a method to an Object;</item><item>
/// It determines if the command is available so the UI component (typically a button Or menu
/// item) can reflect it;</item><item>
/// It notifies the UI components of changes in the availability of the command has changed, so
/// the UI can reflect it (e.g., a relevant UI component can be enabled).</item></list>
/// In the classic cut and paste example, without <see cref="ICommand"/>
/// implementation, both the paste command and paste availability (can paste) have to be
/// addressed at the UI level, which, aside of breaking the DRY principle, adds complexity to the
/// code at the UI level. </para><para>
/// For example, In the classic cut and paste example, without
/// <see cref="ICommand"/>
/// implementation, both the paste command and paste availability (can paste) have to be
/// addressed at the UI level, which, aside of breaking the DRY principle, adds complexity to the
/// code at the UI level. </para><para>
/// On the other hand:</para><para>
/// <list type="bullet"><item>
/// While reducing complexity on the UI code behind, this adds complexity to the view-model;
/// </item><item>
/// This makes debugging the UI/Model interfaces hardware as the model is addressed via an action
/// call;</item><item>
/// Command Binding and command management is not fully implemented in Windows
/// Forms.</item></list>
/// </para> <para>
/// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2018-12-10 </para>
/// </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 2020-09-21. </remarks>
/// <param name="action"> The action. </param>
public class Command( Action action ) : ICommand
{
    /// <summary>
    /// Defines the method that determines whether the command can execute in its current state.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="parameter"> Data used by the command.  If the command does not require data to be
    /// passed, this object can be set to null. </param>
    /// <returns> true if this command can be executed; otherwise, false. </returns>
    public bool CanExecute( object parameter )
    {
        return true;
    }

    event EventHandler? ICommand.CanExecuteChanged
    {
        add => throw new NotImplementedException();

        remove => throw new NotImplementedException();
    }

    /// <summary>   Delegate for handling CanExecuteChanged events. </summary>
    /// <remarks>   David, 2020-09-21. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    public delegate void CanExecuteChangedEventHandler( object? sender, EventArgs e );

    /// <summary> The action. </summary>
    private readonly Action _action = action;

    /// <summary> Defines the method to be called when the command is invoked. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="parameter"> Data used by the command.  If the command does not require data to be
    /// passed, this object can be set to null. </param>
    public void Execute( object parameter )
    {
        this._action();
    }
}
/// <summary> A predicated command. </summary>
/// <remarks>
/// <para>
/// David, 2018-12-10 </para>
/// </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 2020-09-21. </remarks>
/// <param name="action">    The action. </param>
/// <param name="predicate"> The predicate. </param>
public class PredicatedCommand( Action action, Predicate<object> predicate ) : ICommand
{
    /// <summary> The action. </summary>
    private readonly Action _action = action;

    /// <summary> Gets or sets the predicate. </summary>
    /// <value> The predicate. </value>
    private Predicate<object> Predicate { get; set; } = predicate;

    event EventHandler? ICommand.CanExecuteChanged
    {
        add => throw new NotImplementedException();

        remove => throw new NotImplementedException();
    }

    /// <summary> Gets or sets the can execute sentinel. </summary>
    /// <value> The can execute sentinel. </value>
    private bool CanExecuteSentinel { get; set; }

    /// <summary> Raises the can execute changed event. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="parameter"> Data used by the command.  If the command does not require data to be
    /// passed, this object can be set to null. </param>
    /// <returns> True if it succeeds; otherwise, false. </returns>
    private bool RaiseCanExecuteChanged( object parameter )
    {
        if ( this.CanExecuteSentinel != this.Predicate( parameter ) )
        {
            this.CanExecuteSentinel = !this.CanExecuteSentinel;
            this.CanExecuteChanged?.Invoke( this, EventArgs.Empty );
        }

        return this.CanExecuteSentinel;
    }

    /// <summary>
    /// Defines the method that determines whether the command can execute in its current state.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="parameter"> Data used by the command.  If the command does not require data to be
    /// passed, this object can be set to null. </param>
    /// <returns> true if this command can be executed; otherwise, false. </returns>
    public bool CanExecute( object parameter )
    {
        return this.RaiseCanExecuteChanged( parameter );
    }

    /// <summary> Event queue for all listeners interested in CanExecuteChanged events. </summary>
    public event CanExecuteChangedEventHandler? CanExecuteChanged;

    /// <summary>   Delegate for handling CanExecuteChanged events. </summary>
    /// <remarks>   David, 2020-09-21. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    public delegate void CanExecuteChangedEventHandler( object? sender, EventArgs e );

    /// <summary> Defines the method to be called when the command is invoked. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="parameter"> Data used by the command.  If the command does not require data to be
    /// passed, this object can be set to null. </param>
    public void Execute( object parameter )
    {
        if ( this.CanExecute( parameter ) )
        {
            this._action();
        }
    }
}
/// <summary> Implements <see cref="ICommand"/>. </summary>
/// <remarks>
/// <para>
/// David, 2018-12-10 </para>
/// </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 2020-09-21. </remarks>
/// <param name="action">    The action. </param>
/// <param name="predicate"> The predicate. </param>
public class GenericCommand<T>( Action<T> action, Predicate<T> predicate ) : ICommand where T : IComparable<T>, IEquatable<T>, IFormattable
{
    /// <summary> The action. </summary>
    private readonly Action<T> _action = action;

    /// <summary> Gets or sets the predicate. </summary>
    /// <value> The predicate. </value>
    private Predicate<T> Predicate { get; set; } = predicate;

    /// <summary> Gets or sets the can execute sentinel. </summary>
    /// <value> The can execute sentinel. </value>
    private bool CanExecuteSentinel { get; set; }

    /// <summary> Raises the can execute changed event. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="parameter"> Data used by the command.  If the command does not require data to be
    /// passed, this object can be set to <see langword="null" />. </param>
    /// <returns> True if it succeeds; otherwise, false. </returns>
    private bool RaiseCanExecuteChanged( T parameter )
    {
        if ( this.CanExecuteSentinel != this.Predicate( parameter ) )
        {
            this.CanExecuteSentinel = !this.CanExecuteSentinel;
            this.CanExecuteChanged?.Invoke( this, EventArgs.Empty );
        }

        return this.CanExecuteSentinel;
    }

    /// <summary>
    /// Defines the method that determines whether the command can execute in its current state.
    /// </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="parameter"> Data used by the command.  If the command does not require data to be
    /// passed, this object can be set to <see langword="null" />. </param>
    /// <returns>
    /// <see langword="true" /> if this command can be executed; otherwise, <see langword="false" />.
    /// </returns>
    public bool CanExecute( object parameter )
    {
        return this.RaiseCanExecuteChanged( ( T ) parameter );
    }

    /// <summary> Event queue for all listeners interested in CanExecuteChanged events. </summary>
    public event CanExecuteChangedEventHandler? CanExecuteChanged;

    event EventHandler? ICommand.CanExecuteChanged
    {
        add => throw new NotImplementedException();

        remove => throw new NotImplementedException();
    }

    /// <summary>   Delegate for handling CanExecuteChanged events. </summary>
    /// <remarks>   David, 2020-09-21. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    public delegate void CanExecuteChangedEventHandler( object? sender, EventArgs e );

    /// <summary> Defines the method to be called when the command is invoked. </summary>
    /// <remarks> David, 2020-09-21. </remarks>
    /// <param name="parameter"> Data used by the command.  If the command does not require data to be
    /// passed, this object can be set to <see langword="null" />. </param>
    public void Execute( object parameter )
    {
        if ( this.CanExecute( parameter ) )
        {
            this._action( ( T ) parameter );
        }
    }
}
