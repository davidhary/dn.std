using cc.isr.Std.Cartesian.EnumerableStats;
using cc.isr.Std.Tests.Extensions;

namespace cc.isr.Std.Cartesian.Tests;

/// <summary> Summary description for HistogramTests. </summary>
/// <remarks> David, 2020-09-23. </remarks>
[TestClass]
public class HistogramTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    /// <summary> Tests histogram array. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    private static void TestHistogramArray()
    {
        // use a fixed seed to get a predictable random.
        double[] source = [.. new Random( 1 ).GenerateRandomNormals( 10000 )];
        double lowerLimit = -2;
        double upperLimit = 2d;
        int count = 20;
        double binWidth = (upperLimit - lowerLimit) / count;
        IList<CartesianPoint<double>> result;
        Stopwatch sw = Stopwatch.StartNew();
        _ = source.Histogram( lowerLimit, upperLimit, count );
        sw.Restart();
        // needs to run twice to make sure the code is compiled.
        result = source.Histogram( lowerLimit, upperLimit, count );
        long directSpeed = sw.ElapsedTicks;
        long linqSpeed = 33235L;
        Assert.IsTrue( directSpeed < linqSpeed, $"Expected speed {directSpeed} to be lower than {linqSpeed}" );


        // count test: There are two extra bins above the high and below the low limits.
        Assert.AreEqual( result.Count, count + 2 );

        // abscissa range test: First bin is at the low limit; last is at the high limit.
        Assert.AreEqual( lowerLimit, result[0].X, 0.1d * binWidth );
        Assert.AreEqual( upperLimit, result[count + 1].X, 0.1d * binWidth );

        // Second bin is at half the bin width past the low limit; last off by half bin width of the last bin.
        Assert.AreEqual( lowerLimit + 0.5d * binWidth, result[1].X, 0.1d * binWidth );
        Assert.AreEqual( upperLimit - 0.5d * binWidth, result[count].X, 0.1d * binWidth );

        // expected value assuming random returns the same values each time.
        int expectedLowCount = 208;
        Assert.AreEqual( expectedLowCount, ( int ) result[0].Y );
        int expectedHighCount = 230;
        Assert.AreEqual( expectedHighCount, ( int ) result[count + 1].Y );
    }

    /// <summary> Tests histogram list. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    private static void TestHistogramList()
    {
        // use a fixed seed to get a predictable random.
        IList<double> source = new Random( 1 ).GenerateRandomNormals( 10000 );
        double lowerLimit = -2;
        double upperLimit = 2d;
        int count = 20;
        double binWidth = (upperLimit - lowerLimit) / count;
        IList<CartesianPoint<double>> result;
        Stopwatch sw = Stopwatch.StartNew();
        _ = source.Histogram( lowerLimit, upperLimit, count );
        sw.Restart();
        // needs to run twice to make sure the code is compiled.
        result = source.Histogram( lowerLimit, upperLimit, count );
        long directSpeed = sw.ElapsedTicks;
        long linqSpeed = 33235L;
        Assert.IsTrue( directSpeed < linqSpeed, $"Expected speed {directSpeed} to be lower than {linqSpeed}" );


        // count test: There are two extra bins above the high and below the low limits.
        Assert.AreEqual( result.Count, count + 2 );

        // abscissa range test: First bin is at the low limit; last is at the high limit.
        Assert.AreEqual( lowerLimit, result[0].X, 0.1d * binWidth );
        Assert.AreEqual( upperLimit, result[count + 1].X, 0.1d * binWidth );

        // Second bin is at half the bin width past the low limit; last off by half bin width of the last bin.
        Assert.AreEqual( lowerLimit + 0.5d * binWidth, result[1].X, 0.1d * binWidth );
        Assert.AreEqual( upperLimit - 0.5d * binWidth, result[count].X, 0.1d * binWidth );

        // expected value assuming random returns the same values each time.
        int expectedLowCount = 208;
        Assert.AreEqual( expectedLowCount, ( int ) result[0].Y );
        int expectedHighCount = 230;
        Assert.AreEqual( expectedHighCount, ( int ) result[count + 1].Y );
    }

    /// <summary> (Unit Test Method) tests histogram. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod]
    public void HistogramTest()
    {
        TestHistogramList();
        TestHistogramArray();
        return;
    }
}
