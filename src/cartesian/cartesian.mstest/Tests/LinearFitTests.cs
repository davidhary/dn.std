using cc.isr.Std.Cartesian;

namespace cc.isr.Std.Cartesian.Tests;

/// <summary> Linear fit tests. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-02-19 </para>
/// </remarks>
[TestClass]
public class LinearFitTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    /// <summary> (Unit Test Method) builds determinant test method. </summary>
    /// <remarks>
    /// Uses data from c:\my\lib\vs\core\std\tests\cartesian\math\PolyFitTests.cs.
    /// </remarks>
    [TestMethod()]
    public void LinearFitTest()
    {
        LinearFit linFit = new();
        double[] height = [1.47d, 1.5d, 1.52d, 1.55d, 1.57d, 1.6d, 1.63d, 1.65d, 1.68d, 1.7d, 1.73d, 1.75d, 1.78d, 1.8d, 1.83d];
        double[] mass = [52.21d, 53.12d, 54.48d, 55.84d, 57.2d, 58.57d, 59.93d, 61.29d, 63.11d, 64.47d, 66.28d, 68.1d, 69.92d, 72.19d, 74.46d];
        List<CartesianPoint<double>> points = [];
        for ( int i = 0, loopTo = height.Length - 1; i <= loopTo; i++ )
            points.Add( new CartesianPoint<double>( height[i], mass[i] ) );

        double slope = 61.272d;
        double intercept = -39.062d;
        double correlationCoefficient = 0.9945d;
        double goodness = correlationCoefficient * correlationCoefficient;
        double linearStandardError = 1.775d;
        double constantStandardError = 2.938d;
        _ = linFit.DoFit( height, mass );
        Assert.AreEqual( slope, linFit.LinearCoefficient, 0.001d, "Slope" );
        Assert.AreEqual( intercept, linFit.ConstantCoefficient, 0.001d, "intercept" );
        Assert.AreEqual( goodness, linFit.GoodnessOfFit, 0.001d, "Goodness of fit" );
        Assert.AreEqual( linearStandardError, linFit.LinearCoefficientStandardError, 0.001d, "Linear coefficient Standard error" );
        Assert.AreEqual( constantStandardError, linFit.ConstantCoefficientStandardError, 0.001d, "Constant coefficient Standard error" );
        _ = linFit.DoFit( points );
        Assert.AreEqual( slope, linFit.LinearCoefficient, 0.001d, "Slope" );
        Assert.AreEqual( intercept, linFit.ConstantCoefficient, 0.001d, "intercept" );
        Assert.AreEqual( goodness, linFit.GoodnessOfFit, 0.001d, "Goodness of fit" );
        Assert.AreEqual( linearStandardError, linFit.LinearCoefficientStandardError, 0.001d, "Linear coefficient Standard error" );
        Assert.AreEqual( constantStandardError, linFit.ConstantCoefficientStandardError, 0.001d, "Constant coefficient Standard error" );
    }
}
