using System.Data;
using cc.isr.Std.Cartesian;

namespace cc.isr.Std.Cartesian.Tests;

/// <summary> A polygon fit tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-04-13 </para>
/// </remarks>
[TestClass]
public class PolyFitTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    /// <summary> Builds a determinant. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="values"> The values. </param>
    /// <returns> A Double(,) </returns>
    public static double[,] BuildDeterminant( IEnumerable<double> values )
    {
        int colCount = 3;
        int rowCount = 3;
        double[,] a;
        a = new double[colCount, rowCount];
        int i = 0;
        for ( int r = 0, loopTo = rowCount - 1; r <= loopTo; r++ )
            for ( int c = 0, loopTo1 = colCount - 1; c <= loopTo1; c++ )
            {
                a[c, r] = values.ElementAtOrDefault( i );
                i += 1;
            }

        return a;
    }

    /// <summary> (Unit Test Method) builds determinant test method. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void BuildDeterminantTestMethod()
    {
        double[] values = [2d, -3, -2, -6, 3d, 3d, -2, -3, -2];
        double[,] determinant = BuildDeterminant( values );
        int columnNumber = 0;
        int rowNumber = 0;
        double expectedValue = 2d;
        double actualValue = determinant[columnNumber, rowNumber];
        Assert.AreEqual( expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})" );
        columnNumber = 1;
        expectedValue = -3;
        actualValue = determinant[columnNumber, rowNumber];
        Assert.AreEqual( expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})" );
        rowNumber = 2;
        expectedValue = -3;
        actualValue = determinant[columnNumber, rowNumber];
        Assert.AreEqual( expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})" );

        // actualValue = QuadraticPolynomial.Determinant(Me.BuildDeterminant(values))
        // Assert.AreEqual(expectedValue, actualValue)
    }

    /// <summary> (Unit Test Method) calculates the determinant test method. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void CalculateDeterminantTestMethod()
    {
        double[] values = [2d, -3, -2, -6, 3d, 3d, -2, -3, -2];
        double expectedValue = 12d;
        double actualValue = Cartesian.QuadraticPolynomial.Determinant( BuildDeterminant( values ) );
        Assert.AreEqual( expectedValue, actualValue );
        values = [-4, 5d, 2d, -3, 4d, 2d, -1, 2d, 5d];
        expectedValue = -3;
        actualValue = Cartesian.QuadraticPolynomial.Determinant( BuildDeterminant( values ) );
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) Cramer substitution test method. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void CramerSubstitutionTestMethod()
    {
        double[] values = [2d, -1, 6d, -3, 4d, -5, 8d, -7, -9];
        double[] constants = [10d, 11d, 12d];
        double[,] coefficients = BuildDeterminant( values );
        double expectedValue = -141;
        double determinant = Cartesian.QuadraticPolynomial.Determinant( coefficients );
        double actualValue = determinant;
        Assert.AreEqual( expectedValue, actualValue );
        double[,] result = Cartesian.QuadraticPolynomial.CramerSubstitution( 0, coefficients, constants );
        int columnNumber = 0;
        int rowNumber = 0;
        expectedValue = 10d;
        actualValue = result[columnNumber, rowNumber];
        Assert.AreEqual( expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})" );
        columnNumber = 1;
        expectedValue = -1;
        actualValue = result[columnNumber, rowNumber];
        Assert.AreEqual( expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})" );
        rowNumber = 2;
        expectedValue = -7;
        actualValue = result[columnNumber, rowNumber];
        Assert.AreEqual( expectedValue, actualValue, $"(column,row)=({columnNumber},{rowNumber})" );
    }

    /// <summary> (Unit Test Method) Cramer rule test method. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void CramerRuleTestMethod()
    {
        double[] values = [2d, -1, 6d, -3, 4d, -5, 8d, -7, -9];
        double[] constants = [10d, 11d, 12d];
        double[,] coefficients = BuildDeterminant( values );
        double expectedValue = -141;
        double determinant = Cartesian.QuadraticPolynomial.Determinant( coefficients );
        double actualValue = determinant;
        Assert.AreEqual( expectedValue, actualValue );
        double[] result = Cartesian.QuadraticPolynomial.CramerRule( coefficients, constants );
        expectedValue = -1499;
        actualValue = result[0];
        Assert.AreEqual( expectedValue, actualValue );
        expectedValue = -1492;
        actualValue = result[1];
        Assert.AreEqual( expectedValue, actualValue );
        expectedValue = 16d;
        actualValue = result[2];
        Assert.AreEqual( expectedValue, actualValue );
    }

    /// <summary> (Unit Test Method) polynomial fit test method three points. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void PolyFitTestMethodThreePoints()
    {
        QuadraticPolynomial quadPoly = new( 100d, 0.5d, 0.02d );
        List<CartesianPoint<double>> values = [];
        double x = -50;
        values.Add( new CartesianPoint<double>( x, quadPoly.Evaluate( x ) ) );
        x = 1d;
        values.Add( new CartesianPoint<double>( x, quadPoly.Evaluate( x ) ) );
        x = 75d;
        values.Add( new CartesianPoint<double>( x, quadPoly.Evaluate( x ) ) );
        QuadraticPolynomial fitPoly = new();
        _ = fitPoly.PolyFit( values );
        double expectedValue = 1d;
        double actualValue = fitPoly.GoodnessOfFit;
        Assert.AreEqual( expectedValue, actualValue, 0.001d );
        expectedValue = 0d;
        actualValue = fitPoly.StandardError;
        Assert.AreEqual( expectedValue, actualValue, 0.001d );
        expectedValue = quadPoly.ConstantCoefficient;
        actualValue = fitPoly.ConstantCoefficient;
        Assert.AreEqual( expectedValue, actualValue, 0.001d );
        expectedValue = quadPoly.LinearCoefficient;
        actualValue = fitPoly.LinearCoefficient;
        Assert.AreEqual( expectedValue, actualValue, 0.001d );
        expectedValue = quadPoly.QuadraticCoefficient;
        actualValue = fitPoly.QuadraticCoefficient;
        Assert.AreEqual( expectedValue, actualValue, 0.001d );
    }

    /// <summary> (Unit Test Method) polygon fit test method four points. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void PolyFitTestMethodFourPoints()
    {
        QuadraticPolynomial quadPoly = new( 100d, 0.5d, 0.02d );
        List<CartesianPoint<double>> values = [];
        double x = -50;
        values.Add( new CartesianPoint<double>( x, quadPoly.Evaluate( x ) ) );
        x = 1d;
        values.Add( new CartesianPoint<double>( x, quadPoly.Evaluate( x ) ) );
        x = 75d;
        values.Add( new CartesianPoint<double>( x, quadPoly.Evaluate( x ) ) );
        x = 125d;
        values.Add( new CartesianPoint<double>( x, quadPoly.Evaluate( x ) ) );
        QuadraticPolynomial fitPoly = new();
        _ = fitPoly.PolyFit( values );
        double expectedValue = 1d;
        double actualValue = fitPoly.GoodnessOfFit;
        Assert.AreEqual( expectedValue, actualValue, 0.001d );
        expectedValue = 0d;
        actualValue = fitPoly.StandardError;
        Assert.AreEqual( expectedValue, actualValue, 0.001d );
        expectedValue = quadPoly.ConstantCoefficient;
        actualValue = fitPoly.ConstantCoefficient;
        Assert.AreEqual( expectedValue, actualValue, 0.001d );
        expectedValue = quadPoly.LinearCoefficient;
        actualValue = fitPoly.LinearCoefficient;
        Assert.AreEqual( expectedValue, actualValue, 0.001d );
        expectedValue = quadPoly.QuadraticCoefficient;
        actualValue = fitPoly.QuadraticCoefficient;
        Assert.AreEqual( expectedValue, actualValue, 0.001d );
    }

    /// <summary> (Unit Test Method) polygon fit test method four points with error. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void PolyFitTestMethodFourPointsWithError()
    {
        QuadraticPolynomial quadPoly = new( 100d, 0.5d, 0.02d );
        List<CartesianPoint<double>> values = [];
        double x = -50;
        double e = 0.1d;
        values.Add( new CartesianPoint<double>( x, e + quadPoly.Evaluate( x ) ) );
        double ssq = Math.Pow( e, 2d );
        x = 1d;
        e = 0.001d;
        values.Add( new CartesianPoint<double>( x, e + quadPoly.Evaluate( x ) ) );
        ssq += Math.Pow( e, 2d );
        x = 75d;
        e = 0.02d;
        values.Add( new CartesianPoint<double>( x, e + quadPoly.Evaluate( x ) ) );
        ssq += Math.Pow( e, 2d );
        x = 125d;
        e = 0.003d;
        values.Add( new CartesianPoint<double>( x, e + quadPoly.Evaluate( x ) ) );
        ssq += Math.Pow( e, 2d );
        QuadraticPolynomial fitPoly = new();
        _ = fitPoly.PolyFit( values );
        double expectedValue = 1d;
        double actualValue = fitPoly.GoodnessOfFit;
        Assert.AreEqual( expectedValue, actualValue, 0.001d );
        double delta = Math.Sqrt( ssq / values.Count );
        expectedValue = 0d;
        actualValue = fitPoly.StandardError;
        Assert.AreEqual( expectedValue, actualValue, delta );
        expectedValue = quadPoly.ConstantCoefficient;
        actualValue = fitPoly.ConstantCoefficient;
        Assert.AreEqual( expectedValue, actualValue, delta );
        expectedValue = quadPoly.LinearCoefficient;
        actualValue = fitPoly.LinearCoefficient;
        Assert.AreEqual( expectedValue, actualValue, delta );
        expectedValue = quadPoly.QuadraticCoefficient;
        actualValue = fitPoly.QuadraticCoefficient;
        Assert.AreEqual( expectedValue, actualValue, delta );
    }

    /// <summary> (Unit Test Method) polygon fit test method four data points. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void PolyFitTestMethodFourDataPoints()
    {
        List<CartesianPoint<double>> values = [];
        double xo = 273d;
        CartesianPoint<double> p1 = new( 303d, 1048d );
        values.Add( p1 );
        CartesianPoint<double> p2 = new( 327d, 1161d );
        values.Add( p2 );
        CartesianPoint<double> p3 = new( 345d, 1239d );
        values.Add( p3 );
        CartesianPoint<double> p4 = new( 350d, 1257d );
        values.Add( p4 );
        QuadraticPolynomial fitPoly = new();
        _ = fitPoly.PolyFit( values );
        double expectedValue = 1d;
        double actualValue = fitPoly.GoodnessOfFit;
        Assert.AreEqual( expectedValue, actualValue, 0.1d );
        expectedValue = 0.01d + p1.X;
        double delta = fitPoly.StandardError;
        Assert.IsTrue( delta < expectedValue, $"Is low standard error {delta}<{expectedValue}" );
        delta = 3d * delta;
        expectedValue = fitPoly.Evaluate( p1.X );
        Assert.AreEqual( expectedValue, p1.Y, delta );
        expectedValue = fitPoly.Evaluate( p2.X );
        Assert.AreEqual( expectedValue, p2.Y, delta );
        expectedValue = fitPoly.Evaluate( p3.X );
        Assert.AreEqual( expectedValue, p3.Y, delta );
        expectedValue = fitPoly.Evaluate( p4.X );
        Assert.AreEqual( expectedValue, p4.Y, delta );
        expectedValue = 1000d;
        actualValue = fitPoly.Evaluate( xo + 25d );
        Assert.IsTrue( actualValue > expectedValue, $"Nominal value {actualValue}>{expectedValue}" );
        expectedValue = 0d;
        actualValue = fitPoly.Slope( p1.X );
        Assert.IsTrue( actualValue > expectedValue, $"Positive slope @{p1.X} {actualValue}>{expectedValue}" );
        actualValue = fitPoly.Slope( p2.X );
        Assert.IsTrue( actualValue > expectedValue, $"Positive slope @{p2.X} {actualValue}>{expectedValue}" );
        actualValue = fitPoly.Slope( p3.X );
        Assert.IsTrue( actualValue > expectedValue, $"Positive slope @{p3.X} {actualValue}>{expectedValue}" );
        actualValue = fitPoly.Slope( p4.X );
        Assert.IsTrue( actualValue > expectedValue, $"Positive slope @{p4.X} {actualValue}>{expectedValue}" );
    }
}
