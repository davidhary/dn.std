using cc.isr.Json.AppSettings.Models;

namespace cc.isr.Std.Cartesian.Tests;

/// <summary> Test information for the Polynomial Fit Tests. </summary>
/// <remarks> <para>
/// David, 2018-02-12 </para></remarks>
internal sealed class PolyFitTestInfo : CommunityToolkit.Mvvm.ComponentModel.ObservableObject
{
    #region " constructions "

    public PolyFitTestInfo() { }

    #endregion

    #region " singleton "

    /// <summary>   Creates an instance of the <see cref="PolyFitTestInfo"/> after restoring the 
    /// application context settings to both the user and all user files. </summary>
    /// <remarks>   2023-05-15. </remarks>
    /// <returns>   The new instance. </returns>
    private static PolyFitTestInfo CreateInstance()
    {
        // get assembly files using the .Logging suffix.

        AssemblyFileInfo ai = new( typeof( PolyFitTestInfo ).Assembly, null, ".Settings", ".json" );

        // must copy application context settings here to clear any bad settings files.

        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.AllUsersAssemblyFilePath! );
        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.ThisUserAssemblyFilePath! );

        PolyFitTestInfo ti = new();
        AppSettingsScribe.ReadSettings( ai.AllUsersAssemblyFilePath!, nameof( PolyFitTestInfo ), ti );

        return ti;
    }

    /// <summary>   Gets the instance. </summary>
    /// <value> The instance. </value>
    public static PolyFitTestInfo Instance => _instance.Value;

    private static readonly Lazy<PolyFitTestInfo> _instance = new( CreateInstance, true );

    #endregion

    #region " configuration information "

    private TraceLevel _traceLevel = TraceLevel.Verbose;

    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    [System.ComponentModel.Description( "Sets the message level" )]
    public TraceLevel TraceLevel
    {
        get => this._traceLevel;
        set => _ = this.SetProperty( ref this._traceLevel, value );
    }

    private bool _enabled = true;

    /// <summary>   Gets or sets a value indicating whether this object is enabled. </summary>
    /// <value> True if enabled, false if not. </value>
    [System.ComponentModel.Description( "True if testing is enabled for this test class" )]
    public bool Enabled
    {
        get => this._enabled;
        set => this.SetProperty( ref this._enabled, value );
    }

    private bool _all = true;

    /// <summary> Gets or sets all. </summary>
    /// <value> all. </value>
    [System.ComponentModel.Description( "True if all testing is enabled for this test class" )]
    public bool All
    {
        get => this._all;
        set => this.SetProperty( ref this._all, value );
    }

    #endregion

    #region " poly fit "

    /// <summary> Gets or sets the number of minimum elements. </summary>
    /// <value> The number of minimum elements. </value>
    public int MinimumElementCount { get; set; }

    #endregion
}
