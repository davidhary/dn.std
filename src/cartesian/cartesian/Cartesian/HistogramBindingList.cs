using System.ComponentModel;

namespace cc.isr.Std.Cartesian;

/// <summary> A histogram binding list. </summary>
/// <remarks> David, 2020-06-27. </remarks>
public class HistogramBindingList : BindingList<CartesianPoint<double>>
{
    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-06-27. </remarks>
    /// <param name="lowerBound"> The lower bound. </param>
    /// <param name="upperBound"> The upper bound. </param>
    /// <param name="binCount">   The number of bins. </param>
    public HistogramBindingList( double lowerBound, double upperBound, int binCount ) : base()
    {
        this.BinCount = binCount;
        this.LowerBound = lowerBound;
        this.UpperBound = upperBound;
        this.BinWidth = (upperBound - lowerBound) / binCount;
        this.InverseWidth = 1d / this.BinWidth;
        this._counts = ( int[] ) Array.CreateInstance( typeof( int ), binCount + 2 );
    }

    /// <summary> The counts. </summary>
    private readonly int[] _counts;

    /// <summary> Gets the counts. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <returns> An Integer() </returns>
    public int[] Counts()
    {
        return this._counts;
    }

    /// <summary> Gets or sets the number of bins. </summary>
    /// <value> The number of bins. </value>
    public int BinCount { get; set; }

    /// <summary>   Gets or sets the number of totals. </summary>
    /// <value> The total number of count. </value>
    public int TotalCount { get; private set; }

    /// <summary> Gets or sets the width of the bin. </summary>
    /// <value> The width of the bin. </value>
    public double BinWidth { get; set; }

    /// <summary> Gets or sets the width of the inverse. </summary>
    /// <value> The width of the inverse. </value>
    private double InverseWidth { get; set; }

    /// <summary> Gets or sets the lower bound. </summary>
    /// <value> The lower bound. </value>
    public double LowerBound { get; set; }

    /// <summary> Gets or sets the upper bound. </summary>
    /// <value> The upper bound. </value>
    public double UpperBound { get; set; }

    /// <summary> Converts a value to a bin number. </summary>
    /// <remarks> David, 2020-06-27. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> Value as an Integer. </returns>
    public int ToBinNumber( double value )
    {
        double binValue = this.InverseWidth * (value - this.LowerBound);
        return binValue < 0 ? 0 : binValue > this.BinCount ? this.BinCount + 1 : 1 + ( int ) Math.Truncate( binValue );
    }

    /// <summary> Initializes this. </summary>
    /// <remarks> David, 2020-06-27. </remarks>
    public void Initialize()
    {
        this.Clear();
        this.TotalCount = 0;
        for ( int i = 0, loopTo = this.Counts().Length - 1; i <= loopTo; i++ )
        {
            double x;
            if ( i == 0 )
            {
                x = this.LowerBound;
            }
            else if ( i == this.Counts().Length - 1 )
            {
                x = this.UpperBound;
            }
            else
            {
                // x = lowerBound + 0.5 * binWidth + binWidth * (i - 1)
                x = this.LowerBound + (this.BinWidth * (i - 0.5d));
            }

            this.Add( new CartesianPoint<double>( x, this.Counts()[i] ) );
        }
    }

    /// <summary> Initializes this. </summary>
    /// <remarks> David, 2020-09-05. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="source"> Source for the. </param>
    public void Initialize( double[] source )
    {
        if ( source is null )
        {
            throw new ArgumentNullException( nameof( source ) );
        }

        foreach ( double x in source )
        {
            this._counts[this.ToBinNumber( x )] += 1;
        }

        this.Initialize();
    }

    /// <summary> Initializes this. </summary>
    /// <remarks> David, 2020-09-05. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="source"> Source for the. </param>
    public void Initialize( IEnumerable<double> source )
    {
        if ( source is null )
        {
            throw new ArgumentNullException( nameof( source ) );
        }

        foreach ( double x in source )
        {
            this._counts[this.ToBinNumber( x )] += 1;
        }

        this.Initialize();
    }

    /// <summary> Initializes this. </summary>
    /// <remarks> David, 2020-06-27. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="source"> Source for the. </param>
    public void Initialize( IList<double> source )
    {
        if ( source is null )
        {
            throw new ArgumentNullException( nameof( source ) );
        }

        foreach ( double x in source )
        {
            this._counts[this.ToBinNumber( x )] += 1;
        }

        this.Initialize();
    }

    /// <summary>   Updates the given value. </summary>
    /// <remarks>   David, 2020-06-27. </remarks>
    /// <param name="value">    The value. </param>
    /// <returns>   The total count. </returns>
    public int Update( double value )
    {
        int binNumber = this.ToBinNumber( value );
        this._counts[binNumber] += 1;
        CartesianPoint<double> currentPoint = this[binNumber];
        this[binNumber] = new CartesianPoint<double>( currentPoint.X, currentPoint.Y + 1d );
        this.TotalCount += 1;
        return this.TotalCount;
    }

    /// <summary> Updates the given value. </summary>
    /// <remarks> David, 2020-06-27. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns>   The total count. </returns>
    public int Update( double? value )
    {
        return value.HasValue ? this.Update( value.Value ) : this.TotalCount;
    }
}
