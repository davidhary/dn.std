namespace cc.isr.Std.BindingLists.Tests;

/// <summary>   A binding tests. </summary>
/// <remarks>   David, 2020-09-22. </remarks>
[TestClass]
public partial class SortableBindingListTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " sortable binding list "

    /// <summary> A sale. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    private sealed class Sale
    {
        /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public Sale()
        {
            this.SaleDate = DateTimeOffset.Now;
            this.Salesman = string.Empty;
            this.SaleDetails = [];
            this.Client = string.Empty;
        }

        /// <summary> Gets the sale details. </summary>
        /// <value> The sale details. </value>
        public SortableBindingList<SaleDetail> SaleDetails { get; set; }

        /// <summary> Gets the salesman. </summary>
        /// <value> The salesman. </value>
        public string Salesman { get; set; }

        /// <summary> Gets the client. </summary>
        /// <value> The client. </value>
        public string Client { get; set; }

        /// <summary> Gets the sale date. </summary>
        /// <value> The sale date. </value>
        public DateTimeOffset SaleDate { get; set; }

        /// <summary> Gets the total number of amount. </summary>
        /// <value> The total number of amount. </value>
        public decimal TotalAmount
        {
            get
            {
                Debug.Assert( this.SaleDetails is not null );
                return this.SaleDetails.Sum( a => a.TotalAmount );
            }
        }
    }

    /// <summary> A sale detail. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    private sealed class SaleDetail
    {
        /// <summary> Gets the product. </summary>
        /// <value> The product. </value>
        public string Product { get; set; } = string.Empty;

        /// <summary> Gets the quantity. </summary>
        /// <value> The quantity. </value>
        public int Quantity { get; set; }

        /// <summary> Gets the unit price. </summary>
        /// <value> The unit price. </value>
        public decimal UnitPrice { get; set; }

        /// <summary> Gets the total number of amount. </summary>
        /// <value> The total number of amount. </value>
        public decimal TotalAmount => this.UnitPrice * this.Quantity;
    }

    /// <summary> Sortable list. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <returns> A list of. </returns>
    private static SortableBindingList<Sale> SortableList()
    {
        Sale[] sales = [
            new Sale()
            {
                Client = "Jane Doe",
                SaleDate = new DateTime(2008, 1, 10),
                Salesman = "John",
                SaleDetails = [new() { Product = "Sportsman", Quantity = 1, UnitPrice = 80m }, new() { Product = "Tusker Malt", Quantity = 2, UnitPrice = 100m }, new() { Product = "Alvaro", Quantity = 1, UnitPrice = 50m }]
            },
            new Sale()
            {
                Client = "Ben Jones",
                SaleDate = new DateTime(2008, 1, 11),
                Salesman = "Danny",
                SaleDetails = [new() { Product = "Embassy Kings", Quantity = 1, UnitPrice = 80m }, new() { Product = "Tusker", Quantity = 5, UnitPrice = 100m }, new() { Product = "Movie", Quantity = 3, UnitPrice = 50m }]
            },
            new Sale()
            {
                Client = "Tim Kim",
                SaleDate = new DateTime(2008, 1, 12),
                Salesman = "Kaplan",
                SaleDetails = [new() { Product = "Citizen Special", Quantity = 10, UnitPrice = 30m }, new() { Product = "Burn", Quantity = 2, UnitPrice = 100m }]
            }
        ];
        return new SortableBindingList<Sale>( [.. sales] );
    }

    /// <summary> (Unit Test Method) tests sortable binding list. </summary>
    /// <remarks> Tested 2019-05-14. </remarks>
    [TestMethod()]
    public void SortableBindingListTest()
    {
        SortableBindingList<Sale> sales = SortableList();
        List<Sale> l = [.. sales];
        string firstValue = l[0].Client;
        string secondValue = l[1].Client;
        Assert.IsTrue( 0 < string.Compare( firstValue, secondValue, StringComparison.Ordinal ), $"Before sorting {firstValue} must come before {secondValue}" );
        l = [.. sales.OrderBy( x => x.Client )];
        firstValue = l[0].Client;
        secondValue = l[1].Client;
        Assert.IsTrue( 0 > string.Compare( firstValue, secondValue, StringComparison.Ordinal ), $"After sorting ascending {firstValue} must come before {secondValue}" );
        l = [.. sales.OrderByDescending( x => x.Client )];
        firstValue = l[0].Client;
        secondValue = l[1].Client;
        Assert.IsTrue( 0 < string.Compare( firstValue, secondValue, StringComparison.Ordinal ), $"After sorting descending {firstValue} must come before {secondValue}" );

        // adds a last value, the list will remain ordered even without change.
        string expectedLastClient = "Yoda";
        sales.AllowNew = true;
        sales.AllowEdit = true;
        sales.AllowRemove = true;
        Sale item = new()
        {
            Client = expectedLastClient,
            SaleDate = new DateTime( 2009, 1, 12 ),
            Salesman = "Kaplan",
            SaleDetails = [new() { Product = "Cola", Quantity = 4, UnitPrice = 3.1m }, new() { Product = "Avian", Quantity = 12, UnitPrice = 1.5m }]
        };
        sales.Insert( 0, item );
        // sales.Add(item)
        l = [.. sales];
        firstValue = l[0].Client;
        secondValue = l[1].Client;
        Assert.IsTrue( 0 < string.Compare( expectedLastClient, secondValue, StringComparison.Ordinal ), $"After adding {expectedLastClient}={firstValue} must come before {secondValue}" );
        l = [.. sales.OrderBy( x => x.Client )];
        firstValue = l[0].Client;
        secondValue = l[1].Client;
        Assert.IsTrue( 0 > string.Compare( firstValue, secondValue, StringComparison.Ordinal ), $"After sorting ascending {firstValue} must come before {secondValue}" );

        // adds a first value, the list will remain ordered even without change.
        string expectedFirstClient = "Abe";
        sales.Add( new Sale()
        {
            Client = expectedFirstClient,
            SaleDate = new DateTime( 2009, 1, 12 ),
            Salesman = "Kaplan",
            SaleDetails = [new() { Product = "Pepsi", Quantity = 4, UnitPrice = 2.8m }, new() { Product = "Perrier", Quantity = 12, UnitPrice = 1.5m }]
        } );
        l = [.. sales];
        firstValue = l[0].Client;
        secondValue = l[1].Client;
        Assert.IsTrue( 0 > string.Compare( expectedFirstClient, secondValue, StringComparison.Ordinal ), $"After adding {expectedFirstClient}={firstValue} must come before {secondValue}" );
    }

    #endregion
}
