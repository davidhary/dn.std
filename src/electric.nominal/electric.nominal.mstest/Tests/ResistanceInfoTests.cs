namespace cc.isr.Std.ElectricNominal.Tests;
/// <summary>
/// This is a test class for ResistanceInfoTest and is intended to contain all ResistanceInfoTest
/// Unit Tests.
/// </summary>
/// <remarks> David, 2020-09-23. </remarks>
[TestClass]
public class ResistanceInfoTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    /// <summary> A test for TryParse. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="resistanceCode">     The resistance code. </param>
    /// <param name="resistanceExpected"> The resistance expected. </param>
    /// <param name="expected">           True if expected. </param>
    public static void TryParse( string resistanceCode, double resistanceExpected, bool expected )
    {
        double resistance = 0.0d;
        string details = string.Empty;
        bool actual;
        actual = ResistanceInfo.TryParse( resistanceCode, ref resistance, ref details );
        Assert.AreEqual( resistanceExpected, resistance, details );
        Assert.AreEqual( expected, actual, details );
    }

    /// <summary> A test for TryParse. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestMethod()]
    public void TryParseTest()
    {
        TryParse( "121", 120d, true );
        TryParse( "230", 23d, true );
        TryParse( "1000", 100d, true );
        TryParse( "2R67", 2.67d, true );
        TryParse( "2.67", 2.67d, true );
        TryParse( "12K6675", 12667.5d, true );
        TryParse( "100R5", 100.5d, true );
        TryParse( "R0015", 0.0015d, true );
        TryParse( "100R", 100d, true );
    }
}
