namespace cc.isr.Std.ElectricNominal;

/// <summary>   Information about the resistance. </summary>
/// <remarks>
/// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-05-19 </para>
/// </remarks>
public class ResistanceInfo : NominalInfo
{
    #region " construction "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    public ResistanceInfo() : base()
    { }

    /// <summary> Constructor. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    /// <param name="resistanceCode"> The resistance code. </param>
    public ResistanceInfo( string resistanceCode ) : base( resistanceCode )
    { }

    #endregion

    #region " values "

    /// <summary> Gets or sets the nominal resistance. </summary>
    /// <value> The nominal resistance. </value>
    public double NominalResistance
    {
        get => this.NominalValue;
        set => this.NominalValue = value;
    }

    /// <summary> Gets or sets the resistance code. </summary>
    /// <value> The resistance code. </value>
    public string ResistanceCode
    {
        get => this.ValueCode;
        set => this.ValueCode = value;
    }

    /// <summary> Attempts to parse. </summary>
    /// <remarks> David, 2020-04-17. </remarks>
    /// <param name="valueCode"> The value code. </param>
    /// <param name="value">     [in,out] The value. </param>
    /// <param name="details">   [in,out] The details. </param>
    /// <returns> True if it succeeds; otherwise, false. </returns>
    public static bool TryParse( string valueCode, ref double value, ref string details )
    {
        (bool success, double value1, string details1) = TryParse( valueCode );
        value = value1;
        details = details1;
        return success;
    }

    #endregion
}
