# About

cc.isr.Std.ElectricNominal is a .Net library supporting Electric Nominal value operations.

# How to Use

This example is implemented in the [Ohmni Core Repository] MS Test project 
cc.isr.Ohmni.Core.MSTest:

```
        /// <summary>
        /// (Unit Test Method) tests parsing PFC-D1206LF-02-1001-3301-FB divider part specification.
        /// </summary>
        /// <remarks> David, 2020-04-20. </remarks>
        [TestMethod()]
        public void ParseD1206lfPartSpecificationTest()
        {
            string partNumber = "PFC-D1206LF-02-1001-3301-FB";
            var parser = new PartSpecificationParser();
            parser.Parse( partNumber );
            string propertyName = nameof( PartSpecificationParser.Parsed );
            propertyName = nameof( PartSpecificationParser.Parsed );
            Assert.IsTrue( parser.Parsed, $"{propertyName}" );
            propertyName = nameof( PartSpecificationParser.ParseFailed );
            Assert.IsFalse( parser.ParseFailed, $"{propertyName}" );
            propertyName = nameof( PartSpecificationTrait.PartNumber );
            Assert.AreEqual( partNumber, parser.Traits[PartSpecificationTrait.PartNumber], $"{propertyName} should match" );
            propertyName = nameof( PartSpecificationTrait.FamilyCode );
            Assert.AreEqual( "PFC", parser.Traits[PartSpecificationTrait.FamilyCode], $"{propertyName} should match" );
            propertyName = nameof( PartSpecificationTrait.ProductNumber );
            Assert.AreEqual( "D1206LF", parser.Traits[PartSpecificationTrait.ProductNumber], $"{propertyName} should match" );
            propertyName = nameof( PartSpecificationTrait.ModelNumber );
            Assert.AreEqual( "D1206", parser.Traits[PartSpecificationTrait.ModelNumber], $"{propertyName} should match" );
            propertyName = nameof( PartSpecificationTrait.ProductNumberTermination );
            Assert.AreEqual( "LF", parser.Traits[PartSpecificationTrait.ProductNumberTermination], $"{propertyName} should match" );
            propertyName = nameof( PartSpecificationTrait.ProductCaseCode );
            Assert.AreEqual( "1206", parser.Traits[PartSpecificationTrait.ProductCaseCode], $"{propertyName} should match" );
            propertyName = nameof( PartModelType );
            cc.isr.Ohmni.Core.PartModelType model = ModelTypeCollection.Get().FindSupportedModelType( parser.Traits[PartSpecificationTrait.ModelNumber] );
            Assert.AreEqual<PartModelType>( PartModelType.Divider, model, $"{propertyName} should match" );
            propertyName = nameof( PartSpecificationTrait.TcrCode );
            Assert.AreEqual( "02", parser.Traits[PartSpecificationTrait.TcrCode], $"{propertyName} should match" );
            propertyName = nameof( PartSpecificationTrait.ElementValue1 );
            Assert.AreEqual( "1001", parser.Traits[PartSpecificationTrait.ElementValue1], $"{propertyName} should match" );
            propertyName = nameof( PartSpecificationTrait.ElementValue2 );
            Assert.AreEqual( "3301", parser.Traits[PartSpecificationTrait.ElementValue2], $"{propertyName} should match" );
            propertyName = nameof( PartSpecificationTrait.ToleranceCodes );
            Assert.AreEqual( "FB", parser.Traits[PartSpecificationTrait.ToleranceCodes], $"{propertyName} should match" );
            propertyName = nameof( PartSpecificationTrait.ToleranceCode );
            Assert.AreEqual( "F", parser.Traits[PartSpecificationTrait.ToleranceCode], $"{propertyName} should match" );
            propertyName = nameof( PartSpecificationTrait.TrackingCode );
            Assert.AreEqual( "B", parser.Traits[PartSpecificationTrait.TrackingCode], $"{propertyName} should match" );
            propertyName = nameof( PartSpecificationTrait.SpecialProcessSpecification );
            Assert.IsFalse( parser.Traits.ContainsKey( PartSpecificationTrait.SpecialProcessSpecification ), $"{propertyName} should not exist" );
            propertyName = nameof( PartSpecificationTrait.UnknownSuffix );
            Assert.IsFalse( parser.Traits.ContainsKey( PartSpecificationTrait.UnknownSuffix ), $"{propertyName}  should not exist" );
            var nominalValues = new double[] { 1000d, 3300d, 4300d, 1d / 3.3d };
            var nominalEpsilons = new double[] { 0.00001d, 0.00001d, 0.00001d, 0.00001d };
            var tcrPpmValues = new double[] { 50d, 50d, 50d, 50d };
            var toleranceValues = new double[] { 0.01d, 0.01d, 0.02d, 0.001d };
            cc.isr.Ohmni.Core.PartSpecificationElement element = parser.Elements[0];
            string equation = "|R1|R2|/|==|0.30303|0.001|";
            for ( int i = 0, loopTo = parser.Elements.Count - 1; i <= loopTo; i++ )
            {
                element = parser.Elements[i];
                propertyName = $"{element.ElementId} {nameof( cc.isr.Std.ElectricNominal.NominalInfo.NominalValue )}";
                double resistanceValue = nominalValues[i];
                Assert.AreEqual( resistanceValue, element.NominalInfo.NominalValue, nominalEpsilons[i], $"{propertyName} should match" );
                propertyName = $"{element.ElementId} TCR {nameof( cc.isr.Std.ReliabilityIntervals.ReliabilityInterval.HighEndPoint )} PPM";
                double tcrPpmValue = tcrPpmValues[i];
                Assert.AreEqual( tcrPpmValue, 1000000.0d * element.Tcr.RelativeInterval.HighEndPoint, $"{propertyName} should match" );
                propertyName = $"{element.ElementId} TCR {nameof( cc.isr.Std.ReliabilityIntervals.ReliabilityInterval.LowEndPoint )} PPM";
                Assert.AreEqual( -tcrPpmValue, 1000000.0d * element.Tcr.RelativeInterval.LowEndPoint, $"{propertyName} should match" );
                propertyName = $"{element.ElementId} Tolerance {nameof( cc.isr.Std.ReliabilityIntervals.ReliabilityInterval.HighEndPoint )}";
                double toleranceValue = toleranceValues[i];
                Assert.AreEqual( toleranceValue, element.Tolerance.RelativeInterval.HighEndPoint, $"{propertyName} should match" );
                propertyName = $"{element.ElementId} Tolerance {nameof( cc.isr.Std.ReliabilityIntervals.ReliabilityInterval.LowEndPoint )}";
                Assert.AreEqual( -toleranceValue, element.Tolerance.RelativeInterval.LowEndPoint, $"{propertyName} should match" );
                if ( i == 3 )
                {
                    propertyName = $"{element.ElementId} {nameof( PartSpecificationElement.Equation )}";
                    Assert.AreEqual( equation, element.Equation, $"{propertyName} should match" );
                }
            }
        }

```

# Key Features

* Parses nominal information from the value code of a standard component.

# Main Types

The main types provided by this library are:

* _NominalInfo_ A parser for electrical component Value Code.
* _ResistanceInfo_ A _NominalInfo_ for a resistor.

# Feedback

cc.isr.Std.ElectricNominal is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Standard Framework Repository].

[Standard Framework Repository]: https://bitbucket.org/davidhary/dn.std
[Ohmni Core Repository]: https://bitbucket.org/davidhary/dn.Ohmni.core


