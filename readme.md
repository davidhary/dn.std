# .NET Standard Libraries

Support libraries extending the Visual Studio framework for engineering and measurements.
This is a fork of the [dn.core] repository.

* Project README files:
  * [cc.isr.Std.Async](src/libs/async/readme.md) 
  * [cc.isr.Std.BindingLists](src/libs/binding.lists/readme.md) 
  * [cc.isr.Std.Cartesian](src/libs/cartesian/readme.md) 
  * [cc.isr.Std.CircularLists](src/libs/circular.lists/readme.md) 
  * [cc.isr.Std.Concurrent](src/libs/concurrent/readme.md) 
  * [cc.isr.Std.Counters](src/libs/counters/readme.md) 
  * [cc.isr.Std.Dictionaries](src/libs/dictionaries/readme.md) 
  * [cc.isr.Std.Electric](src/libs/electric/readme.md) 
  * [cc.isr.Std.ElectricNominal](src/libs/electric.nominal/readme.md) 
  * [cc.isr.Std.EventContext](src/libs/event.context/readme.md) 
  * [cc.isr.Std.Framework](src/libs/framework/readme.md) 
  * [cc.isr.Std.Lizzie)](src/libs/lizzie/readme.md) 
  * [cc.isr.Std.MovingFilters](src/libs/moving.filters/readme.md) 
  * [cc.isr.Std.Notifiers](src/libs/notifiers/readme.md) 
  * [cc.isr.Std.ObservableLists](src/libs/observable.lists/readme.md) 
  * [cc.isr.Std.Primitives](src/libs/primitives/readme.md) 
  * [cc.isr.Std.RedundancyCheck](src/libs/redundancy.check/readme.md) 
  * [cc.isr.Std.ReliabilityIntervals](src/libs/reliability.intervals/readme.md) 
  * [cc.isr.Std.Statistics](src/libs/statistics/readme.md) 
  * [cc.isr.Std.SyncContext](src/libs/sync.context/readme.md) 
  * [cc.isr.Std.Timers](src/libs/timers/readme.md) 
  * [cc.isr.Std.YieldCounters](src/libs/yield.counters/readme.md) 
* [Attributions](Attributions.md)
* [Change Log](./CHANGELOG.md)
* [Cloning](Cloning.md)
* [Code License](LICENSE-CODE)
* [Code of Conduct](code_of_conduct.md)
* [Contributing](contributing.md)
* [Legal Notices](#legal-notices)
* [License](LICENSE)
* [Open Source](Open-Source.md)
* [Repository Owner](#Repository-Owner)
* [Security](security.md)

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [ATE Coder]  

<a name="legal-notices"></a>
## Legal Notices

Integrated Scientific Resources, Inc., and any contributors grant you a license to the documentation and other content in this repository under the [Creative Commons Attribution 4.0 International Public License] and grant you a license to any code in the repository under the [MIT License].

Integrated Scientific Resources, Inc., and/or other Integrated Scientific Resources, Inc., products and services referenced in the documentation may be either trademarks or registered trademarks of Integrated Scientific Resources, Inc., in the United States and/or other countries. The licenses for this project do not grant you rights to use any Integrated Scientific Resources, Inc., names, logos, or trademarks.

Integrated Scientific Resources, Inc., and any contributors reserve all other rights, whether under their respective copyrights, patents, or trademarks, whether by implication, estoppel or otherwise.

[Creative Commons Attribution 4.0 International Public License]: https://github.com/ATECoder/dn.vi.ivi/blob/main/license
[MIT License]: https://github.com/ATECoder/dn.vi.ivi/blob/main/license-code
 
[ATE Coder]: https://www.IntegratedScientificResources.com
[dn.core]: https://www.bitbucket.org/davidhary/dn.core

