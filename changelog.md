# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [1.1.9154] - 2025-01-23
* Add string extension methods: Try Parse Double and Extract Number.
* Rename core lib and standard folders to lower case indicating departure from namespace synchronization.

## [1.1.9082] - 2024-11-12 Preview 202304
* Update packages.
* Match test namespaces to folders.
* Fix sort tests.
* Fix file info tests.
* Update versions to 9083.
* Upgrade Forms libraries and Test and Demo apps to .Net 9.0.

## [1.1.8955] - 2024-07-08 Preview 202304
* Add MS Test and NET 21 projects.
* Set projects to target .NET Standard 2.0 alone.
* Update .NET standard 2.0 enhanced methods and attributes.

## [1.1.8949] - 2024-07-02 Preview 202304
* Apply code analysis rules.
* Update libraries versions.
* Apply code analysis rules.
* Use ordinal instead of ordinal ignore case string comparisons.
* Apply constants style.
* Condition auto generating binding redirects on .NET frameworks 472 and 480.
* Apply code analysis rules.
* Generate assembly attributes.
* Implement nullable in stopwatch and time span extensions.

## [1.1.8935] - 2024-06-17 Preview 202304
* Update to .Net 8.
* Implement MS Test SDK project format.

## [1.1.8537] - 2023-05-17 Preview 202304
* Set test framework to .NET 7.
* Use Observable object form the community toolkit.
* Implement nullable.
* Rearrange folders.
* Set MSTest namespace to cc.isr.
* Fix orphan links in MD files. 
* update project references due to restructuring folders or referenced libraries.
* Update packages. 

## [1.1.8535] - 2023-05-15 Preview 202304
* Use cc.isr.Json.AppSettings.ViewModels project for settings I/O.

## [1.1.8518] - 2023-04-28 Preview 202304
* Split README.MD to attribution, cloning, open-source and read me files.
* Add code of conduct, contribution and security documents.
* Increment version.

## [1.0.8189] - 2022-06-03
* Use Tuples to implement GetHashCode().

## [1.0.8125] - 2022-03-31
* Pass tests in project reference mode. 

## [1.0.8119] - 2022-03-25
* Use the new Json application settings base class.

## [1.0.8110] - 2022-03-16
* Use the ?. operator, without making a copy of the delegate, 
to check if a delegate is non-null and invoke it in a thread-safe way.

## [1.0.8103] - 2022-03-09
* Forked from [dn.core].

&copy; 2012 Integrated Scientific Resources, Inc. All rights reserved.

[dn.core]: https://www.bitbucket.org/davidhary/dn.core
[1.1.9083]: https://bitbucket.org/davidhary/dn.std/src/main/
